<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_m0
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

//templates\sisite
$this->template = "sisite";

?>

<!-- chart libraries start 
    <script src="<?php echo 'templates/'.$this->template; ?>/bower_components/jquery/jquery.min.js"></script>

<h3>Indicador</h3>
<p><?php //print_r($this->item);?></p>
<h3>Name:<?php echo $this->item->name;?></h3>
<h3>Action:<?php echo $this->item->axis;?></h3>
<h3>Dimension:<?php echo $this->item->dimension;?></h3>
<h3>Source:<?php echo $this->item->source;?></h3>
<p>Definition:<?php echo $this->item->definition;?></p>
<p>Created:<?php echo $this->item->sys_date;?></p>
-->
<div _class="page-header">
	<h3 itemprop="name">Ver Indicador: <?php echo $this->item->name;?></h3>
</div>
<h4><?php //echo $this->item->name;?></h4>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">

<div class="box-content">
Caracterização do indicador:
                    <table class="table table-bordered table-striped table-condensed">
					<!--
                        <thead>
                        <tr>
                            <th width="25%">Username</th>
                            <th>Date registered</th>
                        </tr>
                        </thead>
						-->
                        <tbody>

                        <tr><td><strong>Eixo:</strong></td>
                            <td class="center"><?php echo $this->item->axis;?></td>
							</tr>
                        <tr><td><strong>Dimensão:</strong></td>
                            <td class="center"><?php echo $this->item->dimension;?></td>
							</tr>							
                        <tr><td><strong>Indicador:</strong></td>
                            <td class="center"><strong><?php echo $this->item->name;?></strong></td>
							</tr>
						<tr><td><strong>Definição:</strong></td>
                            <td class="center"><?php echo $this->item->definition;?></td>
							</tr>
                        <tr><td><strong>Fonte:</strong></td>
                            <td class="center"><?php
								echo $this->item->source;		
								if($this->item->sourceURL!="")
									echo ' <a target="_new" href="'.$this->item->sourceURL.'"><i class="glyphicon glyphicon-home"></i> Go...</a>';				
								?></td>
							</tr>
                        <tr><td><strong>Periodicidade:</strong></td>
                            <td class="center"><?php echo $this->item->periodicity;?></td>
							</tr>
                        <tr><td><strong>Notas:</strong></td>
                            <td class="center"><?php echo $this->item->obs;?></td>
							</tr>
						<tr><td><strong>Publicação:</strong></td>
                            <td class="center"><?php echo $this->item->sys_date;?></td>
							</tr>
                        <tr><td><strong>Estado:</strong></td>
                            <td class="center"><span class="label-success label label-default">Active</span></td>
							</tr>
                        </tbody>
                    </table>
                </div>

		</div>
	</div>
</div>
<!--
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
<div class="box-content">
Caracterização do indicador:
	<dl>
		<dt>Definição:</dt>
		<dd><?php echo $this->item->definition;?></dd>
		<dd> </dd>
		<dt>Eixo:</dt>
		<dd><?php echo $this->item->axis;?></dd>
		<dt>Dimensão:</dt>
		<dd><?php echo $this->item->dimension;?></dd>
		<dt>Fonte:</dt>
		<dd><?php
			echo $this->item->source;		
			if($this->item->sourceURL!="")
				echo ' <a target="_new" href="'.$this->item->sourceURL.'"><i class="glyphicon glyphicon-home"></i> Go...</a>';
			
				
				?></dd>
		<dt>Periodicidade:</dt>
		<dd><?php echo $this->item->periodicity;?></dd>
		<dt>Notas:</dt>
		<dd><?php echo $this->item->obs;?></dd>
		<dt>Disponibilidade de dados:</dt>
		<dd><?php echo $this->item->sys_date;?></dd>
	</dl>
</div>
</div>
</div>
</div>
-->

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
<div class="box-content">
Tabela de valores:
<table width="96%" border="1" class="table table-bordered table-striped table-condensed">	
<?php 
$model  = $this->getModel();
$regions= $model->getRegions($this->item->id_indicator);
$i=0;
foreach($regions as $region){
	//$this->drawRegiao($region->id_region, $region->region);
	//echo ("--------".$region->id_region);
	$data 	= $model->getRegionValues($this->item->id_indicator, $region->id_region);
	//print_r ($data);die();
	$i++;
	if($i==1){		
		?>
		<tr>
		<td> </td>
		<?php
		foreach($data as $key => $val){
				?>
				<th align="center"><?php echo $val->year;?></th>
				<?php
		}
		?>
		</tr>
		<?php	
	}
	?>
	<tr >
	<th><?php echo ($region->region) ?></th>
	<?php
	foreach($data as $key => $val){
		$nm1 = 'y'.$val->id_value;
		$nm2 = 'r'.$val->id_value;
		$nm3 = 'e'.$val->id_value;	
		//echo "\n	vals".$region->id_region.".push([".$val->year.",".($val->real_value)."]);";
		
			?>
			<td align="center"><?php 
				//echo $region->id_region;
				echo $val->real_value;
				?></td>
			<?php
	}
	
	?>
	</tr>
	<?php	
}
?>
</table>
</div>
</div>
</div>

</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
 			<!--			<!--       
            <div class="box-header well">

		        <h2><i class="glyphicon glyphicon-list-alt"></i> Chart with points</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>

		     </div>
			-->
        	 <div class="box-content">
Gráfico:
                <div id="sincos" class="center" style="height:300px"></div>
                <p id="hoverdata">Mouse position at (<span id="x">0</span>, <span id="y">0</span>). <span
                        id="clickdata"></span></p>
            </div>

        </div>
    </div>
	
<!--
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-list-alt"></i> Flot</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div id="flotchart" class="center" style="height:300px"></div>
            </div>
        </div>
    </div>

    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-list-alt"></i> Stack Example</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div id="stackchart" class="center" style="height:300px;"></div>

                <p class="stackControls center">
                    <input class="btn btn-default" type="button" value="With stacking">
                    <input class="btn btn-default" type="button" value="Without stacking">
                </p>

                <p class="graphControls center">
                    <input class="btn btn-primary" type="button" value="Bars">
                    <input class="btn btn-primary" type="button" value="Lines">
                    <input class="btn btn-primary" type="button" value="Lines with steps">
                </p>
            </div>
        </div>
    </div>
-->
</div><!--/row-->


<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/flot/excanvas.min.js"></script>
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/flot/jquery.flot.js"></script>
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/flot/jquery.flot.pie.js"></script>
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/flot/jquery.flot.stack.js"></script>
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/flot/jquery.flot.resize.js"></script>
<!-- chart libraries end -->
<script _src="<?php echo 'administrator/components/com_sim0/views/ind/tmpl/init-chart.js'; ?>" ></script>


<script>
//chart with points
if ($("#sincos").length) {
    var sin = [], cos = [];

	
<?php 
///$model  = $this->getModel();
///$regions= $model->getRegions($this->item->id_indicator);
//print_r($regions);
$i=0;
foreach($regions as $region){
	//$this->drawRegiao($region->id_region, $region->region);
	//echo ("--------".$region->id_region);
	$data 	= $model->getRegionValues($this->item->id_indicator, $region->id_region);
	//print_r ($data);die();
	$i++;
	
	?>

	var vals<?php echo $region->id_region;?> = [];
	
	<?php
	foreach($data as $key => $val){
		$nm1 = 'y'.$val->id_value;
		$nm2 = 'r'.$val->id_value;
		$nm3 = 'e'.$val->id_value;	
		//if($i==1) echo "\n	sin.push([".$val->year.",".($val->real_value)."]);";
		//if($i==2) echo "\n	cos.push([".$val->year.",".($val->real_value)."]);";
					echo "\n	vals".$region->id_region.".push([".$val->year.",".($val->real_value)."]);";
	}
}
?>	
	//DO NOT RUN
    for (var i = 110; i < 14; i += 0.5) {
        sin.push([i, Math.sin(i) / i]);
        cos.push([i, Math.cos(i)]);
    }

    var plot = $.plot($("#sincos"),
        [
            //{ data: sin, label: "sin(x)"},
            //{ data: cos, label: "cos(x)" }		
<?php	foreach($regions as $region){	?>
		{ data: vals<?php echo $region->id_region; ?>, label: "<?php echo $region->region; ?>"},
<?php	}	?>

        ], {
            series: {
                lines: { show: true  },
                points: { show: true }
            },
            grid: { hoverable: true, clickable: true, backgroundColor: { colors: ["#fff", "#eee"] } },
            //yaxis: { min: -1.2, max: 1.2 },
            colors: ["#39F2EA", "#3C67A5", "#A3C67A", "#AA3C67"]
        });

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#dfeffc',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#sincos").bind("plothover", function (event, pos, item) {
        //$("#x").text(pos.x.toFixed(2));
        //$("#y").text(pos.y.toFixed(2));
        $("#x").text(pos.x.toFixed(0));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(2);

                showTooltip(item.pageX, item.pageY,
                    item.series.label + " of " + x + " = " + y);
            }
        }
        else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });


    $("#sincos").bind("plotclick", function (event, pos, item) {
        if (item) {
            $("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
            plot.highlight(item.series, item.datapoint);
        }
    });
}
</script>