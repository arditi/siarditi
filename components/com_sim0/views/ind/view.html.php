<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the M0 Component
 *
 * @since  0.0.1
 */
class SIM0ViewInd extends JViewLegacy
{
	/**
	 * Display the Log view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Assign data to the view
		$this->item = $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JLog::add(implode('<br />', $errors), JLog::WARNING, 'jerror');
			
			return false;
		}

		// Display the view
		parent::display($tpl);
	}
	//
	//
	protected function drawRegiao($id_region, $region=""){
		$id 	= $this->item->id_indicator;
		$model 	= $this->getModel('Ind');
		$regions= $model->getRegions($id);
		$data 	= $model->getRegionValues($id, $id_region);
		//print_r($data);
		?>
		<div class="strong"><b><?php echo "Region ".$region; ?></b></div>
		<div class="control-group">
			<div class="control-label">
			<table cellspacing="0" cellpadding="0"><tr>
				<td>Year:
					<br>Real:
					<br>Estimated:<br>
					</td>			
			<?php
				foreach($data as $key => $val){
					$nm1 = 'y'.$val->id_value;
					$nm2 = 'r'.$val->id_value;
					$nm3 = 'e'.$val->id_value;
				?>
				<td><?php //echo $val->year.'<br>';?>
						<input placeholder="year" style="width:30px" size="2" type="text" 
							name="jform[<?php echo $nm1;?>]" id="jform_<?php echo $nm1;?>" value="<?php echo $val->year;?>">
					<br><input placeholder="real" style="width:30px" size="2" type="text" 
							name="jform[<?php echo $nm2;?>]" id="jform_<?php echo $nm2;?>" value="<?php echo $val->real_value;?>">
					<br><input placeholder="estimated" style="width:30px" size="2" type="text" 
							name="jform[<?php echo $nm3;?>]" id="jform_<?php echo $nm3;?>" value="<?php echo $val->estimated_value;?>">
					</td>
				<?php 
				}
			?>
				<td>				
					<button onclick="Joomla.submitbuttonRegion('<?php echo $id_region ?>', 'ind.addyear')" 
						class="btn btn-small btn-success"><span class="icon-new icon-white"></span>add year</button>
					<br />
					<br />
					<button onclick="Joomla.submitbuttonRegion('<?php echo $id_region ?>', 'ind.delyear')" 
						class="btn btn-small btn-error"><span class="icon-minus icon-red"></span>del year</button>
					
					</td>
					</tr></table>
			
		
			</div>
		</div>				
		<?php
			
	}	
}
