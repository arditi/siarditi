<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the M0 Component
 *
 * @since  0.0.1
 */
class SIM0ViewLogActions extends JViewLegacy
{
	/**
	 * Display the Log view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Assign data to the view
//		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		//$this->filterForm    = $this->get('FilterForm');
//		$this->activeFilters = $this->get('ActiveFilters');
		$this->items         = $this->get('Items');
		$this->canDo		 = JHelperContent::getActions('com_sim0', 'logactions', $this->state->get('filter.id_log'));

		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JLog::add(implode('<br />', $errors), JLog::WARNING, 'jerror');

			return false;
		}

		// Display the view
		parent::display($tpl);
	}
}
