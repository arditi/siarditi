<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
?>

<script type="text/javascript">
//
// Scrooll pagination
// See http://www.sitepoint.com/jquery-infinite-scrolling-demos/
//
var pagesRequest = 0;
var pagesCurrent = 1;
var pagesStop 	 = <?php echo $this->pagination->pagesStop?>;

$(document).ready(function(){
	////
	// lastAddedLiveFunc
    function lastAddedLiveFunc()
    {
		if(pagesCurrent<=pagesStop & pagesRequest == 0 ){
			pagesRequest = 1;
			
			// Show loader
			$('div#lastPostsLoader').html('<img src="templates/sisite/img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-8.gif"/>');
			// Request page
			$.ajax({
				url: 'index.php',
				type: "get", //send it through get method
				data:{option:'com_sim0', view:'inds', filename:'range.json', format:'json', pCurrent: pagesCurrent},
				context: document.body,
				//success: display_counts
				success: function(data) {
					//Do Something
					if (data != "") {
						// Se vieram dados...
						if(pagesCurrent<=pagesStop) pagesCurrent++;
						//console.log('Add data in ...'+'index.php'+'pagesCurrent'+pagesCurrent+' pagesStop'+pagesStop);
						$(".items").append(data);
					}
					else{
						$(".items").append("No more data...");
					}
					//$('div#lastPostsLoader').empty();
					$('div#lastPostsLoader').html('<div id="getMoreData" class="btn btn-primary noty" href="#" >Get more results</div>');
					$("#getMoreData").click(function() { lastAddedLiveFunc(); });
					pagesRequest = 0;
				},
				error: function(xhr, status) {
					//Do Something to handle error
					console.log('Error:'+xhr.statusText+' -> '+status);
					pagesRequest = 0;
				}
			});
		}
		else{
			// No more data!
			$('div#lastPostsLoader').empty();
		}
    };
	//
    $(window).scroll(function(){
        var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
        var  scrolltrigger = 0.99;
        if  ((wintop/(docheight-winheight)) > scrolltrigger) {
			//console.log('scroll bottom');
			lastAddedLiveFunc();
        }
    });
	//
    lastAddedLiveFunc();	
});
</script>

<?php 

// Vamos buscar por novo request ajax
$this->items=array();
	
if(1==1){
	?>
<ul class="items">
	<?php 
	foreach ($this->items as $i => $item) : ?>
	
		<li xmlns:dc="http://purl.org/dc/elements/1.1/" class="regularitem">
		<h4 class="itemtitle"><a href="<?php echo JRoute::_($item->url); ?>"><?php echo $this->escape($item->name); ?></a></h4>
		<h5 class="itemposttime"><span>Posted: </span><?php echo $item->sys_date; ?></h5>
		<div class="itemcontent row<?php echo $i % 2; ?>" name="decodeable"><?php echo $item->definition; ?></div>
		</li>
	<?php endforeach; ?>
	</ul>
<div id="lastPostsLoader"><a class="btn" href="javascript:lastAddedLiveFunc();void(0);" >Get more results</a></div>


<?php 
}else{
?>
<div id="archive-items">

	<?php foreach ($this->items as $i => $item) : ?>
		<div class="row<?php echo $i % 2; ?>" >
			<dl>
			<dt>			
				<div class="page-header">
					<h4 style="margin:0px; line-height: 0;">
					<a href="<?php echo JRoute::_($item->url); ?>"><?php echo $this->escape($item->name); ?></a>
					</h4>
				</div>
				</dt>
			<dd>
				<div class="intro"> <?php echo $item->definition; ?> </div>
				</dd>
			<!--
			<dd>
				<div class="published">
					<i class="glyphicon glyphicon-calendar"></i>
					<?php echo JHtml::_('date', $item->sys_date, JText::_('DATE_FORMAT_LC3')); ?>
					
				</div>
				</dd>
				-->
			</dl>
		</div>
	<?php endforeach; ?>
</div>
<div class="pagination">
	<p class="counter"> <?php echo $this->pagination->getPagesCounter(); ?> </p>
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>

<?php
}

/*
$.ajax({
    url: "ajax.aspx?ajaxid=4",
    data: { 
        "VarA": VarA, 
        "VarB": VarB, 
        "VarC": VarC
    },
    cache: false,
    type: "POST",
    success: function(response) {

    },
    error: function(xhr) {

    }
});

$.ajax({
  url: "ajax.aspx",
  type: "get", //send it through get method
  data:{ajaxid:4,UserID: UserID , EmailAddress:encodeURIComponent(EmailAddress)},
  success: function(response) {
    //Do Something
  },
  error: function(xhr) {
    //Do Something to handle error
  },
	error3: function(xhr, error){
		//Do Something to handle error
		console.debug(xhr); 
		console.debug(error);
	},
	error2: function(xhr, status) {
		//Do Something to handle error
		console.log('Error:'+xhr.statusText);
	},
	error1: function (xhr, status) {
		//Do Something to handle error
		  switch (status) {
			 case 404: alert('File not found');	 break;
			 case 500: alert('Server error');	 break;
			 case 0:   alert('Request aborted'); break;
			 default:  alert('Unknown error ' + status);
		 } 
	 }
});

$.ajax({
 type: "GET",
 url: base_url+'/ajax/fetch/counts/',
 dataType: 'json',
 data: {},
 error: function(xhr, error){
        console.debug(xhr); console.debug(error);
 },
 success: display_counts
});
*/
?>