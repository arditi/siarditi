<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
//JHtml::_('formbehavior.chosen', 'select');
//JHtml::_('behavior.caption');
$this->filter = $this->getModel()->getstate('filter.search');
?>
 
<div _class="page-header">
	<h3 itemprop="name">Indicadores</h3>
</div>

<form id="adminForm" action="<?php echo JRoute::_('index.php')?>" method="post" class="form-inline">
	<fieldset class="filters">
	<div class="filter-search">
			<div class="box-content">
                <div class="control-group">

                    <div _class="controls">
						<!--
						<label class="control-label" for="selectError">Filter</label>

                        <select id="selectError" data-rel="chosen">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                            <option>Option 4</option>
                            <option>Option 5</option>
                        </select>
						-->						
						<div class="input-group col-md-6">
							<span class="input-group-addon"><i class="glyphicon glyphicon-search ored"></i></span>
							<input type="text"  name="filter_search" id="filter_search"  
								class="form-control" placeholder="Search for..." 
								value="<?php echo $this->filter ?>">
						</div>
						<div class="input-group hhas-success">
							<button type="submit" class="btn btn-primary"><?php echo JText::_('JGLOBAL_FILTER_BUTTON'); ?></button>
						</div>
						
						<br>
						<br>
						
						<div class="input-group">
							<?php echo $this->form->axes; ?>
						</div>
						<div class="input-group">
							<?php echo $this->form->dimensions; ?>
						</div>
						
						<?php //echo $this->form->yearField; ?>
						<?php //echo $this->form->limitField; ?>
<!--
						<button type="submit" class="input-group btn btn-primary btn-xs" style="vertical-align: top;">
						<i class="glyphicon glyphicon-search"></i>
						<?php echo JText::_('JGLOBAL_FILTER_BUTTON'); ?></button>
	-->	
                    </div>
                </div>
			</div>	
		
		<input type="hidden" name="view" value="inds" />
		<input type="hidden" name="option" value="com_sim0" />
		<input type="hidden" name="limitstart" value="0" />
	</div>
	</fieldset>
	
	<?php //echo(" query=".$this->getModel()->qry); ?>
	
	<?php if (count($this->items)==0) : ?>
			<div class="page-header">
				<strong>Atenção!</strong>
				<br />Não existem Indicadores com estes critérios...
				<br /><br /><br /><br />
			</div>
	<?php else: ?>
			<!-- 
			<div class="searchintro"><br />
				<p><strong>Resultados encontrados:</strong> <span class="badge badge-info">
				<?php echo $this->getModel()->getTotal(); ?>
				</span></p>
			</div>
			-->
			
			
			<div class="searchintro">
				<p><strong>Total: <span class="badge badge-info"><?php echo $this->getModel()->getTotal();?></span> resultados encontrados.</strong></p>
			</div>
			<?php echo $this->loadTemplate('items'); ?>
	<?php endif; 	
	//echo $this->pagination->getListFooter();
	//echo $this->pagination->getPagesCounter ();
	?>	
	
</form>
