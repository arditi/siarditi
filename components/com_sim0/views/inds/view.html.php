<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * HTML View class for the Content component
 *
 * @since  1.5
 */
class SIM0ViewInds extends JViewLegacy
{
	protected $state = null;

	protected $item = null;

	protected $items = null;

	protected $pagination = null;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$app 		= JFactory::getApplication('site');
		$user		= JFactory::getUser();

		$state 		= $this->get('State');
		$items 		= $this->get('Items');
		$pagination	= $this->get('Pagination');

		// Get the page/component configuration
		$params = &$state->params;

		$form = new stdClass;

		// Month Field
		$months = array(
			'' => JText::_('COM_CONTENT_MONTH'),
			'01' => JText::_('JANUARY_SHORT'),
			'02' => JText::_('FEBRUARY_SHORT'),
			'03' => JText::_('MARCH_SHORT'),
			'04' => JText::_('APRIL_SHORT'),
			'05' => JText::_('MAY_SHORT'),
			'06' => JText::_('JUNE_SHORT'),
			'07' => JText::_('JULY_SHORT'),
			'08' => JText::_('AUGUST_SHORT'),
			'09' => JText::_('SEPTEMBER_SHORT'),
			'10' => JText::_('OCTOBER_SHORT'),
			'11' => JText::_('NOVEMBER_SHORT'),
			'12' => JText::_('DECEMBER_SHORT')
		);
		$form->monthField = JHtml::_(
			'select.genericlist',
			$months,
			'jform_month',
			array(
				'list.attr' => 'size="1" class="inputbox"',
				'list.select' => $state->get('filter.month'),
				'option.key' => null
			)
		);

		// Year Field
		$years = array();
		$years[] = JHtml::_('select.option', null, JText::_('JYEAR'));

		for ($year = date('Y'), $i = $year - 10; $i <= $year; $i++)
		{
			$years[] = JHtml::_('select.option', $i, $i);
		}

		$form->yearField = JHtml::_(
			'select.genericlist',
			$years,
			'jform[year]',
			array('list.attr' => 'size="1" class="inputbox"', 'list.select' => $state->get('filter.year'))
		);
		$form->limitField = $pagination->getLimitBox('jform[limit]');
		$form->limitField = "";
		
		$axesd  = array();
		$axesd[]= JHtml::_('select.option', "", "- Select Axis -");
		$axes 	= SIM0Helper::getAxes();
		$axes	= array_merge($axesd, $axes	 );
		
		//$form->axes = JHtml::_('select.genericlist', $axes, 'jform[id_axis]',
		$form->axes = JHtml::_('select.genericlist', $axes, 'filter_id_axis',
						array(	'list.attr'   => 'size="1" class="input-group-addon" __class="btn btn-group-sm" _class="inputbox" _style="height:55px"  data-rel="chosen" onchange="this.form.submit()"', 
								'list.select' => $state->get('filter.id_axis'))
		);

		$dimsd  = array();
		$dimsd[]= JHtml::_('select.option', "", "- Select Dimension -");
		$dims 	= SIM0Helper::getDimensions();
		$dims	= array_merge($dimsd, $dims	 );
		$form->dimensions = JHtml::_('select.genericlist', $dims, 'filter_id_dimension',
						array(	'list.attr'   => 'size="1" class="input-group-addon" __class="control-label" _class="inputbox" style="width:60%" data-rel="chosen" onchange="this.form.submit()"', 
								'list.select' => $state->get('filter.id_dimension'))
		);
		
		// Escape strings for HTML output
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

		$this->filter     = $state->get('list.filter');
		$this->form       = &$form;
		$this->items      = &$items;
		$this->params     = &$params;
		$this->user       = &$user;
		$this->pagination = &$pagination;

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return  void.
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('JGLOBAL_ARTICLES'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}
		$this->document->setTitle($title);


		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
