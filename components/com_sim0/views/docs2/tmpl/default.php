<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_tags
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note that there are certain parts of this layout used only when there is exactly one tag.

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
$description = "";//$this->params->get('all_tags_description');
$descriptionImage = "";//$this->params->get('all_tags_description_image');

//
$this->filter = $this->getModel()->getState('filter.search');

?>
<div _class="page-header">
	<h3 itemprop="name">Biblioteca</h3>
</div>
	
<form id="adminForm" action="<?php echo JRoute::_('index.php')?>" method="post" class="form-inline">
	<fieldset class="filters">
	<div class="filter-search">
			<div class="box-content">
                <div class="control-group">

                    <div _class="controls">
					<!--
					<div class="input-group col-md-8">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit"> 
							<i class="glyphicon glyphicon-search"></i> 
							</button></span>
					</div>	
					-->
						<div class="input-group col-md-6">
							<span class="input-group-addon"><i class="glyphicon glyphicon-search ored"></i></span>
							<input type="text"  name="filter_search" id="filter_search"  
								class="form-control" placeholder="Search for..." value="<?php echo $this->filter ?>">
						</div>
						<div class="input-group hhas-success">
							<button type="submit" class="btn btn-primary"><?php echo JText::_('JGLOBAL_FILTER_BUTTON'); ?></button>
						</div>				
																
                    </div>
                </div>
			</div>	
		
		<input type="hidden" name="view" value="docs" />
		<input type="hidden" name="option" value="com_sim0" />
		<input type="hidden" name="limitstart" value="0" />
	</div>
	</fieldset>
	
	<?php 
	
	if (count($this->items)==0) : ?>
		<div class="row0">
			<div class="page-header">
				<strong>Atenção!</strong>
				<br />Não existem Indicadores com estes critérios...
				<br />
				<br />
		</div>
		</div>
	<?php else: ?>
			<div class="searchintro">
				<p><strong>Total: <span class="badge badge-info"><?php echo count($this->items)?></span> resultados encontrados.</strong></p>
			</div>
		<?php echo $this->loadTemplate('items'); ?>
	<?php endif; ?>	
	
</form>
