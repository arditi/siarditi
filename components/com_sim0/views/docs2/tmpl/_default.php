<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');



?>

			<div class="grid-17">
				

				
	
			<div class="widget widget-table">
					
					<div class="widget-header">
						<span class="icon-list"></span>
						<h3 class="icon aperture">Large Table</h3>
					</div> <!-- .widget-header -->
					
					<div class="widget-content">
						<table class="table table-bordered table-striped data-table">
						<thead>
							<tr>
								<th>Column1</th>
								<th>Column2</th>
								<th>Column3</th>
								<th>Column4</th>
							</tr>
						</thead>
						<tbody>
							<tr class="gradeA">
								<td>AAA</td>
								<td>fuf</td>
								<td>jyfu</td>
								<td class="center">1.5</td>
							</tr>
							<tr class="gradeA">
								<td>AAA</td>
								<td>1568</td>
								<td>ihou</td>
								<td class="center">1.6</td>
							</tr>
							<tr class="gradeA">
								<td>AAA</td>
								<td>5486</td>
								<td>iyfh</td>
								<td class="center">1.7</td>
							</tr>
							<tr class="gradeA">
								<td>BBB</td>
								<td>354</td>
								<td>jyuj</td>
								<td class="center">1.8</td>
							</tr>
							<tr class="gradeA">
								<td>BBB</td>
								<td>546</td>
								<td>yuyh</td>
								<td class="center">1.8</td>
							</tr>
							<tr class="gradeA">
								<td>BBB</td>
								<td>546</td>
								<td>ujuyhju</td>
								<td class="center">1.8</td>
							</tr>
							<tr class="gradeA">
								<td>BBB</td>
								<td>5</td>
								<td>vfjft</td>
								<td class="center">125.5</td>
							</tr>
							<tr class="gradeA">
								<td>CCC</td>
								<td>226841</td>
								<td>iuu</td>
								<td class="center">312.8</td>
							</tr>
															
						</tbody>
					</table>	

						
					</div> <!-- .widget-content -->
					
				</div> <!-- .widget -->	
				
	
				
			</div> <!-- .grid -->