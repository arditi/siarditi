<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
//jimport('joomla.application.component.view');
use Joomla\Registry\Registry;
 
/**
 * HTML View class for the Conetor Component
 */
class SIM0ViewDocs extends JViewLegacy
{
	protected $state;
	protected $items;
	protected $item;
	protected $pagination;
	protected $params;
	
	// Display the template
	public function display($tpl = null)
	{
		/*
		$this->items = $this->get('Items');
		parent::display($tpl);
	}
	*/
		$app		= JFactory::getApplication();
		$params		= $app->getParams();
		$this->params = $params;
		//print_r($this->params);die("");

		// Get some data from the models
		$state		= $this->get('State');
		$items		= $this->get('Items');
		$item		= $this->get('Item');
		$pagination	= $this->get('Pagination');

		/*
		 * // Change to catch
		 * if (count($errors = $this->get('Errors'))) {
		 * JError::raiseError(500, implode("\n", $errors));
		 * return false;
		 */

		// Check whether access level allows access.
		// @todo: Should already be computed in $item->params->get('access-view')
		$user	= JFactory::getUser();
		$groups	= $user->getAuthorisedViewLevels();
		if (!empty($items))
		{
			foreach ($items as $itemElement)
			{
				/*
				if (!in_array($itemElement->access, $groups)){
					//unset($itemElement);
				}
				// Prepare the data.
				$temp = new Registry;
				$temp->loadString($itemElement->params);
				$itemElement->params = clone $params;
				$itemElement->params->merge($temp);
				$itemElement->params = (array) json_decode($itemElement->params);
				*/
			}
		}

		$this->state      = &$state;
		$this->items      = &$items;
		$this->pagination = &$pagination;
		$this->user       = &$user;
		$this->item       = &$item;

		//$this->_prepareDocument();
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_TAGS_DEFAULT_PAGE_TITLE'));
		}

		if ($menu && ($menu->query['option'] != 'com_tags'))
		{
			$this->params->set('page_subheading', $menu->title);
		}

		// Set metadata for all tags menu item
		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

		// If this is not a single tag menu item, set the page title to the tag titles
		$title = '';

		if (!empty($this->item))
		{
			foreach ($this->item as $i => $itemElement)
			{
				if ($itemElement->title)
				{
					if ($i != 0)
					{
						$title .= ', ';
					}

					$title .= $itemElement->title;
				}
			}

			if (empty($title))
			{
				$title = $app->get('sitename');
			}
			elseif ($app->get('sitename_pagetitles', 0) == 1)
			{
				$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
			}
			elseif ($app->get('sitename_pagetitles', 0) == 2)
			{
				$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
			}

			$this->document->setTitle($title);

			foreach ($this->item as $itemElement)
			{
				if ($itemElement->metadesc)
				{
					$this->document->setDescription($this->item->metadesc);
				}
				elseif (!$itemElement->metadesc && $this->params->get('menu-meta_description'))
				{
					$this->document->setDescription($this->params->get('menu-meta_description'));
				}

				if ($itemElement->metakey)
				{
					$this->document->setMetadata('keywords', $this->tag->metakey);
				}
				elseif (!$itemElement->metakey && $this->params->get('menu-meta_keywords'))
				{
					$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
				}

				if ($this->params->get('robots'))
				{
					$this->document->setMetadata('robots', $this->params->get('robots'));
				}

				if ($app->get('MetaAuthor') == '1')
				{
					$this->document->setMetaData('author', $itemElement->created_user_id);
				}

				$mdata = $this->item->metadata->toArray();

				foreach ($mdata as $k => $v)
				{
					if ($v)
					{
						$this->document->setMetadata($k, $v);
					}
				}
			}
		}

		// Add alternative feed link
		if ($this->params->get('show_feed_link', 1) == 1)
		{
			$link    = '&format=feed&limitstart=';
			$attribs = array('type' => 'application/rss+xml', 'title' => 'RSS 2.0');
			$this->document->addHeadLink(JRoute::_($link . '&type=rss'), 'alternate', 'rel', $attribs);
			$attribs = array('type' => 'application/atom+xml', 'title' => 'Atom 1.0');
			$this->document->addHeadLink(JRoute::_($link . '&type=atom'), 'alternate', 'rel', $attribs);
		}
	}	
}


?>