<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_tags
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.core');
JHtml::_('formbehavior.chosen', 'select');

?>
<style>
.tooltip{
	background-color: #ffffff;
	color: #000fff;
}
</style>

<script type="text/javascript">
//http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-tooltips.php
$(document).ready(function(){
	//'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top',
        html: true
    });
});
</script>
<style type="text/css">
	.bs-example{
    	margin: 100px 50px;
    }
</style>
<!--http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-tooltips.php
<div class="bs-example"> 
    <ul class="list-inline">
        <li><a href="#" data-toggle="tooltip" data-original-title="Default tooltip">Tooltip</a></li>
        <li><a href="#" data-toggle="tooltip" data-original-title="A much longer tooltip to demonstrate the max-width of the Bootstrp tooltip.">Large tooltip</a></li>
        <li><a href="#" data-toggle="tooltip" data-original-title="The last tip!">Last tooltip</a></li>
    </ul>
</div>
-->


<div id="archive-items">

	<?php foreach ($this->items as $i => $item) : ?>
	
		<div class="row<?php echo $i % 2; ?>" >
			<dl>
			<dt>			
				<div class="page-header" style="margin-top:25px; margin-bottom:5px">
					<h4 style="margin:0px; line-height: 0;">
						<?php if($item->file_uri){?>
						<a 
						href="<?php echo JUri::base().$item->file_uri; ?>" _class="btn btn-danger" target="_new" 
						  _href="<?php echo JRoute::_($item->url); ?>"><?php echo $this->escape($item->title); ?></a>
						<?php }else{?>
						<?php echo $this->escape($item->title); ?>
						<?php }?>
					
					<a href="#" _class="btn btn-danger" 
						data-toggle="tooltip" 
						data-original-title="<?php 
								echo "Autor: ".$this->escape($item->autor_name) . "\r\n<br />";
								echo "Publicado: ".JHtml::_('date', $item->sys_date, JText::_('DATE_FORMAT_LC3')) . "\r\n<br />";
								if($item->file_uri){
									echo "Ficheiro: ".basename($item->file_uri) . "\r\n<br />";
									echo "Tamanho: ".filesize(JPATH_SITE.'/'.$item->file_uri) . " bytes\r\n<br />";
								}
							 ?>" 
						><i class="glyphicon glyphicon-book"></i></a>
					<!--	&nbsp;
					<a href="#" _class="btn btn-danger" 
						data-toggle="popover" 
						data-content="<?php 
								echo "Autor: ".$this->escape($item->autor_name) . "\r\n";
								echo "FileName: ".$item->file_uri . "\r\n";
								echo "FileSize: ".count($item->file_uri) . "\r\n";
								echo "Publicated: ".JHtml::_('date', $item->sys_date, JText::_('DATE_FORMAT_LC3'));
							 ?>" 
						title="Details..."><i class="glyphicon glyphicon-book"></i></a>
						-->
						&nbsp;
					<?php if($item->file_uri): ?>
					<a href="<?php echo JUri::base().$item->file_uri; ?>" _class="btn btn-danger" target="_new"
						title="Download..."><i class="glyphicon glyphicon-download"></i></a>
					<?php endif ?>
					
					</h4>
					</div>
				</dt>
			<dd>
				<div class="intro">
					<?php echo $item->resume;//."<br />"; ?> 
					<?php //echo $item->tags; ?> 
					</div>
					
					
					
				</dd>

			<!--
			<dd>
				<div class="published">
					<i class="glyphicon glyphicon-calendar"></i>
					<?php echo JHtml::_('date', $item->sys_date, JText::_('DATE_FORMAT_LC3')); ?>
					
				</div>
				</dd>
				-->
			</dl>
		</div>
	<?php endforeach; ?>
</div>
<!--
<div class="pagination">
	<p class="counter"> <?php echo $this->pagination->getPagesCounter(); ?> </p>
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
-->

<?php // Add pagination links ?>
<?php if (!empty($this->items)) : ?>
	<?php if (1==2 & ($this->params->def('show_pagination', 2) == 1  || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
	<div class="pagination">

		<?php if ($this->params->def('show_pagination_results', 1)) : ?>
			<p class="counter pull-right">
				<?php echo $this->pagination->getPagesCounter(); ?>
			</p>
		<?php endif; ?>

		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
	<?php endif; ?>
</form>
<?php endif; ?>

