<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_tags
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note that there are certain parts of this layout used only when there is exactly one tag.
// http://stackoverflow.com/questions/11007826/creating-an-array-using-recursive-php-from-mysql
// http://www.dyn-web.com/tutorials/php-js/json/array.php
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
$description 		= "";//$this->params->get('all_tags_description');
$descriptionImage 	= "";//$this->params->get('all_tags_description_image');


// Listagem de Categorias
JHtml::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/helpers');
$this->cats = SIM0Helper::getDocCategoriesTree( $id_cat_parent=null);
//print_r($this->cats);

// Categorias para Javascript
$json_cats = json_encode($this->cats);
JFactory::getDocument()->addScriptDeclaration('
	// Array de testes
	var tree = '.$json_cats.';
	// Some logic to retrieve, or generate tree structure
	var tree3 = [{
		text: "Trabalhos Científicos",	href: "9", nodes: [{
			text: "Child 1",	href: "1",
		},
		{text: "Child 2",	href: "43"}
	]},
	{text: "Documentos Gerais",	href: "21"}
];
'
);


JFactory::getDocument()->addStyleDeclaration('
#span-advanced-search{
  padding: 20px;
  display: none;
  margin-top: 20px;
}
.bold{
	/*font-weight: bolder;*/
	font-weight: bold;
}
'
);
?>



<div _class="page-header">
	<h3 itemprop="name">Biblioteca</h3>
</div>
	
<div class="row0">
<div class="box col-md-3">

<div class="box-inner">
<div class="box-header well" data-original-title="">
<h2>Áreas Ciêntificas</h2>
<div class="box-icon" style="display:none">
<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
</div>
</div>
<div _class="box-content">
		<!--
			https://github.com/jonmiles/bootstrap-treeview/blob/master/README.md
			-->
		<div id="tree"></div>

	</div>		
	</div>
</div>


<div class="box col-md-9">

<form id="adminForm" action="<?php echo JRoute::_('index.php')?>" method="post" class="form-inline" onSubmit="return false;">
	<fieldset class="filters">
	<div _class="filter-search">
			<div _class="box-content">
                <div class="control-group">

                    <div _class="controls">
						<!--
						<div class="input-group col-md-8">
							<input type="text" class="form-control" placeholder="Search for...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit"> 
								<i class="glyphicon glyphicon-search"></i> 
								</button></span>
						</div>	
						-->
						<div class="input-group col-md-6">
							<span class="input-group-addon"><i class="glyphicon glyphicon-search ored"></i></span>
							<input type="text"  name="filter_search" id="filter_search"  
								class="form-control" placeholder="Search for..." 
								value="<?php echo $this->getModel()->getState('filter.search') ?>">
						</div>
						<div class="input-group hhas-success">
							<button id="btnSubmit" type="submit" onclick="return false" class="btn btn-primary"><?php echo JText::_('JGLOBAL_FILTER_BUTTON'); ?></button>
						</div>				
						<div class="input-group hhas-success">
							<button id="btnAdvanced" type="submit" onclick="return false" class="btn"><?php echo JText::_('Advanced'); ?></button>
						</div>		<!--		
						<div>
						   <label><input type="checkbox" name="colorCheckbox" value="checkbox"></label>
						   Advance Search
						</div>	-->

						<div id="span-advanced-search" class="box-inner col-md-12" style="margin-bottom:20px">
						<div class="box-content ">
							<div class="row0">
							<div class="box col-md-4" style="border-right-style: dotted;">
							<b>Ordem cronológica</b><br />
								Início:<br />
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-calendar ored"></i></span>
									<input type="text"  readonly name="filter_search" id="filter_search"  
										class="form-control" value="<?php echo date("Y/m/d") ?>">
								</div><br />
								Fim:<br />
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-calendar ored"></i></span>
									<input type="text"  readonly name="filter_search" id="filter_search"  
										class="form-control" placeholder="yyyy/mm/dd" >
								</div>	
							</div>
							<div class="box col-md-4" style="border-right-style: dotted;">
								<b>Conteúdo</b><br />
								Título:<br />
								<div class="input-group">
									<input type="text"  readonly name="filter_search" id="filter_search"  
										class="form-control" placeholder="Search for..." 
										value="<?php //echo $this->getModel()->getState('filter.search') ?>">
								</div>		<br />
								Autor:<br />
								<div class="input-group">
									
									<input type="text"  readonly name="filter_search" id="filter_search"  
										class="form-control" placeholder="Search for..." 
										value="<?php //echo $this->getModel()->getState('filter.search') ?>">
								</div>								
							</div>
							<div class="box col-md-4">
							<b>Apresentação</b><br />
								Ordenação:<br />
								<div class="input-group">
									<select name="filter_search" readonly id="filter_search"  class="form-control" >
									<option value="1">Título</option>
									</select>
								</div>	
							</div>
							</div>
							<!--
							<table class="display" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Data</th>
											<th>Conteúdo</th>
											<th>Apresentação</th>
										</tr>
									</thead>
										<tr>
											<th><label>Início:</label> <input type="text"/><br />
												<label>Fim:</label> <input type="text"/></th>
											<th><label>Type de conteúdo:</label> <input type="text"/></th>
											<th><label>Ordenação:</label> <input type="text"/></th>
										</tr>
								</table>						
							-->
						
							<br>
							<br>

						</div>
						</div>
												
                    </div>
                </div>
			</div>	
		
		<input type="hidden" name="view"   value="docs" />
		<input type="hidden" name="option" value="com_sim0" />
		<input type="hidden" name="_limitstart" value="0" />
	</div>
	</fieldset>
</form>	


<div class="items">
<!--
	<?php 
	if (count($this->items)==0) : ?>
		<div class="row0">
			<div class="page-header">
				<strong>Atenção!</strong>
				<br />Não existem Indicadores com estes critérios...
			</div>
		</div>
	<?php else: ?>
		<?php //echo $this->loadTemplate('items'); ?>
	<?php endif; ?>		
	-->



	<div class="box-inner">

	<div class="box-content">
	<table id="example" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Título</th>
					<th>Domínio</th>
					<th>Autor</th>
					<th>Data de Publicação</th>
				</tr>
			</thead>
		</table>
	</div>
	</div>

	</div>
		
	<div id="lastPostsLoader">
		<img src="templates/sisite/img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-8.gif"/>
	</div>

</div><!-- form + docs -->

</div><!--row-->





<script type="text/javascript">
$(document).ready(function() {

});

$(document).ready(function(){
	$('input[type="checkbox"]').click(function(){
		if($(this).attr("value")=="checkbox"){
			$("#span-advanced-search").toggle();
		}
	  });
	  
	  
	$('#btnAdvanced').on( 'click', function () {
		$("#span-advanced-search").toggle();
		return false;
	} );	  
});

///
var pagesRequest = 0;
var firstRequest = 1;
///
var selCategory = null;
var selNode = null;
function inform(){
	document.write("Tmp variable changed from 1 to 3");
}
//watch("selCategory",inform);		

function getTreeData() {
	// Some logic to retrieve, or generate tree structure
	if(tree===undefined){ 
		alert("Attention: tree data is null");
		var tree2 = [{text: "Sem documentos"}];
		return tree2;
	}
	return tree;
}

// JQUERY
$(document).ready(function () {
	
//////////////
//
// DATATABLE
//
	// Loading on Server
	// http://www.datatables.net/release-datatables/examples/api/row_details.html
	//var table = $('#example').dataTable( {	
	var oTable = $('#example').DataTable( {
		"sajax": 'media/dataTables/scripts/data_docs.php?selCategory='+selCategory,
		"gajax": 'media/dataTables/scripts/data_docs.php?option=com_sim0&view=docs&filename=range.json&format=json&selCategory='+selCategory,
		"ajax": 'index.php?option=com_sim0&view=docs&filename=range.json&format=json&selCategory='+selCategory,
		"bJQueryUI": false,
		"bFilter": false,
		"bSort": false,
		"bStateSave": true,
		"bAutoWidth": false,
		"bInfo": true,
		"bLengthChange": false,
		"iDisplayLength": 10,
		"_iPageLength ": 10,
		"_lengthMenu": [[10], [10]],
		"sPaginationType": "full_numbers",
		"columns": [
			{ "data": "title", "className": 'details-control' },
			{ "data": "id_category" },
			{ "data": "autor_name" },
			{ "data": "sys_date" }
		],
        "order": [[1, 'asc']],
		"ordering": true,
		"fnServerParams": function ( aoData ) {
			// Callback fnServerParams: It is often useful to send extra data to the server when making an Ajax request - 
			// for example custom filtering information, and this callback function makes it trivial to send extra information to the server
			aoData.push({"name": "selCategory2", "value": selCategory},
						{"name": "filter_date", "value": $('#filter_search').val() } );
		},
		"fnDrawCallback": function( oSettings ) {
			pagesRequest = 0;
			//alert( 'DataTables has redrawn the table' );
			if(selNode!=null){
				//selNode.state.selected = true;
				$('#tree').treeview('selectNode', [ selNode.nodeId, { silent: true } ]);
			}
			if(firstRequest){
				firstRequest=0;
			}
			else{
				$('div#lastPostsLoader').empty();				
			}
		},
		"_fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
			oSettings.jqXHR = $.ajax( {
				"dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": fnCallback
			} );
		}			
	});
	/* Formatting function for row details - modify as you need */
	function format ( d ) {
		// `d` is the original data object for the row
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:60px;width:100%">'+
			'<tr><td width="170"><bb>Resumo:			</bb></td><td>'+d.resume+'</td>'+'</tr>'+
			'<tr><td><bb>Domínio:		</bb></td><td>'+d.domain+'</td>'+'</tr>'+
			'<tr><td><bb>Download File:	</bb></td><td><a href="'+d.url+'">'+d.title+'</a> (32432 bytes)</td>'+'</tr>'+
		'</table>';
	}
	function formatB ( d ) {
		return '<ul style="padding-left:30px;width:96%">'+
					'<li><b>Resumo: </b>'+d.resume+'</li>'+
					'<li><b>Domínio: </b>'+d.domain+'</li>'+		
					'<li><b>Download File: </b><a href="'+d.url+'">'+d.title+'</a>  (32432 bytes)</li>'+
				'</ul>';	
	}	

	// Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = oTable.row( tr );
        if ( row.child.isShown() ) {// This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
			tr.removeClass('bold');
        }
        else {				        // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
			tr.addClass('bold');
        }
    } );
	/*
	// On Row click go
	$('#example').on( 'click', 'tbody tr', function () {
		//window.location.href = $(this).attr('href');
        var tr = $(this);//.closest('tr');
        var row = oTable.row( tr );
		//alert(tr.html());
		window.location.href = row.data().url;
	} );
	*/
	// On Row click go
	$('#btnSubmit').on( 'click', function () {
		LoadDocumentList();
		return true;
	} );
	
	////
	// LoadDocumentList
    function LoadDocumentList()
    {
		// Se cancelarmos os loads por estarmos a espera, temos de cancelar a selecão do nodo, ou voltar a selecionalo!
		if( pagesRequest == 0 ){
			pagesRequest = 1;
			// Show loader
			$('div#lastPostsLoader').html('<img src="templates/sisite/img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-8.gif"/>');

			// Generate new url					
			var filter_search = '';
			var filter_object = document.getElementById("filter_search");
			if(filter_object!=null & filter_object!=undefined){
				if(!isEmpty(filter_object.value))	// se não coloca 'undefined'
					filter_search = filter_object.value;
			}

			var url = 'index.php?option=com_sim0&view=docs&filename=range.json&format=json&selCategory='+selCategory+'&filter_search='+filter_search;
			// É vantajoso passar todos os parametros no URL, mas o callback fnServerParams funciona bem!
			oTable.ajax.url( url ).load();
			/*//http://www.datatables.net/forums/discussion/3740/how-to-refresh-ajax-source-when-not-using-server-side-processing/p1
			var oSettings = oTable.fnSettings();
			oSettings.aoData.push(	{ "name":  "SD", "value": sStartDate }  );
			oTable.fnReloadAjax();
			oTable.fnDraw();
			*/
		}
		return;
    };
	

//////////////
//} );
//
// TREE
//
//$(document).ready(function () {
	////
	// https://github.com/jonmiles/bootstrap-treeview
	// Example: initializing the treeview expanded to 4 levels with a background color 
	$('#tree').treeview({
	  data: getTreeData(), // data is not optional
	  levels: 4,
	  _backColor: '#cccccc'
	});
	
	// Callback nodeSelected
	$('#tree').on('nodeSelected', function(event, data) {
		// Your logic goes here
		var path = data.text;
		//alert('nodeSelected='+data.href+' path='+path);
		
		if( pagesRequest != 0 ) return;
			
		// Actualiza a categoria selecionada
		if(data.href!==undefined){
			 selCategory = data.href;
			 selNode 	 = data;
		}
		else{
			selNode 	 = null;
			selCategory = 0;
		} 
		
		// Carrega Listagem de documentos
		//UI -> Tive de passar a LoadDocumentList(selCategory);
		setTimeout(function() {LoadDocumentList();}, 50);  // Assim consegue-se passar parametros para a fnção e nao estraga o ui da tree
		
		// Nodo Raiz
		var nodeRoot = $('#tree').treeview('getNode', 0);
	/*	//alert(nodeRoot.text);
		var parent = $('#tree').treeview('getParent', data);		
		var i =0;
		if(path!=nodeRoot.text)
			while(parent!=null & i<15){
				path   = path+'-'+parent.text;
				if(parent.text==nodeRoot.text) break; i++;
				parent = $('#tree').treeview('getParent', parent);
				if(parent===undefined)break;
			}
		//alert(path);
		*/
	});			
	
});
</script>
