﻿<?php
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.view');

/**
 * @param $value
 * @return mixed
 */
function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
	$escapers 		= array("\\",   "/", 	"\"", 	"\n", 	"\r", 	"\t", 	"\x08", "\x0c");
	$replacements 	= array("\\\\", "\\/", 	"\\\"", "\\n", 	"\\r", 	"\\t", 	"\\f", 	"\\b");
	$result = str_replace($escapers, $replacements, $value);
	//return $result;
	return $value;
}

/**
 *
 */	
class SIM0ViewDocs extends JViewLegacy
{
	public function display($tpl = null)
	{
		//$this->importRSS();die("end");

		$app 		= JFactory::getApplication('site');
		$user		= JFactory::getUser();
		$state 		= $this->get('State');
		$pagination	= $this->get('Pagination');
		$items 		= $this->get('Items');
		// Get the page/component configuration
		$params 	= &$state->params;
		
		// para resposta JSON
		$response = array(
			"draw" 				=> 1,
			"recordsTotal"		=> count($items),
			"recordsFiltered"	=> count($items),
			"data" 				=> $items
		);
		
		//echo json_encode($response,  JSON_HEX_QUOT | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE);
		//echo json_encode($response );
		// O correcto
		echo escapeJsonString( json_encode($response) );
		die();
	}
	
	
	public function importRSS($feed_url = "http://www.rcaap.pt/rss.xml")
	{
		echo "Starting to work with feed URL '" . $feed_url . "'";
		/*
		<item>
		<title>...</title>
		<link>http://hdl.handle.net/10400.7/254</link>
		<description>...</description>
		<guid>http://hdl.handle.net/10400.7/254</guid>
		</item>
		*/
		try
		{
			
			//first we import some help from the joomla framework
			JLoader::import('joomla.application.component.model');
			//Load com_foo's items model (notice we are linking to com_foo's model directory)
			//and declare 'items' as the first argument. Note this is case sensitive and used to construct the file path.
			JLoader::import( 'Doc', JPATH_ADMINISTRATOR .  '/components/com_sim0/models' );
			//Now instantiate the model object using Joomla's camel case type naming protocol.
			$items_model = JModelLegacy::getInstance( 'Doc', 'SIM0Model' );
			//JLoader::import( 'books', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_mylibrary' . DS . 'models' );

			//Now that we have the item's model loaded, we can set its state
			$doc = $items_model->getTable( );
			//and use it's methods!
			//$items_model->get_item();
			//print_r($doc);die("aqui");
			
			$sys 		= &SysARDITI::getInstance();
			$db 		= &$sys->getDBO();
				
			/* Parse XML from  http://www.instapaper.com/starred/rss/580483/qU7TKdkHYNmcjNJQSMH1QODLc */
			//$RSS_DOC = simpleXML_load_file('http://www.instapaper.com/starred/rss/580483/qU7TKdkHYNmcjNJQSMH1QODLc');
			//libxml_use_internal_errors(true);
			//$RSS_DOC = simpleXML_load_file($feed_url);

			// CURL way to set Agent
			$ch = curl_init($feed_url);//'http://www.bignewsnetwork.com/index.php/nav/rss/4a6d634cbccbbfe2');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// Identify the rquest User Agent as Chrome - any real browser, or perhaps any value may work
			// depending on the resource you are trying to hit
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36');
			$feed = curl_exec($ch);
			
			$RSS_DOC = simplexml_load_string($feed);
			if (!$RSS_DOC) {
				echo "Failed loading XML\n";
				foreach(libxml_get_errors() as $error) {
					echo "\t", $error->message;
				}
				return;
			}

			/* Get title, link, managing editor, and copyright from the document  */
			$rss_title 		= $RSS_DOC->channel->title;
			$rss_link 		= $RSS_DOC->channel->link;
			$rss_editor 	= $RSS_DOC->channel->managingEditor;
			$rss_copyright	= $RSS_DOC->channel->copyright;
			$rss_date 		= $RSS_DOC->channel->pubDate;
			
			//Loop through each item in the RSS document
			foreach($RSS_DOC->channel->item as $RSSitem)
			{
				$item_id 	= md5((string)$RSSitem->title);
				$fetch_date = date("Y-m-j G:i:s"); //NOTE: we don't use a DB SQL function so its database independant
				$item_title = (string)$RSSitem->title;
				$item_date  = date("Y-m-j G:i:s", strtotime((string)$RSSitem->pubDate));
				$item_url	= (string)$RSSitem->link;
				$item_desc	= (string)$RSSitem->description;
				
/*				echo "Processing item '" , $item_id , "' on " , $fetch_date 	, "<br/>";
				echo "item_title:", $item_title, " - ";
				echo "item_date:", $item_date, "<br/>";
				echo "item_url:", $item_url, "<br/>";
				echo "item_desc:", $item_desc, "<br/>";
	*/			
				//print_r($doc);print_r($RSSitem);die("ss");				
				
				$doc->id_doc	= '';
				$doc->title 	= $item_title;
				$doc->resume 	= $item_desc;
				$doc->autor_name= "rcaap.pt";
				$doc->id_category = 4;
				$doc->validated = 1;
				$doc->state = 0;
				$doc->pub_date = $item_date;
				
/*
https://docs.joomla.org/Creating_content_using_JTableContent

if (!$doc->store(TRUE)) {
	JError::raiseNotice(500, $article->getError()); 
	return FALSE;
}
*/


							
				if($doc->title){
					if($doc->store(true)){
						echo "Saved ok<br/>";
					}
					else{
						echo "Saved nok<br/>";
					}
				}
				else{
					echo "Saved nok, title empty<br/>";
				}
				//print_r($doc);break;
			}

		// End of form //
		} 
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
	}

}