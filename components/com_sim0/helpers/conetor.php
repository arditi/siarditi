<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
/**
 * @package		ConetorHelperConetor
 * @author		Magnum Cap http://www.magnumcap.com
 * @copyright	Copyright (c) 2012 MagnumCap. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
class ConetorHelperConetor {

//define('COM_MEDIA_BASE',	JPATH_ROOT.'/'.$params->get($path, 'images'));
//define('COM_MEDIA_BASEURL', JURI::root().$params->get($path, 'images'));

	/**
	* MyComponentHelper is a component specific factory class that connects to
	* different databases
	* @author Oleksandr Balyuk
	* /
	No just include your helper model/class where you need to connect to different database and call it like so
	// Get first database connection
	$db1 = & ConetorHelperConetor::getDBO1();
	// Get second database connection
	$db2 = & MyComponentHelper::getDBO2();
	// Get Joomla default database connection
	$db = & JFactory::getDBO();
	You can download MyComponentHelper class from here. There are copies of the class, one for php 4 and the other for php 5.
	*/

	/**
	* Returns reference to firstst database connection
	* if connection does not exist will create it
	*
	* @staticvar JDatabase $dbo1
	* @return JDatabase
	*/
	function &getDBO() {
		static $dbo1 = null;
		if (!$dbo1) {
			$option = array();
			$option['driver'] 	= 'mysql';        // Database driver name
			$option['host'] 	= '192.168.1.173';  // Database host name
			//$option['host'] 	= '192.168.1.58';  // Database host name
			$option['user'] 	= 'web';       // User for database authentication
			$option['password'] = 'magnumcap';   // Password for database authentication
			$option['database'] = 'conetor';   // Database name
			$option['prefix'] 	= '';             // Database prefix
			//
			$dbo1 = & JDatabase::getInstance($option);
		}
		return $dbo1;
	}
	/**
	* Gets reference to second database connection
	* if connection does not exist will create it
	*
	* @staticvar JDatabase $dbo2
	* @return JDatabase
	*/
	function &getDBO2() {
		static $dbo2 = null;
		if (!$dbo2) {
			$option = array();
			$option['driver'] = 'mysql';        // Database driver name
			$option['host'] = 'localhost';      // Database host name
			$option['user'] = 'username2';      // User for database authentication
			$option['password'] = 'password2';  // Password for database authentication
			$option['database'] = 'database2';  // Database name
			$option['prefix'] = '';             // Database prefix
			$dbo2 = & JDatabase::getInstance($option);
		}
		return $dbo2;
	}
	
	/**
	 * UploadSmartVendingOfflines()
	 * @return void
	 */
	public function GetXMLPath($idApp, $idShop, $idMachine){
		$pathStr =  'SmartVendingData';
		//MCSVModelShops::CreateNestedDirs($pathStr);
		return $pathStr.'/';
	}
	/**
	 * UploadSmartVendingOfflines()
	 * @return void
	 */
	public function GetXMLPathApp($idApp, $idShop, $idMachine){
		$pathStr =  'SmartVendingData/App'.$idApp.'';
		//MCSVModelShops::CreateNestedDirs($pathStr);
		return $pathStr.'/';
	}
	public function GetXMLPathShop($idApp, $idShop, $idMachine){
		$pathStr =  'SmartVendingData/App'.$idApp.'/Shop'.$idShop.'';
		//MCSVModelShops::CreateNestedDirs($pathStr);
		return $pathStr.'/';
	}
	public function GetXMLPathMachine($idApp, $idShop, $idMachine){
		$pathStr = 'SmartVendingData/App'.$idApp.'/Shop'.$idShop.'/Machine'.$idMachine;
		//MCSVModelShops::CreateNestedDirs($pathStr);
		return $pathStr.'/';
	}

	
	/**
	* Charger related functions
	* 
	*/
	
	/**
	 * TopChargedTypes()
	 * @return Top Energie Charged
	 */
	public static function TopChargedTypes($limit=2)
	{
		$db  	= &JFactory::getDBO(); 
		$sys 	= &SysConetor::getInstance();

		$sql_0 	= "SELECT id_charge_type, SUM(energy) as energy from #__charges ";
		if($sys->isOperator){
			//$sql_0 .= ' WHERE #__charges.id_card IN (SELECT id_card from #__cards WHERE id_operator='.$sys->id_user.')';
			$sql_0 .= ' ';
		}else{
			$sql_0 .= ' WHERE #__charges.id_card IN (SELECT id_card from #__cards WHERE id_user='.$sys->id_user.')';
		}
		//$sql_0 .= ' AND #__charges.sys_date > DATE_SUB(now(), INTERVAL 12 MONTH) ';
		$sql_0 .= ' GROUP BY id_charge_type ORDER BY id_charge_type ASC limit '.$limit;
		$db->setQuery( $sql_0 ); 
		$messages = $db->loadObjectList();
		//SELECT id_charge_type, SUM(energy) as energy from jos_charges WHERE jos_charges.id_card IN (SELECT id_card from jos_cards WHERE id_user=122) GROUP BY id_charge_type ORDER BY SUM(energy) DESC limit 5
		//print_r($options);	die($sql_0);
		return $messages;
	}

	/**
	 * TopLastCharges()
	 * @return Top Last Charges
	 * Podiamos uasr o model Transactions com uma vista esecifica para este top
	 */
	public static function TopLastCharges($limit=5)
	{
		$db  	= &JFactory::getDBO(); 
		$sys 	= &SysConetor::getInstance();

		$sql_0 	= 'SELECT id_charge, id_charge_type, id_card, charge, energy, TIMEDIFF(stop_date, start_date) AS duration, start_date, stop_date, sys_date';
		$sql_0 .= ',(select charge_type FROM #__charges_types AS CT where CT.id_charge_type=CG.id_charge_type) AS charge_type';
		//$sql_0 .= '(select card FROM #__cards AS CT where CT.id_card=CG.id_card) AS card';
		$sql_0 .= ' from #__charges AS CG WHERE  stop_date IS NOT NULL';
		if($sys->isOperator){
			$sql_0 .= ' ';
		}else{
			$sql_0 .= ' AND #__charges.id_card IN (SELECT id_card from #__cards WHERE id_user='.$id_user.')';
		}
		$sql_0 .= ' ORDER BY sys_date DESC limit '.$limit;
		$db->setQuery( $sql_0 );
		$messages = $db->loadObjectList();
		$options  = &$messages; 
		//print_r($options);	die($sql_0);
		return $options;
	}
	

	/**
	 * TopMostActivities()
	 * @return Top Most Activities
	 */
	public static function TopMostActivities($limit=5)
	{
		$db  	= &JFactory::getDBO(); 
		$sys 	= &SysConetor::getInstance();
		$filterDate = '';

		$sql_0 	= "SELECT kiosk_id, kiosk_local, COUNT( energy ) as energy from #__carregadores_kiosks, #__charges ";
		$sql_0 .= " WHERE #__carregadores_kiosks.kiosk_id = #__charges.id_charger ".$filterDate;
		if($sys->isOperator){
			//$sql_0 .= ' AND #__charges.id_card IN (SELECT id_card from #__cards WHERE id_operator='.$sys->id_user.')';
			$sql_0 .= ' ';
		}else{
			$sql_0 .= ' AND #__charges.id_card IN (SELECT id_card from #__cards WHERE id_user='.$sys->id_user.')';
		}
		$sql_0 .= ' AND #__charges.sys_date > DATE_SUB(now(), INTERVAL 12 MONTH) ';
		$sql_0 .= ' GROUP BY #__carregadores_kiosks.kiosk_id ORDER BY COUNT(energy) DESC limit '.$limit;
		$db->setQuery( $sql_0 ); 
		$messages = $db->loadObjectList();
		$options  = array(); 
		if ($messages){
			foreach($messages as $message){
				$options[$message->kiosk_id] = $message->kiosk_local;
			}
		}
		//print_r($options);	die($sql_0);
		return $options;
	}
	
	
	/**
	 * TopMostUsedChargers()
	 * @return Top Most Used Chargers
	 */
	public static function TopMostUsedChargers($limit=5)
	{
		$db  	= &JFactory::getDBO(); 
		$sys 	= &SysConetor::getInstance();

		$sql_0 	= "SELECT kiosk_id, kiosk_local, COUNT( energy ) as energy from #__carregadores_kiosks, #__charges ";
		$sql_0 .= " WHERE #__carregadores_kiosks.kiosk_id = #__charges.id_charger ".$filterDate;
		if($sys->isOperator){
			//$sql_0 .= ' AND #__charges.id_card IN (SELECT id_card from #__cards WHERE id_operator='.$sys->id_user.')';
			$sql_0 .= ' ';
		}else{
			$sql_0 .= ' AND #__charges.id_card IN (SELECT id_card from #__cards WHERE id_user='.$sys->id_user.')';
		}
		$sql_0 .= ' AND #__charges.sys_date > DATE_SUB(now(), INTERVAL 12 MONTH) ';
		$sql_0 .= ' GROUP BY #__carregadores_kiosks.kiosk_id ORDER BY COUNT(energy) DESC limit '.$limit;
		$db->setQuery( $sql_0 ); 
		$messages = $db->loadObjectList();
		return $messages;
		$options  = array(); 
		if ($messages){
			foreach($messages as $message){
				$options[$message->kiosk_id] = $message->kiosk_local;
			}
		}
		//print_r($options);	die($sql_0);
		return $options;
	}

	/**
	 * Returns Cards of the UserId 
	 * @return  object  The indexer state object.
	 * @since   2.5
	 */	
	public static function getCardsOfUser($id_user, $limit=5)
	{
		$db = &JFactory::getDBO();
		$db->setQuery('SELECT id_card from #__cards WHERE id_user='.$id_user.' ORDER BY sys_date DESC limit '.$limit.' OFFSET 0' );
		$result = $db->loadObjectList();
		return $result;
	}
	
	/**
	 * Returns StateOfUserCard of the CardId 
	 * @return  object  The indexer state object.
	 * @since   2.5
	 */	
	public static function getStateOfUserCard($id_user)
	{
		//$db = &JFactory::getDBO();
		// Get first database connection
		$db = &ConetorHelperConetor::getDBO();
		$db->setQuery('SELECT start_date from mccn_charge WHERE id_user='.$id_user.' WHERE stop_date IS NULL order by start_date desc' );
		$result = $db->loadResult();
		if($result) return false;
		else		return true;
		//return $result;
	}

	/**
	 * Returns getPresentCharges
	 * @return  object  The indexer state object.
	 * @since   2.5
	 */	
	public static function getPresentCharges($id_user)
	{
		//$db = &JFactory::getDBO();
		// Get first database connection
		$db = &ConetorHelperConetor::getDBO();
		//`id_charge`, `id_charging_point`, `id_card`, `msisdn`, `id_refuel`, `energy`, `percentage`, `start_date`, `end_date`, `end_reason`, `sys_date`, `sys_update`, `sys_del`, `obs`SELECT * FROM `mccn_charge`
		$db->setQuery('SELECT id_charge, energy, start_date from mccn_charge WHERE id_card IN ( SELECT id_card FROM mccn_card WHERE id_user='.$id_user.') WHERE end_date IS NULL order by start_date desc' );
		$result = $db->loadResult();
		if($result) return false;
		else		return true;
		//return $result;
	}
	
}

?>