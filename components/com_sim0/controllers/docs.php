<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_finder
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */
defined('_JEXEC') or die;

// Include dependancy of the main controllerform class
jimport('joomla.application.component.controllerform');

class SIM0ControllerDocs extends JControllerLegacy
{
	/**
	 * Method to find search query suggestions.
	 * @return  void
	 * @since   2.5
	 */
	public function getModel($name = 'Docs', $prefix = 'SIM0Model', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, array('ignore_request' => false));
	}
	/**
	 * Method to search tags with AJAX
	 *
	 * @return  void
	 */
	public function searchAjax()
	{
		// Required objects
		$app = JFactory::getApplication();

		// Receive request data
		$filters = array(
			'like'      => trim($app->input->get('like', null)),
			'title'     => trim($app->input->get('title', null)),
			'flanguage' => $app->input->get('flanguage', null),
			'published' => $app->input->get('published', 1, 'integer'),
			'parent_id' => $app->input->get('parent_id', null)
		);

		if ($results = JHelperTags::searchTags($filters))
		{
			// Output a JSON object
			echo json_encode($results);
		}
		$app->close();
	}
}

?>