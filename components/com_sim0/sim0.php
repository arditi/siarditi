<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_m0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Get an instance of the controller prefixed by HelloWorld
$controller = JControllerLegacy::getInstance('SIM0');

 		// Register Library jpath_component_administrator 
		JLoader::register('SIM0Helper', JPATH_COMPONENT_ADMINISTRATOR . '/helpers/sim0.php');

// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
