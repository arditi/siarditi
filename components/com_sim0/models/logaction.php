<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

/**
 * Log Model
 *
 * @since  0.0.1
 */
class SIM0ModelLogAction extends JModelItem
{
	/**
	 * @var object item
	 */
	protected $item;
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
		
		// Insert new LogAction
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);

	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	2.5
	 */
	protected function populateState()
	{
		// Get the message id
		$jinput = JFactory::getApplication()->input;
		$id     = $jinput->get('id_log', 1, 'INT');
		$this->setState('message.id_log', $id);

		// Load the parameters.
		$this->setState('params', JFactory::getApplication()->getParams());
		parent::populateState();
	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'LogActions', $prefix = 'M0Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Get the message
	 * @return object The message to be displayed to the user
	 */
	public function getItem()
	{
		if (!isset($this->item)) 
		{
			$id_log= $this->getState('message.id_log');
			$db    = $this->getDbo();
			$query = $db->getQuery(true);
			$query->select('l.source, l.id_log, l.id_action, l.id_user, l.sys_date, c.action as action')
				  ->from('logactions as l')
				  ->leftJoin('logactiontypes as c ON l.id_action=c.id_action')
				  ->where('l.id_log>=' . (int)$id_log)
				  ->setLimit(1,0);
			$db->setQuery((string)$query);

			if ($this->item = $db->loadObject()) 
			{
				//EJ
				// Load the parameters.
				if(!isset($this->item->params))
					$this->item->params = JFactory::getApplication()->getParams();
				
				// Load the JSON string
				$params = new JRegistry;
				$params->loadString($this->item->params, 'JSON');
				$this->item->params = $params;

				// Merge global params with item params
				$params = clone $this->getState('params');
				$params->merge($this->item->params);
				$this->item->params = $params;
			}
		}
		return $this->item;
	}
	/**
	 * Increment the hit counter for the article.
	 *
	 * @param   integer  $pk  Optional primary key of the article to increment.
	 *
	 * @return  boolean  True if successful; false otherwise and internal error set.
	 */
	public function hit($pk = 0)
	{
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);

		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('article.id');

			$table = JTable::getInstance('Content', 'JTable');
			$table->load($pk);
			$table->hit($pk);
		}

		return true;
	}	
}
