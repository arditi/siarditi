<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_tags
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

/**
 * This models supports retrieving a list of tags.
 *
 * @since  3.1
 */
class SIM0ModelLogActions extends JModelList
{
	/**
	 * Model context string.
	 *
	 * @var    string
	 * @since  3.1
	 */
	public $_context = 'com_sim0.logactions';
	
	/**
	 * Constructor
	 */
	public function __construct($config = array()){
		parent::__construct($config);
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @note Calling getState in this method will result in recursion.
	 *
	 * @since   3.1
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication('site');

		// Load state from the request.
		$pid = $app->input->getInt('parent_id');
		$this->setState('tag.parent_id', $pid);

		$language = $app->input->getString('tag_list_language_filter');
		$this->setState('tag.language', $language);

		$offset = $app->input->get('limitstart', 0, 'uint');
		$this->setState('list.offset', $offset);
		$app = JFactory::getApplication();

		$params = $app->getParams();
		$this->setState('params', $params);

		$this->setState('list.limit', $params->get('maximum', 200));

		$this->setState('filter.published', 1);
		$this->setState('filter.access', true);

		$user = JFactory::getUser();

		if ((!$user->authorise('core.edit.state', 'com_tags')) &&  (!$user->authorise('core.edit', 'com_tags')))
		{
			$this->setState('filter.published', 1);
		}

		// Optional filter text
		$itemid = $pid . ':' . $app->input->getInt('Itemid', 0);
		$filterSearch = $app->getUserStateFromRequest('com_tags.tags.list.' . $itemid . '.filter_search', 'filter-search', '', 'string');
		$this->setState('list.filter', $filterSearch);
	}

	/**
	 * Redefine the function and add some properties to make the styling more easy
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   3.1
	 */
	public function getItems()
	{
		// Invoke the parent getItems method to get the main list
		$items = parent::getItems();

		if (!count($items))
		{
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$active = $menu->getActive();
			$params = new Registry;

			if ($active)
			{
				$params->loadString($active->params);
			}
		}

		return $items;
	}
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		require_once JPATH_COMPONENT_ADMINISTRATOR . '/helpers/sim0.php';
		
		// Initialize variables.
		$db    	= $this->getDbo();
		$query 	= $db->getQuery(true);
		
		// Create the base select statement.
		$query->select('*, (SELECT action from logactiontypes AS tb WHERE tb.id_action=ta.id_action) as action')
			  ->from('LogActions as ta');

		// So os SuperAdmins vêem as LogAction todas
		$sys 	= &SysARDITI::getInstance();
		if(!$sys->isRoot()){
			$query->where('id_action > 10');			
		}

		// Filter: like / search
		$search = $this->getState('filter.search');
		if (!empty($search)){
			$like = $db->quote('%' . $search . '%');
			$query->where('source LIKE ' . $like);
		}
		// Filter by id_action
		$id_action = $this->getState('filter.id_action');
		if (is_numeric($id_action)){
			$query->where('id_action=' . (int) $id_action);
		}
		// Filter by id_user
		$id_user = $this->getState('filter.id_user');
		if (is_numeric($id_user)){
			$query->where('id_user = ' . (int) $id_user);
		}
		// Add filter for registration ranges select list
		$range = $this->getState('filter.range');
		// Apply the range filter.
		if ($range){
			// Get UTC for now.
			$dNow   = new JDate;
			$dStart = clone $dNow;
			switch ($range){
				case 'past_week':
					$dStart->modify('-7 day');
					break;
				case 'past_1month':
					$dStart->modify('-1 month');
					break;
				case 'past_3month':
					$dStart->modify('-3 month');
					break;
				case 'past_6month':
					$dStart->modify('-6 month');
					break;
				case 'post_year':
				case 'past_year':
					$dStart->modify('-1 year');
					break;
				case 'today':
					// Ranges that need to align with local 'days' need special treatment.
					$app    = JFactory::getApplication();
					$offset = $app->get('offset');
					
					// Reset the start time to be the beginning of today, local time.
					$dStart = new JDate('now', $offset);
					$dStart->setTime(0, 0, 0);

					// Now change the timezone back to UTC.
					$tz = new DateTimeZone('GMT');
					$dStart->setTimezone($tz);
					break;
			}
			if ($range == 'post_year'){
				$query->where($db->qn('sys_date') . ' < ' . $db->quote($dStart->format('Y-m-d H:i:s')));
			}
			else{
				$query->where($db->qn('sys_date') . ' >= ' . $db->quote($dStart->format('Y-m-d H:i:s')) . ' AND ' . $db->qn('sys_date') . ' <= ' . $db->quote($dNow->format('Y-m-d H:i:s')) );
			}
		}
		// Add the list ordering clause.
		//$orderCol	= $this->state->get('list.ordering',  'sys_date');
		//$orderDirn 	= $this->state->get('list.direction', 'asc');
		//$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		// Add the list ordering clause.
		$orderCol = $this->getState('list.ordering', 'sys_date');
		$query->order($db->escape($orderCol) . ' ' . $db->escape($this->getState('list.direction', 'DESC')));
		
		return $query;
	}
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return  string  An SQL query
	 *
	 * @since   1.6
	 */
	protected function getListQuery2()
	{
		$app = JFactory::getApplication('site');
		$user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedViewLevels());
		$pid = $this->getState('tag.parent_id');
		$orderby = $this->state->params->get('all_tags_orderby', 'title');
		$published = $this->state->params->get('published', 1);
		$orderDirection = $this->state->params->get('all_tags_orderby_direction', 'ASC');
		$language = $this->getState('tag.language');

		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);

		// Select required fields from the tags.
		$query->select('a.*')

			->from($db->quoteName('#__tags') . ' AS a')
			->where($db->quoteName('a.access') . ' IN (' . $groups . ')');

		if (!empty($pid))
		{
			$query->where($db->quoteName('a.parent_id') . ' = ' . $pid);
		}

		// Exclude the root.
		$query->where($db->quoteName('a.parent_id') . ' <> 0');

		// Optionally filter on language
		if (empty($language))
		{
			$language = JComponentHelper::getParams('com_tags')->get('tag_list_language_filter', 'all');
		}

		if ($language != 'all')
		{
			if ($language == 'current_language')
			{
				$language = JHelperContent::getCurrentLanguage();
			}

			$query->where($db->quoteName('language') . ' IN (' . $db->quote($language) . ', ' . $db->quote('*') . ')');
		}

		// List state information
		$format = $app->input->getWord('format');

		if ($format == 'feed')
		{
			$limit = $app->get('feed_limit');
		}
		else
		{
			if ($this->state->params->get('show_pagination_limit'))
			{
				$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->get('list_limit'), 'uint');
			}
			else
			{
				$limit = $this->state->params->get('maximum', 20);
			}
		}

		$this->setState('list.limit', $limit);

		$offset = $app->input->get('limitstart', 0, 'uint');
		$this->setState('list.start', $offset);

		// Optionally filter on entered value
		if ($this->state->get('list.filter'))
		{
			$query->where($db->quoteName('a.title') . ' LIKE ' . $db->quote('%' . $this->state->get('list.filter') . '%'));
		}

		$query->where($db->quoteName('a.published') . ' = ' . $published);

		$query->order($db->quoteName($orderby) . ' ' . $orderDirection . ', a.title ASC');

		return $query;
	}
}
