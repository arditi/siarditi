<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_tags
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;


/**
 * Docs Model
 */
class SIM0ModelDocs extends JModelList
{
	/**
	 * Model context string.
	 *
	 * @var    string
	 * @since  3.1
	 */
	public $_context = 'com_sim0.docs';
	public $href='0';
	
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		//$config['filter_fields']=array_merge($this->searchInFields,array('a.company'));
		//if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
						'id_doc',  'a.id_doc',
						'id_category', 'a.id_category' , 
						'name', 'a.name' , 
						'source', 'a.source', 
						'state', 'a.state', 
						'validated', 'a.validated', 
						'pub_date','a.pub_date', 'sys_date', 'a.sys_date'	
			);
		}
		parent::__construct($config);
		// Insert new LogAction
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);		
	}

	
	/**
	 * Method to Override the predifined limit.
	 * @return	string	An SQL query
	 */
	protected function populateState($ordering = NULL, $direction = NULL)	{
		$this->setState('list.limit', 0);	// Assim mostra todos, mas tem que estar depois de parent::populateState...
		
		$app = JFactory::getApplication('site');
		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout', 'default', 'cmd')){
			$this->context .= '.' . $layout;
		}
		$context = $this->context;
		
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		
        $id_category = $this->getUserStateFromRequest($this->context . '.filter.id_category', 'selCategory', null, 'int');
		$this->setState('filter.id_category', $id_category);
		
        $id_axis = $this->getUserStateFromRequest($this->context . '.filter.id_axis', 'filter_id_axis', null, 'int');
		$this->setState('filter.id_axis', $id_axis);

		$groups = json_decode(base64_decode($app->input->get('groups', '', 'BASE64')));
		if (isset($groups)){
			JArrayHelper::toInteger($groups);
		}
		$this->setState('filter.groups', $groups);

		
		//
		$formData 	= new JRegistry(JFactory::getApplication()->input->get('jform', array(), 'array')); 
		//print_r($formData);
		if($formData['id_axis']!=''){
			$id_axis = $formData['id_axis'];
			//echo "id_axis =>$id_axis";
			$this->setState('filter.id_axis', $id_axis);
		}		
		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_sim0');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('sys_date', 'asc');		
		$this->setState('list.limit', 100);	// 50 livros no máximo
	}
	/**
	 * Method to build an SQL query to load the list data.
	 * @return	string	An SQL query
	 */
	public function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('*');
		$query->from($db->quoteName('docs') . ' AS a');

		// Filter by category
		$id_axis = $this->getState('filter.id_axis');
		if (is_numeric($id_axis)){
			$query->where('a.id_axis = ' . (int) $id_axis);
		}		
		
		// Filter by category
		$id_category = $this->getState('filter.id_category');
		if (is_numeric($id_category) & $id_category!=0){
			$query->where('(a.id_category = '.$id_category.' or a.id_category IN (select id_category from doccategories where path like '.$db->quote($id_category.'|%').' or path like '.$db->quote('%|'.$id_category.'|%').') )');
		}
		
		// If the model is set to check item state, add to the query.
		$query->where('a.state = 0');
		$query->where('a.validated = 1');
		
		// Filter the items over the search string if set.
		if ($this->getState('filter.search') !== '' && $this->getState('filter.search') !== null){
			// Escape the search token.
			$search 	= $db->quote('%' . str_replace(' ', '%', $db->escape(trim($this->getState('filter.search')), true) . '%'));
			// Compile the different search clauses.
			$searches   = array();
			$searches[] = 'a.title LIKE ' . $search;
			$searches[] = 'a.resume LIKE ' . $search;
			// Add the clauses to the query.
			$query->where('(' . implode(' OR ', $searches) . ')');
		}
		
		// Add the list ordering clause.
		$query->order($db->qn($db->escape($this->getState('list.ordering', 'a.title'))) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));
		/*
		$orderCol = $this->getState('list.ordering');
		$orderDirn = $this->getState('list.direction');
		if ($orderCol != '') {
			$query->order($db->getEscaped($orderCol.' '.$orderDirn));
		}		
		$query->order('sys_date DESC');
		*/
		//JFactory::getApplication()->enqueueMessage($query);
		//die($query);
		sleep(0);
		
		return $query;
	}
	/**
	 * Redefine the function and add some properties to make the styling more easy
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
		foreach ($items as &$item) {
			$item->url = JRoute::_('index.php?option=com_sim0&amp;view=doc&amp;id_doc=' . $item->id_doc);
			$item->domain = "";
		}
		return $items;
	}	
}

?>