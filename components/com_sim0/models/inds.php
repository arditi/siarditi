<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of user records.
 *
 * @since  1.6
 */
class SIM0ModelInds extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		//$config['filter_fields']=array_merge($this->searchInFields,array('a.company'));
		//if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
						'id_indicator',  'a.id_indicator',
						'id_category', 'a.id_category' , 
						'name', 'a.name' , 
						'definition', 'a.definition', 
						'source', 'a.source', 
						'state', 'a.state', 
						'validated', 'a.validated', 
						'pub_date','a.pub_date', 'sys_date', 'a.sys_date'	
			);
		}
		parent::__construct($config);
		// Insert new LogAction
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);

		//////////////
		// Scroll List
		$app 	= JFactory::getApplication();
		$config = JFactory::getConfig();
		// Get the pagination request variables
		//$this->setState('limit', $app->getUserStateFromRequest('com_sim0.limit', 'limit', $config->get('config.list_limit'), 'int'));
		//$this->setState('limitstart', JRequest::getVar('limitstart', 0, '', 'int'));
		$limit  	= $config->get('config.list_limit');
		$limit		= $limit==0?10:$limit;
		// Adjust the context to support modal layouts.
		$app->input->set('limit', 	$limit);
		$pagination	= $this->getPagination();
		if ($pagesCurrent = $app->input->get('pCurrent', 1)){
			//$pagesCurrent++;	//esta a ser feito no JavaScript
			if( (int)$pagesCurrent > (int)$pagination->pagesStop ){
				$pagesCurrent = $pagination->pagesStart;
			}
			$pagination->pagesCurrent = $pagesCurrent;
		}
		//
		$limitstart	= $limit * ($pagination->pagesCurrent-1);
		$this->setState('list.limit', $limit);
		$this->setState('list.limitstart', $limitstart);
		$this->setState('list.start', $limitstart);	
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication('site');

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout', 'default', 'cmd')){
			$this->context .= '.' . $layout;
		}
		$context = $this->context;
		
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		
        $id_axis = $this->getUserStateFromRequest($this->context . '.filter.id_axis', 'filter_id_axis', null, 'int');
		$this->setState('filter.id_axis', $id_axis);

        $id_dimension = $this->getUserStateFromRequest($this->context . '.filter.id_dimension', 'filter_id_dimension', null, 'int');
		$this->setState('filter.id_dimension', $id_dimension);

		// Na Infinite List isto nao funciona, passa a ser um segundo pedido que nao tem esteas variaveis
		/*		
		$formData 	= new JRegistry(JFactory::getApplication()->input->get('jform', array(), 'array')); 
		if($formData['id_axis']!==''){
			$id_axis = $formData['id_axis'];
			//echo "id_axis =>$id_axis";
			$this->setState('filter.id_axis', $id_axis);
		}
		if($formData['id_dimension']!=''){
			$id_dimension = $formData['id_dimension'];
			//echo "id_axis =>$id_axis";
			$this->setState('filter.id_dimension', $id_dimension);
		}		
		//print_r($formData); die("s");
		*/
		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_sim0');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('sys_date', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{            
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.id_axis');
		$id .= ':' . $this->getState('filter.id_dimension');
		$id .= ':' . $this->getState('filter.validated');
		$id .= ':' . $this->getState('filter.state');
		$id .= ':' . $this->getState('filter.range');
		return parent::getStoreId($id);
	}

	/**
	 * Gets the list of users and adds expensive joins to the result set.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getItems()
	{
		$items = parent::getItems();
		foreach ($items as &$item) {
			$item->url = 'index.php?option=com_sim0&amp;view=ind&amp;id_indicator=' . $item->id_indicator;
		}
		//$input 		= JFactory::getApplication()->input; 
		//$formData 	= new JRegistry($input->get('jform', array(), 'array')); 
		//print_r($input);
		//print_r($formData);
		return $items;
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('*');
		//$query->select('1 as axis, 2 as dimension');
		$query->select('  (select name from axis as b where b.id_axis=a.id_axis) as axis');
		$query->select('  (select name from dimension as c where c.id_dimension=a.id_dimension) as dimension');
		$query->from($db->quoteName('indicators') . ' AS a ');

		// Filter by category
		$id_axis = $this->getState('filter.id_axis');
		if (is_numeric($id_axis) & $id_axis!=0){
			$query->where('a.id_axis = ' . (int) $id_axis);
		}		
		//$query->where('a.name LIKE=' . $db->quote('%'.$id_axis.'%') );
		
		$id_dimension = $this->getState('filter.id_dimension');
		if (is_numeric($id_dimension)& $id_dimension!=0){
			$query->where('a.id_dimension = ' . (int) $id_dimension);
		}		
		
		// If the model is set to check item state, add to the query.
		$query->where('a.state = 0');
		$query->where('a.validated = 1');
		
		// Filter the items over the search string if set.
		if ($this->getState('filter.search') !== '' && $this->getState('filter.search') !== null){
			// Escape the search token.
			$search 	= $db->quote('%' . str_replace(' ', '%', $db->escape(trim($this->getState('filter.search')), true) . '%'));
			// Compile the different search clauses.
			$searches   = array();
			$searches[] = 'a.name LIKE ' . $search;
			$searches[] = 'a.definition LIKE ' . $search;
			// Add the clauses to the query.
			$query->where('(' . implode(' OR ', $searches) . ')');
		}
		
		// Add filter for registration ranges select list
		$range = $this->getState('filter.range');
		if ($range){
			// Get UTC for now.
			$dNow   = new JDate;
			$dStart = clone $dNow;
			switch ($range)
			{
				case 'past_week':
					$dStart->modify('-7 day');
					break;
				case 'past_1month':
					$dStart->modify('-1 month');
					break;
				case 'past_3month':
					$dStart->modify('-3 month');
					break;
				case 'past_6month':
					$dStart->modify('-6 month');
					break;
				case 'post_year':
				case 'past_year':
					$dStart->modify('-1 year');
					break;
				case 'today':
					// Ranges that need to align with local 'days' need special treatment.
					$app    = JFactory::getApplication();
					$offset = $app->get('offset');
					// Reset the start time to be the beginning of today, local time.
					$dStart = new JDate('now', $offset);
					$dStart->setTime(0, 0, 0);
					// Now change the timezone back to UTC.
					$tz = new DateTimeZone('GMT');
					$dStart->setTimezone($tz);
					break;
			}
			if ($range == 'post_year'){
				$query->where($db->qn('a.sys_date') . ' < ' . $db->quote($dStart->format('Y-m-d H:i:s')));
			}
			else{
				$query->where($db->qn('a.sys_date') . ' >= ' . $db->quote($dStart->format('Y-m-d H:i:s')).' AND '.$db->qn('a.sys_date') . ' <= ' . $db->quote($dNow->format('Y-m-d H:i:s')));
			}
		}
		// Add the list ordering clause.
		$query->order($db->qn($db->escape($this->getState('list.ordering', 'name'))) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));

		$this->qry = $query;
		//JFactory::getApplication()->enqueueMessage($query);
		return $query;
	}
	// to store the last sql query used...
	public $qry="";

}
