<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.database.table' );

/**
 * Log Model
 *
 * @since  0.0.1
 */
class SIM0ModelInd extends JModelItem 
{
	/**
	 * Constructor
	 */
	public function __construct($config = array()){
		parent::__construct($config);
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	/**
	 *
	 *
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication('site');
		// Load state from the request.
		$pid = $app->input->getInt('id_indicator');
		$this->setState('indicator.id_indicator', $pid);
		/*
		$input = JFactory::getApplication()->input;
		$hitcount = $input->getInt('hitcount', 1);
		if ($hitcount)
		{
			$pk = (!empty($pk)) ? $pk : (int) $this->getState('article.id');
			$table = JTable::getInstance('Content', 'JTable');
			$table->load($pk);
			$table->hit($pk);
		}
		*/
	}		
	/**
	 * Get the message
	 * @return object The message to be displayed to the user
	 */
	public function getItem($id=null)
	{
		if (!isset($this->item)) 
		{
			if (empty($id)){
				$id = $this->getState('indicator.id_indicator');
			}			
			$db    = $this->getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->select('  (select name from axis as b where b.id_axis=a.id_axis) as axis');
			$query->select('  (select name from dimension as c where c.id_dimension=a.id_dimension) as dimension');
			$query->from($db->quoteName('indicators') . ' AS a ');
			$query->where('a.id_indicator='.(int)$id);
			$query->setLimit(1,0);
			$db->setQuery((string)$query);
			if ($this->item = $db->loadObject()) 
			{/*
				//EJ
				// Load the parameters.
				if(!isset($this->item->params))
					$this->item->params = JFactory::getApplication()->getParams();
				
				// Load the JSON string
				$params = new JRegistry;
				$params->loadString($this->item->params, 'JSON');
				$this->item->params = $params;

				// Merge global params with item params
				$params = clone $this->getState('params');
				$params->merge($this->item->params);
				$this->item->params = $params;
				*/
			}
		}
		//JFactory::getApplication()->enqueueMessage($query);
		
		return $this->item;
	}
	/**
	 * Method to get an object.
	 *
	 * @param   integer	The id of the object to get.
	 *
	 * @return  mixed  Object on success, false on failure.
	 */
	public function getItem2($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id))
			{
				$id = $this->getState('weblink.id');
			}

			// Get a level row instance.
			$table = JTable::getInstance('indicator', 'Table');

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published)
					{
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties = $table->getProperties(1);
				$this->_item = JArrayHelper::toObject($properties, 'JObject');
			}
			elseif ($error = $table->getError())
			{
				$this->setError($error);
			}
		}
		
		return $this->_item;
	}
	
	/**
	 * Method to get the regios of a indicator or all
	 *
	 * @return  array  The field option objects.
	 */
	public function getRegions($id_indicator="")
	{
		$options= array();
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query  = $db->getQuery(true)
				->select('distinct id_region')
				->select('(select name from regions as b WHERE b.id_region=a.id_region) as region')
				->from  ('indicator_value as a')
				->order ('year ASC');
		if($id_indicator!="")
			$query->where ('id_indicator='.$id_indicator);

		$db->setQuery($query);
		if ($options = $db->loadObjectList())
		{
			foreach ($options as &$option)
			{
				//$options[] = JHtml::_('select.option', $option->value, $option->text);
			}
		}
		return $options;
	}
	
	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 */	
	public function getRegionValues($id_indicator, $id_region)
	{
		//$step 		= $input->getCmd('step', '');
		//$formData		= new JRegistry($input->get('jform', array(), 'array'));		
		//$id_template	= $formData['id_template'];
		$options= array();
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query  = $db->getQuery(true)
			->select('id_value, year, real_value, estimated_value')
			->from  ('indicator_value')
			->where ('id_indicator='.$id_indicator)
			->where ('id_region='.$id_region)
			->order ('year ASC');
		$db->setQuery($query);
		$this->minYear=0;
		$this->maxYear=0;
		if ($data = $db->loadObjectList())
		{
			//foreach ($options as &$option)
			foreach($data as $key => $val)
			{
				if($val->year < $this->minYear) $this->minYear = $val->year;
				if($val->year > $this->maxYear) $this->maxYear = $val->year;
			}
		}
		return $data;
	}

	///
	public function addRegionValue($id_indicator, $id_region, $year, $real, $est){
		$id_value=0;
		try{// Create a new query object.
			$db 	= $this->getDbo();
			$query 	= $db->getQuery(true);
			//INSERT INTO `indicator_value` (`id_value`, `year`, `real_value`, `estimated_value`, `obs`, `id_indicator`, `id_region`) VALUES (NULL, '2010', '32', '33', NULL, '1', '2');
			$query->insert('indicator_value')
				->columns($db->quoteName(array('id_indicator','id_region','year','real_value','estimated_value')))
				->values ((int)$id_indicator.','.(int)$id_region.','.(int)$year.','.(int)$real.','.(int)$est);
			//die("query=".$query);
			$db->setQuery($query);
			$db->execute();
			$id_value = $db->insertid();	// id_not inserido
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		return $id_value;
	}

	/**
	 * Method to save the form data.
	 * @return  boolean  True on success.
	 */
	public function getItemByTemplate($id_template=0)
	{
		$id_tp=0;
		try{
			// Create a new query object.
			$db 	= $this->getDbo();
			$query 	= $db->getQuery(true);
			// Select the required fields from the table.
			$query->select('id_nt');
			$query->from($db->quoteName('notificationtemplates') );
			$query->where('id_template=' . (int) $id_template);
			$db->setQuery($query);
			$db->execute();
			$id_tp = $db->loadResult();
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		// Load all notificationtemplate row
		$row = $this->getTable();
		$row->load($id_tp);
		return $row;
	}
	
	
}
