<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * SIM1 component helper.
 *
 * @param   string  $submenu  The name of the active view.
 *
 * @return  void
 *
 * @since   1.6
 */
abstract class SIM1Helper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($submenu) 
	{
		JSubMenuHelper::addEntry(
			JText::_('com_sim1_SUBMENU_INVESTIGATORS'),
			'index.php?option=com_sim1&view=investigators',
			$submenu == 'investigators'
		);

		JSubMenuHelper::addEntry(
			JText::_('com_sim1_SUBMENU_IUS'),
			'index.php?option=com_sim1&view=ius',
			$submenu == 'ius'
		);

		// set some global property
		$document = JFactory::getDocument();
		$document->addStyleDeclaration('.icon-48-helloworld ' . '{background-image: url(../media/com_sim1/images/tux-48x48.png);}');
		if ($submenu == 'categories') 
		{
			$document->setTitle(JText::_('com_sim1_ADMINISTRATION_CATEGORIES'));
		}
	}
}
