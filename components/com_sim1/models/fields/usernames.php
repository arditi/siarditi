 <?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
 defined('_JEXEC') or die('Restricted access');
 
 // Load modal behavior
JHtml::_('behavior.modal','a.modal');

 jimport('joomla.form.formfield');
 
 /**
  * Book form field class
  */
 class JFormFieldUsernames extends JFormField
 {
     
        /**
           * Method to get the field input markup
           */
          protected function getInput()
          {
                // Setup variables for display
                $users = array();
                $html = '<select id="'.$this->id.'" name="'.$this->name.'">';

              // Create a new query object.
//                if ($this->value) {
                    $dbJ = JFactory::getDbo();
                    $query = $dbJ->getQuery(true);
                    $query->select('*')
                        ->from($dbJ->quoteName('#__users'));
                    $dbJ->setQuery($query);
                    $users = $dbJ->loadObjectList();
 
                    if (!$users) {
                            JError::raiseWarning(500, $db->getErrorMsg());
                    }
//                }
                
                foreach ($users as $i => $row) {
                      $html .= '<option value="'.$row->id.'" '.(($row->id === $this->value)? "selected" : "").'>'.$row->username.'</option>';
                }
                
                $html .=  '</select>';
                
                return $html;
          }
      
 }
