 <?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
 defined('_JEXEC') or die('Restricted access');
 
 // Load modal behavior
JHtml::_('behavior.modal','a.modal');

 jimport('joomla.form.formfield');
 
 /**
  * Book form field class
  */
 class JFormFieldModal_Project extends JFormField
 {
     
        /**
           * Method to get the field input markup
           */
          protected function getInput()
          {
                // Load modal behavior
                JHtml::_('behavior.modal', 'a.modal');

                // Setup variables for display
                $projs = array();
                $html = array();
                $html[] = '<div id="control-projects">';
                
                if ($this->value) {
                    // Create a new query object.
                    $sys = &SysARDITI::getInstance();
                    $db = &$sys->getDBO();
                    $query = $db->getQuery(true);
                    $query->select('*')
                        ->from($db->quoteName('projects'))
                        ->join('LEFT', 'curricula_projects AS cv_proj ON cv_proj.id_project=projects.id_project')
                        ->where($db->quoteName('cv_proj.id_cv') . ' = '.$this->value);
                    $db->setQuery($query);
                    $projs = $db->loadObjectList();
 
                    if (!$projs) {
                            JError::raiseWarning(500, $db->getErrorMsg());
                    }
                }        
                
                $script = array();
                $k=0;
                $script[] = 'var sequence=0;';
                $script[] = 'function registerFunction(functionBody) { '
                              . '"use strict"; '
                              . 'var script = document.createElement("script"); '
                              . 'script.innerHTML = "function " + functionBody; '
                              . 'document.head.appendChild(script);}';
                if (!$projs) {
                      $html[] = '  <div id="projNew" class="control-label btn-wrapper">';
                      $html[] = '      <a class="btn btn-small"  onclick="sequence=-1; projAdd();"><span class="icon-new icon-white"></span>New</a>';
                      $html[] = '  </div>';
                }

                foreach ($projs as $i => $row) {
                      $proj = new Project($row->id_project);

                      // Build the script
                      $script[] = 'registerFunction("projOk_'.$i.'(name, description, obs, url, begin_date, end_date, id_funding, id_project_state){ document.id(\"jform_projects_'.$i.'_name\").value = name; document.id(\"jform_projects_'.$i.'_description\").value = description; document.id(\"jform_projects_'.$i.'_obs\").value = obs; document.id(\"jform_projects_'.$i.'_url\").value = url; document.id(\"jform_projects_'.$i.'_begin_date\").value = begin_date; document.id(\"jform_projects_'.$i.'_end_date\").value = end_date; document.id(\"jform_projects_'.$i.'_id_funding\").value = id_funding; document.id(\"jform_projects_'.$i.'_id_project_state\").value = id_project_state; SqueezeBox.close(); }");';
                      $script[] = 'sequence='.$i.';';

                      $link = 'index.php?option=com_sim1&amp;view=investigator&amp;layout=modal_proj&amp;tmpl=component&amp;id_project='.$row->id_project.'&amp;function=projOk_'.$i;

                      $html[] = '<div id="group_'.$i.'_proj_name" class="control-group">';
                      $html[] = '  <div class="control-label">'.JText::_('COM_SIM1_INVESTIGATOR_PROJECT_NAME_LABEL').'</div>';
                      $html[] = '  <div class="controls">';
                      $html[] = '     <input readonly type="text" id="jform_projects_'.$i.'_name" name="jform[projects]['.$i.'][name]" value="'.$proj->name.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_id_project" name="jform[projects]['.$i.'][id_project]" value="'.$proj->id_project.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_id_cv" name="jform[projects]['.$i.'][id_cv]" value="'.$row->id_cv.'" size="35" />';
                      $html[] = '  </div>';
                      $html[] = '</div>';

                      $html[] = '<div id="group_'.$i.'_proj_description" class="control-group">';
                      $html[] = '  <div class="control-label">'.JText::_('COM_SIM1_INVESTIGATOR_PROJECT_DESCRIPTION_LABEL').'</div>';
                      $html[] = '  <div class="controls">';
                      $html[] = '     <input readonly type="text" id="jform_projects_'.$i.'_description" name="jform[projects]['.$i.'][description]" value="'.$proj->description.'" size="80" />';
                      $html[] = '  </div>';
                      $html[] = '  <div class="control-label btn-wrapper">';
                      $html[] = '      <a id="edit-modal" class="btn btn-small modal"  onclick="SqueezeBox.setContent(\'iframe\',\''.$link.'\'); "><span class="icon-edit"></span>Edit</a>';
                      $html[] = '      <a class="btn btn-small"  onclick="projAdd();"><span class="icon-new icon-white"></span>New</a>';
                      $html[] = '      <a class="btn btn-small"  onclick="projDel('.$i.');"><span class="icon-delete"></span>Del</a>';
                      $html[] = '  </div>';
                      $html[] = '</div>';

                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_url" name="jform[projects]['.$i.'][url]" value="'.$proj->url.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_begin_date" name="jform[projects]['.$i.'][begin_date]" value="'.$proj->begin_date.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_end_date" name="jform[projects]['.$i.'][end_date]" value="'.$proj->end_date.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_id_funding" name="jform[projects]['.$i.'][id_funding]" value="'.$proj->id_funding.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_id_project_state" name="jform[projects]['.$i.'][id_project_state]" value="'.$proj->id_project_state.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_obs" name="jform[projects]['.$i.'][obs]" value="'.$proj->obs.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_del_id_project" name="jform[projects]['.$i.'][del_id_project]" value="" />';
                      $html[] = '     <input type="hidden" id="jform_projects_'.$i.'_del_id_cv" name="jform[projects]['.$i.'][del_id_cv]" value="" />';

                      $k=$i;

                    // Add to document head
                    //JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                }
                $html[] = '</div>';
                $html[] = '<div id="control-new-projects">';
                $html[] = '</div>';

                $link_new = 'index.php?option=com_sim1&amp;view=investigator&amp;layout=modal_proj&amp;tmpl=component&amp;function=projOk_';

                $script[] = 'function projAdd() {';
                $script[] = '      sequence++;';
                $script[] = '      var newProj = document.getElementById("control-new-projects");';
                $script[] = '      var newDiv = document.createElement("div");';
                $script[] = '      var projNew = document.getElementById("projNew");';
                $script[] = '      newProj.appendChild(newDiv);';
                $script[] = '      if (projNew) projNew.parentNode.removeChild(projNew);';
                $script[] = '      newDiv.innerHTML = '
                            . '"<div id=\"group_"+sequence+"_proj_name\" class=\"control-group\">'
                            . '  <div class=\"control-label\">'.JText::_('COM_SIM1_INVESTIGATOR_PROJECT_NAME_LABEL').'</div>'
                            . '  <div class=\"controls\">'
                            . '     <input readonly type=\"text\" id=\"jform_projects_"+sequence+"_name\" name=\"jform[projects]["+sequence+"][name]\" size=\"35\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_id_project\" name=\"jform[projects]["+sequence+"][id_project]\"  size=\"35\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_id_cv\" name=\"jform[projects]["+sequence+"][id_cv]\"  size=\"35\" />'
                            . '  </div>'
                            . '</div>'

                            . '<div id=\"group_"+sequence+"_proj_description\" class=\"control-group\">'
                            . '  <div class=\"control-label\">'.JText::_('COM_SIM1_INVESTIGATOR_PROJECT_DESCRIPTION_LABEL').'</div>'
                            . '  <div class=\"controls\">'
                            . '     <input readonly type=\"text\" id=\"jform_projects_"+sequence+"_description\" name=\"jform[projects]["+sequence+"][description]\"  size=\"35\" />'
                            . '  </div>'
                            . '  <div class=\"control-label btn-wrapper\">'
                            . '     <a class=\"btn btn-small modal\" onclick=\"SqueezeBox.setContent(\'iframe\',\''.$link_new.'"+sequence+"\');\"><span class=\"icon-edit\"></span>Edit</a>'
                            . '     <a class=\"btn btn-small\"  onclick=\"projAdd();\"><span class=\"icon-new icon-white\"></span>New</a>'
                            . '     <a class=\"btn btn-small\"  onclick=\"projDel("+sequence+");\"><span class=\"icon-delete\"></span>Del</a>'
                            . '  </div>'
                            . '</div>'

                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_url\" name=\"jform[projects]["+sequence+"][url]\" size=\"35\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_begin_date\" name=\"jform[projects]["+sequence+"][begin_date]\" size=\"35\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_end_date\" name=\"jform[projects]["+sequence+"][end_date]\" size=\"35\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_id_funding\" name=\"jform[projects]["+sequence+"][id_funding]\" size=\"35\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_id_project_state\" name=\"jform[projects]["+sequence+"][id_project_state]\" size=\"35\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_obs\" name=\"jform[projects]["+sequence+"][obs]\" size=\"35\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_del_id_project\" name=\"jform[projects]["+sequence+"][del_id_project]\" value=\"\" />'
                            . '     <input type=\"hidden\" id=\"jform_projects_"+sequence+"_del_id_cv\" name=\"jform[projects]["+sequence+"][del_id_cv]\" value=\"\" />'
                            . '"';

                $script[] = '      registerFunction("projOk_"+sequence+"(name, description, obs, url, begin_date, end_date, id_funding, id_project_state){ document.id(\"jform_projects_"+sequence+"_name\").value = name; document.id(\"jform_projects_"+sequence+"_description\").value = description; document.id(\"jform_projects_"+sequence+"_obs\").value = obs; document.id(\"jform_projects_"+sequence+"_url\").value = url; document.id(\"jform_projects_"+sequence+"_begin_date\").value = begin_date; document.id(\"jform_projects_"+sequence+"_end_date\").value = end_date; document.id(\"jform_projects_"+sequence+"_id_funding\").value = id_funding; document.id(\"jform_projects_"+sequence+"_id_project_state\").value = id_project_state; SqueezeBox.close(); }");';
                $script[] = '}';
              
                $script[] = 'function projDel(projIndex) {'
                            . '      document.getElementById("jform_projects_"+projIndex+"_del_id_project").setAttribute("value",document.getElementById("jform_projects_"+projIndex+"_id_project").value);'
                            . '      document.getElementById("jform_projects_"+projIndex+"_del_id_cv").setAttribute("value",document.getElementById("jform_projects_"+projIndex+"_id_cv").value);'
                            . '      var group_name = document.getElementById("group_"+projIndex+"_proj_name");'
                            . '      var group_description = document.getElementById("group_"+projIndex+"_proj_description");'
                            . '      var field_url = document.getElementById("jform_projects_"+projIndex+"_url");'
                            . '      var field_begin_date = document.getElementById("jform_projects_"+projIndex+"_begin_date");'
                            . '      var field_end_date = document.getElementById("jform_projects_"+projIndex+"_end_date");'
                            . '      var field_id_funding = document.getElementById("jform_projects_"+projIndex+"_id_funding");'
                            . '      var field_id_project_state = document.getElementById("jform_projects_"+projIndex+"_id_project_state");'
                            . '      var field_obs = document.getElementById("jform_projects_"+projIndex+"_obs");'
                            . '      group_name.parentNode.removeChild(group_name);'
                            . '      group_description.parentNode.removeChild(group_description);'
                            . '      field_url.parentNode.removeChild(field_url);'
                            . '      field_begin_date.parentNode.removeChild(field_begin_date);'
                            . '      field_end_date.parentNode.removeChild(field_end_date);'
                            . '      field_id_funding.parentNode.removeChild(field_id_funding);'
                            . '      field_id_project_state.parentNode.removeChild(field_id_project_state);'
                            . '      field_obs.parentNode.removeChild(field_obs);'
                            . '}';

                // Add to document head
                JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                
                return implode("\n", $html);
          }
      
 }
