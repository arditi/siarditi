<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

$function = JFactory::getApplication()->input->getCmd('function');
$id_project = JFactory::getApplication()->input->getCmd('id_project');
if (isset($id_project) && $id_project>0)
    $project = new Project($id_project);
?>
<div class="row-fluid" style="width:500">
<form action="<?php echo $this->action; ?>" method="post" name="modalForm" id="modalForm">
    <div class="control-group"><?php echo $this->escape($function)?>
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PROJECT_NAME_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="name" name="name" value="<?php echo ((isset($project) && $id_project>0)? $project->name : '') ?>" size="35" />'
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PROJECT_DESCRIPTION_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="description" name="description" value="<?php echo ((isset($project) && $id_project>0)? $project->description : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PROJECT_URL_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="url" name="url" value="<?php echo ((isset($project) && $id_project>0)? $project->url : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PROJECT_BEGIN_DATE_LABEL') ?></div>
       <div class="controls">
            <input type="date" id="begin_date" name="begin_date" value="<?php echo ((isset($project) && $id_project>0)? $project->begin_date : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PROJECT_END_DATE_LABEL') ?></div>
       <div class="controls">
            <input type="date" id="end_date" name="end_date" value="<?php echo ((isset($project) && $id_project>0)? $project->end_date : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PROJECT_STATE_LABEL') ?></div>
       <div class="controls">
           <select id="id_project_state" name="id_project_state" />
            <option value="1" <?php echo ($project->id_project_state==0)? 'selected' : ''; ?>>Iniciado</option>
            <option value="2" <?php echo ($project->id_project_state==1)? 'selected' : ''; ?>>Concluído</option>
          </select>
       </div>
     </div>    

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PROJECT_ENTITY_LABEL') ?></div>
       <div class="controls">
           <select multiple id="id_funding" name="id_funding" size="5" />
            <option value="1" <?php echo ($project->id_funding==0)? 'selected' : ''; ?>>Fundo 0</option>
            <option value="2" <?php echo ($project->id_funding==1)? 'selected' : ''; ?>>Fundo 1</option>
          </select>
       </div>
     </div>    

    <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PROJECT_OBS_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="obs" name="obs" value="<?php echo ((isset($project) && $id_project>0)? $project->obs : '') ?>" size="35" />
       </div>
     </div>    
    
    <div>
        <button class="btn btn-small"
                onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>(
                            document.id('name').value,
                            document.id('description').value,
                            document.id('obs').value,                            document.id('url').value,
                            document.id('begin_date').value,
                            document.id('end_date').value,
                            document.id('id_funding').value,
                            document.id('id_project_state').value);">OK</button>
        <input type="hidden" id="id_project" name="id_project" value="<?php echo ((isset($project) && $id_project>0)? $id_project: '') ?>" size="35" />
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>

</form>
</div>
