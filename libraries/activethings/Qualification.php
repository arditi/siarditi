<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class Qualification extends JObject
{
    var $id_cv;
    var $id_qualification;
    var $name;
    var $obs;
    var $certification;
    var $certification_entity;
    var $certificate;
    var $date;
    var $degree;
    var $grade;
    
    public function Qualification($id) {
        if(isset($id)) {
            $this->getMydata($id);
        }
        $this->populateDbData();
    }
    
    public function getMydata($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('qualifications'))
        ->join('LEFT', 'curricula_qualifications AS cv_qual ON cv_qual.id_qualification=qualifications.id_qualification')
        ->where($db->quoteName('qualifications.id_qualification') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }
    
    private function populateDbData()
    {
        $this->id_cv = $this->MydbObj->id_cv;
        $this->id_qualification = $this->MydbObj->id_qualification;
        $this->name = $this->MydbObj->name;
        $this->obs = $this->MydbObj->obs;
        $this->certification = $this->MydbObj->certification;
        $this->certification_entity = $this->MydbObj->certification_entity;
        $this->certificate = $this->MydbObj->certificate;
        $this->date = $this->MydbObj->date;
        $this->degree = $this->MydbObj->degree;
        $this->grade = $this->MydbObj->grade;   
    }
}
