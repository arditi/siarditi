<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * Class com a API gen�rica da plataforma SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;
 
// Static SIARDITI Configuration Include
require_once JPATH_ROOT.'/configuration_arditi.php';
require_once JPATH_ROOT.'/libraries/activethings/Person.php';
require_once JPATH_ROOT.'/libraries/activethings/Curriculum.php';
require_once JPATH_ROOT.'/libraries/activethings/Qualification.php';
require_once JPATH_ROOT.'/libraries/activethings/Project.php';
require_once JPATH_ROOT.'/libraries/activethings/Entity.php';
require_once JPATH_ROOT.'/libraries/activethings/EntityBond.php';
require_once JPATH_ROOT.'/libraries/activethings/PersonInEntity.php';
<<<<<<< HEAD
require_once JPATH_ROOT.'/libraries/activethings/Service.php';
require_once JPATH_ROOT.'/libraries/activethings/Equipment.php';
=======
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
require_once JPATH_ROOT.'/libraries/activethings/SysM0.php';
require_once JPATH_ROOT.'/libraries/activethings/SysM1.php';
require_once JPATH_ROOT.'/libraries/activethings/SysM2.php';
require_once JPATH_ROOT.'/libraries/activethings/SysM3.php';
require_once JPATH_ROOT.'/libraries/activethings/SysM4.php';
// Log API
jimport('joomla.log.log');

/**
 * SysARDITI Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @see         http://www.php.net/manual/en/function.session-set-save-handler.php
 * @todo        When dropping compatibility with PHP 5.3 use the SessionHandlerInterface and the SessionHandler class
 * @since       11.1
 */
class SysARDITI extends JObject
{
	/**
	 * @var    array  SysARDITI instances container.
	 */
	//protected static $instances = array();
	// Only one instance per session
	protected static $instance 	= null;
	
	// Static configs
	protected $configs = null;

	/**
	 * Constructor
	 * @param   array  $options  Optional parameters.
	 * @since   11.1
	 */
	public function __construct($options = array()){
		// Inicializa��es
		self::$instance = null; //self::$instances	= array();
		// Format Logs
		$this->_formatLogger(defined('JDEBUG'));
		// Load Static SIARDITI Configs
		$this->configs	= new JConfigAT;
		// Main Objects
		$app			= JFactory::getApplication();
		$templateparams	= $app->getTemplate(true)->params;
		$doc			= JFactory::getDocument();
		$session 		= JFactory::getSession();
		$db 			= $this->getDBO();
		$user 			= JFactory::getUser();
		$id_joomla		= isset($user) ? $user->id : 0;	//$this->getJoomlaID();
		
		return;
		/*
		AINDA NAO ESTAMOS A USAR
		////
		// Se est� autenticado
		if(!$user->guest){
			// Carrega as variaveis de sess�o ou directamete da DB
			$this->mccn_user 		= &$this->get('mccn_user');
			$this->mccn_operator 	= &$this->get('mccn_operator');
			$this->carCharge	 	= unserialize($this->get("carCharge"));
			
			if(!isset($this->mccn_user)){		//if($this->mccn_user==null ){
				// Carrega os dados deste user
				try{
					/*
					$db->setQuery('SELECT * from mccn_user WHERE id_joomla='.$id_joomla.' LIMIT 1 OFFSET 0');
					
					$rows = &$db->loadObjectList();
					// Check for a database error.
					if ($db->getErrorNum()){	// Aqui deveria enviar email... registar noutra DB, etc!//	throw new Exception($db->getErrorMsg(), 500);
						SysARDITI::log('SysARDITI::__construct 1 ->'.$db->getErrorMsg());			
					}
					$this->mccn_user = (isset($rows[0])) ? $rows[0] : null;
					// Guarda esta row em Sess�o
					$this->set("mccn_user", $this->mccn_user);
					
					// Tenta ver se � operator
					$db->setQuery('SELECT * from mccn_operator WHERE id_joomla='.$id_joomla.' LIMIT 1 OFFSET 0');
					$rows = &$db->loadObjectList();
					$this->mccn_operator = (isset($rows[0])) ? $rows[0] : null;
					// Guarda esta row em Sess�o
					$this->set("mccn_operator", $this->mccn_operator);
					
					// Cria o objecto 
					$this->carCharge	= new CarCharge();
					$this->set("carCharge", serialize($this->carCharge));
					* /
					
				}catch (RuntimeException $e){
					SysARDITI::log("OnEveryRequest Exception=".$e->getMessage());
				}
			}
			// Aqui tem que ser um utilizador v�lido se n�o, pode ser falta de acesso a db ou user masl configurado!!! e n�o faz mais nada...
			if(isset($this->mccn_user)){
				$this->id_user 	= $this->getUserID();
			}else{
				// Se esta logado no joomla e nao � um user do conetor valido...pode ser dificuldades a chegar � db do mccn
				SysARDITI::log("DB Initialization error. Please contact MagnumCap...");
				JError::raiseError(500, "DB Initialization error. Please contact MagnumCap...");
			}
			// Testa de se tem acesso ao webservice, e tras ja alguma informa��o JSON
			$response = 1;
			// Aqui tem que ser um utilizador v�lido se n�o, pode ser falta de acesso a db ou user masl configurado!!! e n�o faz mais nada...
			if(!isset($response)){
				// Se esta logado no joomla e nao � um user do conetor valido...pode ser dificuldades a chegar � db do mccn
				SysARDITI::log("REST Initialization error. Please contact MagnumCap...");
				JError::raiseError(500, "REST Initialization error. Please contact MagnumCap...");
			}
		}
		
		// Identify user // Assim vai buscar sempre o user
		$user 				= &JFactory::getUser();
		$id_joomla 			= $user->id;
		$this->isGuest 		= $user->guest;
		$this->isRoot 		= false;
		$this->isOperator	= false;
		$this->isSupplier	= false;
		// VER ESTA FORMA DE FAZER
		foreach ($user->groups as $key => $value){
			if($key==GROUP_SUPER_ADMIN)		$this->isRoot 		= true;
			if($key==GROUP_OPERATOR)		$this->isOperator 	= true;
			if($key==GROUP_ENERGYSUPPLIER)	$this->isSupplier 	= true;
		}
		// Se tem entrada da tabela de Operators com este id_user
		if(isset($this->mccn_operator)){
			$this->isOperator	= true;
		}
		else{
			// inicia o objecto representativo da carga...
			//$this->carCharge->SetUser($this->getUserID());
		}
		// para gerar dados
		//$this->GenerateDataToMonth($this->id_user);
		*/
    }
  	
	/**
	 * OnEveryRequestStart
	 * 
	 * @return void
	 */
	public function OnEveryRequestStart(){
		// Registamos a navega��o deste user
		// No BO tamb�m � necess�rio, pois temos v�rios perfis!
		$app = JFactory::getApplication();
		if($app->isAdmin()){
			//return;
		}
		// Log Navigation
		//SysM0::SetLogAction(LogActionTypes::NAVIGATE);
	}
	
	/**
	 * OnEveryRequestStop
	 * 
	 * @return void
	 */
	public function OnEveryRequestStop(){
		// Registamos o fim da navega��o
		//SysM0::SetLogAction(LogActionTypes::PAGE_END);		
	}
	
	/**
	 * Method to set the indexer state.
	 * @param   object  $data  A new indexer state object.
	 * @return  boolean  True on success, false on failure.
	 */
	public static function _set($key, $data){
		$session = &JFactory::getSession();
		$session->set('SysARDITI.'.$key, $data);
		return true;
	}
	/**
	 * Method to set the indexer state.
	 * @param   object  $data  A new indexer state object.
	 * @return  boolean  True on success, false on failure.
	 */
	public static function &_get($key){
		$session = &JFactory::getSession();
		$obj 	 = &$session->get('SysARDITI.'.$key);
		return $obj;
	}
	/**
	 * Returns the global SysARDITI object, only creating it if it doesn't already exist.
	 * @see http://joomlacode.org/gf/project/joomla/tracker/?action=TrackerItemEdit&tracker_item_id=27368
	 * @return  SysARDITI A SysARDITI object.
	 * @since   11.1
	 */
	public static function &getInstance(){
		//if (!is_object(self::$instance))
		if (!isset(self::$instance)){
			self::$instance = new SysARDITI();
		}
		return self::$instance;
	}
	/**
	 * Returns the global ARDITI Configuratios
	 * @see http://joomlacode.org/gf/project/joomla/tracker/?action=TrackerItemEdit&tracker_item_id=27368
	 * @return  ARDITI configurations
	 * @since   11.1
	 */
	public function &getConfig(){
		return $this->configs;
	}
	/**
	 * Include files
	 * @return boolean Returns true if the file was loaded
	 * @since   11.1
	 */
	public static function loadHelper( $hlp = null, $use_include=true){
		// Clean the file name
		$file = preg_replace('/[^A-Z0-9_\.-]/i', '', $hlp);
		// load the template script
		//jimport('joomla.filesystem.path');
		//$helper = JPath::find($this->_path['helper'], $this->_createFileName('helper', array('name' => $file)));
		
		//http://docs.joomla.org/Constants
		$helper = JPATH_BASE . '/components/com_conetor/helpers/'.$file.'.php';
		// Se encontrou o path
		if ($helper != false){
			// include the requested template filename in the local scope
			if($use_include)	include_once $helper;
			else				require_once $helper;
		}
		else return false;
	}

	/**
	 * validateClientSession 
	 * @return boolean
	 */
	public function validateClientSession(){
		// Check for a valid session cookie	
		if(JFactory::getSession()->getState() != 'active'){
			//JError::raiseWarning(403, JText::_('COM_CONTACT_SESSION_INVALID'));
			// Save the data in the session.
			//$app->setUserState('com_contact.contact.data', $data);
			// Redirect back to the contact form.
			//$this->setRedirect(JRoute::_('index.php?option=com_contact&view=contact&id='.$stub, false));
			return false;
		}
		return true;
	}
	/**
	 * Returns reference to first database connection, and if connection does not exist will create it
	 * @static var JDatabase $dbo1
	 * @return JDatabase
	 */
	public function &getDBO() {
		static $dbo1 = null;
		if (!$dbo1) {
			$sysCfg = &$this->getConfig();
			$option = $sysCfg->getOptions();
			$dbo1 	= JDatabase::getInstance($option);
		}
		return $dbo1;
	}
	/**
	 * Returns reference to a JQuery object
	 * @return JQuery
	 */
	public static function &getQuery(/*boolean*/ $toArray = false) {
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		return $db->getQuery($toArray);
	}
	/**
	 * Returns getSqlResult of a sql single value 
	 * @return  object  The result.
	 */	
	public static function getSqlResult($sql){
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();	//$db 	= &SysARDITI::getDBO();//$db 	= JFactory::getDbo();
		$db->setQuery($sql);
		$result = &$db->loadResult();
		// Check for a database error.
		if ($db->getErrorNum()){
			// Aqui deveria gravar LogAction, enviar email... registar noutra DB, etc!
			SysARDITI::log('SysARDITI::getSqlResult('.$sql.')->'.$db->getErrorMsg());		
			return false;
		}
		return $result;
	}

	/**
	 * Returns setSqlQuery of a sql to update or insert 
	 * @return  object  The result.
	 */	
	public static function setSqlQuery($sql){
//$result = $sys->setSqlQuery("SELECT id_user FROM si_user WHERE id_id_joomla={$id}");
//$db = &SysARDITI::getDBO();
//$db->setQuery("SELECT id_user FROM mccn_user WHERE id_id_joomla={$id}");
//$result = $db->loadResult();	 
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();	//$db 	= &SysARDITI::getDBO();//$db 	= JFactory::getDbo();
		$db->setQuery($sql);
		$db->query();
		// Check for a database error.
		if ($db->getErrorNum()){
			// Aqui deveria gravar LogAction, enviar email... registar noutra DB, etc!
			// throw new Exception($db->getErrorMsg(), 500);
			SysARDITI::log('SysARDITI::setSqlResult('.$sql.')->'.$db->getErrorMsg());
			return false;
		}
		// Check new inserted ID
		$id_insert = $db->insertid();
		//print_r($row);echo ("\n<BR>".$sql.'->'.$id_insert);
		return $id_insert;
	}
	
	

	/**
	 * isGuest.
	 * @return  boolean  True if is Guest, false on failure.
	 */
	public function isGuest(){
		$user = JFactory::getUser();
		return isset($user) ? $user->guest : true;
	}
	public function isRoot(){
		$user = JFactory::getUser();
		return $user->get('isRoot');
	}		
	/**
	 * isCondominium.
	 * @return  boolean  True if is Guest, false on failure.
	 * /
	public function isCondominium(){
		return isset($this->mccn_user) ? $this->mccn_user->id_condominium : 0;
	}
	/**
	 * getCondominiumId
	 * @return  int  CondominiumId
	 * /
	public function getCondominiumId(){
		return isset($this->mccn_user) ? $this->mccn_user->id_condominium : 0;
	}
	/**
	 * getUserConfId
	 * @return  int  id_user_conf
	 * /
	public function getUserConfId(){
		//return isset($this->mccn_user) ? $this->mccn_user->id_user_conf : 0;
		// Aqui podia-se testar e se nao houvesse default conf, cria-lo logo para nao vir a dar problemas nao haver um unico con por user
		$id_user_conf = isset($this->mccn_user) ? $this->mccn_user->id_user_conf : 0;
		if($id_user_conf==0){
			//INSERT INTO `mccn_user_conf` (`ev`, `avatar`, `note_start_stop`, `note_energy_updates`, `note_fast_updates`, `note_slow_updates`, `note_unplugged`, `note_errors`, `note_others`, `theme`, `fast_time_update`, `slow_time_update`) VALUES (1, NULL, 1, 1, 1, 1, 1, 1, 1, 0, 5, 15),
			$id_user_conf = $sys->setSqlQuery('INSERT INTO mccn_user_conf (`avatar`) VALUES (NULL);');
			if($id_user_conf!=0){
				$sys->setSqlQuery('UPDATE mccn_user SET id_user_conf='.$id_user_conf.' WHERE id_user='.$this->getUserID().'');
			}
		}
		return $id_user_conf;
	}
	/**
	 * getJoomlaID
	 * @return  boolean  0 if is Guest, id_user on else.
	 */
	public function getJoomlaID(){
		$user 	= &JFactory::getUser();
		return isset($user) ? $user->id : 0;
	}
	/**
	 * getUserID
	 * @return  boolean  0 if is Guest, id_user on else.
	 * /
	public function getUserID(){
		if(!isset($this->mccn_user->id_user)){
			return 0;
		}
		return isset($this->mccn_user) ? $this->mccn_user->id_user : 0;
	}
	
	/**
	 * getOperatorID
	 * @return  boolean  0 if is Guest, id_user on else.
	 * /
	public function getOperatorID(){
		if(!isset($this->mccn_operator->id_operator)){
			//print_r($this); die("getUserID");
			return 0;
		}
		return isset($this->mccn_operator) ? $this->mccn_operator->id_operator : 0;
	}
	/**
	 * getArditiUserID(), Returns UserId
	 * @return  int  The user id.
	 * /
	public function getArditiUserID($id=0){
		if($id<=0) return 0;
		$result = $sys->getSqlResult("SELECT id_user FROM si_users WHERE id_id_joomla={$id}");
		//$db = &SysARDITI::getDBO();
		//$db->setQuery("SELECT id_user FROM mccn_user WHERE id_id_joomla={$id}");
		//$result = $db->loadResult();
		return $result;
	}
	*/

	/**
	 * SysARDITI::SetException
	 * @var Exception $ex 
	 * @return  object  The result.
	 */	
	public static function SetException($ex) {
		$str = $ex->getMessage();
		// Registamos erro no Log do Joomla
		JLog::add($str, JLog::WARNING);
		// Registamos LogAction
		SysM0::SetLogAction(LogActionTypes::ERROR, $str);
		// Registamos em syslog
		if(JDEBUG){			//JDEBUG ? error_log("SysARDITI::__construct UserSessionInfo=".$this->get("UserSessionInfo"), 0) : null;
			//getErrors
			//$str = is_array($str) ? print_r($str, true) : $str;
			error_log('SysARDITI::log('.$str.')', 0);		
		}
		// Mostramos o erro ao utilizador
		// Old Style ?
		// JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		//throw new Exception("COM_SIM0_ERROR_INVALID_LOGACTION", 404);
		//throw $ex;
		JErrorPage::render($ex);
	}	
	
	/**
	 * SysARDITI::log 
	 */	
	public static function log($str, $level=0) {
		//JDEBUG ? error_log("SysARDITI::__construct UserSessionInfo=".$this->get("UserSessionInfo"), 0) : null;
		if(JDEBUG){
			$str = is_array($str) ? print_r($str, true) : $str;
			error_log('SysARDITI::log('.$str.')', 0);		
		}
	}
	
	/**
	 * debug 
	 */	
	public function debug() {
		$user 	= &JFactory::getUser();
		echo 'You are logged in as:<br />';
		echo 'User name: ' 			. $user->username . '<br />';
		echo 'Real name: ' 			. $user->name . '<br />';
		echo 'User Joomla ID  : ' 	. $user->id . '<br />';
		$session= &JFactory::getSession();
		echo 'Session State  : ' 	. $session->getState() . '<br />';
		echo 'Session Is New  : ' 	. $session->isNew() . '<br />';
	}
	
	/**
	 * _formatLogger()
	 */	
	private function _formatLogger($debug=true) {
		// Formatamos o Logger
		// Definir melhor os conceitos, do que � critico e do que em produ��o pode n�o ser usado.
		JLog::addLogger(
		   array(
				// Sets file name
				'text_file' => 'activethings.siarditi.php',
				// Sets the format of each line
				'text_entry_format' => '{DATETIME} {PRIORITY} {MESSAGE}'
		   ),
		   // Sets all but DEBUG log level messages to be sent to the file
		   JLog::ALL & ~JLog::DEBUG,
		   // The log category which should be recorded in this file
		   array('siarditi')
		);
		// Em modo debug logamos tudo
		if($debug){
			JLog::addLogger(
			   array(
					'text_file' => 'activethings.php',
					'text_entry_format' => '{DATETIME} {PRIORITY} {MESSAGE}'
			   ),
			   JLog::ALL
			);
		}		   
		// Use PHP Exceptions
		JError::$legacy = false;
		//
		JLog::add("ActiveThings - Start new Request", JLog::INFO, 'siarditi');
		//JLog::add('Linha 2', JLog::WARNING, 'Test');
		//JLog::add('Linha 3',  JLog::WARNING);
		//JLog::add('Linha 4');
	}
}