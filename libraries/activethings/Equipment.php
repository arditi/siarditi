<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class Equipment extends JObject
{
    var $id_equipment;
    var $id_equipments_type;
    var $id_entity;
    var $name;
    var $description;
    var $procedures;
    var $restrictions;
    var $usage_conditions;
    var $quantity;
    var $costs;

    public function Equipment($id) {
        if(isset($id)) {
            $this->getMydata($id);
        }
        $this->populateDbData();
    }
    
    public function getMydata($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('equipments.*')
            ->from($db->quoteName('equipments'))
            ->where($db->quoteName('id_equipment') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }
    
    private function populateDbData()
    {
        $this->id_equipment  = $this->MydbObj->id_equipment;
        $this->id_equipments_type  = $this->MydbObj->id_equipments_type;
        $this->name  = $this->MydbObj->name;
        $this->description  = $this->MydbObj->description;
        $this->procedures  = $this->MydbObj->procedures;
        $this->restrictions  = $this->MydbObj->restrictions;
        $this->usage_conditions  = $this->MydbObj->usage_conditions;
        $this->quantity  = $this->MydbObj->quantity;
        $this->costs  = $this->MydbObj->costs;
        $this->id_entity  = $this->MydbObj->id_entity;
    }
}
