<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class PersonInEntity extends JObject
{
    var $id_entity;
    var $id_person;
    var $phone;
    var $ext;
    var $fax;
    var $email;
    var $research_kind;
    var $research_percent;
    
    var $bonds = array();
    
    public function PersonInEntity($id_person, $id_entity, $profile="", $id_bond=0) {
        if(isset($id_person) && isset($id_entity)) {
            $this->id_person=$id_person;
            $this->id_entity=$id_entity;
            $this->getMyData($id_person, $id_entity);
            $this->getBonds($id_person, $id_entity, $profile, $id_bond);
            $this->populateDbData();
        }
    }
    
    public function getMydata($id_person, $id_entity)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('entities_persons'))
            ->where($db->quoteName('id_entity') . ' = '.$id_entity)
            ->where($db->quoteName('id_person') . ' = '.$id_person);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }

    public function getBonds($id_person, $id_entity, $profile="", $id_bond=0)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('entities_bonds'))
            ->where($db->quoteName('id_entity') . ' = '.$id_entity)
            ->where($db->quoteName('id_person') . ' = '.$id_person);
        
        $db->setQuery($query);
        $this->bonds = $db->loadObjectList();
        foreach ($this->bonds as $i => $row) {
            if ($id_bond>0 && $row->id_bond==$id_bond)
                $this->bonds[$i] = new EntityBond($id_bond);
            elseif ($profile!="" && $row->bond_type==$profile)
                $this->bonds[$i] = new EntityBond($row->id_bond);
            else
                $this->bonds[$i] = new EntityBond($row->id_bond);
        }
    }
    
    private function populateDbData()
    {
        $this->phone = $this->MydbObj->phone;
        $this->ext = $this->MydbObj->ext;
        $this->email = $this->MydbObj->email;
        $this->research_kind = $this->MydbObj->research_kind;
        $this->research_percent = $this->MydbObj->research_percent;
    }
}
