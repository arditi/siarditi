<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class Person extends JObject
{
    var $id_person;
    var $id_user;
    var $name;
    var $card_id;
    var $card_id_date;
    var $tax_id;
    var $address;
    var $mailing_address;
    var $zip_code; 
    var $location;
    var $email;
    var $home_phone;
    var $mobile_phone;
    var $nationality;
    var $birthdate;
    var $cv;
    
    public function Person($id, $key='person') {
        if(isset($id) && $key=='person') {
            $this->id_person=$id;
            $this->getMydata($this->id_person);
        } elseif (isset($id) && $key=='user') {
            $this->id_user=$id;
            $this->getPersonData($this->id_user);
        }
        $this->populateDbData();
    }
    
    public function getMydata($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('persons'))
        ->join('LEFT', 'curricula ON curricula.id_person=persons.id_person')
        ->where($db->quoteName('persons.id_person') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
        $this->cv = new Curriculum($this->id_person, 'person');
    }
    
    public function getPersonData($userId = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('persons'))
        ->join('LEFT', 'curricula ON curricula.id_person=persons.id_person')
        ->where($db->quoteName('persons.id_user') . ' = '.$userId);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
        $this->cv = new Curriculum($this->MydbObj->id_person, 'person');
    }
    
    private function populateDbData()
    {
        $this->id_person = $this->MydbObj->id_person;
        $this->id_user = $this->MydbObj->id_user;
        $this->name = $this->MydbObj->name;
        $this->card_id = $this->MydbObj->card_id;
        $this->card_id_date = $this->MydbObj->card_id_date;
        $this->tax_id = $this->MydbObj->tax_id;
        $this->address = $this->MydbObj->address;
        $this->mailing_address = $this->MydbObj->mailing_address;
        $this->zip_code = $this->MydbObj->zip_code;
        $this->location = $this->MydbObj->location;
        $this->email = $this->MydbObj->email;
        $this->home_phone = $this->MydbObj->home_phone;
        $this->mobile_phone = $this->MydbObj->mobile_phone;
        $this->nationality = $this->MydbObj->nationality;
        $this->birthdate = $this->MydbObj->birthdate;
    }
    
    public function generateLogin($name=""){
        $PASSWORD_LENGTH = 8;
        $auxName = $name;
        if ($this instanceof Person) {
            $auxName = ($name=="") ? $this->name : $name;
        }
        $auxList = explode(" ",$auxName);
        
        $username = $auxList[0].".".$auxList[sizeof($auxList)-1];
        $password = substr(str_shuffle(strtolower(sha1(rand().time()."havia um pessegueiro na ilha"))),0,$PASSWORD_LENGTH);

        return array($username, $password);
    }
}
