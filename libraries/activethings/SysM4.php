<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M4 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM4 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class SysM4 extends JObject
{
	/**
	 * debug 
	 * @return  object  The result.
	 */	
	public static function debug() {
		$user 	= &JFactory::getUser();
		echo 'You are logged in as:<br />';
		echo 'User name: ' 			. $user->username . '<br />';
		echo 'Real name: ' 			. $user->name . '<br />';
		echo 'User Joomla ID  : ' 	. $user->id . '<br />';
		$session= &JFactory::getSession();
		echo 'Session State  : ' 	. $session->getState() . '<br />';
		echo 'Session Is New  : ' 	. $session->isNew() . '<br />';
	}
	
}
