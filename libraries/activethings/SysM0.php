<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M0 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

jimport( 'joomla.utilities.utility' );

/**
 * BasicEnum
 * Um abordagem interessante!
 * http://stackoverflow.com/questions/254514/php-and-enumerations
 */
abstract class BasicEnum {
    private static $constCacheArray = NULL;

    private static function getConstants() {
        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function isValidName($name, $strict = false) {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }
        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value) {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict = true);
    }
}

/** 
 * LogActionTypes
 *ATENTION, DATABASE INFO AND CODE, SHOWLD BE CONSISTENT
 */
class LogActionTypes extends BasicEnum {
	// AS ACTIONS DE SISTEMA PODEM NAO SER VISUALIZDAS PELO CLIENTE
    const NAVIGATE   		= 1;		// => PAGE_START_SITE
    const REDIRECT 			= 2;		// => PAGE_START_ADMIN
    const ERROR 			= 3;
    const LOGIN 			= 4;
    const LOGOUT 			= 5;
    const LOGIN_FAILED		= 6;
	const USER_CREATED		= 7;
	const USER_DELETED		= 8;
	const PAGE_END			= 9;
	
    const DB_INSERT 		= 10;
    const DB_UPDATE 		= 11;
    const DB_DELETE 		= 12;
    
	// AS ACTIONS APLICACIONAIS DEVEM FICAR ACIMA DE 20
	const VIEW_NOTIFICATION = 20;// ->24
    const SEND_NOTIFICATION = 21;// ->28
    const SEND_MAX_TRIES 	= 22;// ->29

	// NOTIFICATION
	const NOT_CREATE 		= 21;
	const NOT_DELETE 		= 22;
	const NOT_EDIT 			= 23;
	const NOT_VIEW 			= 24;
	const NOT_SEARCH		= 25;	
	const NOT_ENABLE 		= 26;
	const NOT_VALIDATE 		= 27;
	const NOT_SEND 			= 28;
	const NOT_SEND_MAX_TRIES= 29;

	// INDICADOR
	const IND_CREATE 		= 201;
	const IND_DELETE 		= 202;
	const IND_EDIT 			= 203;
	const IND_VIEW 			= 204;
	const IND_SEARCH		= 205;	
	const IND_ENABLE 		= 206;
	const IND_VALIDATE 		= 207;
	
	const IND_VALUES		= 210;
	const IND_REGION_ADD	= 211;
	const IND_REGION_DEL	= 212;
	//const IND_YEAR_ADD	= 213;
	//const IND_YEAR_DEL	= 214;

	
	// AXIS
	const AXIS_CREATE 		= 220;
	const AXIS_DELETE 		= 221;
	const AXIS_EDIT 		= 222;
	// DIM
	const DIM_CREATE 		= 225;
	const DIM_DELETE 		= 226;
	const DIM_EDIT 			= 227;
	// REGION
	const REGION_CREATE 	= 230;
	const REGION_DELETE 	= 231;
	const REGION_EDIT 		= 232;
	
	
	// DOCS
	const DOC_CREATE 		= 401;	//aux1=id_doc
	const DOC_DELETE 		= 402;	//aux1=id_doc
	const DOC_EDIT 			= 403;	//aux1=id_doc
	const DOC_UPDATE		= 403;	//aux1=id_doc
	const DOC_VIEW 			= 404;	//aux1=id_doc
	const DOC_SEARCH		= 405;	//aux1=search
	const DOC_ENABLE 		= 406;	//aux1=id_doc, aux2=value
	const DOC_VALIDATE 		= 407;	//aux1=id_doc, aux2=value
	const DOC_UPLOAD 		= 408;	//aux1=id_doc, aux2=filenameSaved
	/*
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('401', '1', 'DOCUMENT CREATE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('402', '1', 'DOCUMENT DELETE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('403', '1', 'DOCUMENT UPDATE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('404', '1', 'DOCUMENT VIEW', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('405', '1', 'DOCUMENT SEARCH', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('406', '1', 'DOCUMENT ENABLE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('407', '1', 'DOCUMENT VALIDATE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('408', '1', 'DOCUMENT UPLOAD', '');

	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('201', '1', 'INDICATOR CREATE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('202', '1', 'INDICATOR DELETE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('203', '1', 'INDICATOR UPDATE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('204', '1', 'INDICATOR VIEW', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('205', '1', 'INDICATOR SEARCH', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('206', '1', 'INDICATOR ENABLE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('207', '1', 'INDICATOR VALIDATE', '');
	
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('210', '1', 'INDICATOR VALUES', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('211', '1', 'INDICATOR ADD REGION', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('212', '1', 'INDICATOR DEL REGION', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('213', '1', 'INDICATOR ADD YEAR', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('214', '1', 'INDICATOR DEL YEAR', '');
	
	// AXIS	
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('220', '1', 'AXIS CREATE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('221', '1', 'AXIS DELETE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('222', '1', 'AXIS EDIT', '');
	// DIM	
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('225', '1', 'DIMENSION CREATE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('226', '1', 'DIMENSION DELETE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('227', '1', 'DIMENSION EDIT', '');	
	// REGION
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('230', '1', 'REGION CREATE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('231', '1', 'REGION DELETE', '');
	INSERT INTO `siarditi_dbv1`.`logactiontypes` (`id_action`, `id_categoria`, `action`, `uri`) VALUES ('232', '1', 'REGION EDIT', '');		
	*/
}

/**
 * SysM0 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class SysM0 extends JObject
{
	/**
	 * SetLogAction
	 * Regista nova ac��o de user ou do sistema. Pode ser uma ac��o de navega��o ou de edi��o de conte�do
		LogActions('id_action', 'id_user','source','aux1','aux2')
		1 - Navigate				=> Colocar o url na aux1 
		2 - Redirect				=> Colocar o url na aux1 
		3 - Error					=> Colocar o url na aux1 
		4 - Login
		5 - Logout
		9 - Page_end
		10- Create					Any entity deletion	=> Colocar Entity na aux1 e o id na aux2 
		11- Update					Any entity deletion	=> Colocar Entity na aux1 e o id na aux2 
		12- Delete					Any entity deletion	=> Colocar Entity na aux1 e o id na aux2 
		20- Internal Notification	User Received an notification from system (TOs) => Colocar o id na desc
		21- External Notification	GestorSI sent an notification => Colocar o id na desc
	 * @return  boolean  True if is Guest, false on failure.
	 */
	public static function SetLogAction(/*LogActionTypes*/ $id_action, $aux1='', $aux2=''){
		// Test LogActionType Value
		if(!LogActionTypes::isValidValue($id_action)){
			throw new Exception("COM_SIM0_ERROR_INVALID_LOGACTION", 404);
		}
		
		// Detecting Active Variables		
		$sys 	= &SysARDITI::getInstance();			
		$id_user= JFactory::getUser()->id;			// Ver se este ser� o melhor id_Login para se guardar
		//$src	= JUri::getInstance()->toString();	//$src		= JURI::current();// JURI::baseurl() 
		///$src = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on") ? 'https://' : 'http://';
		///$src	= $src. $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
		$src	= $_SERVER["REQUEST_URI"];

		// Limit var_char sizes, no need!
		$src	= substr($src, 0,127);
		$aux1	= substr($aux1,0,31);
		$aux2	= substr($aux2,0,255);
		
		// Insert new LogAction
		$db = &$sys->getDBO();
		$qry= $db->getQuery(true);
		try{
			$qry->insert('logactions')
				->columns($db->quoteName(array('id_action','id_user','source','aux1','aux2')))
				->values((int)$id_action.','.(int) $id_user.','.$db->quote($src).','.$db->quote($aux1).','.$db->quote($aux2));
			$db->setQuery($qry);
			$db->execute();
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
	}
	
	/**
	 * CreateNotification
	 * Regista nova notifica��o para envio
	 * 
	 * @return  int  id_not if registed, 0 on failure.
	 */
	public static function CreateNotification($id_user_from, $id_user_to, $id_template, $source, $from, $to, $subject, $body, $send=true){
		//INSERT INTO `notifications` (`id_user_from`, `id_user_to`, `id_template`, `source`, `from`, `to`, `subject`, `body`, `send_try_count`, `send_try_date`, `send_date`, `sended`, `read_confirmation_date`, `sys_date`) VALUES
		//(782, 783, 10, NULL, NULL, NULL, 'User Registration', 'body Pt', NULL, NULL, NULL, 0, NULL, '2015-03-08 23:41:06');
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		$qry 		= $db->getQuery(true);	
		$id_not		= 0;
		try{
			$qry->insert('notifications')
				->columns($db->quoteName(array('id_user_from','id_user_to','id_template','source','from','to','subject','body')))
				->values((int)$id_user_from.','.(int)$id_user_to.','.(int)$id_template.','.
							$db->quote($source).','.$db->quote($from).','.$db->quote($to).','.$db->quote($subject).','.$db->quote($body));
			$db->setQuery($qry);
			$db->execute();
			
			$id_not = $db->insertid();	// id_not inserido
			// Regista esta Action
			// 20- Internal Notification	User Received an notification from system (TOs) => Colocar o id na desc
			// 21- External Notification	GestorSI sent an notification => Colocar o id na desc
			$id_action = ($id_user_from==USER_SUPER_ADMIN)?21:20;	// Para evitar ir � DB buscar o tipo de notifica��o em fun��o do id_template
			SysM0::SetLogAction( $id_action, $id_not);		
		}
		catch (Exception $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
			if(JDEBUG) die($e->getMessage());
		}
		return $id_not;
	}
/*
$sys 		= &SysARDITI::getInstance();
$sysCfg 	= &$sys->getConfig();
$mail_from 	= &$sysCfg->get("mail_from", "emvjardim@hotmail.com");
$id_not 	= SysM0::CreateNotification(USER_SUPER_ADMIN, USER_SUPER_ADMIN, 10, '', $mail_from, "to", "subject", "body", $send=true);
echo( "mail_from=".$mail_from );	
die( "ID_NOT=".$id_not );	
*/	

	/**
	 *
	 * https://docs.joomla.org/API15:JUtility/sendMail
	 * @return  int  id_not if registed, 0 on failure.
	 */
	public static function sendMail(&$from, &$fromname, &$recipient, &$subject, &$body, $mode=1, $cc=null, $bcc=null, $attachment=null, $replyto=null, $replytoname=null )
	{
		$recipient	= explode (',', str_replace(' ', '', $recipient ));
		$cc			= explode (',', str_replace(' ', '', $cc ));
		$bcc		= explode (',', str_replace(' ', '', $bcc ));		
		
		// Get a JMail instance
		$mail =/*&*/JFactory::getMailer();
		//$mail->SMTPDebug = 8;
 
		$mail->setSender(array($from, $fromname));
		$mail->setSubject($subject);
		$mail->setBody($body);
		$mail->IsHTML($mode ? true:false);
		
		$mail->addRecipient($recipient);
		$mail->addCC($cc);
		$mail->addBCC($bcc);
		$mail->addAttachment($attachment);
 
		// Take care of reply email addresses
		if( is_array( $replyto ) ) {
				$numReplyTo = count($replyto);
				for ( $i=0; $i < $numReplyTo; $i++){
						$mail->addReplyTo( array($replyto[$i], $replytoname[$i]) );
				}
		} elseif( isset( $replyto ) ) {
				$mail->addReplyTo( array( $replyto, $replytoname ) );
		}

		//return  $mail->Send();		
		$result = $mail->Send();
		//print_r($mail);
		return $result;
	}
}