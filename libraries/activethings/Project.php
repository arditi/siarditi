<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class Project extends JObject
{
    var $id_project;
    var $name;
    var $description;
    var $url;
    var $begin_date;
    var $end_date;
    var $id_funding;
    var $id_project_state;
    var $obs;

    var $project_state;
    var $funding_source;

    public function Project($id) {
        if(isset($id)) {
            $this->getMydata($id);
        }
        $this->populateDbData();
    }
    
    public function getMydata($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('projects.*, fund_src.id_funding as id_funding, fund_src.name as funding_name, proj_state.name as proj_state')
            ->from($db->quoteName('projects'))
            ->join('LEFT', 'curricula_projects AS cv_proj ON cv_proj.id_project=projects.id_project')
            ->join('LEFT', 'project_state AS proj_state ON proj_state.id_project_state=projects.id_project_state')
            ->join('LEFT', 'projects_funding AS proj_fund ON proj_fund.id_project=projects.id_project')
            ->join('LEFT', 'project_funding_sources AS fund_src ON fund_src.id_funding=proj_fund.id_funding')
            ->where($db->quoteName('cv_proj.id_project') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }
    
    private function populateDbData()
    {
        $this->id_project  = $this->MydbObj->id_project;
        $this->name  = $this->MydbObj->name;
        $this->description  = $this->MydbObj->description;
        $this->url  = $this->MydbObj->url;
        $this->begin_date  = $this->MydbObj->begin_date;
        $this->end_date  = $this->MydbObj->end_date;
        $this->id_entity  = $this->MydbObj->id_entity;
        $this->id_project_state  = $this->MydbObj->id_project_state;
        $this->obs = $this->MydbObj->obs;
        $this->project_state  = $this->MydbObj->proj_state;
        $this->id_funding  = $this->MydbObj->id_funding;
        $this->funding_source  = $this->MydbObj->funding_name;
    }
}
