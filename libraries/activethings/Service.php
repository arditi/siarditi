<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class Service extends JObject
{
    var $id_service;
    var $id_services_type;
    var $id_entity;
    var $name;
    var $description;
    var $procedures;
    var $restrictions;
    var $costs;

    public function Service($id) {
        if(isset($id)) {
            $this->getMydata($id);
        }
        $this->populateDbData();
    }
    
    public function getMydata($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('services.*')
            ->from($db->quoteName('services'))
            ->where($db->quoteName('id_service') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }
    
    private function populateDbData()
    {
        $this->id_service  = $this->MydbObj->id_service;
        $this->id_services_type  = $this->MydbObj->id_services_type;
        $this->name  = $this->MydbObj->name;
        $this->description  = $this->MydbObj->description;
        $this->procedures  = $this->MydbObj->procedures;
        $this->restrictions  = $this->MydbObj->restrictions;
        $this->costs  = $this->MydbObj->costs;
        $this->id_entity  = $this->MydbObj->id_entity;
    }
}
