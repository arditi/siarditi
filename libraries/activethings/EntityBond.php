<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class EntityBond extends JObject
{
    var $id_bond;
    var $id_entity;
    var $id_person;
    
    var $position_held;
    var $bond_type;
    var $bond_initial_date;
    var $bond_final_date;
    
    public function EntityBond($id_bond) {
        if(isset($id_bond)) {
            $this->id_bond=$id_bond;
            $this->getMyData($id_bond);
        }
        $this->populateDbData();
    }
    
    public function getMydata($id_bond)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('entities_bonds'))
            ->where($db->quoteName('id_bond') . ' = '.$id_bond);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }

    private function populateDbData()
    {
        $this->position_held = $this->MydbObj->position_held;
        $this->bond_type = $this->MydbObj->bond_type;
        $this->bond_initial_date = $this->MydbObj->bond_initial_date;
        $this->bond_final_date = $this->MydbObj->bond_final_date;
    }
}
