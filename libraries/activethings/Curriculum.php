<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;
require_once JPATH_ROOT.'/libraries/activethings/Qualification.php';
require_once JPATH_ROOT.'/libraries/activethings/Project.php';
require_once JPATH_ROOT.'/libraries/activethings/Publication.php';

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class Curriculum extends JObject
{
    var $id_cv;
    var $document;
    var $date;
    var $obs;
    var $id_person;
    var $qualifications=array();
    var $projects=array();
    var $publications=array();

    public function Curriculum($id, $key='cv') {
        if(isset($id) && $key=='cv') {
            $this->getMydata($id);
        } elseif (isset($id) && $key=='person') {
            $this->getPersonData($id);
        }
        $this->populateDbData();
        if ($this->id_cv > 0) {
            $this->getQualifications();
            $this->getProjects();
            $this->getPublications();
        }
    }
    
    public function getMydata($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('curricula'))
        ->where($db->quoteName('id_cv') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }

    public function getPersonData($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('curricula'))
        ->where($db->quoteName('id_person') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }
    
    public function getQualifications()
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('qualifications'))
        ->join('LEFT', 'curricula_qualifications AS cv_qual ON cv_qual.id_qualification=qualifications.id_qualification')
        ->where($db->quoteName('cv_qual.id_cv') . ' = '.$this->id_cv);
        
        $db->setQuery($query);
        $this->qualifications = $db->loadObjectList();
        
        foreach ($this->qualifications as $i => $row) {
            $this->qualifications[$i] = new Qualification($row->id_qualification);
        }

    }

    public function getProjects()
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('projects'))
        ->join('LEFT', 'curricula_projects AS cv_proj ON cv_proj.id_project=projects.id_project')
        ->where($db->quoteName('cv_proj.id_cv') . ' = '.$this->id_cv);
        
        $db->setQuery($query);
        $this->projects = $db->loadObjectList();
        
        foreach ($this->projects as $i => $row) {
            $this->projects[$i] = new Project($row->id_project);
        }

    }

    public function getPublications()
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('publications'))
        ->join('LEFT', 'curricula_publications AS cv_pub ON cv_pub.id_publication=publications.id_publication')
        ->where($db->quoteName('cv_pub.id_cv') . ' = '.$this->id_cv);
        
        $db->setQuery($query);
        $this->publications = $db->loadObjectList();
        
        foreach ($this->publications as $i => $row) {
            $this->publications[$i] = new Publication($row->id_publication);
        }

    }
    
    private function populateDbData()
    {
        $this->id_cv = $this->MydbObj->id_cv;
        $this->document = $this->MydbObj->document;
        $this->date = $this->MydbObj->date;
        $this->obs = $this->MydbObj->obs;
        $this->id_person = $this->MydbObj->id_person;
        $this->qualifications = $this->MydbObj->qualifications;
    }    
}
