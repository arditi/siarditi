<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class Entity extends JObject
{
    var $id_entity;
    var $id_parent;
    var $name;
    var $name_international;
    var $acronym;
    var $address;
    var $phone;
    var $phone_ext;
    var $fax;
    var $url;
    var $email;
    var $zip_code; 
    var $location;
    var $legal_regime;
    var $tax_id;
    var $constitution_date;
    
    var $investigators;
    var $managers;
<<<<<<< HEAD
    var $head_manager;
    var $services;
    var $equipments;
=======
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470

    public function Entity($id) {
        if(isset($id)) {
            $this->id_entity=$id;
            $this->getMydata($this->id_entity);
        }
        $this->populateDbData();
        
        if ($this->id_entity > 0) {
            $this->getInvestigators();
            $this->getManagers();
<<<<<<< HEAD
            $this->getServices();
            $this->getEquipments();
=======
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
        }
    }
    
    public function getMydata($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('entities'))
            ->where($db->quoteName('id_entity') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }

    public function getInvestigators()
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('persons'))
            ->join('LEFT', 'entities_persons AS ent_person ON ent_person.id_person=persons.id_person')
            ->join('LEFT', 'entities_bonds AS ent_bond ON ent_bond.id_person=persons.id_person')
            ->where($db->quoteName('ent_bond.bond_type') . ' IN ("researcher","scolarship")')
            ->where($db->quoteName('ent_bond.id_entity') . ' = '.$this->id_entity)
            ->where($db->quoteName('ent_person.id_entity') . ' = '.$this->id_entity);
        
        $db->setQuery($query);
        $this->investigators = $db->loadObjectList();
        
        foreach ($this->investigators as $i => $row) {
            $this->investigators[$i] = new PersonInEntity($row->id_person,$this->id_entity,"manager",$row->id_bond);
        }
    }
    
    public function getManagers()
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('persons'))
            ->join('LEFT', 'entities_persons AS ent_person ON ent_person.id_person=persons.id_person')
            ->join('LEFT', 'entities_bonds AS ent_bond ON ent_bond.id_person=persons.id_person')
<<<<<<< HEAD
            ->where($db->quoteName('ent_bond.bond_type') . ' IN ("head_manager","manager","main_contact")')
=======
            ->where($db->quoteName('ent_bond.bond_type') . ' IN ("manager","main_contact")')
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
            ->where($db->quoteName('ent_bond.id_entity') . ' = '.$this->id_entity)
            ->where($db->quoteName('ent_person.id_entity') . ' = '.$this->id_entity);
        
        $db->setQuery($query);
        $this->managers = $db->loadObjectList();
        
        foreach ($this->managers as $i => $row) {
            $this->managers[$i] = new PersonInEntity($row->id_person,$this->id_entity,"manager",$row->id_bond);
<<<<<<< HEAD
            if ($row->bond_type=="head_manager")
                $this->head_manager = $this->managers[$i];
        }
    }
    
    public function getServices()
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('services'))
            ->where($db->quoteName('id_entity') . ' = '.$this->id_entity);
        
        $db->setQuery($query);
        $this->services = $db->loadObjectList();
    }

    public function getEquipments()
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('equipments'))
            ->where($db->quoteName('id_entity') . ' = '.$this->id_entity);
        
        $db->setQuery($query);
        $this->equipments = $db->loadObjectList();
    }
    
=======
        }
    }
    
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
    private function populateDbData()
    {
        $this->id_parent = $this->MydbObj->id_parent;
        $this->name = $this->MydbObj->name;
        $this->name_international = $this->MydbObj->name_international;
        $this->acronym = $this->MydbObj->acronym;
        $this->address = $this->MydbObj->address;
        $this->phone = $this->MydbObj->phone;
        $this->phone_ext = $this->MydbObj->phone_ext;
        $this->url = $this->MydbObj->url;
        $this->email = $this->MydbObj->email;
        $this->zip_code = $this->MydbObj->zip_code;
        $this->location = $this->MydbObj->location;
        $this->legal_regime = $this->MydbObj->legal_regime;
        $this->tax_id = $this->MydbObj->tax_id;
        $this->constitution_date = $this->MydbObj->constitution_date;
    }
}
