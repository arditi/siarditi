<?php
/**
 * ActivePortal
 * @package     activethings.siarditi
 * @subpackage  siarditi
 *
 * API relativa ao m�dulo M1 SIARDITI
 *
 */
defined('JPATH_PLATFORM') or die;

/**
 * SysM1 Class
 *
 * @package     activethings.siarditi
 * @subpackage  siarditi
 * @since       11.1
 */
class Publication extends JObject
{
    var $id_cv;
    var $id_publication;
    var $title;
    var $pages;
    var $area;
    var $issn;
    var $authors;
    var $publisher;
    var $date;
    var $document;
    var $link;
    
    public function Publication($id) {
        if(isset($id)) {
            $this->getMydata($id);
        }
        $this->populateDbData();
    }
    
    public function getMydata($pk = null)
    {
        $sys = &SysARDITI::getInstance();
        $db = &$sys->getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
        ->from($db->quoteName('publications'))
        ->join('LEFT', 'curricula_publications AS cv_pub ON cv_pub.id_publication=publications.id_publication')
        ->where($db->quoteName('publications.id_publication') . ' = '.$pk);
        
        $db->setQuery($query);
        $this->MydbObj = $db->loadObject();
    }
    
    private function populateDbData()
    {
        $this->id_cv = $this->MydbObj->id_cv;
        $this->id_publication = $this->MydbObj->id_publication;
        $this->title = $this->MydbObj->title;
        $this->pages = $this->MydbObj->pages;
        $this->area = $this->MydbObj->area;
        $this->issn = $this->MydbObj->issn;
        $this->authors = $this->MydbObj->authors;
        $this->publisher = $this->MydbObj->publisher;
        $this->date = $this->MydbObj->date;
        $this->document = $this->MydbObj->document;   
        $this->link = $this->MydbObj->link;   
    }
}
