<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.SISITE
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Global Objects
$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;
// Getting params from template
$params = $app->getTemplate(true)->params;
// ActivePortal 
$sys = &SysARDITI::getInstance();

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');


/**
 * To change lang
 */
// Default site lang
$langdef	= JComponentHelper::getParams('com_languages')->get('site');
//$language = $app->input->get('lang', $langdef, 'string');
$lang 		= $app->input->get('lang', '', 'string');
$language 	= JFactory::getLanguage();
if ($lang!=''){
	if($lang!=$language->getTag()) {
		$language->setLanguage($lang );
		$language->load();
	}
}
//die('<br>'.$language->getTag().' langdef='. $langdef . '-this->language='. $this->language. '-lang='. $lang.' link='.JText::_('TPL_SISITE_MENU_CONTACTS_LINK'));		
// Para o link de selecçãod e idioma...
if($lang=="pt-PT"){
	$langs = "PT";
	$langt = "UK";
	$langu = 'index.php?lang=en-GB';//JRoute::_('index.php?lang=en-GB', false);
}
else{
	$langs = "UK";
	$langt = "PT";
	$langu = 'index.php?lang=pt-PT';//JRoute::_('index.php?lang=pt-PT', false);		
	$langu = 'index.php?lang=pt-PT';//JRoute::_('index.php?lang=pt-PT', false);		
}
///////////







////
// Breadcrumb improvement
// 		http://stackoverflow.com/questions/12915495/how-to-use-breadcrumbs-in-joomla-in-custom-components
// Mostra só 2 níveis dos componentes 
//
if($option!="com_content" & 1==2){
	$nivel1 = null;$nivel2 = null;$module = -1;
	for($i=0;$i<4; $i++){if($option=='com_sim'.$i){$module=$i;break;}}
	$nivel1 = array(JText::_( strtoupper($option) ), 'index.php?option='.$option);//JText::_( 'MENU_SIM0_LINK' ));			
	$view2	= ($view!='') ? $view : $option;//"default";
	$nivel2 = array(JText::_( strtoupper($option).'_'.strtoupper($view2) ), null);
	$pathway 	= $app->getPathway();
	$breadcrumb = $pathway->setPathway(array());
	if($nivel1 != null){
		$pathway->addItem( $nivel1[0], $nivel1[1]);
		if($nivel2 != null){
			$pathway->addItem( $nivel2[0], $nivel2[1]);
		}
	}
}
////
// Fica Melhor assim
if($option=="com_sim0"){
	$nivel1 = null; $nivel2 = null;
	// 1º nivel
	switch($view){
		case 'docs':
		case 'doc':
			$nivel1 = array(JText::_( strtoupper($option).'_'.strtoupper('docs') ), 'index.php?option='.$option.'&view=docs');
			break;
		case 'inds':
		case 'ind':
			$nivel1 = array(JText::_( strtoupper($option).'_'.strtoupper('inds') ), 'index.php?option='.$option.'&view=inds');
			break;
		default:
			$nivel1 = array(JText::_( strtoupper($option)), 'index.php?option='.$option.'&view=');
			break;
	}
	// 2º nivel
	switch($view){
		case 'doc':
		case 'ind':
			$nivel2 = array(JText::_( strtoupper($option).'_'.strtoupper($view) ), null);
			break;
	}
	// Reescreve o Breadcrumb
	$pathway 	= $app->getPathway();
	// Limpa a Path
	$breadcrumb = $pathway->setPathway(array());
	// Adiciona níveis ao breadcrumb
	if($nivel1 != null){
		$pathway->addItem( $nivel1[0], $nivel1[1]);
		if($nivel2 != null){
			$pathway->addItem( $nivel2[0], $nivel2[1]);
		}
	}
}

/// TEMPLATE
if($task == "edit" || $layout == "form" ){
	$fullWidth = 1;
}
else{
	$fullWidth = 0;
}

//////////////////

$doc->setMetaData('viewport',  'width=device-width,initial-scale=1');
$doc->setMetaData('author', 'ActiveThings, Lda');
$doc->setMetaData('generator', null);

//////////////
// Frameworks
// http://joomla.stackexchange.com/questions/97/disable-script-loading-in-head
JHtml::_('jquery.framework', false);
// Disabling
JHtml::_('bootstrap.framework', false);

// Add Stylesheets and js
//$doc->addScript('templates/' . $this->template . '/js/template.js');
//$doc->addStyleSheet( 'templates/'.$this->template.'/css/template.css');

//$doc->addStyleSheet($this->baseurl . '/media/jui/css/bootstrap.min.css');
//$doc->addStyleSheet($this->baseurl . '/media/jui/css/bootstrap-responsive.css');

// Add Stylesheets
//https://docs.joomla.org/JDocument/addStyleSheet#Example_2
/*
$doc->addStyleSheet( 'templates/'.$this->template.'/css/bootstrap-cerulean.min.css', 'text/css', $media=null, array('id'=>'bs-css') );
$doc->addStyleSheet( 'templates/'.$this->template.'/css/charisma-app.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/bower_components/fullcalendar/dist/fullcalendar.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/bower_components/fullcalendar/dist/fullcalendar.print.css');

$doc->addStyleSheet( 'templates/'.$this->template.'/bower_components/chosen/chosen.min.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/bower_components/colorbox/example3/colorbox.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/bower_components/responsive-tables/responsive-tables.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css');

$doc->addStyleSheet( 'templates/'.$this->template.'/css/jquery.noty.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/css/noty_theme_default.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/css/elfinder.min.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/css/elfinder.theme.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/css/jquery.iphone.toggle.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/css/uploadify.css');
$doc->addStyleSheet( 'templates/'.$this->template.'/css/animate.min.css');

//<!-- jQuery -->
$doc->addScript('templates/' . $this->template . '/bower_components/jquery/jquery.min.js');
	
*/

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
	$span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span9";
}
else
{
	$span = "span12";
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle')) . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}

//<jdoc:include type="head" />
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8">
    <!-- The Head -->	

    <!-- The styles -->
    <link href="<?php echo 'templates/'.$this->template; ?>/css/bootstrap-cerulean.min.css" rel="stylesheet" id="bs-css" >
    <link href="<?php echo 'templates/'.$this->template; ?>/css/charisma-app.css" rel="stylesheet">
    <link href="<?php echo 'templates/'.$this->template; ?>/bower_components/fullcalendar/dist/fullcalendar.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/bower_components/fullcalendar/dist/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link href="<?php echo 'templates/'.$this->template; ?>/bower_components/chosen/chosen.min.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/bower_components/colorbox/example3/colorbox.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/bower_components/responsive-tables/responsive-tables.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css" rel='stylesheet'>
	
    <link href="<?php echo 'templates/'.$this->template; ?>/css/jquery.noty.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/css/noty_theme_default.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/css/elfinder.min.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/css/elfinder.theme.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/css/jquery.iphone.toggle.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/css/uploadify.css" rel='stylesheet'>
    <link href="<?php echo 'templates/'.$this->template; ?>/css/animate.min.css" rel='stylesheet'>

    <!-- jQuery -->
    <script src="<?php echo 'templates/'.$this->template; ?>/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo 'templates/'.$this->template; ?>/favicon.ico">
	
	<?php // Use of Google Font ?>
	<?php if (1==2 & $this->params->get('googleFont')) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
		<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
			}
		</style>
	<?php endif; ?>
	<?php // Template color ?>
	<?php if (1==2 & $this->params->get('templateColor')) : ?>
	<style type="text/css">
		body.site
		{
			border-top: 3px solid <?php echo $this->params->get('templateColor'); ?>;
			background-color: <?php echo $this->params->get('templateBackgroundColor'); ?>
		}
		a
		{
			color: <?php echo $this->params->get('templateColor'); ?>;
		}
		.navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
		.btn-primary
		{
			background: <?php echo $this->params->get('templateColor'); ?>;
		}
		.navbar-inner
		{
			-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
			box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
		}
	</style>
	<?php endif; ?>
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>

<body class="_site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '');
?>">

<script>
var baseUrl = "<?php echo $this->baseurl ?>";

</script>
<?php 
	//$sys->debug();
?>
<!-- topbar starts -->
<div class="navbar navbar-default" role="navigation">

	<div class="navbar-inner">
		<button type="button" class="navbar-toggle pull-left animated flip">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.php">
			<img alt="SIARDITI Logo" src="<?php echo 'templates/'.$this->template; ?>/img/logo_arditi.png" class="hidden-xs" _style="width:150px"/>
			<!--<span>SI.ARDITI</span>--></a>

		<!-- user dropdown starts -->
		<div class="btn-group pull-right">
			<?php 
			// Se não está logado
			if($sys->isGuest()){
				?>
			<!--
			<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs">Guest</span>
			</button> -->
			<a class="btn btn-default dropdown-toggle" href="index.php?option=com_users">
				<i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Login</span>
			</a>			
				<?php 
			}
			// Se está logado
			else{
			?>
			<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo $user->name; ?></span>
				<span class="caret"></span>
			</button>
			
			<ul class="dropdown-menu">
				<li><a href="./index.php?option=com_users&view=profile&layout=edit"><i class="glyphicon glyphicon-pencil"></i> Profile</a></li>
				<li><a href="./index.php?option=com_users&view=profile&layout=edit"><i class="glyphicon glyphicon-cog"></i> Preferences</a></li>
				<li class="divider">
				<form action="<?php echo JRoute::_(JUri::getInstance()->toString(), true, $params->get('usesecure')); ?>" 
						  method="post" id="login-form" class="form-vertical" style="visibility:hidden">
						<input type="submit" name="Submit" class="btn btn-primary" value="<?php echo JText::_('JLOGOUT'); ?>" />
						<input type="hidden" name="option" value="com_users" />
						<input type="hidden" name="task" value="user.logout" />
						<input type="hidden" name="return" value="<?php echo base64_encode(JURI::root()."\n"); ?>" />
						<?php echo JHtml::_('form.token'); ?>
				</form>
					</li>
				<li><a onclick="return document.getElementById('login-form').submit();" href="javascript:void(0);"><i class="glyphicon glyphicon-off"></i> <?php echo JText::_('JLOGOUT'); ?></a></li>
				<?php 
				}
				?>
			</ul>
		</div>
		<!-- user dropdown ends -->
		<?php 
		?>
		<ul class="collapse navbar-collapse nav navbar-nav top-menu pull-right">
			<li><a href="<?php echo JText::_('TPL_SISITE_MENU_CONTACTS_LINK'); ?>"><i class="glyphicon glyphicon-bell"></i>
				<?php echo JText::_('TPL_SISITE_MENU_CONTACTS_NAME'); ?></a></li>
			<li class="dropdown">
				<a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> <?php echo JText::_('TPL_SISITE_MENU_LINKS_NAME'); ?> <span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<!--<li><a href="index.php?option=com_sim0&view=logaction&id_log=2">Log</a></li>-->
					<li><a target="_new" href="http://www.arditi.pt">ARDITI</a></li>			
					<li class="divider"></li>
					<li><a target="_new" href="http://orcid.org/">ORCID</a></li>
					<li><a target="_new" href="http://www.fct.pt">FCT</a></li>
					
				</ul>
			</li>
			

			<li class="dropdown">
				<a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-flag"></i>
					<?php echo $langs;//JText::_('TPL_SISITE_MENU_LINKS_NAME'); ?> <span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					
					<li><a href="index.php?lang=pt-PT">PT</a></li>
					<li><a href="index.php?lang=en-GB">UK</a></li>
					
				</ul>
			</li>		
			<!-- 			
			<li><a href="<?php echo $langu ?>"> <?php echo $langt;//JText::_('TPL_SISITE_MENU_CONTACTS_NAME'); ?></a></li>
			-->
			<li><!-- #search -->
				<form class="navbar-search pull-left" 
					action="index.php?option=com_search" method="post" name="adminForm" id="adminForm">
					<input type="text"   name="searchword" id="searchField" 
						 class="search-query form-control col-md-10" placeholder="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>..." />
					<input type="hidden" name="view" value="search" />
					<input type="hidden" name="layout" value="edit" />
					<input type="hidden" name="task" value="" />
					<?php echo JHtml::_('form.token'); ?>
				</form><!-- /search -->
				
				

			</li>
		</ul>

	</div>
</div>


	<!-- Body -->
<?php
/**
*/
if($sys->isGuest()){
	include("index_logged_on.php");
}
else{
	include("index_logged_on.php");
}
?>
	<!-- /Body -->	

	
			<!--  Context Footer -->	
			<div class="row" style="margin-left: 0px; margin-right: 10px;">
			
				<jdoc:include type="modules" name="footer" style="none" />
				
				<!--
				<p class="pull-right">
					<a href="#top" _id="back-top">
						<?php echo JText::_('TPL_SISITE_BACKTOTOP'); ?>
					</a>
				</p>
				-->
			</div>	
	
	<br /><br /><br />
	
	<jdoc:include type="modules" name="debug" style="none" />
	
	<!-- Footer -->
    <footer class="footer" role="contentinfo">
	<div class="navbar-default xnavbar-inverse navbar-fixed-bottom" >
		<div class="navbar-inner">
		<div class="container" style="width:100%; _background-color:#999999;">
			<div class="rodape" style="color:white"><p>
				<a href="<?php echo JText::_('TPL_SISITE_MENU_COPY_LINK'); ?>" target="_blank" style="color:white"> <?php echo JText::_('TPL_SISITE_MENU_COPY_NAME'); ?></a> |
				<a href="<?php echo JText::_('TPL_SISITE_MENU_TERMS_LINK'); ?>"                 style="color:white"> <?php echo JText::_('TPL_SISITE_MENU_TERMS_NAME'); ?></a> |
				<a href="<?php echo JText::_('TPL_SISITE_MENU_COOKIE_LINK'); ?>"			    style="color:white"> <?php echo JText::_('TPL_SISITE_MENU_COOKIE_NAME'); ?></a>
				<span class="pull-right"  style="color:#bbbbbb"> <a href="http://www.activethings.pt/" target="_blank" style="color:#bbbbbb">By ActiveThings</a></span>
				</p></div>
		</div>
		</div>
	</div>	
    </footer>	

<!-- external javascript -->
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<!-- data table plugin -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/jquery.dataTables.min.js"></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo 'templates/'.$this->template; ?>/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/jquery.history.js"></script>
<!-- application script for ARDITI  -->
<script src="<?php echo 'templates/'.$this->template; ?>/js/arditi.js"></script>

	
</body>
</html>