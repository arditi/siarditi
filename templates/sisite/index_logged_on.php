<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.SISITE
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
	<!-- Body -->
	<div class="ch-container">
    <div class="row">
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">
                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Menu</li>
                        <li><a class="ajax-link" href="index.php?option=com_content"><i 
								class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
							</li>
                        <li><a class="ajax-link" href="<?php echo JText::_('TPL_SISITE_MENU_II_LINK'); ?>"><i 
								class="glyphicon glyphicon-eye-open"></i><span> <?php echo JText::_('TPL_SISITE_MENU_II_NAME'); ?></span></a>
							</li>
                        <li><a class="ajax-link" href="<?php echo JText::_('TPL_SISITE_MENU_LB_LINK'); ?>"><i 
								class="glyphicon glyphicon-share"></i><span> <?php echo JText::_('TPL_SISITE_MENU_LB_NAME'); ?></span></a>
							</li>
							<?php
							// If user is logged
							// Testar se $sys->isInvestigator()
							if(!$sys->isGuest()){
								?>
								
                        <li><a class="ajax-link" href="<?php echo JText::_('TPL_SISITE_MENU_RU_LINK'); ?>"><i 
								class="glyphicon glyphicon-list-alt"></i><span> <?php echo JText::_('TPL_SISITE_MENU_RU_NAME'); ?></span></a>
							</li>
                        <li><a class="ajax-link" href="<?php echo JText::_('TPL_SISITE_MENU_RS_LINK'); ?>"><i 
								class="glyphicon glyphicon-user"></i><span> <?php echo JText::_('TPL_SISITE_MENU_RS_NAME'); ?></span></a>
							</li>
							<!--
                        <li class="accordion">
                            <a href="<?php echo JText::_('TPL_SISITE_MENU_PR_LINK'); ?>"><i 
								class="glyphicon glyphicon-plus"></i><span> <?php echo JText::_('TPL_SISITE_MENU_PR_NAME'); ?></span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="index.php">Project 1</a></li>
                                <li><a href="index.php">Project n</a></li>
								</ul>
							</li>
							-->
								<?php
							}
							?>
                        <li><a class="ajax-link" href="<?php echo JText::_('TPL_SISITE_MENU_AJ_LINK'); ?>"><i 
								class="glyphicon glyphicon-info-sign"><!--class="glyphicon glyphicon-info-sign"--></i><span> <?php echo JText::_('TPL_SISITE_MENU_AJ_NAME'); ?></span></a>
							</li>
                    </ul>
					
					<!--
                    <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
					-->
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>
                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
			
			<!-- breadcrumb -->
			<jdoc:include type="modules" name="position-2" style="none" />
			<!-- /breadcrumb -->
						
						
<div class="row">
    <div class="box col-md-12">
	
			<div _class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
			<!-- Header -->
			<header class="header" role="banner">
				<!--
				<div class="header-inner clearfix">
					<a class="brand pull-left" href="<?php echo $this->baseurl; ?>">
						<?php echo $logo; ?>
						<?php if ($this->params->get('sitedescription')) : ?>
							<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
						<?php endif; ?>
					</a>
					<div class="header-search pull-right">
						<jdoc:include type="modules" name="position-0" style="none" />
					</div>
				</div>
				-->
				<div class="header-search pull-right">
					<jdoc:include type="modules" name="position-0" style="none" />
				</div>
			</header>
			<?php if ($this->countModules('position-1')) : ?>
				<nav class="navigation" role="navigation">
					<jdoc:include type="modules" name="position-1" style="none" />
				</nav>
			<?php endif; ?>
			<jdoc:include type="modules" name="banner" style="xhtml" />
			<div class="row-fluid">
				<?php if ($this->countModules('position-8')) : ?>
					<!-- Begin Sidebar -->
					<div id="sidebar" class="span3">
						<div class="sidebar-nav">
							<jdoc:include type="modules" name="position-8" style="xhtml" />
						</div>
					</div>
					<!-- End Sidebar -->
				<?php endif; ?>
				<main id="content" role="main" class="<?php echo $span; ?>">
					<!-- Begin Content -->
					<jdoc:include type="modules" name="position-3" style="xhtml" />
					
					<jdoc:include type="message" />
					
					<jdoc:include type="component" />
					
					<!-- End Content -->
					
<?php
// If user is logged
// Testar se $sys->isInvestigator()
if($sys->isGuest()){
	?>					
					
			
				
<?php
}
?>				

				<!--  Context Footer -->	
				<div class="row" style="margin-left: 0px; margin-right: 10px;">

					
					<p class="pull-right">
						<a href="#top" _id="back-top">
							<?php echo JText::_('TPL_SISITE_BACKTOTOP'); ?>
						</a>
					</p>
				</div>				
				
				
				</main>
			</div>
		</div>

		</div>
		
<?php if(1==2){ ?>
<!--span box-->		
<div class="box">
	<!--span-->
    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well">
                <h2>Investigator Context Menu</h2>
			<!--<div class="box-icon">
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>-->
            </div>
            <div class="box-content">
                <ul>
                    <li>Inserir Unidade de Investigação</li>
                    <li>Candidaturas</li>
                    <li>Os meus Projectos
                        <ul><li>Projecto 1</li>
                            <li>Projecto 2</li>
                            <li>Projecto 3</li>
                        </ul>
                    </li>
                    <li>Faucibus porta lacus fringilla vel</li>
                </ul>
            </div>
        </div>
    </div>
    <!--/span-->

	<!--span-->
    <div class="box col-md-3">
        <div class="box-inner">
            <div class="box-header well">
                <h2>Unitit Context Menu</h2>
			<!--<div class="box-icon">
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>-->
            </div>
            <div class="box-content">
                <ul>
                    <li>As minhas unidades</li>
                    <li>Os meus Investigadores</li>
                    <li>Os meus Equipamentos</li>
                    <li>Os meus Serviços
                        <ul><li>Serviço 1</li>
                            <li>Serviço 2</li>
                            <li>Serviço 3</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div><!--/span-->
</div><!--/span box-->

<?php }?>
	
	</div><!--/row-->
	
	</div>
			
			
<?php 
if(1==2) {
?>

<?php
}
?>
	<hr />
	
</div><!--/row-->
</div><!--/row-->
	
	<!-- /Body -->
	


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>
	
