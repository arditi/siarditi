<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.SISITE
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Global Objects
$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;
//print_r($params);

// ActivePortal 
$sys 	= &SysARDITI::getInstance();

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

////
// Breadcrumb improvement
// 		http://stackoverflow.com/questions/12915495/how-to-use-breadcrumbs-in-joomla-in-custom-components
// Mostra só 2 níveis dos componentes 
//
if($option!="com_content"){
	$nivel1 = null;$nivel2 = null;$module = -1;
	for($i=0;$i<4; $i++){if($option=='com_sim'.$i){$module=$i;break;}}
	//if($module>=0){	// Only for SIARDITI components
 		$nivel1 = array(JText::_( strtoupper($option) ), 'index.php?option='.$option);//JText::_( 'MENU_SIM0_LINK' ));			
		$view2	= ($view!='') ? $view : $option; //"default";
		$nivel2 = array(JText::_( strtoupper($option).'_'.strtoupper($view2) ), null);
	//}
	$pathway 	= $app->getPathway();
	// Limpa a Path
	$breadcrumb = $pathway->setPathway(array());
	// Adiciona níveis ao breadcrumb
	if($nivel1 != null){
		$pathway->addItem( $nivel1[0], $nivel1[1]);
		if($nivel2 != null){
			$pathway->addItem( $nivel2[0], $nivel2[1]);
		}
	}
}


/// TEMPLATE
if($task == "edit" || $layout == "form" ){
	$fullWidth = 1;
}
else{
	$fullWidth = 0;
}

//////////////////

$doc->setMetaData('viewport',  'width=device-width,initial-scale=1');
$doc->setMetaData('author', 'ActiveThings, Lda');
$doc->setMetaData('generator', null);

//////////////
// Frameworks
// http://joomla.stackexchange.com/questions/97/disable-script-loading-in-head
JHtml::_('jquery.framework', false);
// Disabling
JHtml::_('bootstrap.framework', false);
// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

$span = "span12";
// Logo file or site title param
$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';


//<jdoc:include type="head" />
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8">
    <!-- The Head -->	

    <!-- The styles -->
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/bootstrap-cerulean.min.css" rel="stylesheet" id="bs-css" >
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/charisma-app.css" rel="stylesheet">
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/bower_components/fullcalendar/dist/fullcalendar.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/bower_components/fullcalendar/dist/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/bower_components/chosen/chosen.min.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/bower_components/colorbox/example3/colorbox.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/bower_components/responsive-tables/responsive-tables.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css" rel='stylesheet'>
	
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/jquery.noty.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/noty_theme_default.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/elfinder.min.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/elfinder.theme.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/jquery.iphone.toggle.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/uploadify.css" rel='stylesheet'>
    <link href="<?php echo JURI::base( true ).'/templates/'.$this->template ?>/css/animate.min.css" rel='stylesheet'>

    <!-- jQuery -->
    <script src="<?php echo './templates/'.$this->template; ?>/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/favicon.ico">
	
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>

<body class="_site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '');
?>">

<script>
var baseUrl = "<?php echo $this->baseurl ?>";

</script>
<?php 
	//$sys->debug();
?>
<!-- topbar starts -->
<div class="navbar navbar-default" role="navigation">

	<div class="navbar-inner">
		<button type="button" class="navbar-toggle pull-left animated flip">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.php" style="display:none">
			<img alt="SIARDITI Logo" src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/img/icon_ideia30.png" class="hidden-xs"/>
			<span>SI.ARDITI</span></a>

		<!-- user dropdown starts -->
		
		<!-- user dropdown ends -->



	</div>
</div>



	<!-- Body -->
	<div class="body">
		<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
			<!-- Header -->
			<header class="header" role="banner">
				<div class="header-inner clearfix">
					<a class="brand pull-left" href="<?php echo $this->baseurl; ?>">
						<?php echo $logo; ?>
					</a>
					<div class="header-search pull-right" style="display:none">
						<?php // Display position-0 modules ?>
						<?php echo $doc->getBuffer('modules', 'position-0', array('style' => 'none')); ?>
					</div>
				</div>
			</header>
			<div class="navigation">
				<?php // Display position-1 modules ?>
				<?php echo $doc->getBuffer('modules', 'position-1', array('style' => 'none')); ?>
			</div>
			<!-- Banner -->
			<div class="banner">
				<?php echo $doc->getBuffer('modules', 'banner', array('style' => 'xhtml')); ?>
			</div>
			<div class="row-fluid">
				<div id="content" class="span12">
					<!-- Begin Content -->
					<h1 class="page-header"><?php echo JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'); ?></h1>
					<div class="well">
						<div class="row-fluid">
							<div class="span6">
								<p><strong><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></strong></p>
								<p><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></p>
								<ul>
									<li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
								</ul>
							</div>
							<div class="span6">
								<?php if (JModuleHelper::getModule('search')) : ?>
									<p><strong><?php echo JText::_('JERROR_LAYOUT_SEARCH'); ?></strong></p>
									<p><?php echo JText::_('JERROR_LAYOUT_SEARCH_PAGE'); ?></p>
									<?php echo $doc->getBuffer('module', 'search'); ?>
								<?php endif; ?>
								<p><?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?></p>
								<p><a href="<?php echo $this->baseurl; ?>/index.php" class="btn"><i class="icon-home"></i> <?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a></p>
							</div>
						</div>
						<hr />
						<p><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?></p>
						<blockquote>
							<span class="label label-inverse"> ></span><?php echo $this->error->getCode(); ?> <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8');?>
						</blockquote>
					</div>
					<!-- End Content -->
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- /Body -->	

	
			<!--  Context Footer -->	
			<div class="row" style="margin-left: 0px; margin-right: 10px;">
			
				<jdoc:include type="modules" name="footer" style="none" />
				
				<!--
				<p class="pull-right">
					<a href="#top" _id="back-top">
						<?php echo JText::_('TPL_SISITE_BACKTOTOP'); ?>
					</a>
				</p>
				-->
			</div>	
	
	<br /><br /><br />
	
	<jdoc:include type="modules" name="debug" style="none" />
	
	<!-- Footer -->
    <footer class="footer" role="contentinfo">
	<div class="navbar-default xnavbar-inverse navbar-fixed-bottom" >
		<div class="navbar-inner">
		<div class="container" style="width:100%; _background-color:#999999;">
			<div class="rodape" style="color:white; display:none"><p>
				<a href="<?php echo JText::_('TPL_SISITE_MENU_COPY_LINK'); ?>" target="_blank" style="color:white"> <?php echo JText::_('TPL_SISITE_MENU_COPY_NAME'); ?></a> |
				<a href="<?php echo JText::_('TPL_SISITE_MENU_TERMS_LINK'); ?>"                 style="color:white"> <?php echo JText::_('TPL_SISITE_MENU_TERMS_NAME'); ?></a> |
				<a href="<?php echo JText::_('TPL_SISITE_MENU_COOKIE_LINK'); ?>"			    style="color:white"> <?php echo JText::_('TPL_SISITE_MENU_COOKIE_NAME'); ?></a>
				<span class="pull-right"  style="color:#bbbbbb"> <a href="http://www.activethings.pt/" target="_blank" style="color:#bbbbbb">By ActiveThings</a></span>
				</p></div>
		</div>
		</div>
	</div>	
    </footer>	

<!-- external javascript -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<!-- data table plugin -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/jquery.dataTables.min.js"></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/jquery.history.js"></script>
<!-- application script for ARDITI  -->
<script src="<?php echo JURI::base( true ).'/templates/'.$this->template; ?>/js/arditi.js"></script>

	
</body>
</html>