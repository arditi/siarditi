<?php
/**
 * ActivePortal
 * SI.ARDITI Specific Configurations
 *
 * @file configuration_arditi.php
 *
 */
defined('JPATH_PLATFORM') or die;

////
// Defines
// USER
define('USER_SUPER_ADMIN', 	 	782);
define('USER_ADMIN', 	 		782);
// GRUPOS
define('GROUP_GUEST', 			 9);
define('GROUP_INVESTIGATOR', 	10);
//define('GROUP_UNITY', 	  9999);	// Investigador especial
define('GROUP_GESTORSI', 		11);
define('GROUP_MANAGERSI', 		11);
define('GROUP_AUDITORSI', 		12);
define('GROUP_SUPER_ADMIN', 	 8);


////
// JConfigAT
// Class to hold AT Configurations
//
class JConfigAT {
	
	////
	// Variáveis dependentes do ambiente
	public $runtime	 = "myhost";
	public $debug 	 = true;
	
	// Envirenements Array
	private $runtimes= array(
				"myhost" => array(
					
/* 					'mail_server' 		=> 'smtp.gmail.com',
					'mail_port' 		=> 111,
					'mail_login' 		=> 'ejardim@gmail.com',
					'mail_pass' 		=> '123qwe',
					'mail_from' 		=> 'pedro.colarejo@gmail.com', */
					
					'host' 				=> 'localhost',
					'user'				=> 'root',
					'password' 			=> '',
					'database' 			=> 'siarditi_db_v1_08-05-2015',
					'prefix' 			=> '',
					
					//
					'path_images' 		=> 'D:\Mys\wamp_www\CV2\images',
					'url_images' 		=> 'http://localhost/',
					//
					'dateFormat' 		=> 'Y-m-d H:i:s',
					'timezone'			=> 'Europe/Lisbon',
					
					'PHPExcelBug' 		=> false
				)
			);
				
	
////
////
//// NO NEED TO CHANGE BELOW !!!!
////
////
	/**
	 * Constructor
	 * @param   array  $options  Optional parameters.
	 * @since   11.1
	 */
	public function __construct($options = array()){
		if($this->debug)
			defined('JDEBUG') ? null : define('JDEBUG', 1);
		
		$option = $this->getOptions();
		//echo $option['timezone'];
		date_default_timezone_set($option['timezone']);//'America/Los_Angeles');
	/*	$script_tz = date_default_timezone_get();
		if (strcmp($script_tz, ini_get('date.timezone'))){
			echo 'Script timezone differs from ini-set timezone.';
		} else {
			echo 'Script timezone and ini-set timezone match.';
		}		
		die("s");*/
	}
	/**
	 * getOptions
	 * @param   array  $options  Optional parameters.
	 * @since   11.1
	 */
	public function getOptions($env=null){
		$env = ($env==null) ? $this->runtime : $env;
		return isset($this->runtimes[$env]) ? $this->runtimes[$env] : array();
	}
	/**
	 * getOption
	 * @param   array  $options  Optional parameters.
	 * @since   11.1
	 */
	public function getOption($key, $key2=null){
		$env = $this->runtime;
		if($key2==null)	return isset($this->runtimes[$env][$key]) 			? $this->runtimes[$env][$key] 			: "";
		else 			return isset($this->runtimes[$env][$key][$key2]) 	? $this->runtimes[$env][$key][$key2] 	: "";
	}
	/**
	 * get
	 * @param   array  $options  Optional parameters.
	 * @since   11.1
	 */
	public function get($key, $defval=null){
		$options = &$this->getOptions();
		return isset($options[$key]) ? $options[$key] : $defval;
	}	
}
?>