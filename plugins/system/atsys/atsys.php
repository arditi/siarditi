<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.atsys
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

////
// ActiveThings Library Include
jimport('activethings.SysARDITI');

/**
 * ATSys  Plugin.
 *
 * @since  1.6
 * @deprecate  4.0  Obsolete
 */
class PlgSystemATSys extends JPlugin{
	/**
	 * Application object.
	 */
	protected $app;
	
	/**
	 * Constructor.
	 *
	 * @access protected
	 * @param object $subject The object to observe
	 * @param array   $config  An array that holds the plugin configuration
	 * @since 1.0
	 */
	public function __construct( &$subject, $config ){
		parent::__construct( $subject, $config );	 
		// Do some extra initialisation in this constructor if required
		// Get the application
		if (!$this->app){// Devemos herdar esta variavel, mas não está documentado 
			$this->app = JFactory::getApplication();
		}
		
		/*
		To get Plugin Params
$plugin = JPluginHelper::getPlugin('system', 'sslredirect');
$params = new JRegistry($plugin->params);
echo $params->get('param_name','default_value');
		*/
	}
	
	/**
	 * After initialise.
	 *
	 * @return  void
	 * @since   1.6
	 * @deprecate  4.0  Obsolete
	 */
	public function onAfterInitialise()
	{
		$sys = &SysARDITI::getInstance();
		// Registamos a navegação deste user
		$sys->OnEveryRequestStart();
	}
	
	/**
	* Do something onAfterRoute
	*/
	function onAfterRoute()
	{
	}
	 
	/**
	* Do something onAfterDispatch
	*/
	function onAfterDispatch()
	{		
	}
	/**
	* Do something onAfterRender
	*/
	function onAfterRender()
	{
		$sys = &SysARDITI::getInstance();
		// Registamos o fim da navegação
		$sys->OnEveryRequestStop();
	}
}