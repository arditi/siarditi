<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  User.joomla
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

////
// ActiveThings Library Include
jimport('activethings.SysARDITI');
//use Joomla\Registry\Registry;


/**
 * Joomla User plugin
 *
 * @since  1.5
 */
class PlgUserATUser extends JPlugin
{
	/**
	 * Application object
	 *
	 * @var    JApplicationCms
	 * @since  3.2
	 */
	protected $app;
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  3.1
	 */
	protected $autoloadLanguage = false;
	
	/**
	 * Constructor.
	 *
	 * @access protected
	 * @param object $subject The object to observe
	 * @param array   $config  An array that holds the plugin configuration
	 * @since 1.0
	 */
	public function __construct( &$subject, $config ){
		parent::__construct( $subject, $config );
	}
	
	/**
	 * Remove all sessions for the user name
	 * Method is called after user data is deleted from the database
	 *
	 * @param   array    $user     Holds the user data
	 * @param   boolean  $success  True if user was successfully stored in the database
	 * @param   string   $msg      Message
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	public function onUserAfterDelete($user, $success, $msg)
	{
		SysM0::SetLogAction(LogActionTypes::USER_DELETED, $user['name'], $user['email']);
		if (!$success){
			SysM0::SetLogAction(LogActionTypes::ERROR, $str);
			//return false;
		}
		//return true;
		return false;
	}

	/**
	 * Utility method to act on a user after it has been saved.
	 *
	 * This method sends a registration email to new users created in the backend.
	 *
	 * @param   array    $user     Holds the new user data.
	 * @param   boolean  $isnew    True if a new user is stored.
	 * @param   boolean  $success  True if user was successfully stored in the database.
	 * @param   string   $msg      Message.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function onUserAfterSave($user, $isnew, $success, $msg)
	{
		if ($isnew){
			SysM0::SetLogAction(LogActionTypes::USER_CREATED, $user['email'], $user['id']);
		}
		else{
			// Existing user - nothing to do...yet.
		}
	}

	/**
	 * This method should handle any login logic and report back to the subject
	 *
	 * @param   array  $user     Holds the user data
	 * @param   array  $options  Array holding options (remember, autoregister, group)
	 *
	 * @return  boolean  True on success
	 *
	 * @since   1.5
	 */
	public function onUserLogin($user, $options = array())
	{
		SysM0::SetLogAction(LogActionTypes::LOGIN, $user['email'], $user['id']);
		return false;
	}

	/**
	 * This method should handle any logout logic and report back to the subject
	 *
	 * @param   array  $user     Holds the user data.
	 * @param   array  $options  Array holding options (client, ...).
	 *
	 * @return  object  True on success
	 *
	 * @since   1.5
	 */
	public function onUserLogout($user, $options = array())
	{
		SysM0::SetLogAction(LogActionTypes::LOGOUT, $user['email'], $user['id']);
		return false;
	}
}
