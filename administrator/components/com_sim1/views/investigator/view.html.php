<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Investigator View
 *
 * @since  0.0.1
 */
class SIM1ViewInvestigator extends JViewLegacy
{
	/**
	 * View form
	 *
	 * @var         form
	 */
	protected $form = null;

	/**
	 * Display the Hello World view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		// Get the Data
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->script = $this->get('Script');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}


                // Only set the toolbar if not modal
                if ($this->getLayout() !== 'modal_qual' &&
                    $this->getLayout() !== 'modal_proj' &&
                    $this->getLayout() !== 'modal_pub') {
                    // Set the submenu
                    SIM1Helper::addSubmenu('investigators');
                    $this->addToolBar();
                }

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		$input = JFactory::getApplication()->input;

		// Hide Joomla Administrator Main menu
		$input->set('hidemainmenu', true);

		$isNew = ($this->item->id_person == 0);

		if ($isNew)
		{
			$title = JText::_('com_sim1_MANAGER_INVESTIGATOR_NEW');
		}
		else
		{
			$title = JText::_('com_sim1_MANAGER_INVESTIGATOR_EDIT');
		}

		JToolBarHelper::title($title, 'investigator');
		JToolBarHelper::apply('investigator.apply');
		JToolBarHelper::save('investigator.save');
		JToolBarHelper::cancel(
			'investigator.cancel',
			$isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE'
		);
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$isNew = ($this->item->id_person < 1);
		$document = JFactory::getDocument();
		$document->setTitle($isNew ? JText::_('com_sim1_INVESTIGATOR_CREATING') :
                JText::_('com_sim1_INVESTIGATOR_EDITING'));
		$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "/administrator/components/com_sim1"
		                                  . "/views/investigator/submitbutton.js");
		JText::script('com_sim1_INVESTIGATOR_ERROR_UNACCEPTABLE');
	}
}
