<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

$function = JFactory::getApplication()->input->getCmd('function');
$id_publication = JFactory::getApplication()->input->getCmd('id_publication');
if (isset($id_publication) && $id_publication>0)
    $publication = new Publication($id_publication);
?>
<div class="row-fluid" style="width:500">
<form action="<?php echo $this->action; ?>" method="post" name="modalForm" id="modalForm">
    <div class="control-group"><?php //echo $this->escape($function)?>
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_TITLE_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="title" name="title" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->title : '') ?>" size="35" />'
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_AUTHORS_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="authors" name="authors" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->authors : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_PAGES_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="pages" name="pages" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->pages : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_AREA_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="area" name="area" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->area : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_ISSN_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="issn" name="issn" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->issn : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_PUBLISHER_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="publisher" name="publisher" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->publisher : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_DATE_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="date" name="date" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->date : '') ?>" size="35" />
       </div>
     </div>    
    
     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_DOCUMENT_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="doc" name="doc" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->document : '') ?>" size="35" />
       </div>
     </div>    

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_LINK_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="link" name="link" value="<?php echo ((isset($publication) && $id_publication>0)? $publication->link : '') ?>" size="35" />
       </div>
     </div>    

    <div>
        <button class="btn btn-small"
                onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>(
                            document.id('title').value,
                            document.id('authors').value,
                            document.id('pages').value,
                            document.id('area').value,
                            document.id('issn').value,
                            document.id('publisher').value,
                            document.id('date').value,
                            document.id('doc').value,
                            document.id('link').value);">OK</button>
        <input type="hidden" id="id_publication" name="id_publication" value="<?php echo ((isset($publication) && $id_publication>0)? $id_publication: '') ?>" size="35" />
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
</div>
