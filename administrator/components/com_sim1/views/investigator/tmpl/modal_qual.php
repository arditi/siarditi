<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

$function = JFactory::getApplication()->input->getCmd('function');
$id_qualification = JFactory::getApplication()->input->getCmd('id_qualification');
if (isset($id_qualification) && $id_qualification>0)
    $qualification = new Qualification($id_qualification);
?>
<div class="row-fluid" style="width:500">
<form action="<?php echo $this->action; ?>" method="post" name="modalForm" id="modalForm">
    <div class="control-group"><?php //echo $this->escape($function)?>
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_QUALIFICATION_NAME_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="name" name="name" value="<?php echo ((isset($qualification) && $id_qualification>0)? $qualification->name : '') ?>" size="35" />'
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_CERTIFICATION_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="certification" name="certification" value="<?php echo ((isset($qualification) && $id_qualification>0)? $qualification->certification : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_CERTIFICATION_ENTITY_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="certification_entity" name="certification_entity" value="<?php echo ((isset($qualification) && $id_qualification>0)? $qualification->certification_entity : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_CERTIFICATION_GRADE_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="grade" name="grade" value="<?php echo ((isset($qualification) && $id_qualification>0)? $qualification->grade : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_CERTIFICATION_DEGREE_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="degree" name="degree" value="<?php echo ((isset($qualification) && $id_qualification>0)? $qualification->degree : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_CERTIFICATE_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="certificate" name="certificate" value="<?php echo ((isset($qualification) && $id_qualification>0)? $qualification->certificate : '') ?>" size="35" />
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_INVESTIGATOR_CERTIFICATION_OBS_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="obs" name="obs" value="<?php echo ((isset($qualification) && $id_qualification>0)? $qualification->obs : '') ?>" size="35" />
       </div>
     </div>    
    
    <div>
        <button class="btn btn-small"
                onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>(
                            document.id('name').value,
                            document.id('certification').value,
                            document.id('certification_entity').value,
                            document.id('certificate').value,
                            document.id('grade').value,
                            document.id('degree').value,
                            document.id('obs').value);">OK</button>
        <input type="hidden" id="id_qualification" name="id_qualification" value="<?php echo ((isset($qualification) && $id_qualification>0)? $id_qualification: '') ?>" size="35" />
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
</div>
