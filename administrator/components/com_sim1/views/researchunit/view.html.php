<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * ResearchUnit View
 *
 * @since  0.0.1
 */
class SIM1ViewResearchUnit extends JViewLegacy
{
	/**
	 * View form
	 *
	 * @var         form
	 */
	protected $form = null;

	/**
	 * Display the Hello World view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		// Get the Data
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->script = $this->get('Script');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}


                // Only set the toolbar if not modal
                if ($this->getLayout() !== 'modal_investigator') {
                    // Set the submenu
                    SIM1Helper::addSubmenu('researchunits');
                    $this->addToolBar();
                }

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		$input = JFactory::getApplication()->input;

		// Hide Joomla Administrator Main menu
		$input->set('hidemainmenu', true);

		$isNew = ($this->item->id_entity == 0);

		if ($isNew)
		{
			$title = JText::_('com_sim1_MANAGER_RESEARCHUNIT_NEW');
		}
		else
		{
			$title = JText::_('com_sim1_MANAGER_RESEARCHUNIT_EDIT');
		}

		JToolBarHelper::title($title, 'researchunit');
		JToolBarHelper::apply('researchunit.apply');
		JToolBarHelper::save('researchunit.save');
		JToolBarHelper::cancel(
			'researchunit.cancel',
			$isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE'
		);
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$isNew = ($this->item->id_entity < 1);
		$document = JFactory::getDocument();
		$document->setTitle($isNew ? JText::_('com_sim1_RESEARCHUNIT_CREATING') :
                JText::_('com_sim1_RESEARCHUNIT_EDITING'));
		$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "/administrator/components/com_sim1"
		                                  . "/views/researchunit/submitbutton.js");
		JText::script('com_sim1_RESEARCHUNIT_ERROR_UNACCEPTABLE');
	}
}
