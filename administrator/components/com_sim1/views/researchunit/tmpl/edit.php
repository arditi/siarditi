<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.formvalidation');
if ($this->item->id_entity==0)
    $route = 'index.php?option=com_sim1&layout=edit';
else
    $route = 'index.php?option=com_sim1&layout=edit&id_entity=' . (int) $this->item->id_entity;
    
// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
?>
<form action="<?php echo JRoute::_($route); ?>"
    method="post" name="adminForm" id="adminForm" class="form-validate">
	<div class="form-horizontal">
            <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'COM_SIM1_RESEARCHUNIT_DEFAULT_LABEL')); ?>
            <?php foreach ($this->form->getFieldsets() as $name => $fieldset): ?>
                <?php echo JHtml::_('bootstrap.addTab', 'myTab', $fieldset->label, JText::_($fieldset->label, true)); ?>
                    <fieldset class="adminform">
                        <legend><?php echo JText::_($fieldset->label); ?></legend>
                        <div class="row-fluid">
                            <div class="span9">
				<div class="row-fluid form-horizontal-desktop">
                                    <div id="control-groups" class="span9">
                                        <?php foreach ($this->form->getFieldset($name) as $field): ?>
                                            <?php if (!$field->hidden) {?>
                                                <div class="control-group">
                                                    <div class="control-label"><?php echo $field->label; ?></div>
                                                    <div class="controls"><?php echo $field->input; ?></div>
                                                </div>
                                            <?php } else { ?>
                                                <div><?php echo $field->input; ?></div>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php endforeach; ?>
            <?php echo JHtml::_('bootstrap.endTabSet'); ?>
        </div>
        <input type="hidden" name="task" value="researchunit.edit" />
	<?php echo JHtml::_('form.token'); ?>
</form>
