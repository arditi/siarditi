<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

$function = JFactory::getApplication()->input->getCmd('function');
$name = JFactory::getApplication()->input->getCmd('new_name');
$id_service = JFactory::getApplication()->input->getCmd('id_service');
$id_entity = JFactory::getApplication()->input->getCmd('id_entity');
$id_service_type = JFactory::getApplication()->input->getCmd('$id_service_type');

if($id_service>0 && $id_entity>0) {
    $service = new Service($id_service);
    $name = $service->name;
}
?>

<div class="row-fluid" style="width:500">
<form action="<?php echo $this->action; ?>" method="post" name="modalForm" id="modalForm">
    <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_NAME_LABEL') ?></div>
       <div class="controls">
            <input type="text" readonly id="name" name="name" value="<?php echo $name ?>" size="35" />'
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_DESCRIPTION_LABEL') ?></div>
       <div class="controls">
            <textarea id="description" name="description" cols="70" rows="5" ><?php echo ((isset($service))? $service->description : '') ?></textarea>
       </div>
     </div>    

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_PROCEDURES_LABEL') ?></div>
       <div class="controls">
            <textarea id="procedures" name="procedures" cols="70" rows="5" ><?php echo ((isset($service))? $service->procedures : '') ?></textarea>
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_RESTRICTIONS_LABEL') ?></div>
       <div class="controls">
            <textarea id="restrictions" name="restrictions" cols="70" rows="5" ><?php echo ((isset($service))? $service->restrictions : '') ?></textarea>
       </div>
     </div>    

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_COSTS_LABEL') ?></div>
       <div class="controls">       
            <textarea id="costs" name="costs" cols="70" rows="5" ><?php echo ((isset($service))? $service->costs : '') ?></textarea>
       </div>
     </div>

    <div>
        <button class="btn btn-small"
                onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>(
                            document.id('description').value,
                            document.id('procedures').value,
                            document.id('restrictions').value,
                            document.id('costs').value);">OK</button>
        <input type="hidden" id="bond_type" name="id_services_type" value="<?php echo ((isset($service))? $service->id_service_type : '') ?>" size="35" />
        <input type="hidden" id="id_service" name="id_service" value="<?php echo ((isset($service))? $id_service: '') ?>" size="35" />
        <input type="hidden" id="id_entity" name="id_entity" value="<?php echo ((isset($service))? $id_entity: '') ?>" size="35" />
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>

</form>
</div>
