<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

$function = JFactory::getApplication()->input->getCmd('function');
$name = JFactory::getApplication()->input->getCmd('new_name');
$id_equipment = JFactory::getApplication()->input->getCmd('id_equipment');
$id_entity = JFactory::getApplication()->input->getCmd('id_entity');
$id_equipment_type = JFactory::getApplication()->input->getCmd('id_equipment_type');

if($id_equipment>0 && $id_entity>0) {
    $equipment = new Equipment($id_equipment);
    $name = $equipment->name;
}
?>

<div class="row-fluid" style="width:500">
<form action="<?php echo $this->action; ?>" method="post" name="modalForm" id="modalForm">
    <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_EQUIPMENT_NAME_LABEL') ?></div>
       <div class="controls">
            <input type="text" readonly id="name" name="name" value="<?php echo $name ?>" size="35" />'
       </div>
     </div>

    <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_EQUIPMENT_QUANTITY_LABEL') ?></div>
       <div class="controls">
            <input type="text" id="quantity" name="quantity" value="<?php echo ((isset($equipment))? $equipment->quantity  : ''); ?>" size="10" />'
       </div>
     </div>

    <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_EQUIPMENT_DESCRIPTION_LABEL') ?></div>
       <div class="controls">
            <textarea id="description" name="description" cols="70" rows="5" ><?php echo ((isset($equipment))? $equipment->description : '') ?></textarea>
       </div>
     </div>    

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_EQUIPMENT_PROCEDURES_LABEL') ?></div>
       <div class="controls">
            <textarea id="procedures" name="procedures" cols="70" rows="5" ><?php echo ((isset($equipment))? $equipment->procedures : '') ?></textarea>
       </div>
     </div>

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_EQUIPMENT_RESTRICTIONS_LABEL') ?></div>
       <div class="controls">
            <textarea id="restrictions" name="restrictions" cols="70" rows="5" ><?php echo ((isset($equipment))? $equipment->restrictions : '') ?></textarea>
       </div>
     </div>    

     <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_EQUIPMENT_USAGE_LABEL') ?></div>
       <div class="controls">
            <textarea id="usage_conditions" name="usage_conditions" cols="70" rows="5" ><?php echo ((isset($equipment))? $equipment->usage_conditions : '') ?></textarea>
       </div>
     </div>    

    <div class="control-group">
       <div class="control-label"><?php echo JText::_('COM_SIM1_RESEARCHUNIT_EQUIPMENT_COSTS_LABEL') ?></div>
       <div class="controls">       
            <textarea id="costs" name="costs" cols="70" rows="5" ><?php echo ((isset($equipment))? $equipment->costs : '') ?></textarea>
       </div>
     </div>

    <div>
        <button class="btn btn-small"
                onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>(
                            document.id('description').value,
                            document.id('procedures').value,
                            document.id('restrictions').value,
                            document.id('usage_conditions').value,
                            document.id('quantity').value,
                            document.id('costs').value);">OK</button>
        <input type="hidden" id="id_equipment_type" name="id_equipments_type" value="<?php echo ((isset($equipment))? $equipment->id_equipment_type : '') ?>" />
        <input type="hidden" id="id_equipment" name="id_equipment" value="<?php echo ((isset($equipment))? $id_equipment: '') ?>" />
        <input type="hidden" id="id_entity" name="id_entity" value="<?php echo ((isset($equipment))? $id_entity: '') ?>" />
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>

</form>
</div>
