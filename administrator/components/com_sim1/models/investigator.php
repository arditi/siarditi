<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Load modal behavior
JHtml::_('behavior.modal','a.modal');

use Joomla\Registry\Registry;
/**
 * Investigator Model
 *
 * @since  0.0.1
 */
class SIM1ModelInvestigator extends JModelAdmin
{
	/**
	 *
	 */
	public function __construct($config = array()){
		parent::__construct($config);
		$sys = &SysARDITI::getInstance();
		$db = &$sys->getDBO();
		parent::setDbo($db);
        }

        
        /**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'Investigator', $prefix = 'SIM1Table', $config = array())
	{
            if (!array_key_exists('dbo', $config)) {	
                    $config['dbo'] = &$this->getDBO();
            }		

            return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_sim1.investigator',
			'investigator',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}
	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_sim1/models/forms/helloworld.js';
	}
	
	/**
	 * Method to get a form object.
	 *
	 * @param   string   $name     The name of the form.
	 * @param   string   $source   The form source. Can be XML string if file flag is set to false.
	 * @param   array    $options  Optional array of options for the form creation.
	 * @param   boolean  $clear    Optional argument to force load a new form.
	 * @param   string   $xpath    An optional xpath to search for the fields.
	 *
	 * @return  mixed  JForm object on success, False on error.
	 *
	 * @see     JForm
	 * @since   12.2
	 */
	protected function loadForm($name, $source = null, $options = array(), $clear = false, $xpath = false)
	{
		// Handle the optional arguments.
		$options['control'] = JArrayHelper::getValue($options, 'control', false);

		// Create a signature hash.
		$hash = md5($source . serialize($options));

		// Check if we can use a previously loaded form.
		if (isset($this->_forms[$hash]) && !$clear)
		{
			return $this->_forms[$hash];
		}

		// Get the form.
		JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT . '/models/fields');
		JForm::addFormPath(JPATH_COMPONENT . '/model/form');
		JForm::addFieldPath(JPATH_COMPONENT . '/model/field');

		try
		{
			$form = JForm::getInstance($name, $source, $options, false, $xpath);

			if (isset($options['load_data']) && $options['load_data'])
			{
				// Get the data for the form.
				$data = $this->loadFormData();
			}
			else
			{
				$data = array();
			}

			// Allow for additional modification of the form, and events to be triggered.
			// We pass the data because plugins may require it.
			$this->preprocessForm($form, $data);

			// Load the data into the form after the plugins have operated.
			$form->bind($data);
		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());

			return false;
		}

		// Store the form for later.
		$this->_forms[$hash] = $form;

		return $form;
	}
        
        /**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
        
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_sim1.edit.investigator.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}
       
     
        
	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function getItem($pk = null)
	{
		$pk = (!empty($pk)) ? $pk : (int) $this->getState($this->getName() . '.id');
		$table = $this->getTable();

                $table_jusers = $this->getTable('Jusers', 'SIM1Table', array());
                
                $table_cv = $this->getTable('Cv', 'SIM1Table', array());
                $table_qualifications = $this->getTable('Qualifications', 'SIM1Table', array());
                $table_projects = $this->getTable('Projects', 'SIM1Table', array());
                $table_publications = $this->getTable('Publications', 'SIM1Table', array());
                
                $properties_qualifications = array();
                $properties_projects = array();
                $properties_publications= array();
                
                $investigator = new Person($pk);
                
		if ($pk > 0)
		{
                    // Attempt to load the row.
                    $return = ($table->load($pk) && $table_jusers->load($investigator->id_user));

                    // Check for a table object error.
                    if ($return === false && $table->getError())
                    {
                            $this->setError($table->getError());
                            return false;
                    }

                    if ($investigator->cv->id_cv > 0) {
                        $table_cv->load($investigator->cv->id_cv);

                        $properties_qualifications['id_cv']=$investigator->cv->id_cv;
                        $properties_projects['id_cv']=$investigator->cv->id_cv;
                        $properties_publications['id_cv']=$investigator->cv->id_cv;

                        foreach ($investigator->cv->qualifications as $i => $row) {
                            if ($row->id_qualification > 0) {
                                $table_qualifications->load($row->id_qualification);
                                $properties_qualifications[$i]=$table_qualifications->getProperties();
                            }
                        }

                        foreach ($investigator->cv->projects as $i => $row) {
                            if ($row->id_project > 0) {
                                $table_projects->load($row->id_project);
                                $properties_projects[$i]=$table_projects->getProperties();
                            }
                        }

                        foreach ($investigator->cv->publications as $i => $row) {
                            if ($row->id_publication > 0) {
                                $table_publications->load($row->id_publication);
                                $properties_publications[$i]=$table_publications->getProperties();
                            }
                        }
                    }
		}

		// Convert to the JObject before adding other data.
		$properties = $table->getProperties();
		$properties_user = $table_jusers->getProperties();
		$properties_cv = $table_cv->getProperties();
                
                $item = JArrayHelper::toObject($properties, 'JObject');
                $item->id_user = $investigator->id_user;
                $item->password = $properties_user['password'];
                $item->cv = $properties_cv;
                $item->qualifications = $properties_qualifications;
                $item->projects = $properties_projects;
                $item->publications = $properties_publications;

                if (property_exists($item, 'params'))
		{
			$registry = new Registry;
			$registry->loadString($item->params);
			$item->params = $registry->toArray();
		}

		return $item;
	}
        
	/**
	 * Method to check if it's OK to delete a message. Overwrites JModelAdmin::canDelete
	 */
	protected function canDelete($record)
	{
		if( !empty( $record->id_person ) )
		{
			return JFactory::getUser()->authorise( "core.delete", "com_sim1.investigator." . $record->id_person );
		}
	}
	
	/**
	 * Method to validate the form data.
	 *
	 * @return  boolean  True on success.
	 */
	public function validate(JForm $form, array $data, string $group = null) {

                return parent::validate($form, $data, $group);
	}
        
	/**
	 * Method to delete one or more records.
	 *
	 * @param   array  &$pks  An array of record primary keys.
	 *
	 * @return  boolean  True if successful, false if an error occurs.
	 *
	 * @since   12.2
	 */
	public function delete(&$pks)
	{
		//$dispatcher = JEventDispatcher::getInstance();
		$pks = (array) $pks;
		$table = $this->getTable();
                
                $table_cv = $this->getTable('Cv', 'SIM1Table', array());
                $table_qualifications = $this->getTable('Qualifications', 'SIM1Table', array());
                $table_cvqualifications = $this->getTable('CvQualifications', 'SIM1Table', array());
                $table_projects = $this->getTable('Projects', 'SIM1Table', array());
                $table_cvprojects = $this->getTable('CvProjects', 'SIM1Table', array());
                $table_publications = $this->getTable('Publications', 'SIM1Table', array());
                $table_cvpublications = $this->getTable('CvPublications', 'SIM1Table', array());
               
		// Iterate the items to delete each one.
		foreach ($pks as $i => $pk)
		{
			if ($table->load($pk))
			{
                                $investigator = new Person($pk);

                                if ($investigator->cv->id_cv > 0) {

                                    foreach ($investigator->cv->qualifications as $i => $row) {
                                        if ($row->id_qualification > 0) {
                                            $pkdel = array();
                                            $pkdel['id_qualification']=$row->id_qualification;
                                            $pkdel['id_cv']=$investigator->cv->id_cv;
                                            $table_cvqualifications->delete($pkdel);
                                            $table_qualifications->delete($row->id_qualification);
                                        }
                                    }

                                    foreach ($investigator->cv->projects as $i => $row) {
                                        if ($row->id_project > 0) {
                                            $pkdel = array();
                                            $pkdel['id_project']=$row->id_project;
                                            $pkdel['id_cv']=$investigator->cv->id_cv;
                                            $table_cvprojects->delete($pkdel);
                                            $table_projects->delete($row->id_project);
                                        }
                                    }

                                    foreach ($investigator->cv->publications as $i => $row) {
                                        if ($row->id_publication > 0) {
                                            $pkdel = array();
                                            $pkdel['id_publication']=$row->id_publication;
                                            $pkdel['id_cv']=$investigator->cv->id_cv;
                                            $table_cvpublications->delete($pkdel);
                                            $table_publications->delete($row->id_publication);
                                        }
                                    }
                                    
                                    $table_cv->delete($investigator->cv->id_cv);
                                }
                        }
		}

                return parent::delete($pks);
        }
        
                /**
	 * Method to save the form data.
	 * @param   array  $data  The form data.
	 * @return  boolean  True on success.
	 */
        public function save($data)
        {
            $dispatcher = JEventDispatcher::getInstance();
           
            $table_jusers = $this->getTable('Jusers', 'SIM1Table', array());
            $table_cv = $this->getTable('Cv', 'SIM1Table', array());
            $table_qualifications = $this->getTable('Qualifications', 'SIM1Table', array());
            $table_cvqualifications = $this->getTable('CvQualifications', 'SIM1Table', array());
            $table_projects = $this->getTable('Projects', 'SIM1Table', array());
            $table_cvprojects = $this->getTable('CvProjects', 'SIM1Table', array());
            $table_publications = $this->getTable('Publications', 'SIM1Table', array());
            $table_cvpublications = $this->getTable('CvPublications', 'SIM1Table', array());

            // Store the data.
            $data['password']=JUserHelper::hashPassword($data['password']);
            $data['id'] = $data['id_user'];

            if (!$table_jusers->save($data))
            {
                $this->setError($user->getError());
                return false;
            }
           
            // Chama o parent para gravar a tabela persons e devolve o estado
            // que indica se criou novo registo ou fez update
            parent::save($data);
            $pk = (!empty($data[$key])) ? $data[$key] : (int) $this->getState($this->getName() . '.id');

            if ($data['cv']['id_cv']=="")
               $data['cv']['id_cv']=null;
           
            if ($data['cv']['id_person']=="")
                $data['cv']['id_person']=$pk;

            // Store the data.
            if (!$table_cv->save($data['cv']))
            {
                $this->setError($user->getError());
                return false;
            }

            if ($data['qualifications']['id_cv']=="")
                $data['qualifications']['id_cv']=$table_cv->id_cv;

            if ($data['projects']['id_cv']=="")
                 $data['projects']['id_cv']=$table_cv->id_cv;

            if ($data['publications']['id_cv']=="")
                 $data['publications']['id_cv']=$table_cv->id_cv;

            foreach ($data['qualifications'] as $i => $row) {
                if (is_array($row)) {
                    if (!$row['del_id_qualification']>0) {
                            $table_qualifications->save($row);
                            if (!$row['id_qualification']>0) {
                                $item_cvqualifications = $table_qualifications->getProperties();
                                $item_cvqualifications['id_cv']=$table_cv->id_cv;
                                $table_cvqualifications->save($item_cvqualifications);
                            }
                    } else {
                        $pk = array();
                        $pk['id_qualification']=$row['del_id_qualification'];
                        $pk['id_cv']=$row['del_id_cv'];

                        $table_cvqualifications->delete($pk);
                        $table_qualifications->delete($row['del_id_qualification']);
                    }
                }
            }
            
           foreach ($data['projects'] as $i => $row) {
                if (is_array($row)) {
                    if (!$row['del_id_project']>0) {
                        $table_projects->save($row);
                        if (!$row['id_project']>0) {
                            $item_cvprojects = $table_projects->getProperties();
                            $item_cvprojects['id_cv']=$table_cv->id_cv;
                            $table_cvprojects->save($item_cvprojects);
                        }
                    } else {
                        $pk = array();
                        $pk['id_project']=$row['del_id_project'];
                        $pk['id_cv']=$row['del_id_cv'];

                        $table_cvprojects->delete($pk);
                        $table_projects->delete($row['del_id_project']);
                    }
                }
            }

           foreach ($data['publications'] as $i => $row) {
                if (is_array($row)) {
                    if (!$row['del_id_publication']>0) {
                        $table_publications->save($row);
                        if (!$row['id_publication']>0) {
                            $item_cvpublications = $table_publications->getProperties();
                            $item_cvpublications['id_cv']=$table_cv->id_cv;
                            $table_cvpublications->save($item_cvpublications);
                        }
                    } else {
                        $pk = array();
                        $pk['id_publication']=$row['del_id_publication'];
                        $pk['id_cv']=$row['del_id_cv'];

                        $table_cvpublications->delete($pk);
                        $table_publications->delete($row['del_id_publication']);
                    }
                }
            }

            return true;
        }
}


