 <?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
 defined('_JEXEC') or die('Restricted access');
 
 // Load modal behavior
JHtml::_('behavior.modal','a.modal');
jimport('joomla.form.formfield');
 
 /**
  * Book form field class
  */
 class JFormFieldModal_UnitEquipments extends JFormField
 {
     
        /**
           * Method to get the field input markup
           */
          protected function getInput()
          {
                // Load modal behavior
                //JHtml::_('behavior.modal', 'a.modal');

                // Setup variables for display
                $equipments = array();
                $html = array();
                $html[] = '<div id="control-equipments">';

                if ($this->form->getValue('id_entity')) {
                    // Create a new query object.
                    $sys = &SysARDITI::getInstance();
                    $db = &$sys->getDBO();
                    $query = $db->getQuery(true);
                    $query->select('*')
                        ->from($db->quoteName('equipments'))
                        ->where($db->quoteName('equipments.id_entity') . ' = '.$this->form->getValue('id_entity'));
                    $db->setQuery($query);
                    $equipments = $db->loadObjectList();
 
                    if (!$equipments) {
                            JError::raiseWarning(500, $db->getErrorMsg());
                    }

                }
                
                $script = array();
                $k=0;
                $script[] = 'var sequence=0;';
                $script[] = 'function registerFunction(functionBody) { '
                              . '"use strict"; '
                              . 'var script = document.createElement("script"); '
                              . 'script.innerHTML = "function " + functionBody; '
                              . 'document.head.appendChild(script);}';
                if (!$equipments) {
                      $html[] = '  <div id="equipmentNew" class="control-label btn-wrapper">';
                      $html[] = '      <a class="btn btn-small"  onclick="sequence=-1; equipmentNew();"><span class="icon-new icon-white"></span>New</a>';
                      $html[] = '  </div>';
                }
                
                foreach ($equipments as $i => $row) {
//                    $equipmentInEntity = new PersonInEntity($row->id_equipment, $row->id_entity);

                    // Build the script
                    $script[] = 'registerFunction("equipmentOk_'.$i.'(description, procedures, restrictions, usage_conditions, quantity, costs){ '
                                        . 'document.id(\"'.$this->getSequencedId("description", "description", $i).'\").value = description; '
                                        . 'document.id(\"'.$this->getSequencedId("procedures", "procedures", $i).'\").value = procedures; '
                                        . 'document.id(\"'.$this->getSequencedId("restrictions", "restrictions", $i).'\").value = restrictions; '
                                        . 'document.id(\"'.$this->getSequencedId("usage_conditions", "usage_conditions", $i).'\").value = usage_conditions; '
                                        . 'document.id(\"'.$this->getSequencedId("quantity", "quantity", $i).'\").value = quantity; '
                                        . 'document.id(\"'.$this->getSequencedId("costs", "costs", $i).'\").value = costs; '
                                        . 'SqueezeBox.close(); }");';
                    $script[] = 'sequence='.$i.';';

                    $link = 'index.php?option=com_sim1&amp;view=researchunit&amp;layout=modal_equipment&amp;tmpl=component&amp;id_entity='.$row->id_entity.'&amp;id_equipment='.$row->id_equipment.'&amp;function=equipmentOk_'.$i;

                    $html[] = '<div id="group_'.$i.'_equipment_name" class="control-group">';
                    $html[] = '  <div class="control-label">Nº '.($i+1).' - '.JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_NAME_LABEL').'</div>';
                    $html[] = '  <div class="controls">';
                    $html[] = '     <input type="text" id="'.$this->getSequencedId("name", "name", $i).'" name="'.$this->getSequencedName("name", $i).'" value="'.$row->name.'" size="35" />';
                    $html[] = $this->showSelectEquipments($i, $row->id_equipments_type);
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_equipment", "id_equipment", $i).'" name="'.$this->getSequencedName("id_equipment", $i).'" value="'.$row->id_equipment.'" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_entity", "id_entity", $i).'" name="'.$this->getSequencedName("id_entity", $i).'" value="'.$row->id_entity.'" />';
                    $html[] = '  </div>';
                    $html[] = '  <div class="controls btn-wrapper">';
                    $html[] = '      <a id="edit-modal" class="btn btn-small modal"  onclick="SqueezeBox.setContent(\'iframe\',\''.$link.'\');"><span class="icon-edit"></span>Edit</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="equipmentNew();"><span class="icon-new icon-white"></span>New</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="equipmentDel('.$i.');"><span class="icon-delete"></span>Del</a>';
                    $html[] = '  </div>';
                    $html[] = '</div>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("description", "description", $i).'" name="'.$this->getSequencedName("description", $i).'" value="'.$row->description.'"/>';
//                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_equipments_type", "id_equipments_type", $i).'" name="'.$this->getSequencedName("id_equipments_type", $i).'" value="'.$row->id_equipments_type.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("procedures", "procedures", $i).'" name="'.$this->getSequencedName("procedures", $i).'" value="'.$row->procedures.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("restrictions", "restrictions", $i).'" name="'.$this->getSequencedName("restrictions", $i).'" value="'.$row->restrictions.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("usage_conditions", "usage_conditions", $i).'" name="'.$this->getSequencedName("usage_conditions", $i).'" value="'.$row->usage_conditions.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("quantity", "quantity", $i).'" name="'.$this->getSequencedName("quantity", $i).'" value="'.$row->quantity.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("costs", "costs", $i).'" name="'.$this->getSequencedName("costs", $i).'" value="'.$row->costs.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("del_id_equipment", "del_id_equipment", $i).'" name="'.$this->getSequencedName("del_id_equipment", $i).'" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("del_id_entity", "del_id_entity", $i).'" name="'.$this->getSequencedName("del_id_entity", $i).'" />';

                    $k=$i;
                }
                $html[] = '</div>';
                $html[] = '<div id="control-new-equipments">';
                $html[] = '</div>';

                $link_new = 'index.php?option=com_sim1&amp;view=researchunit&amp;layout=modal_equipment&amp;tmpl=component&amp;id_entity='.$this->form->getValue('id_entity').'&amp;function=equipmentOk_';
                
                $script[] = 'function equipmentNew() {';
                $script[] = '      sequence++;';
                $script[] = '      var newEquipment = document.getElementById("control-new-equipments");';
                $script[] = '      var newDiv = document.createElement("div");';
                $script[] = '      var equipmentNew = document.getElementById("equipmentNew");';
                $script[] = '      newEquipment.appendChild(newDiv);';
                $script[] = '      if (equipmentNew) equipmentNew.parentNode.removeChild(equipmentNew);';
                $script[] = '      newDiv.innerHTML = '
                                . '"<div id=\"group_"+sequence+"_equipment_name\" class=\"control-group\">'
                                . '  <div class=\"control-label\">Nº "+(sequence+1)+" - '.JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_NEW_NAME_LABEL').'</div>'
                                . '  <div class=\"controls\">'
                                . '     <input type=\"text\" id=\"'.$this->getSequencedId("name", "name", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("name", "\"+sequence+\"").'\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("id_equipment", "id_equipment", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("id_equipment", "\"+sequence+\"").'\" />'
                                . $this->showSelectEquipments('"+sequence+"', null)
                                . '  </div>'
                                . '  <div class=\"controls btn-wrapper\">'
                                . '     <a class=\"btn btn-small modal\" onclick=\"'
                                                . '     var _name= (document.getElementById(\''.$this->getSequencedId("name", "name", "\"+sequence+\"").'\').value); '
                                                . '     if (_name) {'
                                                . '             SqueezeBox.setContent(\'iframe\',\''.$link_new.'"+sequence+"&new_name=\'+_name);'
                                                . '     }'
                                . '         \"><span class=\"icon-edit\"></span>Edit</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"equipmentNew();\"><span class=\"icon-new icon-white\"></span>New</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"equipmentDel("+sequence+");\"><span class=\"icon-delete\"></span>Del</a>'
                                . '  </div>'
                                . '</div>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("description", "description", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("description", "\"+sequence+\"").'\"/>'
//                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("id_equipments_type", "id_equipments_type", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("id_equipments_type", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("procedures", "procedures", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("procedures", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("restrictions", "restrictions", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("restrictions", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("usage_conditions", "usage_conditions", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("usage_conditions", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("quantity", "quantity", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("quantity", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("costs", "costs", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("costs", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_equipment", "del_id_equipment", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_equipment", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_entity", "del_id_entity", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_entity", "\"+sequence+\"").'\"/>'
                                . '"';
                $script[] = '      registerFunction("equipmentOk_"+sequence+"(description, procedures, restrictions, usage_conditions, quantity, costs){ '
                                                    . 'document.id(\"'.$this->getSequencedId("description", "description", "\"+sequence+\"").'\").value = description; '
//                                                    . 'document.id(\"'.$this->getSequencedId("id_equipments_type", "id_equipments_type", "\"+sequence+\"").'\").value = id_equipments_type; '
                                                    . 'document.id(\"'.$this->getSequencedId("procedures", "procedures", "\"+sequence+\"").'\").value = procedures; '
                                                    . 'document.id(\"'.$this->getSequencedId("restrictions", "restrictions", "\"+sequence+\"").'\").value = restrictions; '
                                                    . 'document.id(\"'.$this->getSequencedId("usage_conditions", "usage_conditions", "\"+sequence+\"").'\").value = usage_conditions; '
                                                    . 'document.id(\"'.$this->getSequencedId("quantity", "quantity", "\"+sequence+\"").'\").value = quantity; '
                                                    . 'document.id(\"'.$this->getSequencedId("costs", "costs", "\"+sequence+\"").'\").value = costs; '
                                                    . 'SqueezeBox.close(); }");';
                $script[] = '}';
                
                $script[] = 'function equipmentDel(pIndex) {'
                            . '      var id_equipment = document.getElementById("'.$this->getSequencedId("id_equipment", "id_equipment", "\"+pIndex+\"").'");'
                            . '      var id_entity = document.getElementById("'.$this->getSequencedId("id_entity", "id_entity", "\"+pIndex+\"").'");'
                            . '      if (id_equipment != null && id_entity != null) {'
                            . '             document.getElementById("'.$this->getSequencedId("del_id_equipment", "del_id_equipment", "\"+pIndex+\"").'").setAttribute("value", id_equipment.value);'
                            . '             document.getElementById("'.$this->getSequencedId("del_id_entity", "del_id_entity", "\"+pIndex+\"").'").setAttribute("value", id_entity.value);'
                            . '      } '
                            . '      var group_name = document.getElementById("group_"+pIndex+"_equipment_name");'
                            . '      var group_new_name = document.getElementById("group_"+pIndex+"_equipment_new_name");'
                            . '      group_name.parentNode.removeChild(group_name);'
                            . '      if (group_new_name) group_new_name.parentNode.removeChild(group_new_name);'
                            . '}';

                // Add to document head
                JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                
                return implode("\n", $html);
          }
          
          protected function showSelectEquipments($i, $id_selected) {
                // Setup list of investgators for display in combobox
                $sel_persons = array();
                $html_persons = '<select id='.$this->getSequencedId("id_equipments_type", "id_equipments_type", $i).' name='.$this->getSequencedName("id_equipments_type", $i).']>';

              // Create a new query object.
		$sys = &SysARDITI::getInstance();
		$dbJ = &$sys->getDBO();
                $query = $dbJ->getQuery(true);
                    $query->select('*')
                        ->from($dbJ->quoteName('#__equipments_type'));
                $dbJ->setQuery($query);
                $sel_persons = $dbJ->loadObjectList();

                if (!$sel_persons) {
                        JError::raiseWarning(500, $dbJ->getErrorMsg());
                }
                
                foreach ($sel_persons as $i => $row) {
                      $html_persons .= '<option value='.$row->id_equipments_type.' '.(($row->id_equipments_type === $id_selected)? "selected" : "").'>'.$row->name.'</option>';
                }
                
                $html_persons .=  '</select>';
                
                return $html_persons;
          }
          
         /**
	 * Method to get the id used for the field input tag.
	 *
	 * @param   string  $fieldId    The field element id.
	 * @param   string  $fieldName  The field element name.
	 *
	 * @return  string  The id to be used for the field input tag.
	 *
	 * @since   11.1
	 */
	protected function getSequencedId($fieldId, $fieldName, $sequence)
	{
                $id = '';
                $id .= $this->getId($fieldId, $fieldName);
                $prefix = $this->formControl.'_'.$this->group;
                
                return str_replace ($prefix, $prefix.'_'.((string)$sequence), $id);
	}          
 
	/**
	 * Method to get the name used for the field input tag.
	 *
	 * @param   string  $fieldName  The field element name.
	 *
	 * @return  string  The name to be used for the field input tag.
	 *
	 * @since   11.1
	 */
	protected function getSequencedName($fieldName, $sequence)
	{
                $name = '';
                $name .= $this->getName($fieldName);
                $prefix = $this->formControl.'['.$this->group.']';
                
                return str_replace($prefix, $prefix.'['.((string)$sequence).']', $name);
	}        
}
