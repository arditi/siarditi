 <?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
 defined('_JEXEC') or die('Restricted access');
 
 // Load modal behavior
JHtml::_('behavior.modal','a.modal');
jimport('joomla.form.formfield');
 
 /**
  * Book form field class
  */
 class JFormFieldModal_UnitInvestigator extends JFormField
 {
     
        /**
           * Method to get the field input markup
           */
          protected function getInput()
          {
                // Load modal behavior
                //JHtml::_('behavior.modal', 'a.modal');

                // Setup variables for display
                $persons = array();
                $html = array();
                $html[] = '<div id="control-investigators">';

                if ($this->form->getValue('id_entity')) {
                    // Create a new query object.
                    $sys = &SysARDITI::getInstance();
                    $db = &$sys->getDBO();
                    $query = $db->getQuery(true);
                    $query->select('*')
                        ->from($db->quoteName('persons'))
                        ->join('LEFT', 'entities_persons AS ent_person ON ent_person.id_person=persons.id_person')
                        ->join('LEFT', 'entities_bonds AS ent_bond ON ent_bond.id_person=persons.id_person')
                        ->where($db->quoteName('ent_bond.bond_type') . ' IN ("researcher","scolarship")')
                        ->where($db->quoteName('ent_bond.id_entity') . ' = '.$this->form->getValue('id_entity'))
                        ->where($db->quoteName('ent_person.id_entity') . ' = '.$this->form->getValue('id_entity'));                    $db->setQuery($query);
                    $persons = $db->loadObjectList();
 
                    if (!$persons) {
                            JError::raiseWarning(500, $db->getErrorMsg());
                    }

                }
                
                $script = array();
                $k=0;
                $script[] = 'var sequence=0;';
                $script[] = 'function registerFunction(functionBody) { '
                              . '"use strict"; '
                              . 'var script = document.createElement("script"); '
                              . 'script.innerHTML = "function " + functionBody; '
                              . 'document.head.appendChild(script);}';
                if (!$persons) {
                      $html[] = '  <div id="personNew" class="control-label btn-wrapper">';
                      $html[] = '      <a class="btn btn-small"  onclick="sequence=-1; personAdd();"><span class="icon-new icon-white"></span>Add</a>';
                      $html[] = '      <a class="btn btn-small"  onclick="sequence=-1; personNew();"><span class="icon-new icon-white"></span>New</a>';
                      $html[] = '  </div>';
                }
                
                foreach ($persons as $i => $row) {
//                    $personInEntity = new PersonInEntity($row->id_person, $row->id_entity);

                    // Build the script
                    $script[] = 'registerFunction("investigatorOk_'.$i.'(phone, fax, ext, email, position_held, bond_type, bond_initial_date, bond_final_date, research_kind, research_percent){ '
                                        . 'document.id(\"'.$this->getSequencedId("phone", "phone", $i).'\").value = phone; '
                                        . 'document.id(\"'.$this->getSequencedId("fax", "fax", $i).'\").value = fax; '
                                        . 'document.id(\"'.$this->getSequencedId("ext", "ext", $i).'\").value = ext; '
                                        . 'document.id(\"'.$this->getSequencedId("email", "email", $i).'\").value = email; '
                                        . 'document.id(\"'.$this->getSequencedId("position_held", "position_held", $i).'\").value = position_held; '
                                        . 'document.id(\"'.$this->getSequencedId("bond_type", "bond_type", $i).'\").value = bond_type; '
                                        . 'document.id(\"'.$this->getSequencedId("bond_initial_date", "bond_initial_date", $i).'\").value = bond_initial_date; '
                                        . 'document.id(\"'.$this->getSequencedId("bond_final_date", "bond_final_date", $i).'\").value = bond_final_date; '
                                        . 'document.id(\"'.$this->getSequencedId("research_kind", "research_kind", $i).'\").value = research_kind; '
                                        . 'document.id(\"'.$this->getSequencedId("research_percent", "research_percent", $i).'\").value = research_percent; '
                                        . 'SqueezeBox.close(); }");';
                    $script[] = 'sequence='.$i.';';

                    $link = 'index.php?option=com_sim1&amp;view=researchunit&amp;layout=modal_investigator&amp;tmpl=component&amp;id_entity='.$row->id_entity.'&amp;id_person='.$row->id_person.'&amp;id_bond='.$row->id_bond.'&amp;function=investigatorOk_'.$i;

                    $html[] = '<div id="group_'.$i.'_investigator_name" class="control-group">';
                    $html[] = '  <div class="control-label">Nº '.($i+1).' - '.JText::_('COM_SIM1_RESEARCHUNIT_INVESTIGATOR_NAME_LABEL').'</div>';
                    $html[] = '  <div class="controls">';
                    $html[] = '     <input type="text" readonly value="'.$row->name.'" size="35" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("position_held", "position_held", $i).'" name="'.$this->getSequencedName("position_held", $i).'" value="'.$row->position_held.'" size="35" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_person", "id_person", $i).'" name="'.$this->getSequencedName("id_person", $i).'" value="'.$row->id_person.'" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_entity", "id_entity", $i).'" name="'.$this->getSequencedName("id_entity", $i).'" value="'.$row->id_entity.'" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_bond", "id_bond", $i).'" name="'.$this->getSequencedName("id_bond", $i).'" value="'.$row->id_bond.'"/>';
                    $html[] = '  </div>';
                    $html[] = '  <div class="controls btn-wrapper">';
                    $html[] = '      <a id="edit-modal" class="btn btn-small modal"  onclick="SqueezeBox.setContent(\'iframe\',\''.$link.'\');"><span class="icon-edit"></span>Edit</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="personAdd();"><span class="icon-new icon-white"></span>Add</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="personNew();"><span class="icon-new icon-white"></span>New</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="personDel('.$i.');"><span class="icon-delete"></span>Del</a>';
                    $html[] = '  </div>';
                    $html[] = '</div>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("phone", "phone", $i).'" name="'.$this->getSequencedName("phone", $i).'" value="'.$row->phone.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("fax", "fax", $i).'" name="'.$this->getSequencedName("fax", $i).'" value="'.$row->fax.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("ext", "ext", $i).'" name="'.$this->getSequencedName("ext", $i).'" value="'.$row->ext.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("email", "email", $i).'" name="'.$this->getSequencedName("email", $i).'" value="'.$row->email.'"/>';
//                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("position_held", "position_held", $i).'" name="'.$this->getSequencedName("position_held", $i).'" value="'.$row->position_held.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("bond_type", "bond_type", $i).'" name="'.$this->getSequencedName("bond_type", $i).'" value="'.$row->bond_type.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("bond_initial_date", "bond_initial_date", $i).'" name="'.$this->getSequencedName("bond_initial_date", $i).'" value="'.$row->bond_initial_date.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("bond_final_date", "bond_final_date", $i).'" name="'.$this->getSequencedName("bond_final_date", $i).'" value="'.$row->bond_final_date.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("research_kind", "research_kind", $i).'" name="'.$this->getSequencedName("research_kind", $i).'" value="'.$row->research_kind.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("research_percent", "research_percent", $i).'" name="'.$this->getSequencedName("research_percent", $i).'" value="'.$row->research_percent.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("del_id_person", "del_id_person", $i).'" name="'.$this->getSequencedName("del_id_person", $i).'" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("del_id_entity", "del_id_entity", $i).'" name="'.$this->getSequencedName("del_id_entity", $i).'" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("del_id_bond", "del_id_bond", $i).'" name="'.$this->getSequencedName("del_id_bond", $i).'" />';

                    $k=$i;
                }
                $html[] = '</div>';
                $html[] = '<div id="control-new-persons">';
                $html[] = '</div>';

                $link_new = 'index.php?option=com_sim1&amp;view=researchunit&amp;layout=modal_investigator&amp;tmpl=component&amp;id_entity='.$this->form->getValue('id_entity').'&amp;function=investigatorOk_';
                
                $script[] = 'function personAdd() {';
                $script[] = '      sequence++;';
                $script[] = '      var newPerson = document.getElementById("control-new-persons");';
                $script[] = '      var newDiv = document.createElement("div");';
                $script[] = '      var personNew = document.getElementById("personNew");';
                $script[] = '      newPerson.appendChild(newDiv);';
                $script[] = '      if (personNew) personNew.parentNode.removeChild(personNew);';
                $script[] = '      newDiv.innerHTML = '
                                . '"<div id=\"group_"+sequence+"_investigator_name\" class=\"control-group\">'
                                . '  <div class=\"control-label\">Nº "+(sequence+1)+" - '.JText::_('COM_SIM1_RESEARCHUNIT_INVESTIGATOR_NAME_LABEL').'</div>'
                                . '  <div class=\"controls\">'
                                . $this->showSelectInvestigators('"+sequence+"', null)
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("id_entiy", "id_entiy", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("id_entiy", "\"+sequence+\"").'\"  value=\"'.$row->id_entity.'\" size=\"35\" />'
                                . '  </div>'
                                . '  <div class=\"controls btn-wrapper\">'
                                . '     <a class=\"btn btn-small modal\" onclick=\"'
                                                . '     var _id_person= (document.getElementById(\''.$this->getSequencedId("id_person", "id_person", "\"+sequence+\"").'\').value); '
                                                . '     if (_id_person) {'
                                                . '             SqueezeBox.setContent(\'iframe\',\''.$link_new.'"+sequence+"&id_person=\'+_id_person);'
                                                . '     }'
                                . '         \"><span class=\"icon-edit\"></span>Edit</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"personAdd();\"><span class=\"icon-new icon-white\"></span>Add</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"personNew();\"><span class=\"icon-new icon-white\"></span>New</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"personDel("+sequence+");\"><span class=\"icon-delete\"></span>Del</a>'
                                . '  </div>'
                                . '</div>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("phone", "phone", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("phone", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("fax", "fax", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("fax", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("ext", "ext", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("ext", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("email", "email", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("email", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("position_held", "position_held", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("position_held", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("id_bond", "id_bond", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("id_bond", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("bond_type", "bond_type", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("bond_type", "\"+sequence+\"").'\" value=\"investigator\" />'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("bond_initial_date", "bond_initial_date", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("bond_initial_date", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("bond_final_date", "bond_final_date", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("bond_final_date", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("research_kind", "research_kind", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("research_kind", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("research_percent", "research_percent", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("research_percent", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_person", "del_id_person", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_person", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_entity", "del_id_entity", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_entity", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_bond", "del_id_bond", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_bond", "\"+sequence+\"").'\"/>'
                                . '"';
                $script[] = '      registerFunction("investigatorOk_"+sequence+"(phone, fax, ext, email, position_held, bond_type, bond_initial_date, bond_final_date, research_kind, research_percent){ '
                                                    . 'document.id(\"'.$this->getSequencedId("phone", "phone", "\"+sequence+\"").'\").value = phone; '
                                                    . 'document.id(\"'.$this->getSequencedId("fax", "fax", "\"+sequence+\"").'\").value = fax; '
                                                    . 'document.id(\"'.$this->getSequencedId("ext", "ext", "\"+sequence+\"").'\").value = ext; '
                                                    . 'document.id(\"'.$this->getSequencedId("email", "email", "\"+sequence+\"").'\").value = email; '
                                                    . 'document.id(\"'.$this->getSequencedId("position_held", "position_held", "\"+sequence+\"").'\").value = position_held; '
                                                    . 'document.id(\"'.$this->getSequencedId("bond_type", "bond_type", "\"+sequence+\"").'\").value = bond_type; '
                                                    . 'document.id(\"'.$this->getSequencedId("bond_initial_date", "bond_initial_date", "\"+sequence+\"").'\").value = bond_initial_date; '
                                                    . 'document.id(\"'.$this->getSequencedId("bond_final_date", "bond_final_date", "\"+sequence+\"").'\").value = bond_final_date; '
                                                    . 'document.id(\"'.$this->getSequencedId("research_kind", "research_kind", "\"+sequence+\"").'\").value = research_kind; '
                                                    . 'document.id(\"'.$this->getSequencedId("research_percent", "research_percent", "\"+sequence+\"").'\").value = research_percent; '
                                                    . 'SqueezeBox.close(); }");';
                $script[] = '}';
              
                $script[] = 'function personNew() {';
                $script[] = '      sequence++;';
                $script[] = '      var newPerson = document.getElementById("control-new-persons");';
                $script[] = '      var newDiv = document.createElement("div");';
                $script[] = '      var personNew = document.getElementById("personNew");';
                $script[] = '      newPerson.appendChild(newDiv);';
                $script[] = '      if (personNew) personNew.parentNode.removeChild(personNew);';
                $script[] = '      newDiv.innerHTML = '
                                . '"<div id=\"group_"+sequence+"_investigator_name\" class=\"control-group\">'
                                . '  <div class=\"control-label\">Nº "+(sequence+1)+" - '.JText::_('COM_SIM1_RESEARCHUNIT_INVESTIGATOR_NEW_NAME_LABEL').'</div>'
                                . '  <div class=\"controls\">'
                                . '     <input type=\"text\" id=\"'.$this->getSequencedId("name", "name", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("name", "\"+sequence+\"").'\" size=\"35\" />'
                                . '  </div>'
                                . '  <div class=\"controls btn-wrapper\">'
                                . '     <a class=\"btn btn-small modal\" onclick=\"'
                                                . '     var _name= (document.getElementById(\''.$this->getSequencedId("name", "name", "\"+sequence+\"").'\').value); '
                                                . '     if (_name) {'
                                                . '             SqueezeBox.setContent(\'iframe\',\''.$link_new.'"+sequence+"&new_name=\'+_name);'
                                                . '     }'
                                . '         \"><span class=\"icon-edit\"></span>Edit</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"personAdd();\"><span class=\"icon-new icon-white\"></span>Add</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"personNew();\"><span class=\"icon-new icon-white\"></span>New</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"personDel("+sequence+");\"><span class=\"icon-delete\"></span>Del</a>'
                                . '  </div>'
                                . '</div>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("phone", "phone", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("phone", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("fax", "fax", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("fax", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("ext", "ext", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("ext", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("email", "email", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("email", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("position_held", "position_held", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("position_held", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("id_bond", "id_bond", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("id_bond", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("bond_type", "bond_type", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("bond_type", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("bond_initial_date", "bond_initial_date", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("bond_initial_date", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("bond_final_date", "bond_final_date", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("bond_final_date", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("research_kind", "research_kind", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("research_kind", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("research_percent", "research_percent", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("research_percent", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_person", "del_id_person", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_person", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_entity", "del_id_entity", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_entity", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_bond", "del_id_bond", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_bond", "\"+sequence+\"").'\"/>'
                                . '"';
                $script[] = '      registerFunction("investigatorOk_"+sequence+"(phone, fax, ext, email, position_held, bond_type, bond_initial_date, bond_final_date, research_kind, research_percent){ '
                                                    . 'document.id(\"'.$this->getSequencedId("phone", "phone", "\"+sequence+\"").'\").value = phone; '
                                                    . 'document.id(\"'.$this->getSequencedId("fax", "fax", "\"+sequence+\"").'\").value = fax; '
                                                    . 'document.id(\"'.$this->getSequencedId("ext", "ext", "\"+sequence+\"").'\").value = ext; '
                                                    . 'document.id(\"'.$this->getSequencedId("email", "email", "\"+sequence+\"").'\").value = email; '
                                                    . 'document.id(\"'.$this->getSequencedId("position_held", "position_held", "\"+sequence+\"").'\").value = position_held; '
                                                    . 'document.id(\"'.$this->getSequencedId("bond_type", "bond_type", "\"+sequence+\"").'\").value = bond_type; '
                                                    . 'document.id(\"'.$this->getSequencedId("bond_initial_date", "bond_initial_date", "\"+sequence+\"").'\").value = bond_initial_date; '
                                                    . 'document.id(\"'.$this->getSequencedId("bond_final_date", "bond_final_date", "\"+sequence+\"").'\").value = bond_final_date; '
                                                    . 'document.id(\"'.$this->getSequencedId("research_kind", "research_kind", "\"+sequence+\"").'\").value = research_kind; '
                                                    . 'document.id(\"'.$this->getSequencedId("research_percent", "research_percent", "\"+sequence+\"").'\").value = research_percent; '
                                                    . 'SqueezeBox.close(); }");';
                $script[] = '}';
                
                $script[] = 'function personDel(pIndex) {'
                            . '      var id_person = document.getElementById("'.$this->getSequencedId("id_person", "id_person", "\"+pIndex+\"").'");'
                            . '      var id_entity = document.getElementById("'.$this->getSequencedId("id_entity", "id_entity", "\"+pIndex+\"").'");'
                            . '      var id_bond = document.getElementById("'.$this->getSequencedId("id_bond", "id_bond", "\"+pIndex+\"").'");'
                            . '      if (id_person != null && id_entity != null && id_bond != null) {'
                            . '             document.getElementById("'.$this->getSequencedId("del_id_person", "del_id_person", "\"+pIndex+\"").'").setAttribute("value", id_person.value);'
                            . '             document.getElementById("'.$this->getSequencedId("del_id_entity", "del_id_entity", "\"+pIndex+\"").'").setAttribute("value", id_entity.value);'
                            . '             document.getElementById("'.$this->getSequencedId("del_id_bond", "del_id_bond", "\"+pIndex+\"").'").setAttribute("value", id_bond.value);'
                            . '      } '
                            . '      var group_name = document.getElementById("group_"+pIndex+"_investigator_name");'
                            . '      var group_new_name = document.getElementById("group_"+pIndex+"_investigator_new_name");'
                            . '      group_name.parentNode.removeChild(group_name);'
                            . '      if (group_new_name) group_new_name.parentNode.removeChild(group_new_name);'
                            . '}';

                // Add to document head
                JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                
                return implode("\n", $html);
          }
          
          protected function showSelectInvestigators($i, $id_selected) {
                // Setup list of investgators for display in combobox
                $sel_persons = array();
                $html_persons = '<select id='.$this->getSequencedId("id_person", "id_person", $i).' name='.$this->getSequencedName("id_person", $i).']>';

              // Create a new query object.
		$sys = &SysARDITI::getInstance();
		$dbJ = &$sys->getDBO();
                $query = $dbJ->getQuery(true);
                    $query->select('*')
                        ->from($dbJ->quoteName('#__persons'));
//                        ->where($dbJ->quoteName('id_person') . ' NOT IN (SELECT id_person FROM entities_persons WHERE id_entity='.$this->form->getValue('id_entity').')');
                $dbJ->setQuery($query);
                $sel_persons = $dbJ->loadObjectList();

                if (!$sel_persons) {
                        JError::raiseWarning(500, $dbJ->getErrorMsg());
                }
                
                foreach ($sel_persons as $i => $row) {
                      $html_persons .= '<option value='.$row->id_person.' '.(($row->id_person === $id_selected)? "selected" : "").'>'.$row->name.'</option>';
                }
                
                $html_persons .=  '</select>';
                
                return $html_persons;
          }

	/**
	 * Method to get the id used for the field input tag.
	 *
	 * @param   string  $fieldId    The field element id.
	 * @param   string  $fieldName  The field element name.
	 *
	 * @return  string  The id to be used for the field input tag.
	 *
	 * @since   11.1
	 */
	protected function getSequencedId($fieldId, $fieldName, $sequence)
	{
                $id = '';
                $id .= $this->getId($fieldId, $fieldName);
                $prefix = $this->formControl.'_'.$this->group;
                
                return str_replace ($prefix, $prefix.'_'.((string)$sequence), $id);
	}          
 
	/**
	 * Method to get the name used for the field input tag.
	 *
	 * @param   string  $fieldName  The field element name.
	 *
	 * @return  string  The name to be used for the field input tag.
	 *
	 * @since   11.1
	 */
	protected function getSequencedName($fieldName, $sequence)
	{
                $name = '';
                $name .= $this->getName($fieldName);
                $prefix = $this->formControl.'['.$this->group.']';
                
                return str_replace($prefix, $prefix.'['.((string)$sequence).']', $name);
	}        
}
