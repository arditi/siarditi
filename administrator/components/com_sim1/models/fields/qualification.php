 <?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
 defined('_JEXEC') or die('Restricted access');
 
 // Load modal behavior
JHtml::_('behavior.modal','a.modal');

 jimport('joomla.form.formfield');
 
 /**
  * Book form field class
  */
 class JFormFieldModal_Qualification extends JFormField
 {
     
        /**
           * Method to get the field input markup
           */
          protected function getInput()
          {
                // Load modal behavior
                JHtml::_('behavior.modal', 'a.modal');

                // Setup variables for display
                $quals = array();
                $html = array();
                $html[] = '<div id="control-qualifications">';

              // Create a new query object.
                if ($this->value) {
                    $sys = &SysARDITI::getInstance();
                    $db = &$sys->getDBO();
                    $query = $db->getQuery(true);
                    $query->select('*')
                    ->from($db->quoteName('qualifications'))
                    ->join('LEFT', 'curricula_qualifications AS cv_qual ON cv_qual.id_qualification=qualifications.id_qualification')
                    ->where($db->quoteName('cv_qual.id_cv') . ' = '.$this->value);
                    $db->setQuery($query);
                    $quals = $db->loadObjectList();
 
                    if (!$quals) {
                            JError::raiseWarning(500, $db->getErrorMsg());
                    }
                }
                
                $script = array();
                $k=0;
                $script[] = 'var sequence=0;';
                $script[] = 'function registerFunction(functionBody) { '
                              . '"use strict"; '
                              . 'var script = document.createElement("script"); '
                              . 'script.innerHTML = "function " + functionBody; '
                              . 'document.head.appendChild(script);}';
                if (!$quals) {
                      $html[] = '  <div id="qualNew" class="control-label btn-wrapper">';
                      $html[] = '      <a class="btn btn-small"  onclick="sequence=-1; qualAdd();"><span class="icon-new icon-white"></span>New</a>';
                      $html[] = '  </div>';
                }

                foreach ($quals as $i => $row) {
                      $qual = new Qualification($row->id_qualification);

                      // Build the script
                      $script[] = 'registerFunction("qualOk_'.$i.'(name, certification,certification_entity, certificate, grade, degree, obs){ document.id(\"jform_qualifications_'.$i.'_name\").value = name; document.id(\"jform_qualifications_'.$i.'_certification\").value = certification; document.id(\"jform_qualifications_'.$i.'_certification_entity\").value = certification_entity; document.id(\"jform_qualifications_'.$i.'_certificate\").value = certificate; document.id(\"jform_qualifications_'.$i.'_grade\").value = grade; document.id(\"jform_qualifications_'.$i.'_degree\").value = degree; document.id(\"jform_qualifications_'.$i.'_obs\").value = obs; SqueezeBox.close(); }");';
                      $script[] = 'sequence='.$i.';';

                      $link = 'index.php?option=com_sim1&amp;view=investigator&amp;layout=modal_qual&amp;tmpl=component&amp;id_qualification='.$row->id_qualification.'&amp;function=qualOk_'.$i;

                      $html[] = '<div id="group_'.$i.'_name" class="control-group">';
                      $html[] = '  <div class="control-label">'.JText::_('COM_SIM1_INVESTIGATOR_QUALIFICATION_NAME_LABEL').'</div>';
                      $html[] = '  <div class="controls">';
                      $html[] = '     <input readonly type="text" id="jform_qualifications_'.$i.'_name" name="jform[qualifications]['.$i.'][name]" value="'.$qual->name.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_id_qualification" name="jform[qualifications]['.$i.'][id_qualification]" value="'.$qual->id_qualification.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_id_cv" name="jform[qualifications]['.$i.'][id_cv]" value="'.$row->id_cv.'" size="35" />';
                      $html[] = '  </div>';
                      $html[] = '</div>';

                      $html[] = '<div id="group_'.$i.'_entity" class="control-group">';
                      $html[] = '  <div class="control-label">'.JText::_('COM_SIM1_INVESTIGATOR_CERTIFICATION_ENTITY_LABEL').'</div>';
                      $html[] = '  <div class="controls">';
                      $html[] = '     <input readonly type="text" id="jform_qualifications_'.$i.'_certification_entity" name="jform[qualifications]['.$i.'][certification_entity]" value="'.$qual->certification_entity.'" size="35" />';
                      $html[] = '  </div>';
                      $html[] = '  <div class="controls btn-wrapper">';
                      $html[] = '      <a id="edit-modal" class="btn btn-small modal"  onclick="SqueezeBox.setContent(\'iframe\',\''.$link.'\'); "><span class="icon-edit"></span>Edit</a>';
                      $html[] = '      <a class="btn btn-small"  onclick="qualAdd();"><span class="icon-new icon-white"></span>New</a>';
                      $html[] = '      <a class="btn btn-small"  onclick="qualDel('.$i.');"><span class="icon-delete"></span>Del</a>';
                      $html[] = '  </div>';
                      $html[] = '</div>';

                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_certification" name="jform[qualifications]['.$i.'][certification]" value="'.$qual->certification.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_grade" name="jform[qualifications]['.$i.'][grade]" value="'.$qual->grade.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_degree" name="jform[qualifications]['.$i.'][degree]" value="'.$qual->degree.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_certificate" name="jform[qualifications]['.$i.'][certificate]" value="'.$qual->certificate.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_obs" name="jform[qualifications]['.$i.'][obs]" value="'.$qual->obs.'" size="35" />';
                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_del_id_qualification" name="jform[qualifications]['.$i.'][del_id_qualification]" value="" />';
                      $html[] = '     <input type="hidden" id="jform_qualifications_'.$i.'_del_id_cv" name="jform[qualifications]['.$i.'][del_id_cv]" value="" />';

                      $k=$i;

                    // Add to document head
                    //JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                }
                $html[] = '</div>';
                $html[] = '<div id="control-new-qualifications">';
                $html[] = '</div>';

                $link_new = 'index.php?option=com_sim1&amp;view=investigator&amp;layout=modal_qual&amp;tmpl=component&amp;function=qualOk_';

                $script[] = 'function qualAdd() {';
                $script[] = '      sequence++;';
                $script[] = '      var newQual = document.getElementById("control-new-qualifications");';
                $script[] = '      var newDiv = document.createElement("div");';
                $script[] = '      var qualNew = document.getElementById("qualNew");';
                $script[] = '      newQual.appendChild(newDiv);';
                $script[] = '      if (qualNew) qualNew.parentNode.removeChild(qualNew);';
                $script[] = '      newDiv.innerHTML = '
                                . '"<div id=\"group_"+sequence+"_name\" class=\"control-group\">'
                                . '  <div class=\"control-label\">'.JText::_('COM_SIM1_INVESTIGATOR_QUALIFICATION_NAME_LABEL').'</div>'
                                . '  <div class=\"controls\">'
                                . '     <input readonly type=\"text\" id=\"jform_qualifications_"+sequence+"_name\" name=\"jform[qualifications]["+sequence+"][name]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_id_qualification\" name=\"jform[qualifications]["+sequence+"][id_qualification]\"  size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_id_cv\" name=\"jform[qualifications]["+sequence+"][id_cv]\"  size=\"35\" />'
                                . '  </div>'
                                . '</div>'

                                . '<div id=\"group_"+sequence+"_entity\" class=\"control-group\">'
                                . '  <div class=\"control-label\">'.JText::_('COM_SIM1_INVESTIGATOR_CERTIFICATION_ENTITY_LABEL').'</div>'
                                . '  <div class=\"controls\">'
                                . '     <input readonly type=\"text\" id=\"jform_qualifications_"+sequence+"_certification_entity\" name=\"jform[qualifications]["+sequence+"][certification_entity]\"  size=\"35\" />'
                                . '  </div>'
                                . '  <div class=\"controls btn-wrapper\">'
                                . '     <a class=\"btn btn-small modal\" onclick=\"SqueezeBox.setContent(\'iframe\',\''.$link_new.'"+sequence+"\');\"><span class=\"icon-edit\"></span>Edit</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"qualAdd();\"><span class=\"icon-new icon-white\"></span>New</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"qualDel("+sequence+");\"><span class=\"icon-delete\"></span>Del</a>'
                                . '  </div>'
                                . '</div>'

                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_certification\" name=\"jform[qualifications]["+sequence+"][certification]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_grade\" name=\"jform[qualifications]["+sequence+"][grade]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_degree\" name=\"jform[qualifications]["+sequence+"][degree]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_certificate\" name=\"jform[qualifications]["+sequence+"][certificate]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_obs\" name=\"jform[qualifications]["+sequence+"][obs]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_del_id_qualification\" name=\"jform[qualifications]["+sequence+"][del_id_qualification]\" value=\"\" />'
                                . '     <input type=\"hidden\" id=\"jform_qualifications_"+sequence+"_del_id_cv\" name=\"jform[qualifications]["+sequence+"][del_id_cv]\" value=\"\" />'
                                . '"';

                $script[] = '      registerFunction("qualOk_"+sequence+"(name, certification,certification_entity, certificate, grade, degree, obs){ document.id(\"jform_qualifications_"+sequence+"_name\").value = name; document.id(\"jform_qualifications_"+sequence+"_certification\").value = certification; document.id(\"jform_qualifications_"+sequence+"_certification_entity\").value = certification_entity; document.id(\"jform_qualifications_"+sequence+"_certificate\").value = certificate; document.id(\"jform_qualifications_"+sequence+"_grade\").value = grade; document.id(\"jform_qualifications_"+sequence+"_degree\").value = degree; document.id(\"jform_qualifications_"+sequence+"_obs\").value = obs; SqueezeBox.close(); }");';
                $script[] = '}';
              
                $script[] = 'function qualDel(qualIndex) {'
                            . '      document.getElementById("jform_qualifications_"+qualIndex+"_del_id_qualification").setAttribute("value",document.getElementById("jform_qualifications_"+qualIndex+"_id_qualification").value);'
                            . '      document.getElementById("jform_qualifications_"+qualIndex+"_del_id_cv").setAttribute("value",document.getElementById("jform_qualifications_"+qualIndex+"_id_cv").value);'
                            . '      var group_name = document.getElementById("group_"+qualIndex+"_name");'
                            . '      var group_entity = document.getElementById("group_"+qualIndex+"_entity");'
                            . '      var field_certification = document.getElementById("jform_qualifications_"+qualIndex+"_certification");'
                            . '      var field_grade = document.getElementById("jform_qualifications_"+qualIndex+"_grade");'
                            . '      var field_degree = document.getElementById("jform_qualifications_"+qualIndex+"_degree");'
                            . '      var field_certificate = document.getElementById("jform_qualifications_"+qualIndex+"_certificate");'
                            . '      var field_obs = document.getElementById("jform_qualifications_"+qualIndex+"_obs");'
                            . '      group_name.parentNode.removeChild(group_name);'
                            . '      group_entity.parentNode.removeChild(group_entity);'
                            . '      field_certification.parentNode.removeChild(field_certification);'
                            . '      field_grade.parentNode.removeChild(field_grade);'
                            . '      field_degree.parentNode.removeChild(field_degree);'
                            . '      field_certificate.parentNode.removeChild(field_certificate);'
                            . '      field_obs.parentNode.removeChild(field_obs);'
                            . '}';

                // Add to document head
                JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                
                return implode("\n", $html);
          }
      
 }
