 <?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
 defined('_JEXEC') or die('Restricted access');
 
 // Load modal behavior
JHtml::_('behavior.modal','a.modal');

 jimport('joomla.form.formfield');
 
 /**
  * Book form field class
  */
 class JFormFieldModal_Publication extends JFormField
 {
     
        /**
           * Method to get the field input markup
           */
          protected function getInput()
          {
                // Load modal behavior
                JHtml::_('behavior.modal', 'a.modal');

                // Setup variables for display
                $pubs = array();
                $html = array();
                $html[] = '<div id="control-publications">';

                if ($this->value) {
                    // Create a new query object.
                    $sys = &SysARDITI::getInstance();
                    $db = &$sys->getDBO();
                    $query = $db->getQuery(true);
                    $query->select('*')
                        ->from($db->quoteName('publications'))
                        ->join('LEFT', 'curricula_publications AS cv_pub ON cv_pub.id_publication=publications.id_publication')
                        ->where($db->quoteName('cv_pub.id_cv') . ' = '.$this->value);
                    $db->setQuery($query);
                    $pubs = $db->loadObjectList();
 
                    if (!$pubs) {
                            JError::raiseWarning(500, $db->getErrorMsg());
                    }

                }
                
                $script = array();
                $k=0;
                $script[] = 'var sequence=0;';
                $script[] = 'function registerFunction(functionBody) { '
                              . '"use strict"; '
                              . 'var script = document.createElement("script"); '
                              . 'script.innerHTML = "function " + functionBody; '
                              . 'document.head.appendChild(script);}';
                if (!$pubs) {
                      $html[] = '  <div id="pubNew" class="control-label btn-wrapper">';
                      $html[] = '      <a class="btn btn-small"  onclick="sequence=-1; pubAdd();"><span class="icon-new icon-white"></span>New</a>';
                      $html[] = '  </div>';
                }
                  
                foreach ($pubs as $i => $row) {
                    $pub = new Publication($row->id_publication);

                    // Build the script
                    $script[] = 'registerFunction("pubOk_'.$i.'(title, authors, pages, area, issn, publisher, date, doc, link){ '
                                . ' document.id(\"jform_publications_'.$i.'_title\").value = title; '
                                . ' document.id(\"jform_publications_'.$i.'_authors\").value = authors; '
                                . ' document.id(\"jform_publications_'.$i.'_pages\").value = pages; '
                                . ' document.id(\"jform_publications_'.$i.'_area\").value = area; '
                                . ' document.id(\"jform_publications_'.$i.'_issn\").value = issn; '
                                . ' document.id(\"jform_publications_'.$i.'_publisher\").value = publisher; '
                                . ' document.id(\"jform_publications_'.$i.'_date\").value = date; '
                                . ' document.id(\"jform_publications_'.$i.'_doc\").value = doc; '
                                . ' document.id(\"jform_publications_'.$i.'_link\").value = link; '
                                . ' SqueezeBox.close(); }");';

                    $script[] = 'sequence='.$i.';';

                    $link = 'index.php?option=com_sim1&amp;view=investigator&amp;layout=modal_pub&amp;tmpl=component&amp;id_publication='.$row->id_publication.'&amp;function=pubOk_'.$i;

                    $html[] = '<div id="group_'.$i.'_title" class="control-group">';
                    $html[] = '  <div class="control-label">'.JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_TITLE_LABEL').'</div>';
                    $html[] = '  <div class="controls">';
                    $html[] = '     <input readonly type="text" id="jform_publications_'.$i.'_title" name="jform[publications]['.$i.'][title]" value="'.$pub->title.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_id_publication" name="jform[publications]['.$i.'][id_publication]" value="'.$pub->id_publication.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_id_cv" name="jform[publications]['.$i.'][id_cv]" value="'.$row->id_cv.'" size="35" />';
                    $html[] = '  </div>';
                    $html[] = '</div>';

                    $html[] = '<div id="group_'.$i.'_authors" class="control-group">';
                    $html[] = '  <div class="control-label">'.JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_AUTHORS_LABEL').'</div>';
                    $html[] = '  <div class="controls">';
                    $html[] = '     <input readonly type="text" id="jform_publications_'.$i.'_authors" name="jform[publications]['.$i.'][authors]" value="'.$pub->authors.'" size="35" />';
                    $html[] = '  </div>';
                    $html[] = '  <div class="controls btn-wrapper">';
                    $html[] = '      <a id="edit-modal" class="btn btn-small modal"  onclick="SqueezeBox.setContent(\'iframe\',\''.$link.'\'); "><span class="icon-edit"></span>Edit</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="pubAdd();"><span class="icon-new icon-white"></span>New</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="pubDel('.$i.');"><span class="icon-delete"></span>Del</a>';
                    $html[] = '  </div>';
                    $html[] = '</div>';

                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_pages" name="jform[publications]['.$i.'][pages]" value="'.$pub->pages.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_area" name="jform[publications]['.$i.'][area]" value="'.$pub->area.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_issn" name="jform[publications]['.$i.'][issn]" value="'.$pub->issn.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_publisher" name="jform[publications]['.$i.'][publisher]" value="'.$pub->publisher.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_date" name="jform[publications]['.$i.'][date]" value="'.$pub->date.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_doc" name="jform[publications]['.$i.'][doc]" value="'.$pub->document.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_link" name="jform[publications]['.$i.'][link]" value="'.$pub->link.'" size="35" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_del_id_publication" name="jform[publications]['.$i.'][del_id_publication]" value="0" />';
                    $html[] = '     <input type="hidden" id="jform_publications_'.$i.'_del_id_cv" name="jform[publications]['.$i.'][del_id_cv]" value="0" />';

                    $k=$i;

                        // Add to document head
                        //JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                }
                $html[] = '</div>';
                $html[] = '<div id="control-new-publications">';
                $html[] = '</div>';

                $link_new = 'index.php?option=com_sim1&amp;view=investigator&amp;layout=modal_pub&amp;tmpl=component&amp;function=pubOk_';

                $script[] = 'function pubAdd() {';
                $script[] = '      sequence++;';
                $script[] = '      var newQual = document.getElementById("control-new-publications");';
                $script[] = '      var newDiv = document.createElement("div");';
                $script[] = '      var pubNew = document.getElementById("pubNew");';
                $script[] = '      newQual.appendChild(newDiv);';
                $script[] = '      if (pubNew) pubNew.parentNode.removeChild(pubNew);';
                $script[] = '      newDiv.innerHTML = '
                                . '"<div id=\"group_"+sequence+"_title\" class=\"control-group\">'
                                . '  <div class=\"control-label\">'.JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_TITLE_LABEL').'</div>'
                                . '  <div class=\"controls\">'
                                . '     <input readonly type=\"text\" id=\"jform_publications_"+sequence+"_title\" name=\"jform[publications]["+sequence+"][title]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_id_publication\" name=\"jform[publications]["+sequence+"][id_publication]\"  size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_id_cv\" name=\"jform[publications]["+sequence+"][id_cv]\"  size=\"35\" />'
                                . '  </div>'
                                . '</div>'

                                . '<div id=\"group_"+sequence+"_authors\" class=\"control-group\">'
                                . '  <div class=\"control-label\">'.JText::_('COM_SIM1_INVESTIGATOR_PUBLICATIONS_AUTHORS_LABEL').'</div>'
                                . '  <div class=\"controls\">'
                                . '     <input readonly type=\"text\" id=\"jform_publications_"+sequence+"_authors\" name=\"jform[publications]["+sequence+"][authors]\"  size=\"35\" />'
                                . '  </div>'
                                . '  <div class=\"controls btn-wrapper\">'
                                . '     <a class=\"btn btn-small modal\" onclick=\"SqueezeBox.setContent(\'iframe\',\''.$link_new.'"+sequence+"\');\"><span class=\"icon-edit\"></span>Edit</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"pubAdd();\"><span class=\"icon-new icon-white\"></span>New</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"pubDel("+sequence+");\"><span class=\"icon-delete\"></span>Del</a>'
                                . '  </div>'
                                . '</div>'

                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_pages\" name=\"jform[publications]["+sequence+"][pages]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_area\" name=\"jform[publications]["+sequence+"][area]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_issn\" name=\"jform[publications]["+sequence+"][issn]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_publisher\" name=\"jform[publications]["+sequence+"][publisher]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_date\" name=\"jform[publications]["+sequence+"][date]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_doc\" name=\"jform[publications]["+sequence+"][doc]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_link\" name=\"jform[publications]["+sequence+"][link]\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_del_id_publication\" name=\"jform[publications]["+sequence+"][del_id_publication]\" value=\"\" />'
                                . '     <input type=\"hidden\" id=\"jform_publications_"+sequence+"_del_id_cv\" name=\"jform[publications]["+sequence+"][del_id_cv]\" value=\"\" />'
                                . '"';

                $script[] = '      registerFunction("pubOk_"+sequence+"(title, authors, pages, area, issn, publisher, date, doc, link){ '
                                . ' document.id(\"jform_publications_"+sequence+"_title\").value = title;'
                                . ' document.id(\"jform_publications_"+sequence+"_authors\").value = authors;'
                                . ' document.id(\"jform_publications_"+sequence+"_pages\").value = pages;'
                                . ' document.id(\"jform_publications_"+sequence+"_area\").value = area;'
                                . ' document.id(\"jform_publications_"+sequence+"_issn\").value = issn;'
                                . ' document.id(\"jform_publications_"+sequence+"_publisher\").value = publisher;'
                                . ' document.id(\"jform_publications_"+sequence+"_date\").value = date;'
                                . ' document.id(\"jform_publications_"+sequence+"_doc\").value = doc;'
                                . ' document.id(\"jform_publications_"+sequence+"_link\").value = link;'
                                . ' SqueezeBox.close(); }");';
                $script[] = '}';
              
                $script[] = 'function pubDel(pubIndex) {'
                            . '      document.getElementById("jform_publications_"+pubIndex+"_del_id_publication").setAttribute("value",document.getElementById("jform_publications_"+pubIndex+"_id_publication").value);'
                            . '      document.getElementById("jform_publications_"+pubIndex+"_del_id_cv").setAttribute("value",document.getElementById("jform_publications_"+pubIndex+"_id_cv").value);'
                            . '      var group_title = document.getElementById("group_"+pubIndex+"_title");'
                            . '      var group_authors = document.getElementById("group_"+pubIndex+"_authors");'
                            . '      var field_pages = document.getElementById("jform_publications_"+pubIndex+"_pages");'
                            . '      var field_area = document.getElementById("jform_publications_"+pubIndex+"_area");'
                            . '      var field_issn = document.getElementById("jform_publications_"+pubIndex+"_issn");'
                            . '      var field_publisher = document.getElementById("jform_publications_"+pubIndex+"_publisher");'
                            . '      var field_date = document.getElementById("jform_publications_"+pubIndex+"_date");'
                            . '      var field_doc = document.getElementById("jform_publications_"+pubIndex+"_doc");'
                            . '      var field_link = document.getElementById("jform_publications_"+pubIndex+"_link");'
                            . '      group_title.parentNode.removeChild(group_title);'
                            . '      group_authors.parentNode.removeChild(group_authors);'
                            . '      field_pages.parentNode.removeChild(field_pages);'
                            . '      field_area.parentNode.removeChild(field_area);'
                            . '      field_issn.parentNode.removeChild(field_issn);'
                            . '      field_publisher.parentNode.removeChild(field_publisher);'
                            . '      field_date.parentNode.removeChild(field_date);'
                            . '      field_doc.parentNode.removeChild(field_doc);'
                            . '      field_link.parentNode.removeChild(field_link);'
                            . '}';

                // Add to document head
                JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                
                return implode("\n", $html);
          }
      
 }
