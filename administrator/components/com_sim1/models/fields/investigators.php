<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

/**
 * Investigator Form Field class for the SIM0 component
 *
 * @since  0.0.1
 */
class JFormFieldInvestigator extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var         string
	 */
	protected $type = 'Investigator';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	protected function getOptions()
	{
                // Create a new query object.
		$db 	= &$this->getDbo();
		$query 	= $db->getQuery(true);
                
		$query->select('*');
		$query->from('persons');
//		$query->leftJoin('entities_persons on id_person=#entities_persons.id_person');
		$db->setQuery((string) $query);
		$messages = $db->loadObjectList();
		$options  = array();

		if ($messages)
		{
			foreach ($messages as $message)
			{
				$options[] = JHtml::_('select.option', $message->id_person, $message->name);
			}
		}

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
