 <?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
 defined('_JEXEC') or die('Restricted access');
 
 // Load modal behavior
JHtml::_('behavior.modal','a.modal');
jimport('joomla.form.formfield');
 
 /**
  * Book form field class
  */
 class JFormFieldModal_UnitServices extends JFormField
 {
     
        /**
           * Method to get the field input markup
           */
          protected function getInput()
          {
                // Load modal behavior
                //JHtml::_('behavior.modal', 'a.modal');

                // Setup variables for display
                $services = array();
                $html = array();
                $html[] = '<div id="control-services">';

                if ($this->form->getValue('id_entity')) {
                    // Create a new query object.
                    $sys = &SysARDITI::getInstance();
                    $db = &$sys->getDBO();
                    $query = $db->getQuery(true);
                    $query->select('*')
                        ->from($db->quoteName('services'))
                        ->where($db->quoteName('services.id_entity') . ' = '.$this->form->getValue('id_entity'));
                    $db->setQuery($query);
                    $services = $db->loadObjectList();
 
                    if (!$services) {
                            JError::raiseWarning(500, $db->getErrorMsg());
                    }

                }
                
                $script = array();
                $k=0;
                $script[] = 'var sequence=0;';
                $script[] = 'function registerFunction(functionBody) { '
                              . '"use strict"; '
                              . 'var script = document.createElement("script"); '
                              . 'script.innerHTML = "function " + functionBody; '
                              . 'document.head.appendChild(script);}';
                if (!$services) {
                      $html[] = '  <div id="serviceNew" class="control-label btn-wrapper">';
                      $html[] = '      <a class="btn btn-small"  onclick="sequence=-1; serviceNew();"><span class="icon-new icon-white"></span>New</a>';
                      $html[] = '  </div>';
                }
                
                foreach ($services as $i => $row) {
//                    $serviceInEntity = new PersonInEntity($row->id_service, $row->id_entity);

                    // Build the script
                    $script[] = 'registerFunction("serviceOk_'.$i.'(description, procedures, restrictions, costs){ '
                                        . 'document.id(\"'.$this->getSequencedId("description", "description", $i).'\").value = description; '
                                        . 'document.id(\"'.$this->getSequencedId("procedures", "procedures", $i).'\").value = procedures; '
                                        . 'document.id(\"'.$this->getSequencedId("restrictions", "restrictions", $i).'\").value = restrictions; '
                                        . 'document.id(\"'.$this->getSequencedId("costs", "costs", $i).'\").value = costs; '
                                        . 'SqueezeBox.close(); }");';
                    $script[] = 'sequence='.$i.';';

                    $link = 'index.php?option=com_sim1&amp;view=researchunit&amp;layout=modal_service&amp;tmpl=component&amp;id_entity='.$row->id_entity.'&amp;id_service='.$row->id_service.'&amp;id_bond='.$row->id_bond.'&amp;function=serviceOk_'.$i;

                    $html[] = '<div id="group_'.$i.'_service_name" class="control-group">';
                    $html[] = '  <div class="control-label">Nº '.($i+1).' - '.JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_NAME_LABEL').'</div>';
                    $html[] = '  <div class="controls">';
                    $html[] = '     <input type="text" id="'.$this->getSequencedId("name", "name", $i).'" name="'.$this->getSequencedName("name", $i).'" value="'.$row->name.'" size="35" />';
                    $html[] = $this->showSelectServices($i, $row->id_services_type);
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_service", "id_service", $i).'" name="'.$this->getSequencedName("id_service", $i).'" value="'.$row->id_service.'" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_entity", "id_entity", $i).'" name="'.$this->getSequencedName("id_entity", $i).'" value="'.$row->id_entity.'" />';
                    $html[] = '  </div>';
                    $html[] = '  <div class="controls btn-wrapper">';
                    $html[] = '      <a id="edit-modal" class="btn btn-small modal"  onclick="SqueezeBox.setContent(\'iframe\',\''.$link.'\');"><span class="icon-edit"></span>Edit</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="serviceNew();"><span class="icon-new icon-white"></span>New</a>';
                    $html[] = '      <a class="btn btn-small"  onclick="serviceDel('.$i.');"><span class="icon-delete"></span>Del</a>';
                    $html[] = '  </div>';
                    $html[] = '</div>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("description", "description", $i).'" name="'.$this->getSequencedName("description", $i).'" value="'.$row->description.'"/>';
//                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("id_services_type", "id_services_type", $i).'" name="'.$this->getSequencedName("id_services_type", $i).'" value="'.$row->id_services_type.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("procedures", "procedures", $i).'" name="'.$this->getSequencedName("procedures", $i).'" value="'.$row->procedures.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("restrictions", "restrictions", $i).'" name="'.$this->getSequencedName("restrictions", $i).'" value="'.$row->restrictions.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("costs", "costs", $i).'" name="'.$this->getSequencedName("costs", $i).'" value="'.$row->costs.'"/>';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("del_id_service", "del_id_service", $i).'" name="'.$this->getSequencedName("del_id_service", $i).'" />';
                    $html[] = '     <input type="hidden" id="'.$this->getSequencedId("del_id_entity", "del_id_entity", $i).'" name="'.$this->getSequencedName("del_id_entity", $i).'" />';

                    $k=$i;
                }
                $html[] = '</div>';
                $html[] = '<div id="control-new-services">';
                $html[] = '</div>';

                $link_new = 'index.php?option=com_sim1&amp;view=researchunit&amp;layout=modal_service&amp;tmpl=component&amp;id_entity='.$this->form->getValue('id_entity').'&amp;function=serviceOk_';
                
                $script[] = 'function serviceNew() {';
                $script[] = '      sequence++;';
                $script[] = '      var newPerson = document.getElementById("control-new-services");';
                $script[] = '      var newDiv = document.createElement("div");';
                $script[] = '      var serviceNew = document.getElementById("serviceNew");';
                $script[] = '      newPerson.appendChild(newDiv);';
                $script[] = '      if (serviceNew) serviceNew.parentNode.removeChild(serviceNew);';
                $script[] = '      newDiv.innerHTML = '
                                . '"<div id=\"group_"+sequence+"_service_name\" class=\"control-group\">'
                                . '  <div class=\"control-label\">Nº "+(sequence+1)+" - '.JText::_('COM_SIM1_RESEARCHUNIT_SERVICE_NEW_NAME_LABEL').'</div>'
                                . '  <div class=\"controls\">'
                                . '     <input type=\"text\" id=\"'.$this->getSequencedId("name", "name", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("name", "\"+sequence+\"").'\" size=\"35\" />'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("id_service", "id_service", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("id_service", "\"+sequence+\"").'\" />'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("name", "name", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("name", "\"+sequence+\"").'\" size=\"35\" />'
                                . $this->showSelectServices('"+sequence+"', null)
                                . '  </div>'
                                . '  <div class=\"controls btn-wrapper\">'
                                . '     <a class=\"btn btn-small modal\" onclick=\"'
                                                . '     var _name= (document.getElementById(\''.$this->getSequencedId("name", "name", "\"+sequence+\"").'\').value); '
                                                . '     if (_name) {'
                                                . '             SqueezeBox.setContent(\'iframe\',\''.$link_new.'"+sequence+"&new_name=\'+_name);'
                                                . '     }'
                                . '         \"><span class=\"icon-edit\"></span>Edit</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"serviceNew();\"><span class=\"icon-new icon-white\"></span>New</a>'
                                . '     <a class=\"btn btn-small\"  onclick=\"serviceDel("+sequence+");\"><span class=\"icon-delete\"></span>Del</a>'
                                . '  </div>'
                                . '</div>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("description", "description", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("description", "\"+sequence+\"").'\"/>'
//                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("id_services_type", "id_services_type", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("id_services_type", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("procedures", "procedures", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("procedures", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("restrictions", "restrictions", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("restrictions", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("costs", "costs", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("costs", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_service", "del_id_service", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_service", "\"+sequence+\"").'\"/>'
                                . '     <input type=\"hidden\" id=\"'.$this->getSequencedId("del_id_entity", "del_id_entity", "\"+sequence+\"").'\" name=\"'.$this->getSequencedName("del_id_entity", "\"+sequence+\"").'\"/>'
                                . '"';
                $script[] = '      registerFunction("serviceOk_"+sequence+"(description, procedures, restrictions, costs){ '
                                                    . 'document.id(\"'.$this->getSequencedId("description", "description", "\"+sequence+\"").'\").value = description; '
//                                                    . 'document.id(\"'.$this->getSequencedId("id_services_type", "id_services_type", "\"+sequence+\"").'\").value = id_services_type; '
                                                    . 'document.id(\"'.$this->getSequencedId("procedures", "procedures", "\"+sequence+\"").'\").value = procedures; '
                                                    . 'document.id(\"'.$this->getSequencedId("restrictions", "restrictions", "\"+sequence+\"").'\").value = restrictions; '
                                                    . 'document.id(\"'.$this->getSequencedId("costs", "costs", "\"+sequence+\"").'\").value = costs; '
                                                    . 'SqueezeBox.close(); }");';
                $script[] = '}';
                
                $script[] = 'function serviceDel(pIndex) {'
                            . '      var id_service = document.getElementById("'.$this->getSequencedId("id_service", "id_service", "\"+pIndex+\"").'");'
                            . '      var id_entity = document.getElementById("'.$this->getSequencedId("id_entity", "id_entity", "\"+pIndex+\"").'");'
                            . '      if (id_service != null && id_entity != null) {'
                            . '             document.getElementById("'.$this->getSequencedId("del_id_service", "del_id_service", "\"+pIndex+\"").'").setAttribute("value", id_service.value);'
                            . '             document.getElementById("'.$this->getSequencedId("del_id_entity", "del_id_entity", "\"+pIndex+\"").'").setAttribute("value", id_entity.value);'
                            . '      } '
                            . '      var group_name = document.getElementById("group_"+pIndex+"_service_name");'
                            . '      var group_new_name = document.getElementById("group_"+pIndex+"_service_new_name");'
                            . '      group_name.parentNode.removeChild(group_name);'
                            . '      if (group_new_name) group_new_name.parentNode.removeChild(group_new_name);'
                            . '}';

                // Add to document head
                JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
                
                return implode("\n", $html);
          }
          
          protected function showSelectServices($i, $id_selected) {
                // Setup list of investgators for display in combobox
                $sel_persons = array();
                $html_persons = '<select id='.$this->getSequencedId("id_services_type", "id_services_type", $i).' name='.$this->getSequencedName("id_services_type", $i).']>';

              // Create a new query object.
		$sys = &SysARDITI::getInstance();
		$dbJ = &$sys->getDBO();
                $query = $dbJ->getQuery(true);
                    $query->select('*')
                        ->from($dbJ->quoteName('#__services_type'));
                $dbJ->setQuery($query);
                $sel_persons = $dbJ->loadObjectList();

                if (!$sel_persons) {
                        JError::raiseWarning(500, $dbJ->getErrorMsg());
                }
                
                foreach ($sel_persons as $i => $row) {
                      $html_persons .= '<option value='.$row->id_services_type.' '.(($row->id_services_type === $id_selected)? "selected" : "").'>'.$row->name.'</option>';
                }
                
                $html_persons .=  '</select>';
                
                return $html_persons;
          }
          
         /**
	 * Method to get the id used for the field input tag.
	 *
	 * @param   string  $fieldId    The field element id.
	 * @param   string  $fieldName  The field element name.
	 *
	 * @return  string  The id to be used for the field input tag.
	 *
	 * @since   11.1
	 */
	protected function getSequencedId($fieldId, $fieldName, $sequence)
	{
                $id = '';
                $id .= $this->getId($fieldId, $fieldName);
                $prefix = $this->formControl.'_'.$this->group;
                
                return str_replace ($prefix, $prefix.'_'.((string)$sequence), $id);
	}          
 
	/**
	 * Method to get the name used for the field input tag.
	 *
	 * @param   string  $fieldName  The field element name.
	 *
	 * @return  string  The name to be used for the field input tag.
	 *
	 * @since   11.1
	 */
	protected function getSequencedName($fieldName, $sequence)
	{
                $name = '';
                $name .= $this->getName($fieldName);
                $prefix = $this->formControl.'['.$this->group.']';
                
                return str_replace($prefix, $prefix.'['.((string)$sequence).']', $name);
	}        
}
