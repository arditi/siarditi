<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Load modal behavior
JHtml::_('behavior.modal','a.modal');

use Joomla\Registry\Registry;
/**
 * ResearchUnit Model
 *
 * @since  0.0.1
 */
class SIM1ModelResearchUnit extends JModelAdmin
{
	/**
	 *
	 */
	public function __construct($config = array()){
		parent::__construct($config);
		$sys = &SysARDITI::getInstance();
		$db = &$sys->getDBO();
		parent::setDbo($db);
        }

        
        /**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'ResearchUnit', $prefix = 'SIM1Table', $config = array())
	{
            if (!array_key_exists('dbo', $config)) {	
                    $config['dbo'] = &$this->getDBO();
            }		

            return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_sim1.researchunit',
			'researchunit',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}
        
	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_sim1/models/forms/helloworld.js';
	}
	
	/**
	 * Method to get a form object.
	 *
	 * @param   string   $name     The name of the form.
	 * @param   string   $source   The form source. Can be XML string if file flag is set to false.
	 * @param   array    $options  Optional array of options for the form creation.
	 * @param   boolean  $clear    Optional argument to force load a new form.
	 * @param   string   $xpath    An optional xpath to search for the fields.
	 *
	 * @return  mixed  JForm object on success, False on error.
	 *
	 * @see     JForm
	 * @since   12.2
	 */
	protected function loadForm($name, $source = null, $options = array(), $clear = false, $xpath = false)
	{
		// Handle the optional arguments.
		$options['control'] = JArrayHelper::getValue($options, 'control', false);

		// Create a signature hash.
		$hash = md5($source . serialize($options));

		// Check if we can use a previously loaded form.
		if (isset($this->_forms[$hash]) && !$clear)
		{
			return $this->_forms[$hash];
		}

		// Get the form.
		JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT . '/models/fields');
		JForm::addFormPath(JPATH_COMPONENT . '/model/form');
		JForm::addFieldPath(JPATH_COMPONENT . '/model/field');

		try
		{
			$form = JForm::getInstance($name, $source, $options, false, $xpath);

			if (isset($options['load_data']) && $options['load_data'])
			{
				// Get the data for the form.
				$data = $this->loadFormData();
			}
			else
			{
				$data = array();
			}

			// Allow for additional modification of the form, and events to be triggered.
			// We pass the data because plugins may require it.
			$this->preprocessForm($form, $data);

			// Load the data into the form after the plugins have operated.
			$form->bind($data);
		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());

			return false;
		}

		// Store the form for later.
		$this->_forms[$hash] = $form;

		return $form;
	}
        
        /**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
        
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_sim1.edit.researchunit.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}
     
        
	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function getItem($pk = null)
	{
		$pk = (!empty($pk)) ? $pk : (int) $this->getState($this->getName() . '.id');
		$table = $this->getTable();

                $table_investigators = $this->getTable('persons', 'SIM1Table', array());
                $table_entitiesinvestigators = $this->getTable('entitiespersons', 'SIM1Table', array());
                $table_managers = $this->getTable('persons', 'SIM1Table', array());
                $table_entitiesmanagers = $this->getTable('entitiespersons', 'SIM1Table', array());
<<<<<<< HEAD
                $table_managerbonds = $this->getTable('entitiesbonds', 'SIM1Table', array());
                $table_services = $this->getTable('services', 'SIM1Table', array());
                $table_equipments = $this->getTable('equipments', 'SIM1Table', array());
            
                $properties_investigators = array();
                $properties_managers = array();
                $properties_head_manager = null;
                $properties_services = array();
                $properties_equipments = array();
=======
                
                $properties_investigators = array();
                $properties_managers = array();
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
                
                $researchunit = new Entity($pk);

		if ($pk > 0)
		{
                    // Attempt to load the row.
                    $return = $table->load($pk);//($table->load($pk) && $table_jusers->load($investigator->id_user));

                    // Check for a table object error.
                    if ($return === false && $table->getError())
                    {
                            $this->setError($table->getError());
                            return false;
                    }

                    if ($researchunit->id_entity > 0) {
<<<<<<< HEAD
                        
                        // Orgãos dirigentes
=======
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
                        foreach ($researchunit->managers as $i => $row) {
                            $properties_managers['id_entity']=$researchunit->id_entity;
                            if ($row->id_person > 0) {
                                $table_entitiesmanagers->load(array('id_entity' => $researchunit->id_entity, 'id_person' => $row->id_person));
                                $properties_entitiesmanagers[$i]=$table_entitiesmanagers->getProperties();
                                $table_managers->load($row->id_person);
                                $properties_managers[$i]=$table_managers->getProperties();
<<<<<<< HEAD
                                $table_managerbonds->load(array('id_entity' => $researchunit->id_entity, 'id_person' => $row->id_person));
                            }
                        }

                        // Dirigente do órgão directivo
                        if ($researchunit->head_manager->id_person>0) {
                            $table_managers->load($researchunit->head_manager->id_person);
                            $properties_head_manager=$table_managers->getProperties();
                        }
                        
                        // Investigadores
=======
                            }
                        }

>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
                        foreach ($researchunit->investigators as $i => $row) {
                            $properties_investigators['id_entity']=$researchunit->id_entity;
                            if ($row->id_person > 0) {
                                $table_entitiesinvestigators->load(array('id_entity' => $researchunit->id_entity, 'id_person' => $row->id_person));
                                $properties_entitiesinvestigators[$i]=$table_entitiesinvestigators->getProperties();
                                $table_investigators->load($row->id_person);
                                $properties_investigators[$i]=$table_investigators->getProperties();
                            }
                        }
<<<<<<< HEAD

                        // Serviços
                        foreach ($researchunit->services as $i => $row) {
                            $properties_services['id_entity']=$researchunit->id_entity;
                            if ($row->id_service > 0) {
                                $table_services->load($row->id_service);
                                $properties_services[$i]=$table_services->getProperties();
                            }
                        }

                        // Equipmamentos
                        foreach ($researchunit->equipments as $i => $row) {
                            $properties_equipments['id_entity']=$researchunit->id_entity;
                            if ($row->id_equipment > 0) {
                                $table_equipments->load($row->id_equipment);
                                $properties_equipments[$i]=$table_equipments->getProperties();
                            }
                        }
=======
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
                    }
                }

		// Convert to the JObject before adding other data.
		$properties = $table->getProperties();

                $item = JArrayHelper::toObject($properties, 'JObject');
                $item->investigators = $properties_investigators;
<<<<<<< HEAD
                $item->head_manager = $properties_head_manager;
                $item->managers = $properties_managers;
                $item->services = $properties_services;
                $item->equipments = $properties_equipments;
=======
                $item->managers = $properties_managers;
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470

                if (property_exists($item, 'params'))
		{
			$registry = new Registry;
			$registry->loadString($item->params);
			$item->params = $registry->toArray();
		}

		return $item;
	}
      
       
        /**
	 * Method to check if it's OK to delete a message. Overwrites JModelAdmin::canDelete
	 */
	protected function canDelete($record)
	{
		if( !empty( $record->id_entity ) )
		{
			return JFactory::getUser()->authorise( "core.delete", "com_sim1.researchunit." . $record->id_person );
		}
	}
	
	/**
	 * Method to validate the form data.
	 *
	 * @return  boolean  True on success.
	 */
	public function validate(JForm $form, array $data, string $group = null) {

                return parent::validate($form, $data, $group);
	}
        
	/**
	 * Method to delete one or more records.
	 *
	 * @param   array  &$pks  An array of record primary keys.
	 *
	 * @return  boolean  True if successful, false if an error occurs.
	 *
	 * @since   12.2
	 */

 	public function delete(&$pks)
 	{
		$pks = (array) $pks;
		$table = $this->getTable();
                $table_entitiespersons = $this->getTable('entitiespersons', 'SIM1Table', array());
                $table_entitiesbonds = $this->getTable('entitiesbonds', 'SIM1Table', array());
               
		// Iterate the items to delete each one.
		foreach ($pks as $i => $pk)
		{
                    if ($table->load($pk))
                    {
                        $entity = new Entity($pk);

                        foreach ($entity->managers as $i => $row) {
                            if ($row->id_person > 0) {
                                $pkdel = array();
                                $pkdel['id_person']=$row->id_person;
                                $pkdel['id_entity']=$entity->id_entity;
                                foreach ($row->bonds as $k => $rowBond) {
                                    $table_entitiesbonds->delete($rowBond->id_bond);
                                }
                                try {
                                    $table_entitiespersons->delete($pkdel);
                                }
                                catch (Exception $e){
                                        SysARDITI::log('SIM1ModelResearchUnit::sql('.$this->setError($e->getMessage()).')');
                                }
                            }
                        }

                        foreach ($entity->investigators as $i => $row) {
                            if ($row->id_person > 0) {
                                $pkdel = array();
                                $pkdel['id_person']=$row->id_person;
                                $pkdel['id_entity']=$entity->id_entity;
                                foreach ($row->bonds as $k => $rowBond) {
                                    $table_entitiesbonds->delete($rowBond->id_bond);
                                }
                                try {
                                    $table_entitiespersons->delete($pkdel);
                                }
                                catch (Exception $e){
                                        SysARDITI::log('SIM1ModelResearchUnit::sql('.$this->setError($e->getMessage()).')');
                                }
                            }
                        }
<<<<<<< HEAD

                        foreach ($entity->services as $i => $row) {
                            if ($row->id_service > 0) {
                                $table_services->delete($row->id_service);
                            }
                        }

                        foreach ($entity->equipments as $i => $row) {
                            if ($row->id_equipment > 0) {
                                $table_equipments->delete($row->id_equipment);
                            }
                        }
=======
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
                    }
		}
                return parent::delete($pks);
        }
       
        /**
	 * Method to save the form data.
	 * @param   array  $data  The form data.
	 * @return  boolean  True on success.
	 */

        public function save($data)
        {
            $table_investigators = $this->getTable('persons', 'SIM1Table', array());
            $table_managers = $this->getTable('persons', 'SIM1Table', array());
            $table_entitiesinvestigators = $this->getTable('entitiespersons', 'SIM1Table', array());
            $table_entitiesmanagers = $this->getTable('entitiespersons', 'SIM1Table', array());
            $table_investigatorbonds = $this->getTable('entitiesbonds', 'SIM1Table', array());
            $table_managerbonds = $this->getTable('entitiesbonds', 'SIM1Table', array());
<<<<<<< HEAD
            $table_services = $this->getTable('services', 'SIM1Table', array());
            $table_equipments = $this->getTable('equipments', 'SIM1Table', array());
=======
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470

            // Chama o parent para gravar a tabela entities e devolve o estado
            // que indica se criou novo registo ou fez update
            parent::save($data);
            $pk = (!empty($data[$key])) ? $data[$key] : (int) $this->getState($this->getName() . '.id');

            // Store the data.
<<<<<<< HEAD
            // Orgão dirigentes
=======
            // Managers
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
            if ($data['managers']['id_entity']=="")
                $data['managers']['id_entity']=$pk;

            foreach ($data['managers'] as $i => $row) {
                if (is_array($row)) {
                    if (!$row['del_id_person']>0) {
                        $pk_investigators=$row['id_person'];
                        if (isset($row['name']) && $row['name']!="") {
                            $table_jusers = $this->getTable('Jusers', 'SIM1Table', array());
                            $authentication = Person::generateLogin($row['name']);
                            $row['username']= $authentication[0];
                            $row['password']= $authentication[1];
                            $table_jusers->save($row);
                            $row['id_person']="";
                            $row['id_user']=$table_jusers->get('id');
                        } 
                        $table_managers->save($row);
                        $pk_managers=$table_managers->get('id_person');

                        if (!$row['del_id_entity']>0) {
                            if ($row['id_entity']=="")
                                $row['id_entity'] = $pk;
                            if ($row['id_person']=="")
                                $row['id_person'] = $pk_managers;
                            if (!isset($row['id_bond']))
                                $row['id_bond']="";
                            
                            $table_entitiesmanagers->save($row);
                            $table_managerbonds->save($row);
                        }
                    } else {
                        $pki = array();
                        $pki['id_person']=$row['del_id_person'];
                        $pki['id_entity']=$row['del_id_entity'];

                        $table_managerbonds->delete($row['del_id_bond']);
                        try {
                            $table_entitiesmanagers->delete($pki);
                        }
                        catch (Exception $e){
                                SysARDITI::log('SIM1ModelResearchUnit::sql('.$this->setError($e->getMessage()).')');
                        }
                    }
                }
            }

<<<<<<< HEAD
            // Dirigente do órgão directivo
            if ($data['head_manager']['id_person']>0) {
                $table_managers->save($data['head_manager']);
            }
            
            // Investigadores
=======
            // Researchers
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
            if ($data['investigators']['id_entity']=="")
                $data['investigators']['id_entity']=$pk;

            foreach ($data['investigators'] as $i => $row) {
                if (is_array($row)) {
                    if (!$row['del_id_person']>0) {
                        $pk_investigators=$row['id_person'];
                        if (isset($row['name']) && $row['name']!="") {
                            $table_jusers = $this->getTable('Jusers', 'SIM1Table', array());
                            $authentication = Person::generateLogin($row['name']);
                            $row['username']= $authentication[0];
                            $row['password']= $authentication[1];
                            $table_jusers->save($row);
                            $row['id_person']="";
                            $row['id_user']=$table_jusers->get('id');
                        } 
                        $table_investigators->save($row);
                        $pk_investigators=$table_investigators->get('id_person');

                        if (!$row['del_id_entity']>0) {
                            if ($row['id_entity']=="")
                                $row['id_entity'] = $pk;
                            if ($row['id_person']=="")
                                $row['id_person'] = $pk_investigators;
                            $table_entitiesinvestigators->save($row);
                            $table_investigatorbonds->save($row);
                        }
                    } else {
                        $pki = array();
                        $pki['id_person']=$row['del_id_person'];
                        $pki['id_entity']=$row['del_id_entity'];

                        $table_investigatorbonds->delete($row['del_id_bond']);
                        try {
                            $table_entitiesinvestigators->delete($pki);
                        }
                        catch (Exception $e){
                                SysARDITI::log('SIM1ModelResearchUnit::sql('.$this->setError($e->getMessage()).')');
                        }
                    }
                }
            }
<<<<<<< HEAD

            // Serviços
            if ($data['services']['id_entity']=="")
                $data['services']['id_entity']=$pk;

            foreach ($data['services'] as $i => $row) {
                if (is_array($row)) {
                    if (!$row['del_id_service']>0) {
                        if ($row['id_entity']=="")
                            $row['id_entity'] = $pk;
                        $table_services->save($row);
                        $pk_services=$table_services->get('id_service');
                    } else {
                        $pki = array();
                        $pki['id_service']=$row['del_id_service'];
                        $pki['id_entity']=$row['del_id_entity'];
                        $table_services->delete($row['del_id_service']);
                    }
                }
            }
            
            // Equipmentos
            if ($data['equipments']['id_entity']=="")
                $data['equipments']['id_entity']=$pk;

            foreach ($data['equipments'] as $i => $row) {
                if (is_array($row)) {
                    if (!$row['del_id_equipment']>0) {
                        if ($row['id_entity']=="")
                            $row['id_entity'] = $pk;
                        $table_equipments->save($row);
                        $pk_equipments=$table_equipments->get('id_equipment');
                    } else {
                        $pki = array();
                        $pki['id_equipment']=$row['del_id_equipment'];
                        $pki['id_entity']=$row['del_id_entity'];
                        $table_equipments->delete($row['del_id_equipment']);
                    }
                }
            }

=======
            
>>>>>>> 036cd4884b7a2cdb5c1440ca74aaf24013257470
            return true;
        }

}


