<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim1
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tabstate');

// Set some global property
$document = JFactory::getDocument();
$document->addStyleDeclaration('.icon-sim0 {background-image: url(../media/com_sim0/images/logo-m0-16.png);}');

// Access check: is this user allowed to access the backend of this component?
if (!JFactory::getUser()->authorise('core.manage', 'com_sim1'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// require helper file
JLoader::register('SIM1Helper', JPATH_COMPONENT . '/helpers/investigator.php');

// Get an instance of the controller prefixed by SIM1
$controller = JControllerLegacy::getInstance('SIM1');

// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
