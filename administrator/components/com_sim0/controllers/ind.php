<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * SIARDITI Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 * @since       0.0.9
 */
class SIM0ControllerInd extends JControllerForm
{
	/**
	* Implement to allowAdd or not
	*
	* Not used at this time (but you can look at how other components use it....)
	* Overwrites: JControllerForm::allowAdd
	*
	* @param array $data
	* @return bool
	*/
	protected function allowAdd($data = array())
	{
		return parent::allowAdd($data);
	}
	/**
	* Implement to allow edit or not
	* Overwrites: JControllerForm::allowEdit
	*
	* @param array $data
	* @param string $key
	* @return bool
	*/
	protected function allowEdit($data = array(), $key = 'id_indicator')
	{
		$id = isset( $data[ $key ] ) ? $data[ $key ] : 0;
		if( !empty( $id ) )
		{
			return JFactory::getUser()->authorise( "core.edit", "com_sim0.ind." . $id );
		}
	}
	
	protected function apply($data = array())
	{
		return parent::apply($data);
	}	
	
	
	/**
	 * addyear!
	 * 
	 */
	public function addyear(){
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		// Required objects 
		$input = JFactory::getApplication()->input; 
		// Get the form data 
		$formData = new JRegistry($input->get('jform', array(), 'array')); 
		//print_r($formData);die("sss=".$formData['id_indicator']);
		
		// Get the model.
		$model 	= $this->getModel();
		// Save the form
		$result = $model->save($formData);
		if (!$result){
			JError::raiseWarning(500, $model->getError());
		}
		$result = $model->addyear($formData);
		if (!$result){
			JError::raiseWarning(500, $model->getError());
		}
		//JFactory::getApplication()->enqueueMessage(JText::_('OK1'), 'info');
		$this->setMessage(JText::plural('COM_SIM0_IND_ADDYEAR_OK', $count));
		$this->setRedirect('index.php?option=com_sim0&view=ind&layout=edit&id_indicator='.$formData['id_indicator']);
	}
	/**
	 * delyear!
	 * 
	 */
	public function delyear(){
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$input = JFactory::getApplication()->input; 
		$formData = new JRegistry($input->get('jform', array(), 'array')); 
		//print_r($formData);die("sss=".$formData['id_indicator']);
		
		
		// Get the model.
		$model 	= $this->getModel();
		$result = $model->save($formData);
		if (!$result){
			JError::raiseWarning(500, $model->getError());
		}
		$result = $model->delyear($formData);
		if (!$result){
			JError::raiseWarning(500, $model->getError());
		}
		//JFactory::getApplication()->enqueueMessage(JText::_('OK1'), 'info');
		$this->setMessage(JText::plural('COM_SIM0_IND_DELYEAR_OK', $count));
		$this->setRedirect('index.php?option=com_sim0&view=ind&layout=edit&id_indicator='.$formData['id_indicator']);
	}	
	/**
	 * addregion!
	 * 
	 */
	public function addregion(){
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		// Required objects 
		$input = JFactory::getApplication()->input; 
		// Get the form data 
		$formData = new JRegistry($input->get('jform', array(), 'array')); 
		//print_r($formData);die("sss=".$formData['id_indicator']);
		
		// Get the model.
		$model 	= $this->getModel();
		// Save the form
		$result = $model->save($formData);
		if (!$result){
			JError::raiseWarning(500, $model->getError());
		}
		$result = $model->addregion($formData);
		if (!$result){
			JError::raiseWarning(500, $model->getError());
		}
		
		//JFactory::getApplication()->enqueueMessage(JText::_('OK1'), 'info');
		$this->setMessage(JText::_('COM_SIM0_IND_ADDREGION_OK'));
		$this->setRedirect('index.php?option=com_sim0&view=ind&layout=edit&id_indicator='.$formData['id_indicator']);
	}	
}