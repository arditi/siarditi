<?php
defined( '_JEXEC' ) or die;

//jimport('joomla.application.component.controlleradmin');
//class MCSVControllerStats extends JControllerAdmin
class SIM0ControlleStats extends JControllerLegacy 
{

	public function getModel($name = 'Stats', $prefix = 'SIM0Model', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	
	// Usado nas listagens dependentes para regressaema esta agina
	public function cancel($key = null)
	{
		//$return = parent::cancel($key);		// So existe se: extends JControllerForm
		// Redirect to the Prdoducts page.
		//$this->setRedirect(JRoute::_('index.php?option=&amp;view=', false));
		$this->setRedirect('index.php?option=com_sim0&view=dashboard');
	}
		
}