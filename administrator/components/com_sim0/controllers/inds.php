<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Users list controller class.
 *
 * @since  1.6
 */
class SIM0ControllerInds extends JControllerAdmin
{
	/**
	 * @var    string  The prefix to use with controller messages.
	 * @since  1.6
	 */
	protected $text_prefix = 'COM_SIM0_INDS';

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.6
	 * @see     JController
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
		// Another way
		$this->registerTask('block', 	'changeState');
		$this->registerTask('unblock', 	'changeState');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Ind', $prefix = 'SIM0Model', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * Method to change the state status on a record.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function changeState()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('block' => 1, 'unblock' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids)){
			JError::raiseWarning(500, JText::_('COM_SIM0_INDS_NO_ITEM_SELECTED'));
		}
		else{
			// Get the model.
			$model = $this->getModel();
			// Change the state of the records.
			if (!$model->changeState($ids, $value)){
				JError::raiseWarning(500, $model->getError());
			}
			else{
				$this->setMessage(JText::plural( ($value == 0)? 'COM_SIM0_INDS_N_ITEMS_ENABLE' : 'COM_SIM0_INDS_N_ITEMS_DISABLE' , count($ids)));
				if ($value == 1){
					//$this->setMessage(JText::plural('COM_SIM0_N_USERS_BLOCKED', count($ids)));
				}elseif ($value == 0){
					//$this->setMessage(JText::plural('COM_SIM0_N_USERS_UNBLOCKED', count($ids)));
				}
			}
		}

		$this->setRedirect('index.php?option=com_sim0&view=inds');
	}
	
	/**
	 * Method to activate a record.
	 * @return  void
	 */
	public function activate()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$ids = $this->input->get('cid', array(), 'array');
		if (empty($ids)){
			JError::raiseWarning(500, JText::_('COM_SIM0_NO_ITEM_SELECTED'));
		}
		else{
			// Get the model.
			$model = $this->getModel();			
			// Change the state of the records.
			if (!$model->activate($ids)){
				JError::raiseWarning(500, $model->getError());
			}
			else{
				$this->setMessage(JText::plural('COM_SIM0_INDS_N_ITEMS_ACTIVATED', count($ids)));
			}
		}
		$this->setRedirect('index.php?option=com_sim0&view=inds');
	}
	
}
