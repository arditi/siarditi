<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Tracks list controller class.
 *
 * @since  1.6
 */
class SIM0ControllerNotifications extends JControllerLegacy
{
	/**
	 * @var     string  The prefix to use with controller messages.
	 *
	 * @since   1.6
	 */
	protected $context = 'com_sim0.notifications';

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Notifications', $prefix = 'SIM0Model', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to remove a record.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function delete()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Get the model.
		$model = $this->getModel();

		// Load the filter state.
		$app = JFactory::getApplication();

		/*
		$type = $app->getUserState($this->context . '.filter.type');
		$model->setState('filter.type', $type);

		$begin = $app->getUserState($this->context . '.filter.begin');
		$model->setState('filter.begin', $begin);

		$end = $app->getUserState($this->context . '.filter.end');
		$model->setState('filter.end', $end);

		$categoryId = $app->getUserState($this->context . '.filter.category_id');
		$model->setState('filter.category_id', $categoryId);

		$clientId = $app->getUserState($this->context . '.filter.client_id');
		$model->setState('filter.client_id', $clientId);

		$model->setState('list.limit', 0);
		$model->setState('list.start', 0);

		$count = $model->getTotal();

		// Remove the items.
		if (!$model->delete())
		{
			JError::raiseWarning(500, $model->getError());
		}
		else
		{
			$this->setMessage(JText::plural('COM_SIM0_NOTIFICATIONS_N_ITEMS_DELETED_MORE', $count));
		}
		*/

		$this->setRedirect('index.php?option=com_sim0&view=notifications');
		
		/**
	 * Removes an item.
	 *
	 * Overrides JControllerAdmin::delete to check the core.admin permission.
	 *
	 * @return  boolean  Returns true on success, false on failure.
	 *
	 * @since   1.6
	 * /
	public function delete()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids = $this->input->get('cid', array(), 'array');

		if (!JFactory::getUser()->authorise('core.admin', $this->option))
		{
			JError::raiseError(500, JText::_('JERROR_ALERTNOAUTHOR'));
			jexit();
		}
		elseif (empty($ids))
		{
			JError::raiseWarning(500, JText::_('COM_USERS_NO_LEVELS_SELECTED'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			JArrayHelper::toInteger($ids);

			// Remove the items.
			if (!$model->delete($ids))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				$this->setMessage(JText::plural('COM_USERS_N_LEVELS_DELETED', count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_users&view=levels');
		*/
	}
	/**
	 * Method to show notificationtemplates
	 * @return  void
	 */
	public function notificationTemplates()
	{
		$this->setRedirect('index.php?option=com_sim0&view=notificationtemplates');
	}
}
