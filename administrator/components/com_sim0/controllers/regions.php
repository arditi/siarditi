<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Tracks list controller class.
 *
 * @since  1.6
 */
class SIM0ControllerRegions extends JControllerLegacy
{
	/**
	 * @var     string  The prefix to use with controller messages.
	 *
	 * @since   1.6
	 */
	protected $context = 'com_sim0.regions';

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Region', $prefix = 'SIM0Model', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to remove a record.
	 * @return  void
	 */
	public function deleteItem()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Get the model.
		$model = $this->getModel();

		// Load the filter state.
		$app = JFactory::getApplication();
		/*
		$type = $app->getUserState($this->context . '.filter.type');
		$model->setState('filter.type', $type);

		$begin = $app->getUserState($this->context . '.filter.begin');
		$model->setState('filter.begin', $begin);

		$end = $app->getUserState($this->context . '.filter.end');
		$model->setState('filter.end', $end);

		$categoryId = $app->getUserState($this->context . '.filter.category_id');
		$model->setState('filter.category_id', $categoryId);

		$clientId = $app->getUserState($this->context . '.filter.client_id');
		$model->setState('filter.client_id', $clientId);

		$model->setState('list.limit', 0);
		$model->setState('list.start', 0);

		$count = $model->getTotal();

		// Remove the items.
		if (!$model->delete())
		{
			JError::raiseWarning(500, $model->getError());
		}
		else
		{
			$this->setMessage(JText::plural('COM_SIM0_NOTIFICATIONS_N_ITEMS_DELETED_MORE', $count));
		}
		*/
		$this->setRedirect('index.php?option=com_sim0&view=dimensions');
	}
	/**
	 * Method to activate a record.
	 * @return  void
	 */
	public function delete()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$ids = $this->input->get('cid', array(), 'array');
		if (empty($ids)){
			JError::raiseWarning(500, JText::_('COM_SIM0_NO_ITEM_SELECTED'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();
			// Change the state of the records.
			if (!$model->delete($ids))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				$this->setMessage(JText::plural('COM_SIM0_REGS_N_ITEMS_DELETED', count($ids)));
			}
		}
		$this->setRedirect('index.php?option=com_sim0&view=regions');
	}	
	
	/**
	 * Method to go back show notifications
	 * @return  void
	 */
	public function cancel()
	{
		$this->setRedirect('index.php?option=com_sim0&view=regions');
	}	
}
