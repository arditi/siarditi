<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * SIARDITI Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 * @since       0.0.9
 */
class SIM0ControllerNotification extends JControllerForm
{
	/**
	* Implement to allowAdd or not
	*
	* Not used at this time (but you can look at how other components use it....)
	* Overwrites: JControllerForm::allowAdd
	*
	* @param array $data
	* @return bool
	*/
	protected function allowAdd($data = array())
	{
		return parent::allowAdd($data);
	}
	/**
	* Implement to allow edit or not
	* Overwrites: JControllerForm::allowEdit
	*
	* @param array $data
	* @param string $key
	* @return bool
	*/
	protected function allowEdit($data = array(), $key = 'id_nt')
	{
		return true;
		
		// EJ: To understand!
		$id = isset( $data[ $key ] ) ? $data[ $key ] : 0;
		if( !empty( $id ) )
		{
			return JFactory::getUser()->authorise( "core.edit", "com_sim0.notification." . $id );
		}
	}

	/**
	 * Method to save the configuration data.
	 *
	 * @param   array  $data  An array containing all global config data.
	 * @return  boolean  True on success, false on failure.
	 * @throws  RuntimeException
	 */
	public function save($key = null, $urlVar = null){
		return parent::save($key, $urlVar);
	}
	/**
	 * Save form!
	 * Is the old fashion, but I prefer it...
	 *
	 * @param   array  $data  The form data.
	 * @return  boolean  True on success.
	 */
	public function save_MY_NO_NEEDED_NOW(){
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		// Required objects 
		$input = JFactory::getApplication()->input; 
		// Get the form data 
		$formData = new JRegistry($input->get('jform', array(), 'array')); 
		print_r($formData);
		// Get the model.
		$model = $this->getModel();
		// Save the form
		$result = $model->save($formData);
		if (!$result){
			JError::raiseWarning(500, $model->getError());
		}
		//JFactory::getApplication()->enqueueMessage(JText::_('OK1'), 'info');
		$this->setMessage(JText::plural('COM_SIM0_NOTIFICATIONS_N_ITEMS_DELETED_MORE', $count));
		$this->setRedirect('index.php?option=com_sim0&view=notificationtemplates');
	}
	
	/**
	 * SendMail!
	 * @param   array  $data  The form data.
	 * @return  boolean  True on success.
	 */
	public function send(){
		// Required objects 
		$input = JFactory::getApplication()->input; 
		// Get the ID data 
		$id_not = $input->get('id_not', '');
		if(isset($id_not) && is_numeric($id_not)){
			// Get the model.
			$model = $this->getModel();
			// Send the form
			$result = $model->SendNotificationMail($id_not);
			if (!$result){
				JError::raiseWarning(500, $model->getError());
			}
			//JFactory::getApplication()->enqueueMessage(JText::_('OK1'), 'info');
			$this->setMessage(JText::_('COM_SIM0_NOTIFICATION_SEND_OK'));
		}
		else{
			$this->setMessage(JText::_('COM_SIM0_NOTIFICATION_SEND_NOK'));
		}
		$this->setRedirect('index.php?option=com_sim0&view=notifications');
	}	
}
