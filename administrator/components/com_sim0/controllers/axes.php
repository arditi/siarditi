<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Tracks list controller class.
 *
 * @since  1.6
 */
class SIM0ControllerAxes extends JControllerLegacy
{
	/**
	 * @var     string  The prefix to use with controller messages.
	 *
	 * @since   1.6
	 */
	protected $context = 'com_sim0.axes';

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Axis', $prefix = 'SIM0Model', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	
	/**
	 * Method to delete a record.
	 * @return  void
	 */
	public function delete()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
				
		//$app = JFactory::getApplication();
		$ids = $this->input->get('cid', array(), 'array');
		if (empty($ids)){
			JError::raiseWarning(500, JText::_('COM_SIM0_NO_ITEM_SELECTED'));
		}
		else{
			// Get the model.
			$model = $this->getModel();		
			
			//$clientId = $this->getUserStateFromRequest($this->context . '.filter.name');
			//$model->setState('filter.name', $clientId);
			//$purpose = $this->getUserStateFromRequest($this->context . '.filter.purpose', 'filter_purpose');
			//$this->setState('filter.external', $purpose);
			
			$model->setState('list.limit', 0);
			$model->setState('list.start', 0);		

			// Change the state of the records.
			if (!$model->delete($ids)){
				JError::raiseWarning(500, $model->getError());
			}
			else{
				$this->setMessage(JText::plural('COM_SIM0_AXES_N_ITEMS_DELETED_MORE', count($ids)));
			}
		}
		$this->setRedirect('index.php?option=com_sim0&view=axes');
	}
	
	/**
	 * Method to go back show notifications
	 * @return  void
	 */
	public function cancel()
	{
		$this->setRedirect('index.php?option=com_sim0&view=inds');
	}	
}
