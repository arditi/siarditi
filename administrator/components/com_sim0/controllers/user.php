<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * SIARDITI Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 * @since       0.0.9
 */
class SIM0ControllerUser extends JControllerForm
{
	
	/**
	* Implement to allowAdd or not
	*
	* Not used at this time (but you can look at how other components use it....)
	* Overwrites: JControllerForm::allowAdd
	*
	* @param array $data
	* @return bool
	*/
	protected function allowAdd($data = array())
	{
		return parent::allowAdd($data);
	}
	/**
	* Implement to allow edit or not
	* Overwrites: JControllerForm::allowEdit
	*
	* @param array $data
	* @param string $key
	* @return bool
	*/
	protected function allowEdit($data = array(), $key = 'id_user')
	{
		// EJ: To understand!
		$id = isset( $data[ $key ] ) ? $data[ $key ] : 0;
		if( !empty( $id ) )
		{
			return JFactory::getUser()->authorise( "core.edit", "com_sim0.doc." . $id );
		}
	}
	
	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array()){
		$this->id_user = $model->getItem()->id_user;
	}
	/**
	 * Method to save the configuration data.
	 *
	 * @param   array  $data  An array containing all global config data.
	 * @return  boolean  True on success, false on failure.
	 * @throws  RuntimeException
	 * /
	public function save($key = null, $urlVar = null){
			$app     		= JFactory::getApplication();
			$this->id_doc   = $app->input->get('id_doc');
			$result 		= parent::save($key, $urlVar);	// reescreve this->id_doc em caso de novo documento!
            if($result){
				// http://joomla.stackexchange.com/questions/5042/how-can-i-can-get-the-new-record-id-after-saving-data-with-jmodel
				$files 	  	= $app->input->files->get('jform');
				$upload   	= $files['file_uri'];	// file array
				// Se houve upload
                if($upload['tmp_name']!=""){
                    $model  	= $this->getModel();
					// Gera novo nome para este file...
					$file_uri 	= $model->GenerateFileURI($upload, $this->id_doc);
                    // Tenta fazer upload para este file...
					if($return = $model->uploadFile($upload, $file_uri)){
						// Get TableDoc instance
						$table = $model->getTable();
						// Load the message
						$table->load($this->id_doc); 						
						// Update file_uri field...
						$table->file_uri = $file_uri;
						// Update Row
						$table->store();
						
						$app->enqueueMessage(JText::_('COM_SIM0_DOCS_NEWFILE_UPLOAD_SUCCESS') . $upload['name']);
						$redirect = base64_encode($return);
						$url = 'index.php?option=com_sim0&view=doc&layout=edit&id_doc=' . $this->id_doc . '&file=' . $redirect;
						$this->setRedirect(JRoute::_($url, false));
                    }
					else
                    {
						$app->enqueueMessage(JText::_('COM_SIM0_DOCS_NEWFILE_UPLOAD_ERROR'), 'error');
						$url = 'index.php?option=com_sim0&view=doc&layout=edit&id_doc=' . $this->id_doc . '&file=' . $file;
						$this->setRedirect(JRoute::_($url, false));
						$result=false;
                    }
                }
            }
			return $result;
	}
	/**
	 * Method for uploading a file.
	 *
	 * @return  void
	 */
	public function uploadFile()
	{
		$app      = JFactory::getApplication();
		$id_doc   = $app->input->get('id_doc');
		$file     = $app->input->get('file');
		$upload   = $app->input->files->get('files');
		$location = base64_decode($app->input->get('address'));
		$model    = $this->getModel();
		if($return=$model->uploadFile($upload, $location))
		{
			$app->enqueueMessage(JText::_('COM_SIM0_DOCS_NEWFILE_UPLOAD_SUCCESS') . $upload['name']);
			$redirect = base64_encode($return);
			$url = 'index.php?option=com_sim0&view=docs&id_doc=' . $id_doc . '&file=' . $redirect;
			$this->setRedirect(JRoute::_($url, false));
		}
		else
		{
			$app->enqueueMessage(JText::_('COM_SIM0_DOCS_NEWFILE_UPLOAD_ERROR'), 'error');
			$url = 'index.php?option=com_sim0&view=docs&id_doc=' . $id_doc . '&file=' . $file;
			$this->setRedirect(JRoute::_($url, false));
		}
	}
	
	/**
	 * Method to go back show docs
	 * @return  void
	 */
	public function cancel($key = NULL) 
	{
		// Redirect to login
		//$uri        = JFactory::getURI();
		//$return     = $uri->toString();
		//$url  		= 'index.php?option=com_users&amp;view=login&amp;return=' . urlencode(base64_encode($return));
		$app      	= JFactory::getApplication();
		$return  	= $app->input->get('return');
		//print_r($app->input);die("return=".$return);
		if($return!='')	$this->setRedirect($return);
		else			$this->setRedirect('index.php?option=com_sim0&view=users');
	}
	
	/**
	 * Method to go back show docs
	 * @return  void
	 */
	public function goDocs($key = NULL) 
	{
		$this->setRedirect('index.php?option=com_sim0&view=docs');
	}
	/**
	 * Method to go back show docs
	 * @return  void
	 */
	public function goUsers($key = NULL) 
	{
		$this->setRedirect('index.php?option=com_sim0&view=users');
	}
	/**
	 * Method to go back show docs
	 * @return  void
	 */
	public function goLogActions($key = NULL) 
	{
		$this->setRedirect('index.php?option=com_sim0&view=logactions');
	}
	public function goNotifications($key = NULL) 
	{
		$this->setRedirect('index.php?option=com_sim0&view=notifications');
	}
	
}
