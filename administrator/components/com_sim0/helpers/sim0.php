<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * SIM0 component helper.
 *
 * @param   string  $submenu  The name of the active view.
 *
 * @return  void
 *
 * @since   1.6
 */
function buildTree(array $elements, $parentId = 0) {
    $branch = array();
    foreach ($elements as $element) {
        if ($element['id_category_parent'] == $parentId) {
            $children = buildTree($elements, $element['id_category']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}
 
abstract class SIM0Helper{
	
	/**
	 * Configure the Linkbar on the Left.
	 */
	public static function addSubmenu($submenu) 
	{
		/////////////
		// TOOLBAR
		//
	
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
		//$bar->appendButton('Standard', 'save-copy', "Templates", "logactions.notificationTemplates", false);
		//JToolbarHelper::title(JText::_('COM_USERS_VIEW_USERS_TITLE'), 'users user');
		
		// Home
		//JToolbarHelper::cancel('dashboard.home');
		JToolbarHelper::custom('dashboard.home', 'arrow-up', 'arrow-up', 'DashBoard', false, false);
		JToolBarHelper::divider();
	

		
		// Options
		//if ($canDo->get('core.admin'))
		if($submenu=="dashboard")
		{
			JToolbarHelper::preferences('com_sim0');
			JToolbarHelper::divider();
		}
		// Help
		JToolBarHelper::help('components_sim0_help', true);
		
		
		/////////////
		// SUBMENU
		//
		if($submenu=="notificationtemplates"){
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_SUBMENU_BACK'),			'index.php?option=com_sim0&view=notifications',	$submenu == 'notifications'
			);
		}
		elseif($submenu=="docs" || $submenu=="doccats" || $submenu=="docstats"){
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_DOCS_SUBMENU_DOCS'),		'index.php?option=com_sim0&view=docs',	$submenu == 'docs'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_DOCS_SUBMENU_CATS'),		'index.php?option=com_sim0&view=doccats',	$submenu == 'doccats'
			);
		//	JHtmlSidebar::addEntry(
		//		JText::_('COM_SIM0_DOCS_SUBMENU_STATS'),	'index.php?option=com_sim0&view=docstats',	$submenu == 'docstats'
		//	);
		}
		elseif($submenu=="inds" || $submenu=="axes" ||  $submenu=="dimensions" || $submenu=="regions"){
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_INDS_SUBMENU_DOCS'),		'index.php?option=com_sim0&view=inds',	$submenu == 'inds'
			);
			
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_INDS_SUBMENU_CATS'),		'index.php?option=com_sim0&view=axes',	$submenu == 'axes'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_INDS_SUBMENU_DIMS'),	'index.php?option=com_sim0&view=dimensions',	$submenu == 'dimensions'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_INDS_SUBMENU_REGS'),	'index.php?option=com_sim0&view=regions',	$submenu == 'regions'
			);/*
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_INDS_SUBMENU_STATS'),	'index.php?option=com_sim0&view=indstats',	$submenu == 'indstats'
			);
			*/
		}		
		else{
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_SUBMENU_LOGACTIONS'),	'index.php?option=com_sim0&view=logactions',	$submenu == 'logactions'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_SIM0_SUBMENU_NOTIFICATIONS'),	'index.php?option=com_sim0&view=notifications',	$submenu == 'notifications'
			);
		}
		/*
		// Old Style!
		JSubMenuHelper::addEntry(
			JText::_('COM_SIM0_SUBMENU_LOGACTIONS'),'index.php?option=com_sim0',$submenu == 'logactions'
		);

		JSubMenuHelper::addEntry(
			JText::_('COM_SIM0_SUBMENU_CATEGORIES'),'index.php?option=com_categories&view=categories&extension=com_sim0',$submenu == 'categories'
		);
		*/

		// Retirar daqui
		// set some global property
		$document = JFactory::getDocument();
		$document->addStyleDeclaration('.icon-48-m0 '.'{background-image: url(../media/com_sim0/images/logo-m0-48.png);}');
		if ($submenu == 'categories') 
		{
			$document->setTitle(JText::_('COM_SIM0_ADMINISTRATION_CATEGORIES'));
		}
	}

	/**
	 * Get the actions
	 */
	public static function getActions2($messageId = 0)
	{	
		$result	= new JObject;
		if (empty($messageId)) {
			$assetName = 'com_sim0';
		}
		else {
			$assetName = 'com_sim0.message.'.(int) $messageId;
		}
		$actions = JAccess::getActions('com_sim0', 'component');
		foreach ($actions as $action) {
			$result->set($action->name, JFactory::getUser()->authorise($action->name, $assetName));
		}
		return $result;
	}
	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return  JObject
	 * @deprecated  3.2  Use JHelperContent::getActions() instead
	 */
	public static function getActions()
	{
		// Log usage of deprecated function
		JLog::add(__METHOD__ . '() is deprecated, use JHelperContent::getActions() with new arguments order instead.', JLog::WARNING, 'deprecated');
		//die("TIRAR DAQUI");

		// Get list of actions
		$result = JHelperContent::getActions('com_sim0');
		return $result;
	}	
	
	
	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	public static function getActionOptions()
	{
		$sys   = &SysARDITI::getInstance();
		$db    = &$sys->getDBO();		
		$query = $db->getQuery(true);
		$query->select('id_action, action');
		$query->from('logactiontypes');
		// Só os SuperAdmins vêem as LogAction todas
		if(!$sys->isRoot()){
			$query->where('id_action > 10');			
		}
		$query->order('id_action ASC');		
		// Retrieve only published items
		$db->setQuery((string) $query);
		$messages = $db->loadObjectList();

/*		$messages = array();
		$messages[0]= new JObject();
		$messages[0]->id_action = 1;
		$messages[0]->action 	= "Navigate";	*/		
		$options  = array();
		if ($messages)
			foreach ($messages as $message){
				$options[] = JHtml::_('select.option', $message->id_action, $message->action);
			}
		//$options = array_merge(parent::getOptions(), $options);
		return $options;
	}
	
	/**
	 * Get client list in text/value format for a select field
	 *
	 * @return  array
	 */
	public static function getTemplateOptions($category='')
	{
		$options 	= array();
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		//$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->quoteName('id_template').' As value, subject As text')
			->from('notificationtemplates AS t');
		if (($category)&& !empty($category))
			if (is_numeric($category))
				$query->where('t.category = '.$category);
		$query->order('t.subject');
		// Get the options.
		$db->setQuery($query);
		try
		{
			$options = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JError::raiseWarning(500, $e->getMessage());
		}
		// Merge any additional options in the XML definition.
		// $options = array_merge(parent::getOptions(), $options);
		//array_unshift($options, JHtml::_('select.option', '0', JText::_('COM_SIM0_NO_CLIENT')));
		return $options;
	}
	
	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	public static function getDocCategoriesOptions($id_cat_parent=null)
	{
		/*
		$sys   = &SysARDITI::getInstance();
		$db    = &$sys->getDBO();		
		$query = $db->getQuery(true);
		$query->select('id_action, action');
		$query->from('logactiontypes');
		// Só os SuperAdmins vêem as LogAction todas
		if(!$sys->isRoot()){
			$query->where('id_action > 10');			
		}		
		// Retrieve only published items
		$db->setQuery((string) $query);
		$messages = $db->loadObjectList();
	
		$options  = array();
		if ($messages)
			foreach ($messages as $message){
				$options[] = JHtml::_('select.option', $message->id_action, $message->action);
			}
		//$options = array_merge(parent::getOptions(), $options);
		return $options;
		*/
			$options= array();
			$sys 	= &SysARDITI::getInstance();
			$db 	= &$sys->getDBO();	
			$query  = $db->getQuery(true)
				->select('id_category AS value')
				->select('category AS text')
				->select('id_category_parent')
				->select('level')
				->from  ('doccategories')		
				->order ('path ASC, category DESC');
			if (($id_cat_parent)&& !empty($id_cat_parent))
				if (is_numeric($id_cat_parent))
					$query->where('id_category_parent = '.$id_cat_parent);
				
			$db->setQuery($query);
			if ($options = $db->loadObjectList())
			{
				//ksort($options, SORT_STRING);
				
				foreach ($options as &$option)
				{
					$option->text = str_repeat('- ', $option->level) . $option->text;
				}
				//static::$options[$hash] = array_merge(static::$options[$hash], $options);
			}
			return $options;
	}
	
	

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	 
	public static function getDocCategoriesTree( $id_cat_parent=null)
	{
			$tree= array();
			$sys 	= &SysARDITI::getInstance();
			$db 	= &$sys->getDBO();	
			$query  = $db->getQuery(true)
				->select('id_category')
				->select('category AS text')
				->select('id_category_parent')
				->select('level')
				->select('path')
				->select('id_category as href')
				->from  ('doccategories')		
				->order ('path ASC, category DESC');
			if (($id_cat_parent)&& !empty($id_cat_parent))
				if (is_numeric($id_cat_parent))
					$query->where('id_category_parent = '.$id_cat_parent);
				else
					$query->where('id_category_parent is null');
					
			$db->setQuery($query);
			if ($rows = $db->loadAssocList())
			{
				//print_r($rows);	die();
				$data = array();
				//$tree = buildTree($rows);
				
				foreach ($rows as &$row){
					
					$row['nodes'] = array();
					$vn = "row" . $row['id_category'];
					${$vn} = $row;
					if(!is_null($row['id_category_parent'])) {
						$vp = "parent" . $row['id_category_parent'];
						if(isset($data[$row['id_category_parent']])) {
							${$vp} = $data[$row['id_category_parent']];
						}
						else {
							${$vp} = array('id_category' => $row['id_category_parent'], 'id_category_parent' => null, 'text' => 'Raiz', 'nodes' => array());
							$data[$row['id_category_parent']] = &${$vp};
						}
						${$vp}['nodes'][] = &${$vn};
						$data[$row['id_category_parent']] = ${$vp};
					}
					$data[$row['id_category']] = &${$vn};
	
				}
				//$data[0]['text']='wwwwwwwwww';
				//print_r($data);
				//die();
			}
			
			
			/*
$dbh = new PDO(CONNECT_STRING, USERNAME, PASSWORD);
$dbs = $dbh->query("SELECT n_id, n_parent_id from test_table order by n_parent_id, n_id");
$elems = array();

while(($row = $dbs->fetch(PDO::FETCH_ASSOC)) !== FALSE) {
    $row['children'] = array();
    $vn = "row" . $row['n_id'];
    ${$vn} = $row;
    if(!is_null($row['n_parent_id'])) {
        $vp = "parent" . $row['n_parent_id'];
        if(isset($data[$row['n_parent_id']])) {
            ${$vp} = $data[$row['n_parent_id']];
        }
        else {
            ${$vp} = array('n_id' => $row['n_parent_id'], 'n_parent_id' => null, 'children' => array());
            $data[$row['n_parent_id']] = &${$vp};
        }
        ${$vp}['children'][] = &${$vn};
        $data[$row['n_parent_id']] = ${$vp};
    }
    $data[$row['n_id']] = &${$vn};
}
$dbs->closeCursor();

$result = array_filter($data, function($elem) { return is_null($elem['n_parent_id']); });
print_r($result);			
			*/
			
			$tree = array_filter($data, function($elem) { return is_null($elem['id_category_parent']); });
			//print_r($tree);
			//$tree = array_shift($tree);
			//print_r($tree);
			return $tree;
	}	
	
	
	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   3.2
	 */
	public static function getAxes()
	{
		$options= array();
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query = $db->getQuery(true)
			->select('id_axis AS value')
			->select('name AS text')
			->from  ('axis')		
			//->group ('id_category_parent, category')
			->order ('name DESC');
		$db->setQuery($query);
		if ($options = $db->loadObjectList())
		{
			//ksort($options, SORT_STRING);
			foreach ($options as &$option)
			{
			//$options[] = JHtml::_('select.option', $option->value, $option->text);
			}
		}
		return $options;
	}
	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   3.2
	 */
	public static function getDimensions()
	{
		$options= array();
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query = $db->getQuery(true)
			->select('id_dimension AS value')
			->select('name AS text')
			->from  ('dimension')
			->order ('name DESC');
		$db->setQuery($query);
		if ($options = $db->loadObjectList())
		{
			foreach ($options as &$option)
			{
				//$options[] = JHtml::_('select.option', $option->value, $option->text);
			}
		}
		return $options;
	}	
	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   3.2
	 */
	public static function getRegions()
	{
		$options= array();
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query = $db->getQuery(true)
			->select('id_region AS value')
			->select('name AS text')
			->from  ('regions')
			->order ('name DESC');
		$db->setQuery($query);
		if ($options = $db->loadObjectList())
		{
			foreach ($options as &$option)
			{
				//$options[] = JHtml::_('select.option', $option->value, $option->text);
			}
		}
		return $options;
	}		
	
	/**
	 * Creates a list of validate options used in filter select list
	 * used in com_sim0 on docs view
	 *
	 * @return  array
	 */
	public static function getValidatedOptions()
	{
		$options = array(
			JHtml::_('select.option', '1', 	 JText::_('COM_SIM0_FILTER_VALIDATED_YES')),
			JHtml::_('select.option', '0',   JText::_('COM_SIM0_FILTER_VALIDATED_NO')),
		);
		return $options;
	}	
	
	/**
	 * Creates a list of range options used in filter select list
	 * used in com_users on users view
	 *
	 * @return  array
	 *
	 * @since   2.5
	 */
	public static function getRangeOptions()
	{
		//COM_USERS_OPTION_FILTER_DATE
		$options = array(
			JHtml::_('select.option', 'today', 		 JText::_('COM_USERS_OPTION_RANGE_TODAY')),
			JHtml::_('select.option', 'past_week', 	 JText::_('COM_USERS_OPTION_RANGE_PAST_WEEK')),
			JHtml::_('select.option', 'past_1month', JText::_('COM_USERS_OPTION_RANGE_PAST_1MONTH')),
			JHtml::_('select.option', 'past_3month', JText::_('COM_USERS_OPTION_RANGE_PAST_3MONTH')),
			JHtml::_('select.option', 'past_6month', JText::_('COM_USERS_OPTION_RANGE_PAST_6MONTH')),
			JHtml::_('select.option', 'past_year', 	 JText::_('COM_USERS_OPTION_RANGE_PAST_YEAR')),
			JHtml::_('select.option', 'post_year',   JText::_('COM_USERS_OPTION_RANGE_POST_YEAR')),
		);
		return $options;
	}	

	
	//// STATS
	/**
	 * TopSells()
	 * @return Returns the Top Sells
	 */
	public static function TopSells($limit=5){
		$id_event_type = 26;	// Buy Products
		return SIM0Helper::Tops($id_event_type, $limit);
	}
	/**
	 * Tops()
	 * @return Returns the Top Sells
	 */
	public static function Tops($id_event_type, $limit=5)
	{
		//$id_event_type = 24;	// Click Product
		//$id_event_type = 26;	// Buy Products
		
		//$db 		= &JFactory::getDBO();
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		$query 		= $db->getQuery(true);
		$session 	= JFactory::getSession();
		$idApp		= $session->get('filter.apps','');
		$idShop		= $session->get('filter.shops','');
		$idMachine	= $session->get('filter.machines','');
		$start  	= 'now()';
		$steps  	= $session->get('filter.steps', 1);
/*	SELECT events.id_product, products.name, COUNT( * ) AS total FROM jos_mcsv_events AS 
	EVENTS INNER JOIN jos_mcsv_products AS products ON events.id_product = products.id_product
	WHERE events.id_machine =1 AND events.id_shop =1 AND events.id_app =1 AND events.sys_date > DATE_SUB( NOW( ) , INTERVAL 12 MONTH ) 
	ORDER BY total DESC LIMIT 3 OFFSET 0
		*/
		/*
		$query->select('events.id_product, products.name, count(*) as total');
		$query->from('logaction AS events INNER JOIN #__mcsv_products AS products ON events.id_product=products.id_product');
		$query->where('events.id_product IS NOT NULL');
		$query->where('events.id_event_type='.$id_event_type);
		*/
		$query->select('id_action as id_product, source as name, count(*) as total');
		$query->from('logactions as events');
/*		// filtra a query pelos formulario
		if($idMachine!="")	$query->where('events.id_machine='.$idMachine);
		if($idShop!="")		$query->where('events.id_shop='.$idShop);
		if($idApp!="")		$query->where('events.id_app='.$idApp);
		if($steps!="")		$query->where('events.sys_date > DATE_SUB( '.$start.', INTERVAL '.$steps.' MONTH)');
		$query->group('events.id_product');
*/		//sys_date >= DATE_SUB(now(), INTERVAL 365 DAY)";
		$query->order('total DESC limit '.$limit.' OFFSET 0');
		
		$db->setQuery((string)$query );
		$options  = $db->loadObjectList();
		//print_r($options);die((string)$query);
		return $options;
	}
	
	
	/**
	 * Tries to load the router for the component and calls it. Otherwise calls getRoute.
	 *
	 * @param   integer  $id  The ID of the tag
	 *
	 * @return  string  URL link to pass to JRoute
	 *
	 * @since   3.1
	 */
	public static function getDocRoute($id)
	{
		$needles = array(
			'doc'  => array((int) $id)
		);

		if ($id < 1){
			$link = '';
		}
		else{
			$link = 'index.php?option=com_sim0&view=doc&id_doc=' . $id;

//			if ($item = self::_findItem($needles)){
//				$link .= '&Itemid=' . $item;
//			}
		}
		return $link;
	}
	
}
