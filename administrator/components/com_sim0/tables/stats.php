<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
 
// import Joomla table library
jimport('joomla.database.table');
 
/**
 * Cards Table class
 */
class MCSVTableStats extends JTable
{
	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	public function __construct(&$db) 
	{
		parent::__construct('#__mcsv_applications', 'id_app', $db);
	}
}

?>