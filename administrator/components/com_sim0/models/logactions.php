<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorldList Model
 *
 * @since  0.0.1
 */
class SIM0ModelLogActions extends JModelList
{
	/**
	 * @since   1.6
	 */
	protected $basename;
		
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])){
			$config['filter_fields'] = array('a.id_log', 'a.id_user', 'a.id_action', 'a.source', 'a.sys_date');
		}
		parent::__construct($config);
		// Insert new LogAction
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		/*
		$app = JFactory::getApplication();//'administrator');
		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout', 'default', 'cmd')){
		//if ($layout = $app->input->get('layout')){
			$this->context .= '.' . $layout;
		}
*/
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		
		//$category = $this->getUserStateFromRequest($this->context . '.filter.category', 'filter_category');
		//$this->setState('filter.category', $category);
		$id_action = $this->getUserStateFromRequest($this->context . '.filter.id_action', 'filter_id_action');
		$this->setState('filter.id_action', $id_action);
		
		$range = $this->getUserStateFromRequest($this->context . '.filter.range', 'filter_range');
		$this->setState('filter.range', $range);

/*		$excluded = json_decode(base64_decode($app->input->get('excluded', '', 'BASE64')));
		if (isset($excluded)){
			JArrayHelper::toInteger($excluded);
		}
		$this->setState('filter.excluded', $excluded);
*/

		// Load the parameters.
		$params = JComponentHelper::getParams('com_sim0');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('sys_date', 'desc');
	}
	/**
	 * Method to get a store id based on model configuration state.
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 * @return  string  A store id.
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.id_user');
		$id .= ':' . $this->getState('filter.id_action');
		//$id .= ':' . $this->getState('filter.range');
		return parent::getStoreId($id);
	}	
	
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		require_once JPATH_COMPONENT . '/helpers/sim0.php';
		
		// Initialize variables.
		$db    	= $this->getDbo();
		$query 	= $db->getQuery(true);
		
		// Create the base select statement.
		$query->select('*, (SELECT action from logactiontypes AS tb WHERE tb.id_action=a.id_action) as action')
			  ->select('(SELECT username from users AS c WHERE c.id_user=a.id_user) as username')
			  ->from('LogActions as a');

		// So os SuperAdmins vêem as LogAction todas
		$sys 	= &SysARDITI::getInstance();
		if(!$sys->isRoot()){
			$query->where('id_action > 10');			
		}

		// Filter: like / search
		$search = $this->getState('filter.search');
		if (!empty($search)){
			$like = $db->quote('%' . $search . '%');
			$query->where('a.source LIKE ' . $like);
		}
		// Filter by id_action
		$id_action = $this->getState('filter.id_action');
		if (is_numeric($id_action)){
			$query->where('a.id_action=' . (int) $id_action);
		}
		// Filter by id_user
		$id_user = $this->getState('filter.id_user');
		if (is_numeric($id_user)){
			$query->where('a.id_user = ' . (int) $id_user);
		}
		// Add filter for registration ranges select list
		$range = $this->getState('filter.range');
		// Apply the range filter.
		if ($range){
			// Get UTC for now.
			$dNow   = new JDate;
			$dStart = clone $dNow;
			switch ($range){
				case 'past_week':
					$dStart->modify('-7 day');
					break;
				case 'past_1month':
					$dStart->modify('-1 month');
					break;
				case 'past_3month':
					$dStart->modify('-3 month');
					break;
				case 'past_6month':
					$dStart->modify('-6 month');
					break;
				case 'post_year':
				case 'past_year':
					$dStart->modify('-1 year');
					break;
				case 'today':
					// Ranges that need to align with local 'days' need special treatment.
					$app    = JFactory::getApplication();
					$offset = $app->get('offset');
					
					// Reset the start time to be the beginning of today, local time.
					$dStart = new JDate('now', $offset);
					$dStart->setTime(0, 0, 0);

					// Now change the timezone back to UTC.
					$tz = new DateTimeZone('GMT');
					$dStart->setTimezone($tz);
					break;
			}
			if ($range == 'post_year'){
				$query->where($db->qn('a.sys_date') . ' < ' . $db->quote($dStart->format('Y-m-d H:i:s')));
			}
			else{
				$query->where($db->qn('a.sys_date') . ' >= ' . $db->quote($dStart->format('Y-m-d H:i:s')) . ' AND ' . $db->qn('a.sys_date') . ' <= ' . $db->quote($dNow->format('Y-m-d H:i:s')) );
			}
		}
		// Add the list ordering clause.
		//$orderCol	= $this->state->get('list.ordering',  'sys_date');
		//$orderDirn 	= $this->state->get('list.direction', 'asc');
		//$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		// Add the list ordering clause.
		$orderCol = $this->getState('list.ordering', 'a.sys_date');
		$query->order($db->escape($orderCol) . ' ' . $db->escape($this->getState('list.direction', 'DESC')));
		
		return $query;
	}
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	public function getItems()
	{
		$items = parent::getItems();
		foreach ($items as &$item) {
			//$item->url = 'index.php?option=com_conetor&amp;task=card.edit&amp;id_card=' . $item->id_card;
		}
		return $items;
	}
	/**
	 * Get file name
	 *
	 * @return  string    The file name
	 *
	 * @since   1.6
	 */
	public function getBaseName()
	{	
			return $this->basename;
	}
}
