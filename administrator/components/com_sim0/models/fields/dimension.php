<?php
/**
 * @package     Joomla.Libraries
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');

/**
 * Field to load a list of available users statuses
 *
 * @since  3.2
 */
class JFormFieldDimension extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   3.2
	 */
	protected $type = 'Dimension';

	/**
	 *
	 */
	protected function getOptions()
	{
		$options = SIM0Helper::getDimensions();
		return array_merge(parent::getOptions(), $options);
	}	
}
