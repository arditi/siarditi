<?php
/**
 * @package     Joomla.Libraries
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');

/**
 * Field to load a list of available users statuses
 *
 * @since  3.2
 */
class JFormFieldAxes extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   3.2
	 */
	protected $type = 'Axes';

	/**
	 * Cached array of the category items.
	 *
	 * @var    array
	 * @since  3.2
	 */
	protected static $options = array();

	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   3.2
	 */
	protected function getOptions2()
	{
		// Hash for caching
		$hash = md5($this->element);

		if (!isset(static::$options[$hash]))
		{
			static::$options[$hash] = parent::getOptions();

			$options= array();
			$sys 	= &SysARDITI::getInstance();
			$db 	= &$sys->getDBO();
			$query = $db->getQuery(true)
				->select('id_axis AS value')
				->select('name AS text')
				->from  ('axis')		
				//->group ('id_category_parent, category')
				->order ('name DESC, category DESC');
			$db->setQuery($query);
			if ($options = $db->loadObjectList())
			{
				//ksort($options, SORT_STRING);
				foreach ($options as &$option)
				{
					//$option->text = str_repeat('- ', $option->level) . $option->text;
					$level = ($option->id_category_parent!="") ? 1 : 0;
					$option->text = str_repeat('- ', $level) . $option->text;
				}
				static::$options[$hash] = array_merge(static::$options[$hash], $options);
			}
		}
		return static::$options[$hash];
	}
	
	/**
	 *
	 */
	protected function getOptions()
	{
		$options = SIM0Helper::getAxes();
		return array_merge(parent::getOptions(), $options);
	}	
}
