<?php
/**
 * @package     Joomla.Libraries
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');

/**
 * Field to load a list of available users statuses
 *
 * @since  3.2
 */
class JFormFieldDocCategories extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   3.2
	 */
	protected $type = 'DocCategories';

	/**
	 * Cached array of the category items.
	 *
	 * @var    array
	 * @since  3.2
	 */
	protected static $options = array();

	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   3.2
	 */
	protected function getOptions2()
	{
		// Hash for caching
		$hash = md5($this->element);

		if (!isset(static::$options[$hash]))
		{
			static::$options[$hash] = parent::getOptions();

			$options= array();
			$sys 	= &SysARDITI::getInstance();
			$db 	= &$sys->getDBO();
			/*
			$db = JFactory::getDbo();
			$query = $db->getQuery(true)
				->select('a.id AS value')
				->select('a.title AS text')
				->select('COUNT(DISTINCT b.id) AS level')
				->from('#__usergroups as a')
				->join('LEFT', '#__usergroups  AS b ON a.lft > b.lft AND a.rgt < b.rgt')
				->group('a.id, a.title, a.lft, a.rgt')
				->order('a.lft ASC');
				*/
			$query = $db->getQuery(true)
				->select('id_category AS value')
				->select('category AS text')
				->select('id_category_parent')
				->from  ('doccategories')
				->group ('id_category_parent, category')
				->order ('id_category_parent DESC, category DESC');
			$db->setQuery($query);
			if ($options = $db->loadObjectList())
			{
				ksort($options, SORT_STRING);
				
				foreach ($options as &$option)
				{
					//$option->text = str_repeat('- ', $option->level) . $option->text;
					$level = ($option->id_category_parent!="") ? 1 : 0;
					$option->text = str_repeat('- ', $level) . $option->text;
				}
				static::$options[$hash] = array_merge(static::$options[$hash], $options);
			}
		}
		return static::$options[$hash];
	}
	
	/**
	 *
	 */
	protected function getOptions()
	{
		$options = SIM0Helper::getDocCategoriesOptions();
		return array_merge(parent::getOptions(), $options);
	}	
}
