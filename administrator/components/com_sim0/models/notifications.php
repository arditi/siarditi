<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of tracks.
 *
 * @since  1.6
 */
class SIM0ModelNotifications extends JModelList
{
	/**
	 * @since   1.6
	 */
	protected $basename;
	
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array()){
		if (empty($config['filter_fields'])){
			$config['filter_fields'] = array('a.id_not', 'a.id_template', 'a.from', 'a.to', 'a.subject', 'a.count', 'a.send_date', 'a.sys_date');
		}
		parent::__construct($config);
		// Insert new LogAction
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);		
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type');
		$this->setState('filter.type', $type);

		$begin = $this->getUserStateFromRequest($this->context . '.filter.begin', 'filter_begin', '', 'string');
		$this->setState('filter.begin', $begin);

		$end = $this->getUserStateFromRequest($this->context . '.filter.end', 'filter_end', '', 'string');
		$this->setState('filter.end', $end);

		$id_template = $this->getUserStateFromRequest($this->context . '.filter.id_template', 'filter_id_template', '');
		$this->setState('filter.id_template', $id_template);

		$clientId = $this->getUserStateFromRequest($this->context . '.filter.client_id', 'filter_client_id', '');
		$this->setState('filter.client_id', $clientId);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_sim0');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.sys_date', 'asc');
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 *
	 * @since   1.6
	 */
	protected function getListQuery()
	{
		require_once JPATH_COMPONENT . '/helpers/sim0.php';

		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			'*,(select subject FROM notificationtemplates as tmpls WHERE tmpls.id_template=a.id_template) as template'
			.','. $db->quoteName('sys_date') . ' as ' . $db->quoteName('data')
		);
		$query->from($db->quoteName('Notifications') . ' AS a');
		
		// Join with the templates
		//$query->join('LEFT', $db->quoteName('notificationtemplates') . ' as tmpls ON tmpls.id_template=n.id_template')
		//	->select('tmpls.subject as template');
		
		// Filter by type
		$type = $this->getState('filter.type');
		if (!empty($type)){
			$query->where('a.id_template in (select id_template from notificationtemplates where external=' . (int) $type. ') ');
		}			
		// Filter by category
		$id_template = $this->getState('filter.id_template');
		if (is_numeric($id_template))
		{
			$query->where('a.id_template = ' . (int) $id_template);
		}

		// Filter by begin date
		$begin = $this->getState('filter.begin');
		if (!empty($begin)){
			$query->where('a.sys_date >= ' . $db->quote($begin));
		}

		// Filter by end date
		$end = $this->getState('filter.end');
		if (!empty($end)){
			$query->where('a.sys_date <= ' . $db->quote($end));
		}

		// Add the list ordering clause.
		$orderCol = $this->getState('list.ordering', 'a.send_date');
		$query->order($db->quoteName($orderCol) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));

		return $query;
	}

	/**
	 * Method to delete rows.
	 *
	 * @return  boolean  Returns true on success, false on failure.
	 */
	public function delete()
	{
		$user = JFactory::getUser();
		$categoryId = $this->getState('id_template');
/*
		// Access checks.
		if ($categoryId)
		{
			$allow = $user->authorise('core.delete', 'com_sim0.category.' . (int) $categoryId);
		}
		else
		{
			$allow = $user->authorise('core.delete', 'com_sim0');
		}

		if ($allow)
		{
			// Delete tracks from this banner
			$db = $this->getDbo();
			$query = $db->getQuery(true)
				->delete($db->quoteName('#__banner_tracks'));

			// Filter by type
			$type = $this->getState('filter.type');

			if (!empty($type))
			{
				$query->where('track_type = ' . (int) $type);
			}

			// Filter by begin date
			$begin = $this->getState('filter.begin');

			if (!empty($begin))
			{
				$query->where('track_date >= ' . $db->quote($begin));
			}

			// Filter by end date
			$end = $this->getState('filter.end');

			if (!empty($end))
			{
				$query->where('track_date <= ' . $db->quote($end));
			}

			$where = '1';

			// Filter by client
			$clientId = $this->getState('filter.client_id');

			if (!empty($clientId))
			{
				$where .= ' AND cid = ' . (int) $clientId;
			}

			// Filter by category
			if (!empty($categoryId))
			{
				$where .= ' AND catid = ' . (int) $categoryId;
			}

			$query->where('banner_id IN (SELECT id FROM ' . $db->quoteName('#__banners') . ' WHERE ' . $where . ')');

			$db->setQuery($query);
			$this->setError((string) $query);

			try
			{
				$db->execute();
			}
			catch (RuntimeException $e)
			{
				$this->setError($e->getMessage());

				return false;
			}
		}
		else
		{
			JError::raiseWarning(403, JText::_('JERROR_CORE_DELETE_NOT_PERMITTED'));
		}
*/
		return true;
	}

	/**
	 * Get file name
	 *
	 * @return  string    The file name
	 *
	 * @since   1.6
	 */
	public function getBaseName()
	{
		/*
		if (!isset($this->basename))
		{
			$app = JFactory::getApplication();
			$basename = $this->getState('basename');
			$basename = str_replace('__SITE__', $app->get('sitename'), $basename);
			$categoryId = $this->getState('filter.category_id');

			if (is_numeric($categoryId))
			{
				if ($categoryId > 0)
				{
					$basename = str_replace('__CATID__', $categoryId, $basename);
				}
				else
				{
					$basename = str_replace('__CATID__', '', $basename);
				}

				$categoryName = $this->getCategoryName();
				$basename = str_replace('__CATNAME__', $categoryName, $basename);
			}
			else
			{
				$basename = str_replace('__CATID__', '', $basename);
				$basename = str_replace('__CATNAME__', '', $basename);
			}

			$clientId = $this->getState('filter.client_id');

			if (is_numeric($clientId))
			{
				if ($clientId > 0)
				{
					$basename = str_replace('__CLIENTID__', $clientId, $basename);
				}
				else
				{
					$basename = str_replace('__CLIENTID__', '', $basename);
				}

				$clientName = $this->getClientName();
				$basename = str_replace('__CLIENTNAME__', $clientName, $basename);
			}
			else
			{
				$basename = str_replace('__CLIENTID__', '', $basename);
				$basename = str_replace('__CLIENTNAME__', '', $basename);
			}

			$type = $this->getState('filter.type');

			if ($type > 0)
			{
				$basename = str_replace('__TYPE__', $type, $basename);
				$typeName = JText::_('COM_BANNERS_TYPE' . $type);
				$basename = str_replace('__TYPENAME__', $typeName, $basename);
			}
			else
			{
				$basename = str_replace('__TYPE__', '', $basename);
				$basename = str_replace('__TYPENAME__', '', $basename);
			}

			$begin = $this->getState('filter.begin');

			if (!empty($begin))
			{
				$basename = str_replace('__BEGIN__', $begin, $basename);
			}
			else
			{
				$basename = str_replace('__BEGIN__', '', $basename);
			}

			$end = $this->getState('filter.end');

			if (!empty($end))
			{
				$basename = str_replace('__END__', $end, $basename);
			}
			else
			{
				$basename = str_replace('__END__', '', $basename);
			}

			$this->basename = $basename;
		}
*/
		return $this->basename;
	}

	/**
	 * Get the category name.
	 *
	 * @return  string    The category name
	 *
	 * @since   1.6
	 */
	protected function getCategoryName()
	{
		$categoryId = $this->getState('filter.category_id');

		if ($categoryId)
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true)
				->select('title')
				->from($db->quoteName('#__categories'))
				->where($db->quoteName('id') . '=' . $db->quote($categoryId));
			$db->setQuery($query);

			try
			{
				$name = $db->loadResult();
			}
			catch (RuntimeException $e)
			{
				$this->setError($e->getMessage());

				return false;
			}
		}
		else
		{
			$name = JText::_('COM_BANNERS_NOCATEGORYNAME');
		}

		return $name;
	}

	/**
	 * Get the category name
	 *
	 * @return  string    The category name.
	 *
	 * @since   1.6
	 */
	protected function getClientName()
	{
		$clientId = $this->getState('filter.client_id');
		if ($clientId)/*
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true)
				->select('name')
				->from($db->quoteName('#__banner_clients'))
				->where($db->quoteName('id') . '=' . $db->quote($clientId));
			$db->setQuery($query);

			try
			{
				$name = $db->loadResult();
			}
			catch (RuntimeException $e)
			{
				$this->setError($e->getMessage());

				return false;
			}
		}
		else*/
		{
			$name = JText::_('COM_BANNERS_NOCLIENTNAME');
		}

		return $name;
	}
}
