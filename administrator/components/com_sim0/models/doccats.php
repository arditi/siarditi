<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Model Notificationtemplates
 *
 * @since  1.6
 */
class SIM0ModelDoccats extends JModelList
{
	/**
	 * Constructor.
	 * @param   array  $config  An optional associative array of configuration settings.
	 */
	public function __construct($config = array()){
		if (empty($config['filter_fields'])){
			$config['filter_fields'] = array('a.id_category', 'a.id_category_parent', 'a.category', 'a.category_uk', 'a.del_date',	'a.path',	'a.sys_date');
		}
		parent::__construct($config);
		// 
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}

	/**
	 * Method to auto-populate the model state.
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication('administrator');
		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout', 'default', 'cmd')){
			$this->context .= '.' . $layout;
		}

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$category = $this->getUserStateFromRequest($this->context . '.filter.category', 'filter_category');
		$this->setState('filter.category', $category);

		$id_parent = $this->getUserStateFromRequest($this->context . '.filter.id_parent', 'filter_id_parent');
		$this->setState('filter.id_parent', $id_parent);

		$range = $this->getUserStateFromRequest($this->context . '.filter.range', 'filter_range');
		$this->setState('filter.range', $range);


		// Load the parameters.
		$params = JComponentHelper::getParams('com_sim0');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.path', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 * @return  string  A store id.
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.category');
		$id .= ':' . $this->getState('filter.parent');
		$id .= ':' . $this->getState('filter.range');
		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery()
	{
		require_once JPATH_COMPONENT . '/helpers/sim0.php';

		// Create a new query object.
		$db 	= $this->getDbo();
		$query 	= $db->getQuery(true);
		
		// Select the required fields from the table.
		$query->select('*');
		$query->select(' (SELECT category FROM doccategories as d WHERE a.id_category_parent=d.id_category) as parent ');
		
		$query->from($db->quoteName('doccategories') . ' AS a');

		// Filter the items over the search string if set.
		if ($this->getState('filter.search') !== '' && $this->getState('filter.search') !== null){
			// Escape the search token.
			$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($this->getState('filter.search')), true) . '%'));
			// Compile the different search clauses.
			$searches   = array();
			$searches[] = 'a.category LIKE ' . $search;
			//$searches[] = 'a.body LIKE ' . $search;
			//$searches[] = 'a.body_uk LIKE ' . $search;
			// Add the clauses to the query.
			$query->where('(' . implode(' OR ', $searches) . ')');
		}
		// If the model is set to check the parent, add to the query.
		$id_parent = $this->getState('filter.id_parent');
		if (is_numeric($id_parent)){
			$query->where('a.id_category_parent='.(int)$id_parent);
		}			
		// Add filter for registration ranges select list
		$range = $this->getState('filter.range');
		// Apply the range filter.
		if ($range){
			// Get UTC for now.
			$dNow   = new JDate;
			$dStart = clone $dNow;
			switch ($range){
				case 'past_week':
					$dStart->modify('-7 day');
					break;
				case 'past_1month':
					$dStart->modify('-1 month');
					break;
				case 'past_3month':
					$dStart->modify('-3 month');
					break;
				case 'past_6month':
					$dStart->modify('-6 month');
					break;
				case 'post_year':
				case 'past_year':
					$dStart->modify('-1 year');
					break;
				case 'today':
					// Ranges that need to align with local 'days' need special treatment.
					$app    = JFactory::getApplication();
					$offset = $app->get('offset');
					
					// Reset the start time to be the beginning of today, local time.
					$dStart = new JDate('now', $offset);
					$dStart->setTime(0, 0, 0);

					// Now change the timezone back to UTC.
					$tz = new DateTimeZone('GMT');
					$dStart->setTimezone($tz);
					break;
			}
			if ($range == 'post_year'){
				$query->where($db->qn('a.sys_date') . ' < ' . $db->quote($dStart->format('Y-m-d H:i:s')));
			}
			else{
				$query->where($db->qn('a.sys_date') . ' >= ' . $db->quote($dStart->format('Y-m-d H:i:s')) . ' AND ' . $db->qn('a.sys_date') . ' <= ' . $db->quote($dNow->format('Y-m-d H:i:s')) );
			}
		}
		
		// Add the list ordering clause.
		$orderCol = $this->getState('list.ordering', 'a.category');
		$query->order('a.path ASC');		
		$query->order($db->escape($orderCol) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));
		
//		SELECT *, (SELECT category FROM doccategories as d WHERE a.id_category_parent=d.id_category) as parent FROM `doccategories` AS a ORDER BY a.sys_date asc LIMIT 0, 20
//		SELECT a.*,COUNT(DISTINCT c2.id) AS level FROM `dbv1_usergroups` AS a LEFT OUTER JOIN `dbv1_usergroups` AS c2 ON a.lft > c2.lft AND a.rgt < c2.rgt GROUP BY a.id, a.lft, a.rgt, a.parent_id, a.title ORDER BY a.lft asc LIMIT 0, 20
//		SELECT a.*,COUNT(DISTINCT c2.id_category) AS level FROM doccategories AS a LEFT OUTER JOIN doccategories AS c2 ON a.id_category_parent = c2.id_category         GROUP BY a.id_category, a.parent_id, a.title ORDER BY a.sys_date asc LIMIT 0, 20
//		SELECT a.*,COUNT(DISTINCT c2.id_category) AS level FROM doccategories AS a LEFT OUTER JOIN doccategories AS c2 ON a.id_category_parent = c2.id_category         GROUP BY a.id_category, a.id_category_parent, a.category ORDER BY a.sys_date asc LIMIT 0, 20		
//		JFactory::getApplication()->enqueueMessage($query);
		return $query;
	}
	
	/**
	 * Gets the list of groups and adds expensive joins to the result set.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getItems()
	{
		$this->items = parent::getItems();
		// 
		foreach ($this->items as &$item)
		{
			$item->docs = $this->getCategoryDocs($item->id_category);
		}
		return $this->items;
	}

	/**
	 * getCategoryDocs
	 * @return  int  total docs of the category idc
	 */
	public function getCategoryDocs($idc=0)
	{
		$res=0;
		try{
			// Create a new query object.
			$db 	= $this->getDbo();
			$query 	= $db->getQuery(true);
			// Select the required fields from the table.
			$query->select('count(*)');
			$query->from($db->quoteName('docs') );
			$query->where('id_category=' . (int) $idc);
			$db->setQuery($query);
			$db->execute();
			$res = $db->loadResult();
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		// Load all notificationtemplate row
		//$row = $this->getTable();
		//$row->load($res);
		//return $row;
		return $res;
	}
	
}
