<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Log Model
 *
 * @since  0.0.1
 */
class SIM0ModelNotification extends JModelAdmin /* JModelAdmin extends JModelForm*/
{
	/**
	 * Message
	 */
	//protected $item;
	
	public static $step;
	public $vars;
	
	/**
	 *
	 */
	public function __construct($config = array()){
		parent::__construct($config);

		SIM0ModelNotification::$step=1;
		
		$this->vars	= array('VAR_APP'=>'SI.ARDITI');
		
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	
	public function addVar($n, $v){
		$this->vars[$n] = $v;
		$this->setState('notification.var', $this->vars);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 */
	protected function populateState()
	{
		parent::populateState();

		$input = JFactory::getApplication()->input;

		//$messageId = (int) $input->getInt('message_id');
		//$this->setState('message.id', $messageId);
		
		//$user  = JFactory::getUser();
		//$this->setState('user.id', $user->get('id'));

		//$replyId = (int) $input->getInt('reply_id');
		//$this->setState('reply.id', $replyId);
		
	}
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 * @return  JTable  A JTable object
	 */
	public function getTable($type = 'Notifications', $prefix = 'SIM0Table', $config = array())
	{
		// EJ: tive que colocar isto pois as tabelas nao eram contradas???
		// If a database object was passed in the configuration array use it, otherwise get the global one from JFactory.
        if (!array_key_exists('dbo', $config)) {	
			$config['dbo'] = $this->getDBO();
        }		
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 * @return  mixed    A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_sim0.notification', 'notification', array('control' => 'jform','load_data' => $loadData));
		if (empty($form)){
			return false;
		}
		return $form;
	}
	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_sim0/models/forms/notification.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 */
	protected function loadFormData(){
		/*
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_sim0.edit.notification.data',array());
		if (empty($data)){
			$data = $this->getItem();
		}
		return $data;
		*/
		$data2 = JFactory::getApplication()->getUserState('com_sim0.edit.notification.data',array());
		
		//array_merge($this->getItem(), $data);
		$data  = $this->getItem();

		// UserField lost selected user!!
		if(isset($data2->id_user_to)){
			$data->id_user_to = $data2->id_user_to;
		}
		//
		$this->ProcessDataFromPost($data);
		
		JFactory::getApplication()->SetUserState('com_sim0.edit.notification.data',$data);
		//$data2 = JFactory::getApplication()->getUserState('com_sim0.edit.notification.data',array());		
		//echo "<BR>-Aqui";print_r($data2);die("s");
		return $data;
	}
	/**
	 * Method to check if it's OK to delete a message. Overwrites JModelAdmin::canDelete
	 */
	protected function canDelete($record)
	{
		if( !empty( $record->id ) )
		{
			return JFactory::getUser()->authorise( "core.delete", "com_sim0.notification." . $record->id );
		}
	}
	
	// Tive muitos problemas por causa da chave da tabela nao ser autonumber...
	// Mas, após ter iserido um dummy, os metodos default passaram a funcionar bem...!
	public function save3($data)
	{		
		// Get a default NotificationTemplates row
		$table = $this->getTable();
		// Bind the data
		if (!$table->bind($data)){
			$this->setError($table->getError());
			return false;
		}				
		// Assign empty values.
		if (empty($table->id_template)){
			//$table->id_template = JFactory::getUser()->get('id');
		}
		// Nao é necessário, tem SQL defaul, mas gostei desta forma de gerar a data para outros casos
		if ((int) $table->sys_date == 0){
			$table->sys_date = JFactory::getDate()->toSql();
		}
		// Check the data.
		if (!$table->check()){
			$this->setError($table->getError());
			return false;
		}

		// Store the data.
		if (!$table->store(true)){
			$this->setError($table->getError());
			return false;
		}
		//print_r($data);print_r($table);die("Save Aqui!");
			

		return true;
	}	
	
	/**
	 *
	 */
	private function ProcessDataFromPost(&$data){
		$app      	= JFactory::getApplication();
		$input 	  	= JFactory::getApplication()->input; 
		//$step 		= $input->getCmd('step', '');
		$formData	= new JRegistry($input->get('jform', array(), 'array'));
		
		$id_template= $formData['id_template'];
		$id_user_to = $formData['id_user_to'];
		$subject 	= $formData['subject'];
		$body 		= $formData['body'];
		$id_user_from=JFactory::getUser()->id;
				
		$step="";
		// Detect Active Step
		if($step==""){
			$step = ($id_template=="" || $id_user_to=="" || !is_numeric($id_user_to) ) ? 1 : 2;
		}
		if($step==2){ 
			$step = ($subject!="" && $body!="" ) ? 3 : 2;
		}
		/*
		echo ("<br>DATA=".$step);
		print_r($data);
		echo ("<br>FORMDATA=".$step);
		print_r($formData);
		echo ("<br>");
		*/
		$data->id_template 	= $id_template;
		$data->id_user_to 	= $id_user_to;
		$data->id_user_from	= $id_user_from;
		// STEP 1: PROCESS NOTHING
		if($step==1){
			
		}
		// STEP 2: PROCESS ID_TEMPLATE & ID_USER
		elseif($step==2){
			// TO
			$user = JFactory::getUser($id_user_to);
			if($user->id == 0){
				JFactory::getApplication()->enqueueMessage(JText::_('Please, set a valid user recipient field.'), 'Warning');				
				$step = 1;
			}
			$data->to 		= $user->email;
			$data->name 	= $user->name;
			$this->addVar('VAR_USERNAME', $user->name);			
			//$lang = $user->getParam('admin_language'); 	// Back-end language
			$lang = $user->getParam('language'); 			// Front-end language
			if($lang==""){
				$doc		= JFactory::getDocument();
				$lang  		= $doc->language;				// Default language
			}
			
			/// TEMPLATE			
			//JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_sim0/models', 'SIM0ModelNotificationtemplate');
			JModelLegacy::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/models', 'SIM0ModelNotificationtemplate');
			$updateModel	= JModelLegacy::getInstance('Notificationtemplate', 'Sim0Model');
			$table 			= $updateModel->getItemByTemplate($id_template);
			if($table->id_nt == 0){
				JFactory::getApplication()->enqueueMessage(JText::_('Please, set a valid Template field.'), 'Warning');				
				$step = 1;
			}
			if($lang == "pt-PT"){
				$data->subject 	= $table->subject;
				$data->body 	= $table->body;
			}
			else{	//en-gb
				$data->subject 	= $table->subject_uk;
				$data->body 	= $table->body_uk;
			}
			$data->subject 	= $this->replaceVarsOn($data->subject);
			$data->body		= $this->replaceVarsOn($data->body . "\n<br />" . 'VAR_SITE / VAR_DATETIME');
		}
		// STEP 3: PROCESS SEND
		if($step==3){
			//echo ("<br>DATA=".$step);		print_r($data);
			//echo ("<br>FORMDATA=".$step);	print_r($formData);
			//FROM
			$formData['id_user_from']	= JFactory::getUser()->id;
			
			// TO
			$user = JFactory::getUser($id_user_to);
			if($user->id == 0){
				JFactory::getApplication()->enqueueMessage(JText::_('Please, set the Notification recipient Id field.'), 'Warning');				
				$step = 1;
			}
			//print_r($formData->toArray());print_r($data);		
			$formData['to']	= $user->email;
			// Save Notification
			try{
				parent::save($formData->toArray());
				//
				JFactory::getApplication()->enqueueMessage(JText::_('COM_SIM0_NOTIFICATION_CREATED_OK'));//, 'message');
			}
			catch(Exception $ex){
				JFactory::getApplication()->enqueueMessage(JText::_('COM_SIM0_NOTIFICATION_CREATED_NOK')."-".$ex->getMessage(), 'Warning');
			}
			//So no controller
			//$this->setRedirect('index.php?option=com_sim0&view=notifications');			
			//$app	= JFactory::getApplication(); 
			//$app->Redirect(JRoute::_('index.php?option=com_sim0&view=notifications', false));
		}
		SIM0ModelNotification::$step = $step;
		//echo ("<br />Step=".SIM0ModelNotification::$step);
		return $data;
	}
	/**
	 * ReplaceVarsOn
	 * 
Variáveis comuns a todos as notificações (comuns a todos os idiomas)
VAR_SITE 		-> Site Name SIARDITI
VAR_NAMEUSER 	-> Nome do user
VAR_DATETIME 		-> Data actual no formato yyyy/MM/dd 
VAR_DATE 		-> Data actual no formato yyyy/MM/dd 
VAR_FOOTER 		-> Rodapé da mensagem
VAR_ID_NOT 		-> ID da Notificação

VAR_SITE / VAR_DATETIME

Variáveis comuns nas notificações M1

Variáveis comuns nas notificações M2

Variáveis comuns nas notificações M3
VAR_PROMOTOR 	-> nome do promotor
VAR_ACRONIMO 	-> acronimo da candidatura

Variáveis comuns nas notificações M4
VAR_DOCUMENTO 	-> nome do documento
VAR_AUTOR 		-> nome do autor do documento
	 */
	private function ReplaceVarsOn($str){
		//
		$this->vars['VAR_DATETIME']	= date("Y-m-d H:i");
		$this->vars['VAR_DATE'] 	= date("Y-m-d");
		// Load Parameters
		$params 		= JComponentHelper::getParams('com_sim0');//jimport('joomla.application.component.helper');
		$var_translate	= $params->get('var_translate');
		if($var_translate){
			$this->vars['VAR_SITE'] 	= $params->get('email_site');
			$this->vars['VAR_FROM'] 	= $params->get('email_from');
			$this->vars['VAR_FOOTER'] 	= $params->get('email_footer');
		}
		//echo "var_translate=".$var_translate;print_r($this->vars);

		// Load fixed data
		//$this->vars = $this->setState('notification.var', $this->vars);
				
		// Substitui todas as variáveis
		foreach($this->vars as $key => $value){
			$str = str_replace($key, $value, $str);
		}
		return $str;
	}
	/**
	 * SendNotificationMail
	 *
	 */
	public function SendNotificationMail($id_nt){
	/*	// Load the user details (already valid from table check).
		$fromUser         = JUser::getInstance($table->user_id_from);
		$toUser           = JUser::getInstance($table->user_id_to);
		$debug            = JFactory::getConfig()->get('debug_lang');
		$default_language = JComponentHelper::getParams('com_languages')->get('administrator');
		$lang             = JLanguage::getInstance($toUser->getParam('admin_language', $default_language), $debug);
		$lang->load('com_messages', JPATH_ADMINISTRATOR);
		// Build the email subject and message
		$sitename = JFactory::getApplication()->get('sitename');
		$siteURL  = JUri::root() . 'administrator/index.php?option=com_messages&view=message&message_id=' . $table->message_id;
		$subject  = sprintf($lang->_('COM_MESSAGES_NEW_MESSAGE_ARRIVED'), $sitename);
		$msg      = sprintf($lang->_('COM_MESSAGES_PLEASE_LOGIN'), $siteURL);
		// Send the email
		JFactory::getMailer()->sendMail($fromUser->email, $fromUser->name, $toUser->email, $subject, $msg);
		*/
		// Carrega notificação
		$not 		= $this->getItem($id_nt);
		$toUser     = JUser::getInstance($not->id_user_to);
		
		$fromUser   = JUser::getInstance(($not->id_user_from!=0) ? $not->id_user_from : JFactory::getUser()->id);
		$fromEmail	= $fromUser->email;
		$fromName	= $fromUser->name;
		$params 	= JComponentHelper::getParams('com_sim0'); //jimport('joomla.application.component.helper');
		if($params->get('force_from')){
			$fromEmail	= $params->get('from_email');
			$fromName	= $params->get('from_name');
		}
		// Envia o email
		$result = SysM0::sendMail($fromEmail, $fromName, $toUser->email, $not->subject, $not->body);//, $mode=1, $cc=null, $bcc=null, $attachment=null, $replyto=null, $replytoname=null )
		// UpdateTrySend
		$this->UpdateTrySend($id_nt, $result=="1");
		// Regista LogAction
		SysM0::SetLogAction(LogActionTypes::SEND_NOTIFICATION, $id_nt, $result);
		//echo "<br>SendMail( $id_nt ) => ".$result; print_r($not);die("");
		return $result;
	}
	
	/**
	 * UpdateTrySend
	 *
	 */
	private function UpdateTrySend($id_not=0, $success=false){
		if($id_not==0) $id_not = (int)$this->id_not;
		if($id_not==0) return false;
		
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query 	=  $db->getQuery(true);
		$fields	= array(
			'send_try_count = send_try_count+1',
			$db->quoteName('send_try_date') . ' = CURRENT_TIMESTAMP'
		);
		if($success){
			array_push($fields, 'send_date = CURRENT_TIMESTAMP', 'sended = 1');
		}
		$query->update($db->quoteName('notifications'))
			  ->set($fields)
			  ->where(array(
					$db->quoteName('id_not') . ' = '.$id_not
				)); 
		try{
			$db->setQuery($query);
			$result = $db->execute();
		}
		catch(Exception $ex){
			//
		}
		// Check for a database error.
		if ($db->getErrorNum()){
			SysARDITI::log('SIM0ModelNotification::sql('.$sql.')->'.$db->getErrorMsg());
		}
		
		// Test if this Notification arrived MaxTries
		$params 	= JComponentHelper::getParams('com_sim0');
		$maxtries	= $params->get('maxtries');
		$query 		=  $db->getQuery(true);
		$query->select('send_try_count')
			  ->from($db->quoteName('notifications'))
			  ->where($db->quoteName('id_not')." = ".$db->quote($id_not));
		$db->setQuery($query);
		$tries = $db->loadResult();
		if($tries>=$maxtries){
			SysM0::SetLogAction(LogActionTypes::SEND_MAX_TRIES, $id_not);		
		}
	}
}
