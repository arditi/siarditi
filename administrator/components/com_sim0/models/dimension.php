<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Log Model
 *
 * @since  0.0.1
 */
class SIM0ModelDimension extends JModelAdmin /* JModelAdmin extends JModelForm*/
{
	/**
	 *
	 */
	public function __construct($config = array()){
		parent::__construct($config);
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	 
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 * @return  JTable  A JTable object
	 */
	public function getTable($type = 'Dimensions', $prefix = 'SIM0Table', $config = array())
	{
		// EJ: tive que colocar isto pois as tabelas nao eram contradas???
		// If a database object was passed in the configuration array use it, otherwise get the global one from JFactory.
        if (!array_key_exists('dbo', $config)) {	
			$config['dbo'] = $this->getDBO();
        }		
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 * @return  mixed    A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_sim0.dimension', 'region', array('control' => 'jform','load_data' => $loadData));
		if (empty($form)){
			return false;
		}
		return $form;
	}
	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_sim0/models/forms/dimension.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 */
	protected function loadFormData(){
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_sim0.edit.dimension.data',array());
		if (empty($data)){
			$data = $this->getItem();
		}
		return $data;
	}
	/**
	 * Method to check if it's OK to delete a message. Overwrites JModelAdmin::canDelete
	 */
	protected function canDelete($record)
	{
		if( !empty( $record->id ) )
		{
			return JFactory::getUser()->authorise( "core.delete", "com_sim0.dimension." . $record->id );
		}
	}
	
	/**
	 * Method to delete rows.
	 * @param   array  &$pks  An array of item ids.
	 * @return  boolean  Returns true on success, false on failure.
	 */
	public function delete(&$pks)
	{
		$user  = JFactory::getUser();
		$table = $this->getTable();
		$pks   = (array) $pks;

		// Check if I am a Super Admin
		$iAmSuperAdmin = $user->authorise('core.admin');
		//if (in_array($user->id, $pks))
		if(1==2)
		{
			$this->setError(JText::_('COM_USERS_USERS_ERROR_CANNOT_DELETE_SELF'));
			return false;
		}

		// Iterate the items to delete each one.
		foreach ($pks as $i => $pk)
		{
			if ($table->load($pk))
			{
				// Access checks.
				$allow = $user->authorise('core.delete', 'com_sim0');
				
				// Don't allow non-super-admin to delete a super admin
				$allow = (!$iAmSuperAdmin && JAccess::check($pk, 'core.admin')) ? false : $allow;
				
				// Testa se esta região esta ou foi usada...
				//$allow = (JAccess::check($pk, 'core.admin')) ? false : $allow;

				if ($allow){
					// Delete item $pk
					if (!$table->delete($pk)){
						$this->setError($table->getError());
						return false;
					}
					
					// LogAction
					SysM0::SetLogAction(LogActionTypes::DIM_DELETE, $pk, true);						
				}
				else{
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JERROR_CORE_DELETE_NOT_PERMITTED'));
				}
			}
			else
			{
				$this->setError($table->getError());
				return false;
			}
		}
		return true;
	}
	

}
