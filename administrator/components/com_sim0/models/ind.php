<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Log Model
 *
 * @since  0.0.1
 */
class SIM0ModelInd extends JModelAdmin /* JModelAdmin extends JModelForm*/
{
	/**
	 * Constructor
	 */
	public function __construct($config = array()){
		parent::__construct($config);
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	 
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 * @return  JTable  A JTable object
	 */
	public function getTable($type = 'Inds', $prefix = 'SIM0Table', $config = array())
	{
		// EJ: tive que colocar isto pois as tabelas nao eram contradas???
		// If a database object was passed in the configuration array use it, otherwise get the global one from JFactory.
		if (!array_key_exists('dbo', $config)) {	
				 $config['dbo'] = $this->getDBO();
		}		
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 * @return  mixed    A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_sim0.ind', 'ind', array('control' => 'jform','load_data' => $loadData));
		if (empty($form)){
			return false;
		}
		return $form;
	}
	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_sim0/models/forms/ind.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 */
	protected function loadFormData(){
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_sim0.edit.ind.data',array());
		if (empty($data)){
			$data = $this->getItem();
		}
		return $data;
	}
	/**
	 * Method to check if it's OK to delete a message. Overwrites JModelAdmin::canDelete
	 *
	 */
	protected function canDelete($record)
	{
		if( !empty( $record->id_indicator ) )
		{
			return JFactory::getUser()->authorise( "core.delete", "com_sim0.ind." . $record->id_indicator );
		}
	}
	
	/**
	 * Method to change document State records.
	 * @param   array  &$pks  The ids of the items to activate.
	 * @param   integer  $value  The value of the published state
	 * @return  boolean  True on success.
	 */
	public function changeState(&$pks, $value = 1)
	{
		$user		= JFactory::getUser();
		// Check if I am a Super Admin
		$iAmSuperAdmin = $user->authorise('core.admin');
		$table         = $this->getTable();
		$pks           = (array) $pks;
		// Access checks.
		foreach ($pks as $i => $pk)
		{	//echo("Loading $i => $pk");
			if ($table->load($pk))
			{
				$old	= $table->getProperties();
				$allow	= $user->authorise('core.edit.state', 'com_sim0');
				// Don't allow non-super-admin to delete a super admin
				$allow 	= (!$iAmSuperAdmin && JAccess::check($pk, 'core.admin')) ? false : $allow;
				//$allow 	= true;
				if ($allow){
					// Pode estar vazio
					$table->state = ($table->state=="") ? 0 : $table->state;
					// Invert value
					$table->state = (1-$table->state);
					//print_r($table);die();
					try
					{
						// Check if no Error
						if (!$table->check()){
							$this->setError($table->getError());
							return false;
						}
						// Store the table.
						if (!$table->store()){
							$this->setError($table->getError());
							return false;
						}
						// LogAction
						SysM0::SetLogAction(LogActionTypes::IND_ENABLE, $pk, $table->state);						
					}
					catch (Exception $e){
						$this->setError($e->getMessage());
						return false;
					}
				}
				else{
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}// Load
		}
		return true;
	}
	
	/**
	 * Method to activate document records.
	 *
	 * @param   array  &$pks  The ids of the items to activate.
	 * @return  boolean  True on success.
	 */
	public function activate(&$pks)
	{		
		$user		= JFactory::getUser();
		// Check if I am a Super Admin
		$iAmSuperAdmin = $user->authorise('core.admin');
		$table         = $this->getTable();
		$pks           = (array) $pks;
		// Access checks.
		foreach ($pks as $i => $pk)
		{	//echo("Loading $i => $pk");
			if ($table->load($pk))
			{
				$old	= $table->getProperties();
				$allow	= $user->authorise('core.edit.state', 'com_sim0');
				// Don't allow non-super-admin to delete a super admin
				$allow 	= (!$iAmSuperAdmin && JAccess::check($pk, 'core.admin')) ? false : $allow;
				//$allow 	= true;
				if ($allow){
					// Pode estar vazio
					$table->validated = ($table->validated=="") ? 0 : $table->validated;
					// If it is invalid
					if($table->validated==0){
						$table->validated = (1-$table->validated);
					}
					else{
						// We do not invalidate
						return false;
					}
					//print_r($table);die();
					try
					{
						// Check if no Error
						if (!$table->check()){
							$this->setError($table->getError());
							return false;
						}
						// Store the table.
						if (!$table->store()){
							$this->setError($table->getError());
							return false;
						}
						// LogAction
						SysM0::SetLogAction(LogActionTypes::IND_VALIDATE, $pk, $table->validated);
					}
					catch (Exception $e)
					{
						$this->setError($e->getMessage());
						return false;
					}
				}
				else{
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}// Load
		}
		return true;
	}	
	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 */	
	public function save($data)
	{
		$result = parent::save($data);
		// If insert or edit ok
		if($result){
			// Aqui temos de 
			$this->saveValues($data);
		}
		return $result;
	}
	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 */	
	public function saveValues($data)
	{
		// No apply, $data só chega com os valors default do form...
		$input 		= JFactory::getApplication()->input; 
		$formData 	= new JRegistry($input->get('jform', array(), 'array')); 
		//print_r($data);print_r($formData);die("ss");
		$data 	= $formData;
		
		$id_indicator	= $data['id_indicator'];
		if($id_indicator==""){
			$id_indicator = $this->getItem()->id_indicator;
		}

		// LogAction
		SysM0::SetLogAction(LogActionTypes::IND_EDIT, $id_indicator);						
		
		//
		$regions	= $this->getRegions($id_indicator);
		//print_r($regions);
		foreach($regions as $region){
			$values = $this->getRegionValues($id_indicator, $region->id_region);
			foreach($values as $key => $val){
				$year = $data['y'.$val->id_value];
				$real = $data['r'.$val->id_value];
				$est  = $data['e'.$val->id_value];
				//
				$this->updateRegionValue($val->id_value, $year, $real, $est);
			}
		}
		//print_r($data); die("save");
	}
	/**
	 * Method to get the regios of a indicator or all
	 *
	 * @return  array  The field option objects.
	 */
	public function getRegions($id_indicator="")
	{
		$options= array();
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query  = $db->getQuery(true)
				->select('distinct id_region')
				->select('(select name from regions as b WHERE b.id_region=a.id_region) as region')
				->from  ('indicator_value as a')
				->order ('year ASC');
		if($id_indicator!="")
			$query->where ('id_indicator='.$id_indicator);

		$db->setQuery($query);
		if ($options = $db->loadObjectList())
		{
			foreach ($options as &$option)
			{
				//$options[] = JHtml::_('select.option', $option->value, $option->text);
			}
		}
		return $options;
	}
	
	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 */	
	public function getRegionValues($id_indicator, $id_region)
	{
		//$step 		= $input->getCmd('step', '');
		//$formData		= new JRegistry($input->get('jform', array(), 'array'));		
		//$id_template	= $formData['id_template'];
		$options= array();
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query  = $db->getQuery(true)
			->select('id_value, year, real_value, estimated_value')
			->from  ('indicator_value')
			->where ('id_indicator='.$id_indicator)
			->where ('id_region='.$id_region)
			->order ('year ASC');
		$db->setQuery($query);
		$this->minYear=0;
		$this->maxYear=0;
		if ($data = $db->loadObjectList())
		{
			//foreach ($options as &$option)
			foreach($data as $key => $val)
			{
				if($val->year < $this->minYear) $this->minYear = $val->year;
				if($val->year > $this->maxYear) $this->maxYear = $val->year;
			}
		}
		return $data;
	}
	
	/**
	 * Method to save the form data.
	 * @return  boolean  True on success.
	 */
	public function addyear($formData){
		$id_indicator 	= $formData['id_indicator'];
		$id_region 		= $formData['id_region'];
		// 	select max_year from indicator_value
		// 	if empty year=2010
		// 	select regions form this indicator
		// 	foreach (region )
		//		addyear(indicator region)
		if($id_region==""){
			$regions	= $this->getRegions($id_indicator);
			//print_r($regions); die("kkk");
			foreach($regions as $region){		
				$this->addRegionValue($id_indicator, $region->id_region, 2020, 0, 0);
			}
		}
		else{
			$this->addRegionValue($id_indicator, $id_region, 2020, 0, 0);
		}
		return true;
	}
	///
	public function updateRegionValue($id_value, $year, $real, $est){
		try{// Create a new query object.
			$db 		= $this->getDbo();
			$query 		= $db->getQuery(true);
			//UPDATE indicator_value SET `year`='2011', `real_value`='32', `estimated_value`=33 WHERE id_value=4
			$conditions = array('id_value='.$id_value);
			$fields 	= array('year='.$db->quote($year), 'real_value='.$db->quote($real), 'estimated_value='.$db->quote($est));
			$query->update('indicator_value')
				  ->set($fields)
				  ->where($conditions);
			//die("query=".$query);
			$db->setQuery($query);
			$res = $db->execute();
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		return true;
	}
	///
	public function addRegionValue($id_indicator, $id_region, $year, $real, $est){
		$id_value=0;
		try{// Create a new query object.
			$db 	= $this->getDbo();
			$query 	= $db->getQuery(true);
			//INSERT INTO `indicator_value` (`id_value`, `year`, `real_value`, `estimated_value`, `obs`, `id_indicator`, `id_region`) VALUES (NULL, '2010', '32', '33', NULL, '1', '2');
			$query->insert('indicator_value')
				->columns($db->quoteName(array('id_indicator','id_region','year','real_value','estimated_value')))
				->values ((int)$id_indicator.','.(int)$id_region.','.(int)$year.','.(int)$real.','.(int)$est);
			//die("query=".$query);
			$db->setQuery($query);
			$db->execute();
			$id_value = $db->insertid();	// id_not inserido
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		return $id_value;
	}
	/**
	 * Method to save the form data.
	 * @return  boolean  True on success.
	 */
	public function delyear($formData){
		$id_indicator 	= $formData['id_indicator'];
		$id_region 		= $formData['id_region'];
		if($id_region==""){
			$regions	= $this->getRegions($id_indicator);
			//print_r($regions); die("kkk");
			foreach($regions as $region){		
				//$this->delRegionValue($id_indicator, $region->id_region, 2020, 0, 0);
			}
		}
		else{
			$this->delRegionValue($id_indicator, $id_region, 0);
		}
		return true;
	}

	//
	public function delRegionValue($id_indicator, $id_region, $year=0){
		try{// Create a new query object.
			$db 	= $this->getDbo();
			if($year==0){
			}
			/*
					$result = &$db->loadResult();
				// Check for a database error.
				if ($db->getErrorNum()){
					// Aqui deveria gravar LogAction, enviar email... registar noutra DB, etc!
					SysARDITI::log('SysARDITI::getSqlResult('.$sql.')->'.$db->getErrorMsg());		
					return false;
				}
			*/
			$query  = $db->getQuery(true)
				->select('id_value')//->select('year')
				->from  ('indicator_value')
				->where ('id_indicator='.$id_indicator)
				->where ('id_region='.$id_region)
				->order ('year DESC');
			$db->setQuery($query);
			//if ($data = $db->loadObjectList())
			$year = $db->loadResult();
			if($year!="")
			{
				$conditions = array(
					$db->quoteName('id_indicator') 	. ' = ' . $id_indicator, 
					$db->quoteName('id_region') 	. ' = ' . $id_region,
					$db->quoteName('year') 			. ' = ' . $year
				);
				$conditions = array(
					$db->quoteName('id_value') 	. ' = ' . $year, 
				);				
				$query 	= $db->getQuery(true);
				$query->delete('indicator_value')
					  ->where($conditions);
				//die("query=".$query);
				$db->setQuery($query);
				$res = $db->execute();
				//$id_not = $db->insertid();	// id_not inserido
			}
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		return $id_tp;
	}
	/**
	 * Method to save the form data.
	 * @return  boolean  True on success.
	 */
	public function addRegion($formData){
		$id_indicator 	= $formData['id_indicator'];
		$id_region 		= $formData['id_region2'];
		if($id_region!=""){
			$this->addRegionValue($id_indicator, $id_region, 2010, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2011, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2012, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2013, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2014, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2015, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2016, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2017, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2018, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2019, 0, 0);
			$this->addRegionValue($id_indicator, $id_region, 2020, 0, 0);
			
			// LogAction
			SysM0::SetLogAction(LogActionTypes::IND_REGION_ADD, $formData['id_indicator'], json_encode($formData));						
					
			return true;
		}
		return false;
	}	
	/**
	 * Method to save the form data.
	 * @return  boolean  True on success.
	 */
	public function getItemByTemplate($id_template=0)
	{
		$id_tp=0;
		try{
			// Create a new query object.
			$db 	= $this->getDbo();
			$query 	= $db->getQuery(true);
			// Select the required fields from the table.
			$query->select('id_nt');
			$query->from($db->quoteName('notificationtemplates') );
			$query->where('id_template=' . (int) $id_template);
			$db->setQuery($query);
			$db->execute();
			$id_tp = $db->loadResult();
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		// Load all notificationtemplate row
		$row = $this->getTable();
		$row->load($id_tp);
		return $row;
	}
	
	/**
	 * Method to delete one or more records.
	 * Returns boolean True if successful, false if an error occurs.
	 *
	 * @return  boolean  True on success.
	 */
	public function delete (&$pks){
		$result = parent::delete($pks);
		// LogAction
		SysM0::SetLogAction(LogActionTypes::IND_DELETE, $result, json_encode($pks));		
		return $result;
	}
}
