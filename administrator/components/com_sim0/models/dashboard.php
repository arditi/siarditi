<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * dashboard Model
 *
 * @since  0.0.1
 */
class SIM0ModelDashboard extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array('id_log', 'id_user', 'id_action', 'source', 'sys_date');
		}
		parent::__construct($config);
		
		// Insert new LogAction
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$id_action = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type');
		$id_action = $this->getUserStateFromRequest($this->context . '.filter.id_action', 'filter_id_action', '');
		$this->setState('filter.id_action', $id_action);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_sim0');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('sys_date', 'asc');
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Initialize variables.
		$db    	= $this->getDbo();
		$query 	= $db->getQuery(true);
		
		// Create the base select statement.
		$query->select('*')
			  ->from($db->quoteName('LogActions'));

		// Filter: like / search
		$search = $this->getState('filter.search');
		if (!empty($search)){
			$like = $db->quote('%' . $search . '%');
			$query->where('source LIKE ' . $like);
		}
		// Filter by id_action
		$id_action = $this->getState('filter.id_action');
		if (is_numeric($id_action)){
			$query->where('id_action=' . (int) $id_action);
		}
		// Filter by id_user
		$id_user = $this->getState('filter.id_user');
		if (is_numeric($id_user)){
			$query->where('id_user = ' . (int) $id_user);
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering',  'sys_date');
		$orderDirn 	= $this->state->get('list.direction', 'asc');

		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		return $query;
	}
	/**
		Retorna o array que popula o SUBMENU DE ADMINSTRAÇÂO na Dashboard
	 */
	public function getModulesIcon(){
		/*
		- Vista e Edição da Informação da Unidade 
		- Responsáveis da Instituição 
		- Listagem e Edição de Serviços de Unidades de Investigação
		- Novo Serviço
		- Listagem e Edição de Equipamentos Unidades de Investigação
		- Novo Equipaemnto
		- Listagem de Investigadores Activos
		- Listagem de Investigadores Candidatos
		- Listagem de Candidaturas em Curso
		- Quadro Resumo de Candidaturas
		- Formulário de Reclamação
		- Histórico de Candidaturas da Unidade
		- Formulário de Candidatura FDCTI
		- Inserir Relatório/Justificação de despesas
		- Inserção de recibo de pagamento
		- Listagem de pagamentos
		- Listagem de documentos em falta
		*/
		$icons = array(
			'ADMIN'=> array(
				array(
					'link' 		=> JRoute::_('index.php?option=com_sim0&view=logactions'),
					'image' 	=> 'eye modal',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_LOGACTIONS'),
					'access' 	=> array('core.manage', 'com_sim0'),
					'group' 	=> 'MOD_QUICKICON_CONFIGURATION'
				),
				/*
				array(
					'link' 		=> JRoute::_('index.php?option=com_sim0&view=notification&layout=edit'), 
					'image' 	=> 'cog',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_NOTIFICATIONS_NEW'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				),
				*/
				array(
					'link' 		=> JRoute::_('index.php?option=com_sim0&view=notifications'),
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_NOTIFICATIONS'),
					'access' 	=> array('core.manage', 'com_sim0'),
					'group' 	=> 'MOD_QUICKICON_CONFIGURATION'
				)
			),
			'M1'=> array(
				array(
					'link' 		=> "index.php?option=com_sim1&view=investigators",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_RESEARCHERS'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				),
				array(
					'link' 		=> "index.php?option=com_sim1&view=researchunits",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_IUNITS'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				)
			),
			'M2'=> array(
				array(
					'link' 		=> "index.php?option=com_sim0&view=inds",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_INDICATORS'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				),
				array(
					'link' 		=> "index.php?option=com_sim0&view=ind&layout=edit",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_INDICATORS_NEW'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				)
			),
			'M3'=> array(
				array(
					'link' 		=> "index.php?option=com_sim0&view=indicators",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_CALLS'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				),
				array(
					'link' 		=> "index.php?option=com_sim0&view=indicators&layout=edit",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_CALLS_NEW'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				)				
			),
			'M4'=> array(
				array(
					'link' 		=> "index.php?option=com_sim0&view=docs&filter[validated]=1",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_LIBRARY'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				),
				array(
					'link' 		=> "index.php?option=com_sim0&view=docs&filter[validated]=0",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_LIBRARY_VALIDATE'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				),
				array(
					'link' 		=> "index.php?option=com_sim0&view=doc&layout=edit",
					'image' 	=> 'eye',
					'icon' 		=> 'header/icon-48-themes.png',
					'text' 		=> JText::_('COM_SIM0_DASHBOARD_SUBMENU_LIBRARY_NEW'),
					'access' 	=> true,
					'group' 	=> 'MOD_QUICKICON_EXTENSIONS'
				)				
			)
		);
		//$icon = array_merge($default, $icons);
		//foreach ($arrays as $response)
		return $icons;
	}
	/**
	 * Method to get the regios of a indicator or all
	 *
	 * @return  array  The field option objects.
	 */
	public function getTotalDocs($valid=0)
	{
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query  = $db->getQuery(true)
				->select('count(*) as total, max(sys_date) as last')
				->from  ('docs as a')
				->where ('validated='.$valid);
		$db->setQuery($query);
		if ($res = $db->loadObject()) 	//loadAssoc->array name, loadRow->array int
			return $res;
		return null;
	}	
	/**
	 * Method to get the regios of a indicator or all
	 *
	 * @return  array  The field option objects.
	 */
	public function getTotalInds($valid=0)
	{
		$sys 	= &SysARDITI::getInstance();
		$db 	= &$sys->getDBO();
		$query  = $db->getQuery(true)
				->select('count(*) as total, max(sys_date) as last')
				->from  ('indicators as a')
				->where ('validated='.$valid);
		$db->setQuery($query);
		if ($res = $db->loadObject()) 	//loadAssoc->array name, loadRow->array int
			return $res;
		return null;
	}		
}
