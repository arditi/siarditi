<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla modelitem library
//jimport('joomla.application.component.modeladmin');
//jimport('joomla.application.component.modellist');

/**
 * Stats Model
 */
class SIM0ModelStats extends JModelList ///JModelAdmin
{
 
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	//public function getTable($type = 'mcsv_applications', $prefix = 'MCSVTable', $config = array()) 
	
	/**
	 * Get the message
	 * @return string The message to be displayed to the user
	 */
	protected function loadFormData_2()
	{
		$data = JFactory::getApplication()->getUserState('com_mcsv.edit.stats.data', array());

		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}

	/**
	 * Method to get the form.
	 *
	 * @access      public
	 * @return      mixed   JForm object on success, false on failure.
	 */
	public function getForm_2($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_sim0.stats', 'stats', array('control' => 'jform', 'load_data' => $loadData));
		return $form;
	}
	
	public function getForm($data = array(), $loadData = true)
	{
	
		// Get the form.
		$app = JFactory::getApplication();
		JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT . '/models/fields');
		$form = JForm::getInstance('com_sim0.stats', 'stats', array('load_data' => $loadData));
/*
		// Check for an error.
		if ($form == false) {
			$this->setError($form->getMessage());
			return false;
		}
		// Check the session for previously entered form data.
		$data = $this->loadFormData();

		// Bind the form data if present.
		if (!empty($data)) {
			$form->bind($data);
		}
*/
		return $form;
	}
		/**
	 * Populate State
	 * @return void
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published');
		$this->setState('filter.published', $published);

		$status = $this->getUserStateFromRequest($this->context.'.filter.status', 'filter_status');
		$this->setState('filter.status', $status);

		//
		/*
		$apps = $this->getUserStateFromRequest($this->context.'.filter.apps', 'filter_apps');
		$this->setState('filter.apps', $apps);
		$shops = $this->getUserStateFromRequest($this->context.'.filter.shops', 'filter_shops');
		$this->setState('filter.shops', $shops);
		$macs = $this->getUserStateFromRequest($this->context.'.filter.macs', 'filter_macs');
		$this->setState('filter.macs', $macs);
		*/

		// PODE-SE RETIRAR ESTA NA VIEW_HTML
		//$apps = $this->getUserStateFromRequest($this->context.'.filter.apps', 'filter_apps');
		//$this->setState('filter.apps', $apps);
		$apps = $this->getUserStateFromRequest('filter.apps', 'filter_apps');
		$session = JFactory::getSession();
		$session->set('filter.apps', $apps);
		$shops = $this->getUserStateFromRequest($this->context.'.filter.shops', 'filter_shops');
		$session->set('filter.shops', $shops);
		$this->setState('filter.shops', $shops);
		$macs = $this->getUserStateFromRequest($this->context.'.filter.macs', 'filter_macs');
		$this->setState('filter.macs', $macs);
		$machines = $this->getUserStateFromRequest($this->context.'.filter.machines', 'filter_machines');
		$this->setState('filter.machines', $machines);		
		//print_r($this->_state);

		//die("this->context=".$this->context." apps->context=".$apps);
		
		parent::populateState($ordering, $direction);
	}
}