<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Model Notificationtemplates
 *
 * @since  1.6
 */
class SIM0ModelNotificationtemplates extends JModelList
{
	/**
	 * Constructor.
	 * @param   array  $config  An optional associative array of configuration settings.
	 */
	public function __construct($config = array()){
		if (empty($config['filter_fields'])){
			$config['filter_fields'] = array('id_template', 'category', 'external',	'subject', 'subject_uk', 'sys_del', 'sys_date');
		}
		parent::__construct($config);
		// 
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}

	/**
	 * Method to auto-populate the model state.
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication('administrator');
		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout', 'default', 'cmd')){
			$this->context .= '.' . $layout;
		}

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$category = $this->getUserStateFromRequest($this->context . '.filter.category', 'filter_category');
		$this->setState('filter.category', $category);

		$external = $this->getUserStateFromRequest($this->context . '.filter.external', 'filter_external');
		$this->setState('filter.external', $external);

		$range = $this->getUserStateFromRequest($this->context . '.filter.range', 'filter_range');
		$this->setState('filter.range', $range);

		$excluded = json_decode(base64_decode($app->input->get('excluded', '', 'BASE64')));
		if (isset($excluded)){
			JArrayHelper::toInteger($excluded);
		}
		$this->setState('filter.excluded', $excluded);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_sim0');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('subject', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 * @return  string  A store id.
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.category');
		$id .= ':' . $this->getState('filter.external');
		$id .= ':' . $this->getState('filter.range');
		return parent::getStoreId($id);
	}

	/**
	 * Gets the list of users and adds expensive joins to the result set.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems_EXEMPLO_COM_CACHE()
	{
		// Get a storage key.
		$store = $this->getStoreId();
		// Try to load the data from internal storage.
		if (empty($this->cache[$store]))
		{
			$groups  = $this->getState('filter.groups');
			$groupId = $this->getState('filter.group_id');
			if (isset($groups) && (empty($groups) || $groupId && !in_array($groupId, $groups))){
				$items = array();
			}
			else{
				$items = parent::getItems();
			}

			// Bail out on an error or empty list.
			if (empty($items)){
				$this->cache[$store] = $items;
				return $items;
			}
			// Joining the groups with the main query is a performance hog.
			// Find the information only on the result set.
			
			// Add the items to the internal cache.
			$this->cache[$store] = $items;
		}
		return $this->cache[$store];
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery()
	{
		require_once JPATH_COMPONENT . '/helpers/sim0.php';

		// Create a new query object.
		$db 	= $this->getDbo();
		$query 	= $db->getQuery(true);
		
		// Select the required fields from the table.
		$query->select('*');
		$query->from($db->quoteName('NotificationTemplates') . ' AS n');

		// Filter the items over the search string if set.
		if ($this->getState('filter.search') !== '' && $this->getState('filter.search') !== null){
			// Escape the search token.
			$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($this->getState('filter.search')), true) . '%'));
			// Compile the different search clauses.
			$searches   = array();
			$searches[] = 'subject LIKE ' . $search;
			$searches[] = 'subject_uk LIKE ' . $search;
			$searches[] = 'body LIKE ' . $search;
			$searches[] = 'body_uk LIKE ' . $search;
			// Add the clauses to the query.
			$query->where('(' . implode(' OR ', $searches) . ')');
		}
		// Filter by category
		$category = $this->getState('filter.category');
		if (!empty($category)){
			$query->where('category LIKE '.$db->quote($category).' ');
		}			
		// Filter by category
		$external = $this->getState('filter.external');
		if (is_numeric($external)){
			$query->where('external = ' . (int) $external);
		}
		// Add filter for registration ranges select list
		$range = $this->getState('filter.range');
		// Apply the range filter.
		if ($range){
			// Get UTC for now.
			$dNow   = new JDate;
			$dStart = clone $dNow;
			switch ($range){
				case 'past_week':
					$dStart->modify('-7 day');
					break;
				case 'past_1month':
					$dStart->modify('-1 month');
					break;
				case 'past_3month':
					$dStart->modify('-3 month');
					break;
				case 'past_6month':
					$dStart->modify('-6 month');
					break;
				case 'post_year':
				case 'past_year':
					$dStart->modify('-1 year');
					break;
				case 'today':
					// Ranges that need to align with local 'days' need special treatment.
					$app    = JFactory::getApplication();
					$offset = $app->get('offset');
					
					// Reset the start time to be the beginning of today, local time.
					$dStart = new JDate('now', $offset);
					$dStart->setTime(0, 0, 0);

					// Now change the timezone back to UTC.
					$tz = new DateTimeZone('GMT');
					$dStart->setTimezone($tz);
					break;
			}
			if ($range == 'post_year'){
				$query->where($db->qn('sys_date') . ' < ' . $db->quote($dStart->format('Y-m-d H:i:s')));
			}
			else{
				$query->where($db->qn('sys_date') . ' >= ' . $db->quote($dStart->format('Y-m-d H:i:s')) . ' AND ' . $db->qn('sys_date') . ' <= ' . $db->quote($dNow->format('Y-m-d H:i:s')) );
			}
		}
		
		// Add the list ordering clause.
		$orderCol = $this->getState('list.ordering', 'subject');
		$query->order($db->escape($orderCol) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));
		
		return $query;
	}

}
