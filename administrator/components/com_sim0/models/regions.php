<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Model Regions
 */
class SIM0ModelRegions extends JModelList
{
	/**
	 * Constructor.
	 * @param   array  $config  An optional associative array of configuration settings.
	 */
	public function __construct($config = array()){
		if (empty($config['filter_fields'])){
			$config['filter_fields'] = array('a.id_region', 'a.name', 'a.name_uk', 'a.definition', 'a.definition_uk', 'a.sys_date');
		}
		parent::__construct($config);
		// 
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}

	/**
	 * Method to auto-populate the model state.
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication('administrator');
		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout', 'default', 'cmd')){
			$this->context .= '.' . $layout;
		}

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$name = $this->getUserStateFromRequest($this->context . '.filter.name', 'filter_name');
		$this->setState('filter.name', $name);

		$purpose = $this->getUserStateFromRequest($this->context . '.filter.purpose', 'filter_purpose');
		$this->setState('filter.external', $purpose);

		$range = $this->getUserStateFromRequest($this->context . '.filter.range', 'filter_range');
		$this->setState('filter.range', $range);

		$excluded = json_decode(base64_decode($app->input->get('excluded', '', 'BASE64')));
		if (isset($excluded)){
			JArrayHelper::toInteger($excluded);
		}
		$this->setState('filter.excluded', $excluded);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_sim0');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.sys_date', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 * @return  string  A store id.
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.name');
		$id .= ':' . $this->getState('filter.definition');
		$id .= ':' . $this->getState('filter.range');
		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 */
	protected function getListQuery()
	{
		require_once JPATH_COMPONENT . '/helpers/sim0.php';

		// Create a new query object.
		$db 	= $this->getDbo();
		$query 	= $db->getQuery(true);
		
		// Select the required fields from the table.
		$query->select('*');
		$query->from($db->quoteName('regions') . ' AS a');

		// Filter the items over the search string if set.
		if ($this->getState('filter.search') !== '' && $this->getState('filter.search') !== null){
			// Escape the search token.
			$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($this->getState('filter.search')), true) . '%'));
			// Compile the different search clauses.
			$searches   = array();
			$searches[] = 'a.name LIKE ' . $search;
			$searches[] = 'a.name_uk LIKE ' . $search;
			$searches[] = 'a.purpose LIKE ' . $search;
			$searches[] = 'a.purpose_uk LIKE ' . $search;
			//$searches[] = 'a.body LIKE ' . $search;
			//$searches[] = 'a.body_uk LIKE ' . $search;
			// Add the clauses to the query.
			$query->where('(' . implode(' OR ', $searches) . ')');
		}
		// Add filter for registration ranges select list
		$range = $this->getState('filter.range');
		// Apply the range filter.
		if ($range){
			// Get UTC for now.
			$dNow   = new JDate;
			$dStart = clone $dNow;
			switch ($range){
				case 'past_week':
					$dStart->modify('-7 day');
					break;
				case 'past_1month':
					$dStart->modify('-1 month');
					break;
				case 'past_3month':
					$dStart->modify('-3 month');
					break;
				case 'past_6month':
					$dStart->modify('-6 month');
					break;
				case 'post_year':
				case 'past_year':
					$dStart->modify('-1 year');
					break;
				case 'today':
					// Ranges that need to align with local 'days' need special treatment.
					$app    = JFactory::getApplication();
					$offset = $app->get('offset');
					
					// Reset the start time to be the beginning of today, local time.
					$dStart = new JDate('now', $offset);
					$dStart->setTime(0, 0, 0);

					// Now change the timezone back to UTC.
					$tz = new DateTimeZone('GMT');
					$dStart->setTimezone($tz);
					break;
			}
			if ($range == 'post_year'){
				$query->where($db->qn('a.sys_date') . ' < ' . $db->quote($dStart->format('Y-m-d H:i:s')));
			}
			else{
				$query->where($db->qn('a.sys_date') . ' >= ' . $db->quote($dStart->format('Y-m-d H:i:s')) . ' AND ' . $db->qn('a.sys_date') . ' <= ' . $db->quote($dNow->format('Y-m-d H:i:s')) );
			}
		}
		
		// Add the list ordering clause.
		$orderCol = $this->getState('list.ordering', 'a.name');
		$query->order($db->escape($orderCol) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));
		
		//JFactory::getApplication()->enqueueMessage($query);
		return $query;
	}
	
	/**
	 * Method to delete rows of regions.
	 * @param   array  &$pks  An array of item ids.
	 * @return  boolean  Returns true on success, false on failure.
	 */
	public function delete(&$pks)
	{
		$pks   = (array) $pks;
		$user  = JFactory::getUser();
		$table = $this->getTable();
		
		// Check if I am a Super Admin
		$iAmSuperAdmin = $user->authorise('core.admin');
		//if (in_array($user->id, $pks))
		if(1==2){
			$this->setError(JText::_('COM_USERS_USERS_ERROR_CANNOT_DELETE_SELF'));
			return false;
		}

		// Iterate the items to delete each one.
		foreach ($pks as $i => $pk)
		{
			if ($table->load($pk))
			{
				// Access checks.
				$allow = $user->authorise('core.delete', 'com_sim0');
				
				// Don't allow non-super-admin to delete a super admin
				$allow = (!$iAmSuperAdmin && JAccess::check($pk, 'core.admin')) ? false : $allow;
				
				// Testa se esta região esta ou foi usada...
				//$allow = (JAccess::check($pk, 'core.admin')) ? false : $allow;

				if ($allow){
					// Delete item $pk
					if (!$table->delete($pk)){
						$this->setError($table->getError());
						return false;
					}
				}
				else{
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JERROR_CORE_DELETE_NOT_PERMITTED'));
				}
			}
			else{
				$this->setError($table->getError());
				return false;
			}
		}
		return true;
	}
}
