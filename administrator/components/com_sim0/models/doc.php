<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Log Model
 *
 * @since  0.0.1
 */
class SIM0ModelDoc extends JModelAdmin /* JModelAdmin extends JModelForm*/
{
	/**
	 * Constructor
	 */
	public function __construct($config = array()){
		parent::__construct($config);
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	 
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 * @return  JTable  A JTable object
	 */
	public function getTable($type = 'Docs', $prefix = 'SIM0Table', $config = array())
	{
		// EJ: tive que colocar isto pois as tabelas nao eram contradas???
		// If a database object was passed in the configuration array use it, otherwise get the global one from JFactory.
		if (!array_key_exists('dbo', $config)) {	
				 $config['dbo'] = $this->getDBO();
		}		
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 * @return  mixed    A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_sim0.doc', 'doc', array('control' => 'jform','load_data' => $loadData));
		if (empty($form)){
			return false;
		}
		return $form;
	}
	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_sim0/models/forms/doc.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 */
	protected function loadFormData(){
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_sim0.edit.doc.data',array());
		if (empty($data)){
			$data = $this->getItem();
		}
		return $data;
	}
	/**
	 * Method to check if it's OK to delete a message. Overwrites JModelAdmin::canDelete
	 *
	 */
	protected function canDelete($record)
	{
		if( !empty( $record->id ) )
		{
			return JFactory::getUser()->authorise( "core.delete", "com_sim0.doc." . $record->id );
		}
	}
	/**
	 * Tive muitos problemas por causa da chave da tabela nao ser autonumber...
	 * Mas, após ter iserido um dummy, os metodos default passaram a funcionar bem...!
	 */
	public function save2($data)
	{		
		// Get a default NotificationTemplates row
		$table = $this->getTable();
		// Bind the data
		if (!$table->bind($data)){
			$this->setError($table->getError());
			return false;
		}				
		// Assign empty values.
		if (empty($table->id_template)){
			//$table->id_template = JFactory::getUser()->get('id');
		}
		// Nao é necessário, tem SQL defaul, mas gostei desta forma de gerar a data para outros casos
		if ((int) $table->sys_date == 0){
			$table->sys_date = JFactory::getDate()->toSql();
		}
		// Check the data.
		if (!$table->check()){
			$this->setError($table->getError());
			return false;
		}

		// Store the data.
		if (!$table->store(true)){
			$this->setError($table->getError());
			return false;
		}
		//print_r($data);print_r($table);die("Save Aqui!");
		return true;
	}	
	/**
	 * Method to save the form data.
	 * @return  boolean  True on success.
	 */
	public function getItemByTemplate($id_template=0)
	{
		$id_tp=0;
		try{
			// Create a new query object.
			$db 	= $this->getDbo();
			$query 	= $db->getQuery(true);
			// Select the required fields from the table.
			$query->select('id_nt');
			$query->from($db->quoteName('notificationtemplates') );
			$query->where('id_template=' . (int) $id_template);
			$db->setQuery($query);
			$db->execute();
			$id_tp = $db->loadResult();
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		// Load all notificationtemplate row
		$row = $this->getTable();
		$row->load($id_tp);
		return $row;
	}
	/**
	 * Generate new file upload URI.
	 *
	 * @param   string  $file      The name of the file.
	 * @return   boolean  True if file uploaded successfully, false otherwise
	 */
	public function GenerateFileURI($file, $id)
	{
		jimport('joomla.filesystem.file');
		$fileName 	= JFile::makeSafe($id.'_'.$file['name']);
		$randDate 	= date('ym');	//201502
		$uri 		= '/media/com_sim0/'.$randDate.'/'.$fileName;
		return $uri;
	}

	/**
	 * Upload new file.
	 *
	 * @param   string  $file      The name of the file.
	 * @param   string  $file_uri  The URI file_name of the file from joomla root.
	 * @return   boolean  True if file uploaded successfully, false otherwise
	 */
	public function uploadFile($file, $file_uri)
	{
		//print_r
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		/*
		$fileName 	= JFile::makeSafe($id.'_'.$file['name']);
		$randDate 	= date('ym');	//201502
		$url 		= JURI::root().'/media/com_sim0/'.$randDate.'/'.$fileName;
		$uploadPath = JPATH_ROOT.  '/media/com_sim0/'.$randDate;
		$uploadFile = JPath::clean( $uploadPath .'/'. $fileName);
		*/
		$url 		= JURI::root().'/'. $file_uri;
		$uploadFile = JPATH_ROOT.  '/'. $file_uri;
		$uploadFile = JPath::clean( $uploadFile );
		$err 		= null;
		$app     	= JFactory::getApplication();
		JLoader::register('UploadHelper', JPATH_COMPONENT_ADMINISTRATOR . '/helpers/upload.php');
                
		if (!UploadHelper::canUpload($file, $err)){
			$app->enqueueMessage(JText::_('Cant upload the file'), 'error');
			// Can't upload the file
			return false;
		}
		// Apagamos o que existe para deixar re-escrever???
		if (file_exists($uploadFile)){
			//
			if(!rename($uploadFile, $uploadFile.'.delete')){
				$app->enqueueMessage(JText::_('COM_SIM0_DOCS_NEWFILE_FILE_EXISTS'), 'error');
				return false;
			}
		}
		if (!JFile::upload($file['tmp_name'], $uploadFile)){
			$app->enqueueMessage(JText::_('COM_SIM0_DOCS_NEWFILE_FILE_UPLOAD_ERROR'), 'error');
			return false;
		}
		return $url;
	}

	/**
	 * Method to change document State records.
	 * @param   array  &$pks  The ids of the items to activate.
	 * @param   integer  $value  The value of the published state
	 * @return  boolean  True on success.
	 */
	public function changeState(&$pks, $value = 1)
	{
		$user		= JFactory::getUser();
		// Check if I am a Super Admin
		$iAmSuperAdmin = $user->authorise('core.admin');
		$table         = $this->getTable();
		$pks           = (array) $pks;
		// Access checks.
		foreach ($pks as $i => $pk)
		{	//echo("Loading $i => $pk");
			if ($table->load($pk))
			{
				$old	= $table->getProperties();
				$allow	= $user->authorise('core.edit.state', 'com_sim0');
				// Don't allow non-super-admin to delete a super admin
				$allow 	= (!$iAmSuperAdmin && JAccess::check($pk, 'core.admin')) ? false : $allow;
				//$allow 	= true;
				if ($allow){
					// Pode estar vazio
					$table->state = ($table->state=="") ? 0 : $table->state;
					// Invert value
					$table->state = (1-$table->state);
					//print_r($table);die();
					try
					{
						// Check if no Error
						if (!$table->check()){
							$this->setError($table->getError());
							return false;
						}
						// Store the table.
						if (!$table->store()){
							$this->setError($table->getError());
							return false;
						}
						// LogAction
						SysM0::SetLogAction(LogActionTypes::DOC_ENABLE, $pk, $table->state);						
					}
					catch (Exception $e){
						$this->setError($e->getMessage());
						return false;
					}
				}
				else{
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}// Load
		}
		return true;
	}
	
	/**
	 * Method to activate document records.
	 *
	 * @param   array  &$pks  The ids of the items to activate.
	 * @return  boolean  True on success.
	 */
	public function activate(&$pks)
	{		
		$user		= JFactory::getUser();
		// Check if I am a Super Admin
		$iAmSuperAdmin = $user->authorise('core.admin');
		$table         = $this->getTable();
		$pks           = (array) $pks;
		// Access checks.
		foreach ($pks as $i => $pk)
		{	//echo("Loading $i => $pk");
			if ($table->load($pk))
			{
				$old	= $table->getProperties();
				$allow	= $user->authorise('core.edit.state', 'com_sim0');
				// Don't allow non-super-admin to delete a super admin
				$allow 	= (!$iAmSuperAdmin && JAccess::check($pk, 'core.admin')) ? false : $allow;
				//$allow 	= true;
				if ($allow){
					// Pode estar vazio
					$table->validated = ($table->validated=="") ? 0 : $table->validated;
					// If it is invalid
					if($table->validated==0){
						$table->validated = (1-$table->validated);
						$table->state 	  = 0;// Esta a er incializado com null e aqui rescrevemos...
						$dNow	= new JDate;
						$table->pub_date 	= $dNow->format('Y-m-d H:i:s');
						$table->pub_id_user = $user->id;
					}
					else{
						// We do not invalidate
						return false;
					}
					//print_r($table);die();
					try
					{
						// Check if no Error
						if (!$table->check()){
							$this->setError($table->getError());
							return false;
						}
						// Store the table.
						if (!$table->store()){
							$this->setError($table->getError());
							return false;
						}
						
						// LogAction
						SysM0::SetLogAction(LogActionTypes::DOC_VALIDATE, $pk, $table->validated);
						
					}
					catch (Exception $e)
					{
						$this->setError($e->getMessage());
						return false;
					}
				}
				else{
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}// Load
		}
		return true;
	}
	
	
	/**
	 * Method to delete one or more records.
	 * Returns boolean True if successful, false if an error occurs.
	 *
	 * @return  boolean  True on success.
	 */
	public function delete (&$pks){
		$result = parent::delete($pks);
		// LogAction
		SysM0::SetLogAction(LogActionTypes::DOC_DELETE, $result, json_encode($data));
		
		return $result;
	}
}
