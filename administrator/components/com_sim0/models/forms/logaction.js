jQuery(function() {
    document.formvalidator.setHandler('source',
        function (value) {
            regex=/^[^0-9]+$/;
            return regex.test(value);
        });
});