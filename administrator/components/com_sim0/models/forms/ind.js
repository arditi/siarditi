jQuery(function() {
    document.formvalidator.setHandler('subject',
        function (value) {
            regex=/^[^0-9]+$/;
            return regex.test(value);
        });
});