<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Log Model
 *
 * @since  0.0.1
 */
class SIM0ModelNotificationtemplate extends JModelAdmin /* JModelAdmin extends JModelForm*/
{
	/**
	 *
	 */
	public function __construct($config = array()){
		parent::__construct($config);
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		parent::setDbo($db);
	}
	 
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 * @return  JTable  A JTable object
	 */
	public function getTable($type = 'Notificationtemplates', $prefix = 'SIM0Table', $config = array())
	{
		// EJ: tive que colocar isto pois as tabelas nao eram contradas???
		// If a database object was passed in the configuration array use it, otherwise get the global one from JFactory.
        if (!array_key_exists('dbo', $config)) {	
			$config['dbo'] = $this->getDBO();
        }		
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 * @return  mixed    A JForm object on success, false on failure
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_sim0.notificationtemplate', 'notificationtemplate', array('control' => 'jform','load_data' => $loadData));
		if (empty($form)){
			return false;
		}
		return $form;
	}
	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript() 
	{
		return 'administrator/components/com_sim0/models/forms/notificationtemplate.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 */
	protected function loadFormData(){
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_sim0.edit.notificationtemplate.data',array());
		if (empty($data)){
			$data = $this->getItem();
		}
		return $data;
	}
	/**
	 * Method to check if it's OK to delete a message. Overwrites JModelAdmin::canDelete
	 */
	protected function canDelete($record)
	{
		if( !empty( $record->id ) )
		{
			return JFactory::getUser()->authorise( "core.delete", "com_sim0.notificationtemplate." . $record->id );
		}
	}
	
	/**
	 * Method to validate the form data.
	 *
	 * @return  boolean  True on success.
	 */
	public function validate($form, $data, $group = null) {
	//public function validate(JForm $form, array $data, string $group = null) {
		//id_template é uma palavra reservada, vem sempre vazia!!!
		//if(isset($data['id_template2']))
		//	$data['id_template'] = $data['id_template2'];
		//print_r($data);
		//die($data);
		return parent::validate($form, $data, $group);
	}
	/**
	 * Method to save the form data.
	 * @param   array  $data  The form data.
	 * @return  boolean  True on success.
	 */
	public function save_Default_NaoGrava($data)
	{
		////
		//id_template é uma palavra reservada, vem sempre vazia!!!
		if(isset($data['id_template2'])){
			$data['id_template'] = $data['id_template2'];
		}
		return parent::save($data);
	}
	
	// Tive muitos problemas por causa da chave da tabela nao ser autonumber...
	// Mas, após ter iserido um dummy, os metodos default passaram a funcionar bem...!
	public function save($data)
	{		
		// Get a default NotificationTemplates row
		$table = $this->getTable();
		// Bind the data
		if (!$table->bind($data)){
			$this->setError($table->getError());
			return false;
		}				
		// Assign empty values.
		if (empty($table->id_template)){
			//$table->id_template = JFactory::getUser()->get('id');
		}
		// Nao é necessário, tem SQL defaul, mas gostei desta forma de gerar a data para outros casos
		if ((int) $table->sys_date == 0){
			$table->sys_date = JFactory::getDate()->toSql();
		}
		// Check the data.
		if (!$table->check()){
			$this->setError($table->getError());
			return false;
		}

		// Store the data.
		if (!$table->store(true)){
			$this->setError($table->getError());
			return false;
		}
		//print_r($data);print_r($table);die("Save Aqui!");
		return true;
	}	
	/**
	 * Method to save the form data.
	 * @return  boolean  True on success.
	 */
	public function getItemByTemplate($id_template=0)
	{
		$id_tp=0;
		try{
			// Create a new query object.
			$db 	= $this->getDbo();
			$query 	= $db->getQuery(true);
			// Select the required fields from the table.
			$query->select('id_nt');
			$query->from($db->quoteName('notificationtemplates') );
			$query->where('id_template=' . (int) $id_template);
			$db->setQuery($query);
			$db->execute();
			$id_tp = $db->loadResult();
		}
		catch (RuntimeException $e){
			JError::raiseError(500, __FILE__ . __LINE__ . $e->getMessage());
		}
		// Load all notificationtemplate row
		$row = $this->getTable();
		$row->load($id_tp);
		return $row;
	}
}
