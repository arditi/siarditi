<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'notificationtemplate.cancel' || document.formvalidator.isValid(document.getElementById('notificationtemplate-form')))
			{
				Joomla.submitform(task, document.getElementById('notificationtemplate-form'));
			}
		};
");
?>
<form action="<?php echo JRoute::_('index.php?option=com_sim0&id_nt='.(int) $this->item->id_nt); ?>); ?>" method="post" name="adminForm" id="notificationtemplate-form" class="form-validate form-horizontal">
	<fieldset class="adminform">
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_template'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_template'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('category'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('category'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('external'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('external'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('subject'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('subject'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('subject_uk'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('subject_uk'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('body'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('body'); ?>
			</div>
			<div class="control-label">
				<?php echo $this->form->getLabel('body_uk'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('body_uk'); ?>
			</div>
		</div>
	</fieldset>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
