<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * HTML View class for the Messages component
 *
 * @since  1.6
 */
class SIM0ViewRegion extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $state;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$this->form		= $this->get('Form');
		$this->item		= $this->get('Item');
		$this->state	= $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		if ($this->getLayout() == 'edit')
		{
			JFactory::getApplication()->input->set('hidemainmenu', true);
			$title = ($this->item->id_region=="")?JText::_('COM_SIM0_REG_TITLE_NEW'):JText::_('COM_SIM0_REG_TITLE_EDIT');
			JToolbarHelper::title($title, 'envelope-opened new-privatemessage');
			
			JToolbarHelper::apply('region.apply');
			JToolbarHelper::save('region.save');
			//JToolbarHelper::save('region.save', 'COM_SIM0_NOTIFICATION_TEMPLATE_TOOLBAR_SEND');
			JToolbarHelper::save2new('region.save2new');
			JToolbarHelper::cancel('region.cancel');
			
			JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_WRITE');
		}
		else
		{
			JToolbarHelper::title(JText::_('COM_SIM0_REG_TITLE_VIEW'), 'envelope inbox');
			$sender = JUser::getInstance($this->item->id_region);
			if ($sender->authorise('core.admin') || $sender->authorise('core.manage', 'com_sim0') && $sender->authorise('core.login.admin'))
			{
				JToolbarHelper::custom('region.reply', 'redo', null, 'COM_SIM0_TOOLBAR_REPLY', false);
			}

			JToolbarHelper::cancel('region.cancel');
			JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_READ');
		}
	}
}
