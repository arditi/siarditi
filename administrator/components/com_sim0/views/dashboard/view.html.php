<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_dashboard
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * HTML View class for the Cpanel component
 *
 * @since  1.0
 */
class SIM0ViewDashboard extends JViewLegacy
{
	/**
	 * Array of cpanel modules
	 *
	 * @var  array
	 */
	protected $modules = null;
	protected $iconmodules = null;
	
	/**
	 * Execute and display a template script.
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null){
		/*
		 * Set the template - this will display cpanel.php
		 * from the selected admin template.
		 */
		$input = JFactory::getApplication()->input;
		$input->set('tmpl', 'cpanel');

		// Display the cpanel modules
		$this->modules 		= JModuleHelper::getModules('cpanel');
		$this->iconmodules 	= $this->get('ModulesIcon');
		$this->canDo       	= JHelperContent::getActions('com_sim0');
		
		$this->docsInvalid	= $this->getModel()->getTotalDocs(0);
		$this->indsInvalid	= $this->getModel()->getTotalInds(0);
		//print_r($this->docsInvalid);die("s");
		
		
		// Set toolbar items for the page
		JToolbarHelper::title(JText::_('COM_SIM0_MANAGER_DASHBOARD'), 'arrow-up');
		if ($this->canDo->get('core.admin')){
			JToolbarHelper::preferences('com_sim0');
			JToolbarHelper::divider();
		}

		$bar = JToolBar::getInstance('toolbar');
		$bar->appendButton('Popup', 'save-copy', JText::_('COM_SIM0_NOTIFICATION_BUTTON_CREATE'), 'index.php?option=com_sim0&view=notification&layout=edit&tmpl=component', 600, 600);
		
		JToolbarHelper::help('screen.dashboard');
		//JToolbarHelper::help('JHELP_COMPONENTS_SIM0_NOTIFICATIONS');
		//JToolbarHelper::cancel('logactions.cancel');

		parent::display($tpl);
	}
	
	
}
