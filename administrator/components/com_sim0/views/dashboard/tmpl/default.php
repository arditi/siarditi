<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

$user = JFactory::getUser();
?>
<div class="row-fluid">
	<?php 
	$iconmodules = JModuleHelper::getModules('icon');
	if ($iconmodules) : ?>
		<div class="span3">
			<div class="cpanel-links">
				<?php
				// Display the submenu position modules
				foreach ($iconmodules as $iconmodule2){
					//echo JModuleHelper::renderModule($iconmodule);
				}
				
				// Usamos os nossos links
				$iconmodules2 = $this->iconmodules;
				// Display the submenu position modules
				foreach($iconmodules2 as $section => $iconmodules) {
					?>
					<div class="sidebar-nav quick-icons">
					<div class="j-links-groups"><h2 class="nav-header"><?php echo $section; ?></h2>
					<ul class="j-links-group nav nav-list">
						<?php
						foreach ($iconmodules as $iconmodule){
						?>
							<li><a href="<?php echo $iconmodule['link']; ?>">
									<i class="icon-pencil-2"></i> <span class="j-links-<?php if(isset($iconmodule['icon'])) echo $iconmodule['icon']; ?>">
									<?php echo $iconmodule['text']; ?></span>	</a>
								</li>					
						<?php
						}
						?>
					</ul>
					</div>
					</div>				
				<?php		
				}
				?>
			</div>
		</div>
	<?php endif; ?>
	<div class="span<?php echo ($iconmodules) ? 7 : 10; ?>">
		<div class="row-fluid">	

			<div class="well well-small"><h2 class="module-title nav-header">Investigadores por Validar</h2>
			<div class="row-striped">
			<div class="row-fluid">
				<div class="span9">
					<strong class="row-title"><a href="/Joomla_3.3.6v1/administrator/index.php?option=com_users&amp;task=user.edit&amp;id=782" 
							class="hasTooltip" title="ID : 782">Manuel João da Silva</a></strong>
					<small class="small hasTooltip" title="Location">Investigator</small>
					</div>
				<div class="span3">
					<span class="small hasTooltip" title="Last Activity"><i class="icon-calendar"></i> 2015-03-09</span>
					</div>
				</div>
			<div class="row-fluid">
				<div class="span9">
					<strong class="row-title"><a href="/Joomla_3.3.6v1/administrator/index.php?option=com_users&amp;task=user.edit&amp;id=782" 
							class="hasTooltip" title="ID : 782">Antónia Maria Fernandes Casemiro</a></strong>
					<small class="small hasTooltip" title="Location">Investigator</small>
					</div>
				<div class="span3">
					<span class="small hasTooltip" title="Last Activity"><i class="icon-calendar"></i> 2015-03-09</span>
					</div>
				</div>
			</div></div>
			
			
			<div class="well well-small"><h2 class="module-title nav-header">Candidaturas</h2>
			<div class="row-striped">
			<div class="row-fluid">
				<div class="span9">
					<span class="badge badge-info hasTooltip" title="" data-original-title="Hits">17</span>
					<strong class="row-title"><a href="/Joomla_3.3.6v1/administrator/index.php?option=com_users&amp;task=user.edit&amp;id=782" 
							class="hasTooltip" title="ID : 782">Candidaturas Submetidas</a></strong>
					<small class="small hasTooltip" title="Location">Tipo: Candidatura Anual, Promotor:Tecnopolo, Valor: 50.000,00€</small>
					</div>
				<div class="span3">
					<span class="small hasTooltip" title="Last Activity"><i class="icon-calendar"></i> 2015-03-09</span>
					</div>
				</div>
			<div class="row-fluid">
				<div class="span9">
					<span class="badge badge-info hasTooltip" title="" data-original-title="Hits">3</span>
					<strong class="row-title"><a href="/Joomla_3.3.6v1/administrator/index.php?option=com_users&amp;task=user.edit&amp;id=782" 
							class="hasTooltip" title="ID : 782">Candidaturas Reclamadas </a></strong>
					<small class="small hasTooltip" title="Location"></small>
					</div>
				<div class="span3">
					<span class="small hasTooltip" title="Last Activity"><i class="icon-calendar"></i> 2015-03-09</span>
					</div>
				</div>
			</div></div>
			
			
			<div class="well well-small"><h2 class="module-title nav-header">Candidaturas Submetidas</h2>
			<div class="row-striped">
			<div class="row-fluid">
				<div class="span9">
					<strong class="row-title"><a href="/Joomla_3.3.6v1/administrator/index.php?option=com_users&amp;task=user.edit&amp;id=782" 
							class="hasTooltip" title="ID : 782">Frutos Silvestres Marítimos</a></strong>
					<small class="small hasTooltip" title="Location">Tipo: Candidatura Anual, Promotor:Tecnopolo, Valor: 50.000,00€</small>
					</div>
				<div class="span3">
					<span class="small hasTooltip" title="Last Activity"><i class="icon-calendar"></i> 2015-03-09</span>
					</div>
				</div>
			<div class="row-fluid">
				<div class="span9">
					<strong class="row-title"><a href="/Joomla_3.3.6v1/administrator/index.php?option=com_users&amp;task=user.edit&amp;id=782" 
							class="hasTooltip" title="ID : 782">Mobilidade Eléctrica </a></strong>
					<small class="small hasTooltip" title="Location">Tipo: Candidatura Anual, Promotor:Tecnopolo, Valor: 200.000,00€</small>
					</div>
				<div class="span3">
					<span class="small hasTooltip" title="Last Activity"><i class="icon-calendar"></i> 2015-03-09</span>
					</div>
				</div>
			</div></div>
			
			<div class="well well-small"><h2 class="module-title nav-header">Candidaturas Reclamadas</h2>
			<div class="row-striped">
			<div class="row-fluid">
				<div class="span9">
					<strong class="row-title"><a href="/Joomla_3.3.6v1/administrator/index.php?option=com_users&amp;task=user.edit&amp;id=782" 
							class="hasTooltip" title="ID : 782">Frutos Silvestres Marítimos</a></strong>
					<small class="small hasTooltip" title="Location">2 dias</small>
					</div>
				<div class="span3">
					<span class="small hasTooltip" title="Last Activity"><i class="icon-calendar"></i> 2015-03-09</span>
					</div>
				</div>
			</div></div>				
			<?php
			// DEFAULT MODULES RESUME FROM COMPONENTS
			$spans = 0;
			foreach ($this->modules as $module)
			{
				// Get module parameters
				$params = new Registry;
				$params->loadString($module->params);
				$bootstrapSize = $params->get('bootstrap_size');
				if (!$bootstrapSize){
					$bootstrapSize = 12;
				}
				$spans += $bootstrapSize;
				if ($spans > 12){
					echo '</div><div class="row-fluid">';
					$spans = $bootstrapSize;
				}
				if($module->module=="mod_logged" || $module->module=="mod_popular")
					echo JModuleHelper::renderModule($module, array('style' => 'well'));
			}
			?>
		</div>
	</div>
	
	<div class="span2">
		<div class="row-fluid">	
				
			<div class="well well-small"><h2 class="module-title nav-header">Atenção</h2>
			<div class="row-striped">	
				
			<div class="row-fluid">
				<div class="span9">
					<strong class="row-title"><a href="index.php?option=com_sim0&amp;view=docs&filter[validated]=0" 
							class="hasTooltip" title="Por validar">Indicadores por validar</a></strong>
					<span class="badge badge-info hasTooltip" title="" data-original-title="Hits"><?php echo $this->indsInvalid->total; ?></span>
					<br /><small class="small hasTooltip" title="Location">Último em <?php echo $this->indsInvalid->last; ?> </small>
					</div>
				</div>
			
			<div class="row-fluid">
				<div class="span9">
					<span class="badge badge-info hasTooltip" title="" data-original-title="Hits"><?php echo $this->docsInvalid->total; ?></span>
					<strong class="row-title"><a href="index.php?option=com_sim0&amp;view=docs&filter[validated]=0" 
							class="hasTooltip" title="ID : 782">Documentos por validar</a></strong><br />
					<small class="small hasTooltip" title="Location">Último em <?php echo $this->docsInvalid->last; ?> </small>
					</div>
				</div>

			
			</div>
			</div>		
		
		</div>
	</div>

</div>
