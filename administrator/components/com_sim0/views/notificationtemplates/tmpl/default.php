<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
$loggeduser = JFactory::getUser();
?>
<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=notificationtemplates');?>" 
		method="post" name="adminForm" id="adminForm">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
		<?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else : ?>
			<table class="table table-striped" id="userList">
				<thead>
					<tr>
						<th width="2%" class="nowrap center">
							<?php echo JHtml::_('grid.checkall'); ?>
						</th>
						<th width="2%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'id_template', $listDirn, $listOrder); ?>
						</th>						
						<th width="5%" class="nowrap center">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_NOTIFICATIONS_TEMPLATES_HEADING_CATEGORY', 'category', $listDirn, $listOrder); ?>
						</th>
						<th width="5%" class="nowrap center">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_NOTIFICATIONS_TEMPLATES_HEADING_EXTERNAL', 'external', $listDirn, $listOrder); ?>
						</th>
						<th class="left">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_NOTIFICATIONS_TEMPLATES_HEADING_SUBJECT', 'subject', $listDirn, $listOrder); ?>
						</th>
						<!--
						<th width="15%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_USERS_HEADING_LAST_VISIT_DATE', 'a.lastvisitDate', $listDirn, $listOrder); ?>
						</th>
						-->
						<th width="15%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_USERS_HEADING_REGISTRATION_DATE', 'a.registerDate', $listDirn, $listOrder); ?>
						</th>

					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="15">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$canEdit   = $this->canDo->get('core.edit');
					$canChange = $loggeduser->authorise('core.edit.state',	'com_users');
					// If this group is super admin and this user is not super admin, $canEdit is false
					if ((!$loggeduser->authorise('core.admin')) && JAccess::check($item->id_nt, 'core.admin'))	{
						$canEdit   = false;
						$canChange = false;
					}
				?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="center">
							<?php if ($canEdit) : ?>
								<?php echo JHtml::_('grid.id', $i, $item->id_nt); ?>
							<?php endif; ?>
						</td>
						
						<td class="center hidden-phone">
							<?php echo (int) $item->id_template; ?>
						</td>						
						
						<td class="center hidden-phone">
							<?php echo "M".$this->escape($item->category); ?>
						</td>
						<td class="center hidden-phone">
							<?php echo ($this->escape($item->external)?"Yes":"No"); ?>
						</td>
						
						<td>
							<?php if ($canEdit) : ?>
								<a href="<?php echo JRoute::_('index.php?option=com_sim0&task=notificationtemplate.edit&id_template='.(int)$item->id_template.'&id_nt='.(int)$item->id_nt); ?>" 
										title="<?php echo  $this->escape($item->subject_uk); ?>">
									<?php echo $this->escape($item->subject); ?></a>
							<?php else : ?>
									<?php echo $this->escape($item->subject); ?>
							<?php endif; ?>
						</td>

						<!--
						<td class="center hidden-phone">
							<?php if ($item->sys_del != '0000-00-00 00:00:00'):?>
								<?php echo JHtml::_('date', $item->sys_del, 'Y-m-d H:i:s'); ?>
							<?php else:?>
								<?php echo JText::_('JNEVER'); ?>
							<?php endif;?>
						</td>
						-->
						<td class="center hidden-phone">
							<?php echo JHtml::_('date', $item->sys_date, 'Y-m-d H:i:s'); ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>

		<?php //Load the batch processing form. ?>
		<?php echo $this->loadTemplate('batch'); ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
