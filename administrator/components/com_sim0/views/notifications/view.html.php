<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of tracks.
 *
 * @since  1.6
 */
class SIM0ViewNotifications extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		// Get application
		$app = JFactory::getApplication();
		$context = "sim0.list.admin.notification";
		
		$this->items      = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state      = $this->get('State');
		$this->filter_order 	= $app->getUserStateFromRequest($context.'filter_order', 'filter_order', 'greeting', 'cmd');
		$this->filter_order_Dir = $app->getUserStateFromRequest($context.'filter_order_Dir', 'filter_order_Dir', 'asc', 'cmd');
		$this->canDo	  = JHelperContent::getActions('com_sim0', 'notifications', $this->state->get('filter.id_not'));

		
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		require_once JPATH_COMPONENT . '/helpers/sim0.php';
		SIM0Helper::addSubmenu('notifications');
		
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
		$this->setDocument();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 */
	protected function addToolbar()
	{
		$title = JText::_('COM_SIM0_MANAGER_NOTIFICATIONS');
		if ($this->pagination->total){
			$title .= " <span style='font-size: 0.5em; vertical-align: middle;'>(" . $this->pagination->total . ")</span>";
		}
		JToolbarHelper::title($title, 'bookmark banners-tracks');
		
		$bar = JToolBar::getInstance('toolbar');
		$bar->appendButton('Popup', 'save-copy', JText::_('COM_SIM0_NOTIFICATION_BUTTON_CREATE'), 'index.php?option=com_sim0&view=notification&layout=edit&tmpl=component', 600, 400);
		//$bar->appendButton('Popup', 'download', 'JTOOLBAR_EXPORT', 'index.php?option=com_sim&amp;view=notification&amp;layout=edit&amp;tmpl=empty', 600, 300);
		//$bar->appendButton('Popup', 'download', 'JTOOLBAR_EXPORT', 'index.php?option=com_sim0&amp;view=download&amp;tmpl=empty', 600, 300);
			//JToolBarHelper::custom( 'apps.SaveCatalogs',    'save-copy', 'save', 'Publish Catalog', false, false );
			// é igual a ;
			//$bar->appendButton('Standard', $icon, $alt, $task, $listSelect);
		$bar->appendButton('Standard', 'save-copy', "Templates", "notifications.notificationTemplates", false);
		//JToolbarHelper::addNew('notificationtemplate.add','title');
		
		if ($this->canDo->get('core.delete')){
			//$bar->appendButton('Confirm', 'COM_SIM0_DELETE_MSG', 'delete', 'COM_SIM0_NOTIFICATIONS_DELETE', 'notifications.delete', false);
			JToolbarHelper::divider();
		}
				
		JHtmlSidebar::setAction('index.php?option=com_sim0&view=notifications');
		JHtmlSidebar::addFilter(
			JText::_('COM_SIM0_SELECT_EXTERNAL'),'filter_type',
				JHtml::_('select.options',	array(	JHtml::_('select.option', 0, JText::_('COM_SIM0_NOTIFICATIONS_HEADING_INTERNAL')), 
													JHtml::_('select.option', 1, JText::_('COM_SIM0_NOTIFICATIONS_HEADING_EXTERNAL'))
											),'value','text', $this->state->get('filter.type')
			)
		);
		JHtmlSidebar::addFilter(
			JText::_('COM_SIM0_SELECT_TEMPLATES'),'filter_id_template',
			JHtml::_('select.options', SIM0Helper::getTemplateOptions($this->state->get('filter.type')), 'value', 'text', $this->state->get('filter.id_template'))
		);		
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		//$document->setTitle(JText::_('COM_HELLOWORLD_ADMINISTRATION'));
	}
	
	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields()
	{
		return array(
			'a.id_template' 	=> JText::_('COM_SIM0_NOTIFICATIONS_HEADING_TEMPLATE'),
			'a.from' 			=> JText::_('COM_SIM0_NOTIFICATIONS_HEADING_FROM'),
			'a.to' 				=> JText::_('COM_SIM0_NOTIFICATIONS_HEADING_TO'),
			'a.sys_date' 		=> JText::_('COM_SIM0_NOTIFICATIONS_HEADING_DATA')
		);
	}
}
