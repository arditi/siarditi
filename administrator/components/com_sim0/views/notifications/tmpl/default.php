<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal', 'a.modal');
JHtml::_('formbehavior.chosen', 'select');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
$sortFields = $this->getSortFields();

JFactory::getDocument()->addScriptDeclaration('
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != "' . $listOrder . '")
		{
			dirn = "asc";
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, "");
	};
	Joomla.closeModalDialog = function()
	{
		window.jQuery("#modal-download").modal("hide");
	};
');

$canEdit=true;

?>
<script type="text/javascript">

</script>
<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=notifications'); ?>" method="post" name="adminForm" id="adminForm">
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<label class="filter-hide-lbl" for="filter_begin"><?php echo JText::_('COM_SIM0_NOTIFICATIONS_BEGIN_LABEL'); ?></label>
				<?php echo JHtml::_('calendar', $this->state->get('filter.begin'), 'filter_begin', 'filter_begin', '%Y-%m-%d', array('size' => 10, 'onchange' => "this.form.fireEvent('submit');this.form.submit()")); ?>
			</div>
			<div class="filter-search btn-group pull-left">
				<label class="filter-hide-lbl" for="filter_end"><?php echo JText::_('COM_SIM0_NOTIFICATIONS_END_LABEL'); ?></label>
				<?php echo JHtml::_('calendar', $this->state->get('filter.end'), 'filter_end', 'filter_end', '%Y-%m-%d', array('size' => 10, 'onchange' => "this.form.fireEvent('submit');this.form.submit()")); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
					<option value="asc" <?php echo $listDirn == 'asc' ? 'selected="selected"' : ''; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
					<option value="desc" <?php echo $listDirn == 'desc' ? 'selected="selected"' : ''; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING'); ?></option>
				</select>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY'); ?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
				</select>
			</div>
		</div>
		<div class="clearfix"></div>
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else : ?>
			<table class="table table-striped">
				<thead>
					<tr>
						<th width="13%" class="nowrap hidden-phone">
							<?php echo JText::_('COM_SIM0_NOTIFICATIONS_HEADING_FROM'); ?>
							<!--
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_NOTIFICATIONS_HEADING_FROM', 'a.from', $listDirn, $listOrder); ?>
							-->
							</th>
						<th class="title">
							<?php echo JText::_('COM_SIM0_NOTIFICATIONS_HEADING_SUBJECT'); ?>
							</th>
						<th width="15%" class="nowrap hidden-phone">
							<?php echo JText::_('COM_SIM0_NOTIFICATIONS_HEADING_TO'); ?>
							<!--
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_NOTIFICATIONS_HEADING_TO', 'a.to', $listDirn, $listOrder); ?>
							-->
							</th>
						<th width="12%" class="nowrap hidden-phone">
							<?php echo JText::_('JDATE'); ?>
							</th>
						<th width="3%" class="nowrap hidden-phone">
							<?php echo JText::_('COM_SIM0_NOTIFICATIONS_HEADING_SENDCOUNT'); ?>
							</th>
						<th width="12%" class="nowrap hidden-phone">
							<?php echo JText::_('COM_SIM0_NOTIFICATIONS_HEADING_SENDDATE'); ?>
							</th>						
						<th width="3%" class="nowrap">
							<?php echo JText::_('COM_SIM0_NOTIFICATIONS_HEADING_STATUS'); ?>
							</th>
						<th width="4%" class="nowrap hidden-phone">
							<?php echo JText::_('COM_SIM0_NOTIFICATIONS_HEADING_ACTION'); ?>
							</th>						
							
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($this->items as $i => $item) : ?>
						<tr class="row<?php echo $i % 2; ?>">
							<td class="hidden-phone">
								<?php if ($canEdit) : 
										$return     = JFactory::getURI()->toString();
										//$return     = urlencode(base64_encode($return));
										$url  		= 'index.php?option=com_sim0&amp;view=user&amp;layout=edit&amp;id_user='.(int)$item->id_user_from.'&amp;return=' . urlencode(base64_encode($return));
									?>
									<a href="<?php echo $url; ?>" title="View">
										<?php echo JStringPunycode::emailToUTF8($this->escape($item->from)); ?></a>
								<?php else : ?>
										<?php echo JStringPunycode::emailToUTF8($this->escape($item->from)); ?>
								<?php endif; ?>

								</td>
							<td>
								<?php if ($canEdit) : 
										$return     = JFactory::getURI()->toString();
										//$return     = urlencode(base64_encode($return));
										$url  		= JRoute::_('index.php?option=com_sim0&amp;view=user&amp;layout=edit&amp;id_user='.(int)$item->id_user_from.'&amp;return=' . urlencode(base64_encode($return)));
										$url  		= JRoute::_('index.php?option=com_sim0&amp;view=notification&amp;layout=default&amp;id_not=' . (int) $item->id_not); 
										//administrator/index.php?option=com_sim0&view=logaction&_tmpl=component&layout=edit&id_log=10652
										?>
									<a href="<?php echo $url; ?>" title="View">
										<?php echo $item->subject; ?></a>
								<?php else : ?>
										<?php echo $item->subject; ?>
								<?php endif; ?>
								
								<div class="small">
									<?php echo $item->template; ?>
								</div>
								</td>						
							<td class="hidden-phone">
								<?php //echo $item->to; ?>
								<?php if ($canEdit) : 
										$return     = JFactory::getURI()->toString();
										//$return     = urlencode(base64_encode($return));
										$url  		= 'index.php?option=com_sim0&amp;view=user&amp;layout=edit&amp;id_user='.(int)$item->id_user_to.'&amp;return=' . urlencode(base64_encode($return));
									?>
									<a href="<?php echo $url; //JRoute::_('index.php?option=com_sim0&view=user&layout=edit&id=' . (int) $item->autor_id_user); ?>" title="View">
										<?php echo JStringPunycode::emailToUTF8($this->escape($item->to)); ?></a>
								<?php else : ?>
										<?php echo JStringPunycode::emailToUTF8($this->escape($item->to)); ?>
								<?php endif; ?>

								</td>
							<td class="hidden-phone">
								<?php echo JHtml::_('date', $item->sys_date, JText::_('DATE_FORMAT_LC4') . ' H:i'); ?>
								</td>
							<td class="small hidden-phone">
							<!--
								<?php if($item->send_try_count) echo JHtml::_('date', $item->send_try_count, JText::_('DATE_FORMAT_LC4') . ' H:i'); ?>
								-->
								<?php echo $item->send_try_count; ?>
								</td>
							<td class="hidden-phone">
								<?php echo $item->send_date; ?>
								</td>
							<td><?php 
								//echo $item->template;
								if ($item->send_date!="")
									echo "<span class=\"icon-publish\"></span>"; 
								else
									echo "<span class=\"icon-unpublish\"></span>"; 
								?>
								</td>
							<td class="hidden-phone" align="center">
							<?php if ($canEdit) : ?>
								<a  _class="modal" _data-toggle="modal" _data-target="#modal-save-copy" 
									href="<?php echo JRoute::_('index.php?option=com_sim0&_tmpl=component&task=notification.send&id_not='.(int)$item->id_not); ?>" 
									title="<?php echo  $this->escape($item->source); ?>">
									<span class="icon-mail"></span>
									<?php //echo $this->escape("Send"); ?></a>
							<?php else : ?>
									<?php echo $this->escape($item->source); ?>
							<?php endif; ?>
								</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
