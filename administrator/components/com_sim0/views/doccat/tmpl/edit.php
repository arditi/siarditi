<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'doccat.cancel' || document.formvalidator.isValid(document.getElementById('doccat-form')))
			{
				Joomla.submitform(task, document.getElementById('doccat-form'));
			}
		};
");
?>
<form   action="<?php echo JRoute::_('index.php?option=com_sim0&id_category='.(int) $this->item->id_category); ?>); ?>" 
		method="post" name="adminForm" id="doccat-form" class="form-validate form-horizontal">
	<fieldset class="adminform">
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_category'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_category'); ?>
			</div>
		</div>
		
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('category'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('category'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('category_uk'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('category_uk'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_category_parent'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_category_parent'); ?>
			</div>
		</div>

	</fieldset>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
