<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'logaction.cancel' || document.formvalidator.isValid(document.getElementById('logaction-form')))
			{
				Joomla.submitform(task, document.getElementById('logaction-form'));
			}
		};
");
?>
<form action="<?php echo JRoute::_('index.php?option=com_sim0&id_log='.(int) $this->item->id_log); ?>); ?>" 
		method="post" name="adminForm" id="logaction-form" class="form-validate form-horizontal">
	<fieldset class="adminform">
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_log'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_log'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_user'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_user'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_action'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_action'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('source'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('source'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('sys_date'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('sys_date'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('desc'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getData()['desc']; ?>
			</div>
		</div>
	</fieldset>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
