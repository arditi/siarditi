<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'ind.cancel' || document.formvalidator.isValid(document.getElementById('ind-form')))
			{
				Joomla.submitform(task, document.getElementById('ind-form'));
			}
		};

		Joomla.submitbuttonRegion = function(region, task)
		{
			var fm = document.getElementById('ind-form');
			fm.jform_id_region.value = region;
			
			Joomla.submitbutton(task);
		};
");
?>
<form   action="<?php echo JRoute::_('index.php?option=com_sim0&id_indicator='.(int) $this->item->id_indicator); ?>); ?>" 
		method="post" name="adminForm" id="ind-form" class="form-validate">
		

	<?php //echo JLayoutHelper::render('joomla.edit.title_alias', $this); ?>
	<div class="form-inline form-inline-header">
		<?php
		echo $this->form->renderField('name');
		echo $this->form->renderField('name_uk');
		?>
	</div>		
	<?php //echo $this->form->getControlGroup('name'); ?>
	<?php //echo $this->form->getControlGroup('name_uk'); ?>
		
	<div class="form-horizontal">
	<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_SIM0_IND_EDIT_DETAILS', true)); ?>
	
	<fieldset class="adminform">
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_indicator'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_indicator'); ?>
			</div>
		</div>
		
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_axis'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_axis'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_dimension'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_dimension'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('definition'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('definition'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('source'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('source'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('sourceURL'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('sourceURL'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('periodicity'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('periodicity'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('units'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('units'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('obs'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('obs'); ?>
			</div>
		</div>

	</fieldset>
	
		<?php echo JHtml::_('bootstrap.endTab'); ?>


<?php
////
// Se é uma edição
$model 		= $this->getModel('Ind');
$id 		= $this->item->id_indicator;

////
// Se é uma edição
if($id!=""){	
	?>
	
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'axis', JText::_('COM_SIM0_IND_EDIT_VALUES', true)); ?>
		<?php //echo $this->form->renderFieldset('images'); ?>
		
		<?php
			$regions	= $model->getRegions($id);
			//print_r($regions);
			foreach($regions as $region){
				$this->drawRegiao($region->id_region, $region->region);
				//echo ("--------".$region->id_region);
			}
			?>		
			<div class="control-group">
				<div class="control-label">
					<?php echo ' - '; ?>
				</div>
				<div class="controls">
					<select name="jform[id_region2]" id="jform_id_region2">
						<?php 
						$allregs = SIM0Helper::getRegions();
						echo JHtml::_('select.options', $allregs); 
						?>
					</select>
<button onclick="Joomla.submitbuttonRegion('<?php //echo $id_region ?>', 'ind.addregion')" 
	class="btn btn-small btn-success"><span class="icon-new icon-white"></span>add region</button>
					<br />
				</div>
			</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		
<?php		
}


if(1==2){
?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'images', JText::_('COM_SIM0_IND_EDIT_VALUES', true)); ?>
		<?php //echo $this->form->renderFieldset('images'); ?>

		<?php
		$itemsData =  array(
			'1'=>array(2010,29,30),
			'2'=>array(2011,31,30),
			'3'=>array(2012,32,31)
		); 		
		foreach($itemsData as $key => $data){
			?>
			<div class="control-group">
				<div class="control-label">
					<?php echo $key.' - '//.$this->form->getLabel('source'); ?>
					<?php echo $data[0];?>
				</div>
				<div class="controls">
					<select name="x_<?php echo $key;?>">
						<option value=""><?php echo JText::_('JSELECT') ?></option>
						<?php 
						$messages = SIM0Helper::getRegions();
						$options  = array();
						foreach($messages as $message) {
							$options[] = JHtml::_('select.option', $message->value, $message->text);
						}
						//echo JHtml::_('select.options', $options);
						?>
						<?php echo JHtml::_('select.options', $messages); ?>
						
					</select>
					<input size="3" type="text" name="x_<?php echo $key;?>" value="<?php echo $data[0];?>">
					<input size="3" type="text" name="r_<?php echo $key;?>" value="<?php echo $data[1];?>">
					<input size="3" type="text" name="e_<?php echo $key;?>" value="<?php echo $data[2];?>">
				</div>
			</div>
			<?php
		}
		?>
		
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php //echo $this->form->renderFieldset('urls'); ?>
	</div>

<?php 
}
?>
				<!--
					<select name="batch[group_id]" id="batch-group-id">
						<option value=""><?php echo JText::_('JSELECT') ?></option>
						<?php echo JHtml::_('select.options', JHtml::_('user.groups')); ?>
					</select>
					-->


	<input type="hidden" name="jform[id_region]" id="jform_id_region" />
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
