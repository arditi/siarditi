<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.framework');
JHtml::_('formbehavior.chosen', 'select');
?>
<!-- chart libraries start -->
<script src="bower_components/flot/excanvas.min.js"></script>
<script src="bower_components/flot/jquery.flot.js"></script>
<script src="bower_components/flot/jquery.flot.pie.js"></script>
<script src="bower_components/flot/jquery.flot.stack.js"></script>
<script src="bower_components/flot/jquery.flot.resize.js"></script>
<!-- chart libraries end -->
<script src="js/init-chart.js"></script>



<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=ind'); ?>" 
	method="post" name="adminForm" id="adminForm" class="form-horizontal">
	<fieldset>
		<div class="control-group">
			<div class="control-label">
				<?php echo JText::_('COM_SIM0_IND_SYSDATE'); ?>
			</div>
			<div class="controls">
				<?php echo JHtml::_('date', $this->item->sys_date);?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo JText::_('COM_SIM0_IND_NAME'); ?>
			</div>
			<div class="controls">
				<?php echo $this->item->name;?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo JText::_('COM_SIM0_IND_DEFINITION'); ?>
			</div>
			<div class="controls">
				<?php echo $this->item->definition; ?>
			</div>
		</div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="reply_id" value="<?php echo $this->item->id_axis; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</fieldset>
	

	
</form>
