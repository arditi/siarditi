<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * HTML View class for the Messages component
 *
 * @since  1.6
 */
class SIM0ViewInd extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $state;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$this->form		= $this->get('Form');
		$this->item		= $this->get('Item');
		$this->state	= $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		$this->addToolbar();
		//
		parent::display($tpl);
		// Set the document
		$this->setDocument();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		$id = $this->item->id_indicator;
		if ($this->getLayout() == 'edit')
		{
			JFactory::getApplication()->input->set('hidemainmenu', true);
			$title = ($id=="")?JText::_('COM_SIM0_IND_TITLE_NEW'):JText::_('COM_SIM0_IND_TITLE_EDIT');
			JToolbarHelper::title($title, 'envelope-opened new-privatemessage');
			
			JToolbarHelper::apply('ind.apply');
			JToolbarHelper::save('ind.save');
			//JToolbarHelper::save('ind.save', 'COM_SIM0_NOTIFICATION_TEMPLATE_TOOLBAR_SEND');
			JToolbarHelper::save2new('ind.save2new');
			
			// Preview Indicator
$bar = JToolBar::getInstance('toolbar');
$bar->appendButton('Popup', 'save-copy', JText::_('COM_SIM0_IND_PREVIEW'),
		'../index.php?option=com_sim0&view=ind&layout=default&tmpl=graph&id_indicator='.$id, 600, 400);

			JToolbarHelper::cancel('ind.cancel');
			JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_WRITE');
		}
		else
		{
			JToolbarHelper::title(JText::_('COM_SIM0_IND_TITLE_VIEW'), 'envelope inbox');
			/*
			$sender = JUser::getInstance($id);
			if ($sender->authorise('core.admin') || $sender->authorise('core.manage', 'com_sim0') && $sender->authorise('core.login.admin'))
			{
				JToolbarHelper::custom('ind.reply', 'redo', null, 'COM_SIM0_TOOLBAR_REPLY', false);
			}
			*/
			JToolbarHelper::cancel('ind.cancel');
			JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_READ');
		}
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$isNew = ($this->item->id_indicator == 0);
		$document = JFactory::getDocument();
		//$document->setTitle($isNew ? JText::_('COM_M0_LOG_CREATING') : JText::_('COM_M0_LOG_EDITING'));
		//$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "/administrator/components/com_sim0"
		                                  . "/views/ind/init-chart.js");
		//JText::script('COM_M0_LOG_ERROR_UNACCEPTABLE');
		
		
	}
	
	//
	//
	protected function drawRegiao($id_region, $region=""){
		$id 	= $this->item->id_indicator;
		$model 	= $this->getModel('Ind');
		$regions= $model->getRegions($id);
		$data 	= $model->getRegionValues($id, $id_region);
		//print_r($data);
		?>
		<div class="strong"><b><?php echo "Region ".$region; ?></b></div>
		<div class="control-group">
			<div class="control-label">
			<table cellspacing="0" cellpadding="0"><tr>
				<td>Year:
					<br>Real:
					<br>Estimated:<br>
					</td>			
			<?php
				foreach($data as $key => $val){
					$nm1 = 'y'.$val->id_value;
					$nm2 = 'r'.$val->id_value;
					$nm3 = 'e'.$val->id_value;
				?>
				<td><?php //echo $val->year.'<br>';?>
						<input placeholder="year" style="width:30px" size="2" type="text" 
							name="jform[<?php echo $nm1;?>]" id="jform_<?php echo $nm1;?>" value="<?php echo $val->year;?>">
					<br><input placeholder="real" style="width:30px" size="2" type="text" 
							name="jform[<?php echo $nm2;?>]" id="jform_<?php echo $nm2;?>" value="<?php echo $val->real_value;?>">
					<br><input placeholder="estimated" style="width:30px" size="2" type="text" 
							name="jform[<?php echo $nm3;?>]" id="jform_<?php echo $nm3;?>" value="<?php echo $val->estimated_value;?>">
					</td>
				<?php 
				}
			?>
				<td>				
					<button onclick="Joomla.submitbuttonRegion('<?php echo $id_region ?>', 'ind.addyear')" 
						class="btn btn-small btn-success"><span class="icon-new icon-white"></span>add year</button>
					<br />
					<br />
					<button onclick="Joomla.submitbuttonRegion('<?php echo $id_region ?>', 'ind.delyear')" 
						class="btn btn-small btn-error"><span class="icon-minus icon-red"></span>del year</button>
					
					</td>
					</tr></table>
			
		
			</div>
		</div>				
		<?php
			
	}
	
}