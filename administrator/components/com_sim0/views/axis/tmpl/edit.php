<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'axis.cancel' || document.formvalidator.isValid(document.getElementById('axis-form')))
			{
				Joomla.submitform(task, document.getElementById('axis-form'));
			}
		};
");
?>
<form   action="<?php echo JRoute::_('index.php?option=com_sim0&id_axis='.(int) $this->item->id_axis); ?>); ?>" 
		method="post" name="adminForm" id="axis-form" class="form-validate form-horizontal">
	<fieldset class="adminform">
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_axis'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_axis'); ?>
			</div>
		</div>
		
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('name'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('name'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('name_uk'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('name_uk'); ?>
			</div>
		</div>

		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('purpose'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('purpose'); ?>
			</div>
			<div class="control-label">
				<?php echo $this->form->getLabel('purpose_uk'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('purpose_uk'); ?>
			</div>
		</div>
		
		
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('definition'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('definition'); ?>
			</div>
			<div class="control-label">
				<?php echo $this->form->getLabel('definition_uk'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('definition_uk'); ?>
			</div>
		</div>
	</fieldset>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
