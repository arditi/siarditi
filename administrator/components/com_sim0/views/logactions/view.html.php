<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of LogActions.
 *
 */
class SIM0ViewLogActions extends JViewLegacy
{
	// The item data.
	protected $items;

	// The pagination object.
	protected $pagination;
	
	// The model state.
	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 * @return  void
	 */
	public function display($tpl = null){
		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->items         = $this->get('Items');
		$this->canDo		 = JHelperContent::getActions('com_sim0', 'logactions', $this->state->get('filter.id_log'));
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		$app      	= JFactory::getApplication();
		$return  	= $app->input->get('return');
		$this->state->set('return', $return);
		
		// Add required JS
        //JHtml::_('behavior.framework');
		// Include the component HTML helpers.
		//JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
		require_once JPATH_COMPONENT . '/helpers/sim0.php';
		SIM0Helper::addSubmenu('logactions');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		$title = JText::_('COM_SIM0_MANAGER_LOGACTIONS');
		if ($this->pagination->total){
			$title .= " <span style='font-size: 0.5em; vertical-align: middle;'>(" . $this->pagination->total . ")</span>";
		}
		JToolbarHelper::title($title, 'shield banners-tracks');

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
		//$bar->appendButton('Standard', 'save-copy', "Templates", "logactions.notificationTemplates", false);
/*		
		// Nao podemos inserir
		if ($this->canDo->get('core.create')){
			JToolbarHelper::addNew('logaction.add');
		}
		if ($this->canDo->get('core.edit')){
			JToolbarHelper::editList('logaction.edit');
		}
*/
		// Filtros Laterais
		JHtmlSidebar::setAction('index.php?option=com_sim0&view=logactions');		
		JHtmlSidebar::addFilter(
			JText::_('COM_SIM0_SELECT_ACTIONTYPES'), 'filter_id_action',
				JHtml::_('select.options', SIM0Helper::getActionOptions(), 'value', 'text', $this->state->get('filter.id_action'))
		);	
		JHtmlSidebar::addFilter(
			JText::_('COM_SIM0_SELECT_RANGE'), 'filter_range',
				JHtml::_('select.options', SIM0Helper::getRangeOptions(), 'value', 'text', $this->state->get('filter.range'))
		);
		
	}
			
	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields()
	{
		return array(
				'a.id_log' 		=> JText::_('ID'),
				'a.id_action' 	=> JText::_('COM_SIM0_LOGACTIONS_HEADING_ACTION'),
				'a.id_user' 	=> JText::_('COM_SIM0_LOGACTIONS_HEADING_IDUSER'),
				'a.source' 		=> JText::_('COM_SIM0_LOGACTIONS_HEADING_SOURCE'),
				'a.sys_date' 	=> JText::_('COM_SIM0_LOGACTIONS_HEADING_SYSDATE')
		);
	}
}
