<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal', 'a.modal');
JHtml::_('formbehavior.chosen', 'select');


$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
$sortFields = $this->getSortFields();

// javscript para ordenar a tabela no cliente
JFactory::getDocument()->addScriptDeclaration('
	Joomla.orderTable = function()
	{
		table 		= document.getElementById("sortTable");
		direction 	= document.getElementById("directionTable");
		order 		= table.options[table.selectedIndex].value;
		if (order != "' . $listOrder . '")
		{
			dirn = "asc";
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, "");
	};
	Joomla.closeModalDialog = function()
	{
		window.jQuery("#modal-download").modal("hide");
	};
');
?>
<script type="text/javascript">
	</script>

<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=logactions'); ?>" 
		method="post" name="adminForm" id="adminForm">
		
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
		<?php
		// Search tools bar
		//echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		//echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this), null, array('debug' => true));
		?>
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<input type="text" name="filter_search" id="filter_search" class="hasTooltip"
				placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" 
				value="<?php echo $this->escape($this->state->get('filter.search')); ?>"  
				title="<?php echo JText::_('JSEARCH_FILTER');//JHtml::tooltipText('COM_USERS_SEARCH_IN_NOTE_TITLE'); ?>" />
			</div>
			<div class="btn-group">
				<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.getElementById('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');  ?></option>
				</select>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
				</select>
			</div>
		</div>
		
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
			
		<?php else : ?>
			<table class="table table-striped" id="userList">
				<thead>
					<tr>
						<th width="2%" class="nowrap center">
							<?php echo JHtml::_('grid.checkall'); ?>
						</th>
						<th width="2%" class="nowrap center hidden-phone">
							<?php //echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'id_log', $listDirn, $listOrder); ?>
							<?php echo JHTML::_('grid.sort', 'JGRID_HEADING_ID', 'a.id_log', $listDirn, $listOrder); ?>
						</th>						
						<th width="5%" class="nowrap center">
							<?php //echo JHtml::_('searchtools.sort', 'COM_SIM0_LOGACTIONS_HEADING_IDUSER', 'id_user', $listDirn, $listOrder); ?>
							<?php echo JHTML::_('grid.sort', 'COM_SIM0_LOGACTIONS_HEADING_IDUSER', 'a.id_user', $listDirn, $listOrder); ?>
						</th>
						<th width="15%" class="nowrap center">
							<?php //echo JHtml::_('searchtools.sort', 'COM_SIM0_LOGACTIONS_HEADING_ACTION', 'id_action', $listDirn, $listOrder); ?>
							<?php echo JHTML::_('grid.sort', 'COM_SIM0_LOGACTIONS_HEADING_ACTION', 'a.id_action', $listDirn, $listOrder); ?>
						</th>
						<th class="left">
							<?php //echo JHtml::_('searchtools.sort', 'COM_SIM0_LOGACTIONS_HEADING_SOURCE', 'source', $listDirn, $listOrder); ?>
							<?php echo JHTML::_('grid.sort', 'COM_SIM0_LOGACTIONS_HEADING_SOURCE', 'a.source', $listDirn, $listOrder); ?>
						</th>
						<th width="15%" class="nowrap center hidden-phone">
							<?php //echo JHtml::_('searchtools.sort', 'COM_SIM0_LOGACTIONS_HEADING_SYSDATE', 'sys_date', $listDirn, $listOrder);  ?>
							<?php echo JHTML::_('grid.sort', 'COM_SIM0_LOGACTIONS_HEADING_SYSDATE', 'a.sys_date', $listDirn, $listOrder); ?>
						</th>

					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="15">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
				<?php 
				foreach ($this->items as $i => $item) :
					/*
					$canEdit   = $this->canDo->get('core.edit');
					$canChange = $loggeduser->authorise('core.edit.state',	'com_sim0');
					// If this group is super admin and this user is not super admin, $canEdit is false
					if ((!$loggeduser->authorise('core.admin')) && JAccess::check($item->id_log, 'core.admin'))	{
						$canEdit   = false;
						$canChange = false;
					}*/
					$canEdit   = true;
					$canChange   = true;
				?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="center">
							<?php if ($canEdit) : ?>
								<?php echo JHtml::_('grid.id', $i, $item->id_log); ?>
							<?php endif; ?>
						</td>
						
						<td class="center hidden-phone">
							<?php echo (int) $item->id_log; ?>
						</td>						
						
						<td class="center hidden-phone">
							<a href="<?php //echo JRoute::_($item->id_user_url); ?>">
							
							<?php //echo "".$this->escape($item->id_user); ?>
							<?php if ($canEdit) : 
									$return     = JFactory::getURI()->toString();
									//$return     = urlencode(base64_encode($return));
									$url  		= 'index.php?option=com_sim0&amp;view=user&amp;layout=edit&amp;id_user='.(int)$item->id_user.'&amp;return=' . urlencode(base64_encode($return));
								?>
								<a href="<?php echo $url; //JRoute::_('index.php?option=com_sim0&view=user&layout=edit&id=' . (int) $item->autor_id_user); ?>" title="View">
									<?php echo JStringPunycode::emailToUTF8($this->escape($item->id_user)); ?></a>
							<?php else : ?>
									<?php echo JStringPunycode::emailToUTF8($this->escape($item->id_user)); ?>
							<?php endif; ?>
							
							
						</td>
						<td class="center hidden-phone">
							<?php echo $this->escape($item->action);//($this->escape($item->external)?"Yes":"No"); ?>
						</td>
						
						<td>
							<?php if ($canEdit) : ?>
								<a  class="modal" data-toggle="modal" data-target="#modal-save-copy" 
									href="<?php echo JRoute::_('index.php?option=com_sim0&tmpl=component&task=logaction.edit&id_log='.(int)$item->id_log); ?>" 
									title="<?php echo  $this->escape($item->source); ?>">
									<?php echo $this->escape($item->source); ?></a>
							<?php else : ?>
									<?php echo $this->escape($item->source); ?>
							<?php endif; ?>
						</td>

						<td class="center hidden-phone">
							<?php echo JHtml::_('date', $item->sys_date, 'Y-m-d H:i:s'); ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; 
?>
		<?php //Load the batch processing form. ?>
		<?php //echo $this->loadTemplate('batch'); ?>
		
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
			
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
