<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.framework');
//JHtml::_('formbehavior.chosen', 'select');
//print_r($this->item);
?>

<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=notification'); ?>" method="post" name="adminForm" id="adminForm" class="form-horizontal">
	<fieldset>
	
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_user_from'); ?>
			</div>
			<div class="controls">
				<?php echo $this->item->id_user_from; ?>
				<?php echo $this->item->get('from_user_name');?>
			</div>
		</div>	
		
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_user_to'); ?>
			</div>
			<div class="controls">
				<?php echo $this->item->id_user_to; ?>
				<?php echo $this->item->get('to');?>
			</div>
		</div>	
	
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('subject'); ?>
			</div>
			<div class="controls">
				<?php echo $this->item->subject; ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('body'); ?>
			</div>
			<div class="controls">
				<?php echo $this->item->body; ?>
			</div>
		</div>	
		<div class="control-group">
			<div class="control-label">
			    <?php echo JText::_('Retries');//COM_SIM0_NOTIFICATION_SENDDATE'); ?>
			</div>
			<div class="controls">
				<?php echo $this->item->get('send_try_count');?>
			</div>
		</div>


		<div class="control-group">
			<div class="control-label">
				<?php echo JText::_('Send date');//COM_SIM0_NOTIFICATION_SENDDATE'); ?>
			</div>
			<div class="controls">
				<?php echo JHtml::_('date', $this->item->send_date);?>
			</div>
		</div>


		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</fieldset>
</form>
