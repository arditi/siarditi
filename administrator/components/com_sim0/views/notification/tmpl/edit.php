<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'notification.cancel' || document.formvalidator.isValid(document.getElementById('notification-form')))
			{
				Joomla.submitform(task, document.getElementById('notification-form'));
			}
		};
");

$app      	= JFactory::getApplication();
$view     	= $app->input->getCmd('view', '');
$layout   	= $app->input->getCmd('layout', '');
$tmpl     	= $app->input->getCmd('tmpl', 'index');
$step     	= SIM0ModelNotification::$step;

/*
$input 	  	= JFactory::getApplication()->input; 
$formData	= new JRegistry($input->get('jform', array(), 'array')); 
$id_template= $formData['id_template'];
//print_r($formData);

/*
$foo = $input->get('id_template2', null, null);
$id_template = $input->getInt('id_template2');
$id_template = (int)$input->post->get("id_template2");

die($foo." = ".$id_template." = ".$formData['id_template2']);
*/

?>
<!--
	<div id="j-sidebar-container" class="span1">
		<?php //echo $this->sidebar; ?>
	</div>
	-->
	<div id="j-main-container" class="span11">
<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=notification&layout=edit&id_not='.$this->item->id_not.'&tmpl='.$tmpl); ?>" 
	  method="post" name="adminForm" id="notification-form" class="form-validate form-horizontal">
	<fieldset class="adminform">

<?php
//
if($step==1){
	?>
		<div class="control-group">
			<div class="control-label">&nbsp;</div>
			<div class="controls">
				<?php echo $this->form->getLabel('note_template'); ?>
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_template'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_template'); ?>
			</div>
		</div>
		
		<div class="control-group">
			<div class="control-label">
				<?php echo $this->form->getLabel('id_user_to'); ?>
			</div>
			<div class="controls">
				<?php echo $this->form->getInput('id_user_to'); ?>
			</div>
		</div>		
<?php
}
///
elseif($step==2){
	?>
	<div style="display:none">
	<?php
		//echo $this->form->getControlGroup('to');
		echo $this->form->getControlGroup('id_template');
		//echo $this->form->getControlGroup('id_user_to');
	?>
	</div>
	<?php
		echo $this->form->getControlGroup('id_user_to');
		echo $this->form->getControlGroup('subject');
		echo $this->form->getControlGroup('body');
?>
<?php
}
///
elseif($step==3){
?>
<?php
}
?>		
		<div class="control-group">
			<div class="control-label">
				<?php //echo $this->form->getLabel('body'); ?>
			</div>
			<div class="controls">
				<?php //echo $this->form->getInput('body', $group= '_default', $formControl= '_default', $groupControl= '_default', $body );
						//echo $this->form->getInput('body'); ?>
				<button type="button" class="btn" onclick="this.form.submit();window.top.setTimeout('window.parent.jModalClose()', 500);">
					<?php echo JText::_('COM_SIM0_NOTIFICATION_BUTTON'); ?></button>
			</div>
		</div>

	</fieldset>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
	</div>
	<div id="j-sidebar-container" class="span1">
	
		<!--
		<button type="button" class="btn" onclick="this.form.submit();window.top.setTimeout('window.parent.jModalClose()', 500);">
			<?php echo "Continuar";//JText::_('COM_BANNERS_TRACKS_EXPORT'); ?></button>
		-->
		<!-- not working
		<button type="button" class="btn" onclick="window.parent.jModalClose();"><?php echo JText::_('COM_BANNERS_CANCEL'); ?></button>
		-->
		
	</div>	
</form>
<?php
JFactory::getDocument()->addScriptDeclaration("
		tinyMCE.execCommand('mceToggleEditor', false, 'jform_body');
");
?>