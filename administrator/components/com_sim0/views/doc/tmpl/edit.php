<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'doc.cancel' || document.formvalidator.isValid(document.getElementById('doc-form')))
			{
				Joomla.submitform(task, document.getElementById('doc-form'));
			}
		};
");
?>
<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=doc&layout=edit&id_doc='.$this->item->id_doc);?>" 
	  enctype="multipart/form-data" method="post" name="adminForm" id="doc-form" class="form-validate form-horizontal">
	<fieldset class="adminform">
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('title'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('title'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('autor_id_user'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('autor_id_user'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('autor_name'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('autor_name'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('leader'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('leader'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('id_category'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('id_category'); ?></div>
		</div>
		<?php if($this->item->id_doc!=""){ ?>
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('file_uri'); ?></div>
			<div class="controls"><?php 		if($this->item->file_uri!="")
													echo '<a href="'.JURI::root().$this->item->file_uri.'" target=_blank>'.$this->item->file_uri.'<a><br />';
												else
													echo '---<br />';
												
												echo $this->form->getInput('file_uri'); ?></div>
		</div>
		<?php } ?>

		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('tags'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('tags'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('resume'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('resume'); ?></div>
		</div>
		
	</fieldset>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
