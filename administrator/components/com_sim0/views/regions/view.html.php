<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of regions.
 *
 */
class SIM0ViewRegions extends JViewLegacy
{
	// The item data.
	protected $items;

	// The pagination object.
	protected $pagination;
	
	// The model state.
	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 * @return  void
	 */
	public function display($tpl = null){
		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->items         = $this->get('Items');
		$this->canDo         = JHelperContent::getActions('com_sim0');
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		// Include the component HTML helpers.
		JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
		SIM0Helper::addSubmenu('regions');		
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		$canDo	= $this->canDo;
		$user 	= JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_SIM0_REGS_TITLE') );//, 'envelope-opened new-privatemessage');	
		
		if ($canDo->get('core.create')){
			JToolbarHelper::addNew('region.add');
		}

		if ($canDo->get('core.edit')){
			JToolbarHelper::editList('region.edit');
		}
		if ($canDo->get('core.delete'))
		{
			JToolbarHelper::deleteList('', 'regions.delete');
			JToolbarHelper::divider();
		}
		if ($canDo->get('core.admin')){
			JToolbarHelper::preferences('com_sim0');
			JToolbarHelper::divider();
		}
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields_TODELETE()
	{
		return array(
				'name' => JText::_('name'),
				'definition' => JText::_('definition')
		);
	}
}
