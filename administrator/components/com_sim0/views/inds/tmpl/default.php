<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
//JHtml::_('formbehavior.chosen', 'select');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');
JHtml::_('behavior.tabstate');

$input = JFactory::getApplication()->input;

JFactory::getDocument()->addScriptDeclaration("
jQuery(document).ready(function($){

	// Hide all the folder when the page loads
	$('.folder ul, .component-folder ul').hide();

	// Display the tree after loading
	$('.directory-tree').removeClass('directory-tree');

	// Show all the lists in the path of an open file
	$('.show > ul').show();

	// Stop the default action of anchor tag on a click event
	$('.folder-url, .component-folder-url').click(function(event){
		event.preventDefault();
	});

	// Prevent the click event from proliferating
	$('.file, .component-file-url').bind('click',function(e){
		e.stopPropagation();
	});

	// Toggle the child indented list on a click event
	$('.folder, .component-folder').bind('click',function(e){
		$(this).children('ul').toggle();
		e.stopPropagation();
	});

	// New file tree
	$('#fileModal .folder-url').bind('click',function(e){
		$('.folder-url').removeClass('selected');
		e.stopPropagation();
		$('#fileModal input.address').val($(this).attr('data-id'));
		$(this).addClass('selected');
	});

	// Folder manager tree
	$('#folderModal .folder-url').bind('click',function(e){
		$('.folder-url').removeClass('selected');
		e.stopPropagation();
		$('#folderModal input.address').val($(this).attr('data-id'));
		$(this).addClass('selected');
	});
});");

JFactory::getDocument()->addStyleDeclaration("
	/* Styles for modals */
	.selected{
		background: #08c;
		color: #fff;
	}
	.selected:hover{
		background: #08c !important;
		color: #fff;
	}
	.modal-body .column {
		width: 50%; float: left;
	}
	#deleteFolder{
		margin: 0;
	}

	#image-crop{
		max-width: 100% !important;
		width: auto;
		height: auto;
	}

	.directory-tree{
		display: none;
	}

	.tree-holder{
		overflow-x: auto;
	}
");

$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));

$loggeduser = JFactory::getUser();
$return     = JFactory::getURI()->toString();
?>
<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=inds');?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
		<?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else : ?>
			<table class="table table-striped" id="userList">
				<thead>
					<tr>
						<th width="1%" class="nowrap center">
							<?php echo JHtml::_('grid.checkall'); ?>
						</th>
						<th class="left">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_INDS_NAME', 'a.name', $listDirn, $listOrder); ?>
						</th>
						<th width="10%" class="nowrap left hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_INDS_AXIS', 'a.id_axis', $listDirn, $listOrder); ?>
						</th>
						<th width="18%" class="nowrap left hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_INDS_DIMENSION', 'a.id_dimension', $listDirn, $listOrder); ?>
						</th>
						<th width="5%" class="nowrap left hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_INDS_PERIOD', 'a.periodicity', $listDirn, $listOrder); ?>
						</th>
						<th width="5%" class="nowrap center">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_INDS_STATE', 'a.state', $listDirn, $listOrder); ?>
						</th>
						<th width="5%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_INDS_VALIDATED', 'a.validated', $listDirn, $listOrder); ?>
						</th>
						<th width="12%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_INDS_SYSDATE', 'a.sys_date', $listDirn, $listOrder); ?>
						</th>
						<th width="1%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id_indicator', $listDirn, $listOrder); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="15">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
				<?php 
				//INSERT INTO `siarditi_dbv1`.`docs` (`id_doc`, `id_category`, `title`, `resume`, `autor_name`, `autor_id_user`, `leader`, `validated`, `active`, `pub_id_user`, `pub_date`, `del_date`, `sys_date`, `tags`, `id_project_parent`) VALUES (NULL, '1', 'DOcumento Geral', 'Resumo do DOcumento Geral', 'Eduardo', '783', NULL, '0', '1', '782', '2015-03-01 00:00:00', NULL, CURRENT_TIMESTAMP, 'Inovação Regulamento', NULL);
				foreach ($this->items as $i => $item) :
					$canEdit   = $this->canDo->get('core.edit');
					$canChange = $loggeduser->authorise('core.edit.state',	'com_users');
					// If this group is super admin and this user is not super admin, $canEdit is false
					if ((!$loggeduser->authorise('core.admin')) && JAccess::check($item->id_indicator, 'core.admin')){
						$canEdit   = false;
						$canChange = false;
					}
					//$canEdit   = true;
					//$canChange = true;			
					//$canEdit   = false;
					//$canChange = false;					
////
// Prepare item links
// Atenção! Demora muito tempo este metodo!!!!
$item->autor_id_user_lnk= JStringPunycode::emailToUTF8($this->escape($item->name));
$item->title_lnk		= $this->escape($item->name);
if ($canEdit){
	$item->title_url		= JRoute::_('index.php?option=com_sim0&task=ind.edit&id_indicator=' . (int) $item->id_indicator );
	$item->title_lnk 		= '<a href="'.$item->title_url.'" title="'.JText::sprintf('COM_USERS_EDIT_USER', $this->escape($item->name)).'">'.$item->title_lnk.'</a>';
}
					?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="center">
							<?php if ($canEdit) : ?>
								<?php echo JHtml::_('grid.id', $i, $item->id_indicator); ?>
							<?php endif; ?>
						</td>
						<td>
							<?php echo $item->title_lnk; ?>
							<div>
								<?php //echo JHtml::_('users.filterNotes', /*$item->note_count*/ 10, $item->id_indicator); ?>
								<?php //echo JHtml::_('users.notes', /*$item->note_count*/10, $item->id_indicator); ?>
								<?php //echo JHtml::_('users.addNote', $item->axis); ?>
								<!--
								<span _class="label label-info"><?php echo $item->axis; ?></span>
								-->
								<?php if ($item->validated != '1') : ?>
								<span class="label label-warning"><?php echo JText::_('COM_SIM0_INDS_VALIDATE_REQUIRED'); ?></span>
								<?php endif; ?>
								
							</div>
							<?php if (JDEBUG) : ?>
								<div class="small"><a href="<?php echo JRoute::_('index.php?option=com_users&view=debuguser&user_id=' . (int) $item->id_indicator);?>">
								<?php echo JText::_('COM_USERS_DEBUG_USER');?></a></div>
							<?php endif; ?>
						</td>
						<td class="left">
							<?php 
							$SMAX=15;
							if($item->axis!="" & strlen($item->axis)>$SMAX)
								 echo $this->escape(substr($item->axis, 0, $SMAX).' ...'); 
							else echo $this->escape($item->axis);
							?>
						</td>
						<td class="left">
							<?php 
							if($item->dimension!="" & strlen($item->dimension)>$SMAX)
								 echo $this->escape(substr($item->dimension, 0, $SMAX).' ...'); 
							else echo $this->escape($item->dimension);
							?>
						</td>

						<td class="left">
							<?php echo $this->escape($item->periodicity); ?>
						</td>
						<td class="center">
							<?php 
							$validated = (empty($item->validated) OR $item->validated=='0') ? 0 : 1;
							if ($validated){
								$self 		= false;//($loggeduser->id == $item->autor_id_user);
								$state 		= $item->state=='1' ? 1 : 0;
								//	?? public static function state($filter_state = '*', $published = 'Published', $unpublished = 'Unpublished', $archived = null, $trashed = null)
								echo JHtml::_('jgrid.state', JHtmlUsers::blockStates($self), $state, $i, 'inds.', (boolean) $canChange);
							}
							?>
						</td>
						<td class="center hidden-phone">
							<?php
							echo JHtml::_('jgrid.state', JHtmlUsers::activateStates(), $validated, $i, 'inds.', (boolean) ( (!$validated) && $canEdit) );
							?>
						</td>
						<td class="center hidden-phone">
							<?php echo JHtml::_('date', $item->sys_date, 'Y-m-d H:i:s'); ?>
						</td>
						<td class="center hidden-phone">
							<?php echo (int) $item->id_indicator; ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>

		<?php //Load the batch processing form. ?>
		<?php //echo $this->loadTemplate('batch'); ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
