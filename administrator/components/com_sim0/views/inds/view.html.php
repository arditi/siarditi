<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of users.
 *
 * @since  1.6
 */
class SIM0ViewInds extends JViewLegacy
{
        /**
	 * The item data.
	 *
	 * @var   object
	 * @since 1.6
	 */
	protected $items;

	/**
	 * The pagination object.
	 *
	 * @var   JPagination
	 * @since 1.6
	 */
	protected $pagination;

	/**
	 * The model state.
	 *
	 * @var   JObject
	 * @since 1.6
	 */
	protected $state;


	/**
	 * Encrypted file path
	 */
	protected $file;
	/**
	 * List of available overrides
	 */
	protected $overridesList;

	/**
	 * Name of the present file
	 */
	protected $fileName;

	/**
	 * Type of the file - image, source, font
	 */
	protected $type;
	
	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->state         = $this->get('State');
		$this->items         = $this->get('Items');
		$this->pagination    = $this->get('Pagination');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->canDo         = JHelperContent::getActions('com_sim0');
		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		// require helper file
		JLoader::register('JHtmlUsers', JPATH_COMPONENT . '/helpers/html/users.php');
		
		// Include the component HTML helpers.
		JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
		SIM0Helper::addSubmenu('inds');
		
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
        protected function addToolbar()
	{
		$canDo	= $this->canDo;
		$user 	= JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
		
		//JToolbarHelper::title(JText::_('COM_SIM0_INDS_TITLE'), 'users user');
		$validated = $this->state->get('filter.validated');
		if(!isset($validated)) 	$title = JText::_('COM_SIM0_MANAGER_INDS');
		elseif($validated==1)  	$title = JText::_('COM_SIM0_MANAGER_INDS_ON');
		else 					$title = JText::_('COM_SIM0_MANAGER_INDS_OFF');
		if ($this->pagination->total){
			$title .= " <span style='font-size: 0.5em; vertical-align: middle;'>(" . $this->pagination->total . ")</span>";
		}
		JToolbarHelper::title($title, 'shield banners-tracks');		
		
		if ($canDo->get('core.create')){
			JToolbarHelper::addNew('ind.add');
		}
		if ($canDo->get('core.edit')){
			JToolbarHelper::editList('ind.edit');
		}
		if ($canDo->get('core.delete')){
			JToolbarHelper::deleteList('', 'inds.delete');
			JToolbarHelper::divider();
		}
		if ($canDo->get('core.admin')){
			JToolbarHelper::preferences('com_sim0');
			JToolbarHelper::divider();
		}	
		
		/*
		if ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::divider();
			JToolbarHelper::publish('users.activate', 'COM_USERS_TOOLBAR_ACTIVATE', true);
			JToolbarHelper::unpublish('users.block', 'COM_USERS_TOOLBAR_BLOCK', true);
			JToolbarHelper::custom('users.unblock', 'unblock.png', 'unblock_f2.png', 'COM_USERS_TOOLBAR_UNBLOCK', true);
			JToolbarHelper::divider();
		}
		*/
		//Mostra modal da div
		//JToolbarHelper::modal('fileModal', 'icon-file', 'COM_SIM0_INDS_NEWFILE_BUTTON', 500,300);

		/* TENHO ERROS*/
		// Filtros Laterais
		JHtmlSidebar::setAction('index.php?option=com_sim0&view=inds');
		JHtmlSidebar::addFilter(
			JText::_('COM_SIM0_FILTER_VALIDATED'), 'validated',
				JHtml::_('select.options', SIM0Helper::getValidatedOptions(), 'value', 'text', $this->state->get('filter.validated'), false)
		);
		
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
				'a.name' 			=> JText::_('COM_USERS_HEADING_NAME'),
				'a.id_indicator' 	=> JText::_('JGRID_HEADING_ID')
		);
	}

}
