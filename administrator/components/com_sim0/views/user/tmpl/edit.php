<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');

JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'user.cancel' || document.formvalidator.isValid(document.getElementById('user-form')))
			{
				Joomla.submitform(task, document.getElementById('user-form'));
			}
		};
");
$isInvestigator = true;
?>
<form action="<?php echo JRoute::_('index.php?option=com_sim0&id_user='.$this->item->id_user);?>" 
	  enctype="multipart/form-data" method="post" name="adminForm" id="user-form" class="form-validate form-horizontal">
	<fieldset class="adminform">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_SIM0_USER_DETAILS', true)); ?>
				<div class="control-group">
					<div class="control-label"><?php 	echo $this->form->getLabel('name'); ?></div>
					<div class="controls"><?php 		echo $this->form->getInput('name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php 	echo $this->form->getLabel('autor_id_user'); ?></div>
					<div class="controls"><?php 		echo $this->form->getInput('autor_id_user'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><strong><?php 	echo $this->form->getLabel('email'); ?></strong></div>
					<div class="controls"><?php 		echo $this->form->getInput('email'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php 	echo $this->form->getLabel('file_uri'); ?></div>
					<div class="controls"><?php 		if($this->item->file_uri!="")
															echo '<a href="'.JURI::root().$this->item->file_uri.'" target=_blank>'.$this->item->file_uri.'<a><br />';
														//else echo '---<br />';
														
														echo $this->form->getInput('file_uri'); ?></div>
				</div>		
			<?php echo JHtml::_('bootstrap.endTab'); ?>
			
			<?php if ($isInvestigator) : ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS', true)); ?>
					<?php //echo $this->loadTemplate('investigator'); ?>
					<div class="control-group">
						<div class="control-label"><?php 	echo $this->form->getLabel('resume'); ?></div>
						<div class="controls"><?php 		echo $this->form->getInput('resume'); ?></div>
					</div>				
				<?php echo JHtml::_('bootstrap.endTab'); ?>		
				
			<?php endif; ?>
			
			<?php if ($isInvestigator & 1==2) : ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS', true)); ?>
					<?php //echo $this->loadTemplate('groups'); ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endif; ?>
		
		
		<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
	</fieldset>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
