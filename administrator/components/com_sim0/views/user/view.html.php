<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_sim0
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * HTML View class for the Messages component
 *
 * @since  1.6
 */
class SIM0ViewUser extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $state;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$this->form		= $this->get('Form');
		$this->item		= $this->get('Item');
		$this->state	= $this->get('State');
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		$this->form->setValue('password', null);
		$this->form->setValue('id_category', 2);
		
		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		$user      = JFactory::getUser();
		$canDo     = JHelperContent::getActions('com_users');
		$isNew     = ($this->item->id_user == 0);
		$isProfile = $this->item->id_user == $user->id;
		
		// Editing
		if ($this->getLayout() == 'edit')
		{
			JFactory::getApplication()->input->set('hidemainmenu', true);
			JToolbarHelper::title(JText::_('COM_SIM0_USER_NEW'), 'envelope-opened new-privatemessage');
			
			JToolbarHelper::apply('user.apply');
			JToolbarHelper::save('user.save');
			JToolbarHelper::save('user.save', 'COM_SIM0_NOTIFICATION_TEMPLATE_TOOLBAR_SEND');
			//JToolbarHelper::save2new('user.save2new');
			JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_WRITE');
		}
		// Viewing
		else
		{
			JToolbarHelper::title(JText::_('COM_SIM0_USER_VIEW'), 'envelope inbox');
			$sender = JUser::getInstance($this->item->autor_id_user);
			if ($sender->authorise('core.admin') || $sender->authorise('core.manage', 'com_sim0') && $sender->authorise('core.login.admin'))
			{
				JToolbarHelper::custom('user.reply', 'redo', null, 'COM_SIM0_TOOLBAR_REPLY', false);
			}
			JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_READ');
		}
		
		$task 		= 'user.cancel';		
		// To a good return 
		$array 		= array();
		$return   	= JFactory::getApplication()->input->get('return');
		parse_str(base64_decode($return), $array);
		if(array_key_exists('view', $array) ){
			if($array['view']=='docs') 				$task = 'user.goDocs';
			elseif($array['view']=='logactions')	$task = 'user.goLogActions';
			elseif($array['view']=='notifications')	$task = 'user.goNotifications';
			elseif($array['view']=='users') 		$task = 'user.goUsers';
		}
		//
		if (empty($this->item->id_user)){
			JToolbarHelper::cancel($task);
		}
		else{
			JToolbarHelper::cancel($task, 'JTOOLBAR_CLOSE');
		}		
		
	}
}
