<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
//JHtml::_('formbehavior.chosen', 'select');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');
JHtml::_('behavior.tabstate');

$input = JFactory::getApplication()->input;

JFactory::getDocument()->addScriptDeclaration("
jQuery(document).ready(function($){

	// Hide all the folder when the page loads
	$('.folder ul, .component-folder ul').hide();

	// Display the tree after loading
	$('.directory-tree').removeClass('directory-tree');

	// Show all the lists in the path of an open file
	$('.show > ul').show();

	// Stop the default action of anchor tag on a click event
	$('.folder-url, .component-folder-url').click(function(event){
		event.preventDefault();
	});

	// Prevent the click event from proliferating
	$('.file, .component-file-url').bind('click',function(e){
		e.stopPropagation();
	});

	// Toggle the child indented list on a click event
	$('.folder, .component-folder').bind('click',function(e){
		$(this).children('ul').toggle();
		e.stopPropagation();
	});

	// New file tree
	$('#fileModal .folder-url').bind('click',function(e){
		$('.folder-url').removeClass('selected');
		e.stopPropagation();
		$('#fileModal input.address').val($(this).attr('data-id'));
		$(this).addClass('selected');
	});

	// Folder manager tree
	$('#folderModal .folder-url').bind('click',function(e){
		$('.folder-url').removeClass('selected');
		e.stopPropagation();
		$('#folderModal input.address').val($(this).attr('data-id'));
		$(this).addClass('selected');
	});
});");

JFactory::getDocument()->addStyleDeclaration("
	/* Styles for modals */
	.selected{
		background: #08c;
		color: #fff;
	}
	.selected:hover{
		background: #08c !important;
		color: #fff;
	}
	.modal-body .column {
		width: 50%; float: left;
	}
	#deleteFolder{
		margin: 0;
	}

	#image-crop{
		max-width: 100% !important;
		width: auto;
		height: auto;
	}

	.directory-tree{
		display: none;
	}

	.tree-holder{
		overflow-x: auto;
	}
");

$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));

$loggeduser = JFactory::getUser();
$return     = JFactory::getURI()->toString();
?>
<form action="<?php echo JRoute::_('index.php?option=com_sim0&view=docs');?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
		<?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else : ?>
			<table class="table table-striped" id="userList">
				<thead>
					<tr>
						<th width="1%" class="nowrap center">
							<?php echo JHtml::_('grid.checkall'); ?>
						</th>
						<th class="left">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_DOCS_TITLE', 'a.title', $listDirn, $listOrder); ?>
						</th>
						<th width="10%" class="nowrap left">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_DOCS_AUTOR', 'a.autor_name', $listDirn, $listOrder); ?>
						</th>
						<th width="10%" class="nowrap center">
							<?php echo JText::_('COM_SIM0_DOCS_CATEGORY'); ?>
						</th>
						<th width="15%" class="nowrap left hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_DOCS_LEADER', 'a.leader', $listDirn, $listOrder); ?>
						</th>
						<th width="5%" class="nowrap center">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_DOCS_STATE', 'a.state', $listDirn, $listOrder); ?>
						</th>
						<th width="5%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_DOCS_VALIDATED', 'a.validated', $listDirn, $listOrder); ?>
						</th>
						<!--
						<th width="10%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_DOCS_PUBDATE', 'a.pub_date', $listDirn, $listOrder); ?>
						</th>
                                                -->
						<th width="10%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIM0_DOCS_SYSDATE', 'a.sys_date', $listDirn, $listOrder); ?>
						</th>
						<th width="1%" class="nowrap center hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id_doc', $listDirn, $listOrder); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="15">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
				<?php 
				//INSERT INTO `siarditi_dbv1`.`docs` (`id_doc`, `id_category`, `title`, `resume`, `autor_name`, `autor_id_user`, `leader`, `validated`, `active`, `pub_id_user`, `pub_date`, `del_date`, `sys_date`, `tags`, `id_project_parent`) VALUES (NULL, '1', 'DOcumento Geral', 'Resumo do DOcumento Geral', 'Eduardo', '783', NULL, '0', '1', '782', '2015-03-01 00:00:00', NULL, CURRENT_TIMESTAMP, 'Inovação Regulamento', NULL);
				foreach ($this->items as $i => $item) :
					$canEdit   = $this->canDo->get('core.edit');
					$canChange = $loggeduser->authorise('core.edit.state',	'com_users');
					// If this group is super admin and this user is not super admin, $canEdit is false
					if ((!$loggeduser->authorise('core.admin')) && JAccess::check($item->id_doc, 'core.admin')){
						$canEdit   = false;
						$canChange = false;
					}
					//$canEdit   = true;
					//$canChange = true;			
					//$canEdit   = false;
					//$canChange = false;					
////
// Prepare item links
$item->autor_id_user_lnk= JStringPunycode::emailToUTF8($this->escape($item->autor_name));
$item->title_lnk		= $this->escape($item->title);
if ($canEdit){
	$item->autor_id_user_url= JRoute::_('index.php?option=com_sim0&amp;view=user&amp;layout=edit&amp;id_user='.$item->autor_id_user.'&amp;return=' . urlencode(base64_encode($return)) );
	$item->autor_id_user_lnk= '<a href="'.$item->autor_id_user_url.'" title="View User">'.$item->autor_id_user_lnk.'</a>';
	
	$item->title_url		= JRoute::_('index.php?option=com_sim0&task=doc.edit&id_doc=' . (int) $item->id_doc );
	$item->title_lnk 		= '<a href="'.$item->title_url.'" title="'.JText::sprintf('COM_USERS_EDIT_USER', $this->escape($item->title)).'">'.$item->title_lnk.'</a>';
}
					?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="center">
							<?php if ($canEdit) : ?>
								<?php echo JHtml::_('grid.id', $i, $item->id_doc); ?>
							<?php endif; ?>
						</td>
						<td>
							<?php echo $item->title_lnk; ?>
							<div>
								<?php //echo JHtml::_('users.filterNotes', $item->note_count, $item->id_doc); ?>
								<?php //echo JHtml::_('users.notes', $item->note_count, $item->id_doc); ?>
								<?php //echo JHtml::_('users.addNote', $item->id_doc); ?>
								<?php if ($item->file_uri == '') : ?>
								<span class="label label-warning"><?php echo JText::_('COM_SIM0_DOCS_FILE_REQUIRED'); ?></span>
								<?php elseif ($item->validated != '1') : ?>
								<span class="label label-warning"><?php echo JText::_('COM_SIM0_DOCS_VALIDATE_REQUIRED'); ?></span>
								<?php endif; ?>
							</div>
							<?php if (JDEBUG) : ?>
								<div class="small"><a href="<?php echo JRoute::_('index.php?option=com_users&view=debuguser&user_id=' . (int) $item->id_doc);?>">
								<?php echo JText::_('COM_USERS_DEBUG_USER');?></a></div>
							<?php endif; ?>
						</td>
						
						<td class="hidden-phone">
							<?php echo $item->autor_id_user_lnk ?>
						</td>
						
						<td class="center">
							<?php if (substr_count($item->tags, "\n") > 1) : ?>
								<span class="hasTooltip" title="<?php echo JHtml::tooltipText(JText::_('COM_USERS_HEADING_GROUPS'), nl2br($item->tags), 0); ?>"><?php echo JText::_('COM_USERS_USERS_MULTIPLE_GROUPS'); ?></span>
							<?php else : ?>
								<?php echo nl2br($item->tags); ?>
							<?php endif; ?>
						</td>
						
						<td class="left">
							<?php echo $this->escape($item->leader); ?>
						</td>
						<td class="center">
							<?php 
							$validated = (empty($item->validated) OR $item->validated=='0') ? 0 : 1;
							if ($validated){
								$self 		= ($loggeduser->id == $item->autor_id_user);
								$state 		= $item->state=='1' ? 1 : 0;
								//	?? public static function state($filter_state = '*', $published = 'Published', $unpublished = 'Unpublished', $archived = null, $trashed = null)
								echo JHtml::_('jgrid.state', JHtmlUsers::blockStates($self), $state, $i, 'docs.', (boolean) $canChange);
							}
							?>
						</td>
						<td class="center hidden-phone">
							<?php
							echo JHtml::_('jgrid.state', JHtmlUsers::activateStates(), $validated, $i, 'docs.', (boolean) ( (!$validated) && ($item->file_uri!='') && $canEdit) );
							?>
						</td>
						<!--
						<td class="center hidden-phone">
							<?php if ($item->pub_date != '0000-00-00 00:00:00'):?>
								<?php echo JHtml::_('date', $item->pub_date, 'Y-m-d H:i:s'); ?>
							<?php else:?>
								<?php echo JText::_('JNEVER'); ?>
							<?php endif;?>
						</td>
                                                -->
						<td class="center hidden-phone">
							<?php echo JHtml::_('date', $item->sys_date, 'Y-m-d H:i:s'); ?>
						</td>
						<td class="center hidden-phone">
							<?php echo (int) $item->id_doc; ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>

		<?php //Load the batch processing form. ?>
		<?php echo $this->loadTemplate('batch'); ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>


<div  id="fileModal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_('COM_SIM0_DOCS_NEWFILE_TITLE');?></h3>
	</div>
	<div class="modal-body">
		<div class="column">
			<?php echo $this->loadTemplate('folders');?>
		</div>
		<div class="column">
		<!--
			<form method="post" action="<?php echo JRoute::_('index.php?option=com_templates&task=template.createFile&id='/* . $input->getInt('id') . '&file=' . $this->file*/); ?>"
				class="well" >
				<fieldset>
					<label><?php echo JText::_('COM_TEMPLATES_NEW_FILE_TYPE');?></label>
					<select name="type" required >
						<option value="null">- <?php echo JText::_('COM_TEMPLATES_NEW_FILE_SELECT');?> -</option>
						<option value="css">css</option>
						<option value="php">php</option>
						<option value="js">js</option>
						<option value="xml">xml</option>
						<option value="ini">ini</option>
						<option value="less">less</option>
						<option value="txt">txt</option>
					</select>
					<label><?php echo JText::_('COM_TEMPLATES_FILE_NAME');?></label>
					<input type="text" name="name" required />
					<input type="hidden" class="address" name="address" />

					<input type="submit" value="<?php echo JText::_('COM_TEMPLATES_BUTTON_CREATE');?>" class="btn btn-primary" />
				</fieldset>
			</form>
		-->
		<form method="post" action="<?php echo JRoute::_('index.php?option=com_sim0&task=docs.uploadFile&id=' . $input->getInt('id') . '&file=' . $this->file); ?>"
				class="well" enctype="multipart/form-data" >
				<fieldset>
					<input type="hidden" class="address" name="address" />
					<input type="file" name="files" required />
					<input type="submit" value="<?php echo JText::_('COM_SIM0_DOCS_NEWFILE_BUTTON_UPLOAD');?>" class="btn btn-primary" />
				</fieldset>
			</form>
			<!--
			<?php if ($this->type != 'home'): ?>
				<form method="post" action="<?php echo JRoute::_('index.php?option=com_templates&task=template.copyFile&id=' . $input->getInt('id') . '&file=' . $this->file); ?>"
					  class="well" enctype="multipart/form-data" >
					<fieldset>
						<input type="hidden" class="address" name="address" />
						<div class="control-group">
							<label for="new_name" class="control-label hasTooltip" title="<?php echo JHtml::tooltipText('COM_TEMPLATES_FILE_NEW_NAME_DESC'); ?>"><?php echo JText::_('COM_TEMPLATES_FILE_NEW_NAME_LABEL')?></label>
							<div class="controls">
								<input type="text" id="new_name" name="new_name" required />
							</div>
						</div>
						<input type="submit" value="<?php echo JText::_('COM_TEMPLATES_BUTTON_COPY_FILE');?>" class="btn btn-primary" />
					</fieldset>
				</form>
			<?php endif; ?>
			-->
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal"><?php echo JText::_('COM_SIM0_DOCS_NEWFILE_BUTTON_CLOSE'); ?></a>
	</div>
</div>
