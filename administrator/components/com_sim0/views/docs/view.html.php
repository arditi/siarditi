<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of users.
 *
 * @since  1.6
 */
class SIM0ViewDocs extends JViewLegacy
{
        /**
	 * The item data.
	 *
	 * @var   object
	 * @since 1.6
	 */
	protected $items;

	/**
	 * The pagination object.
	 *
	 * @var   JPagination
	 * @since 1.6
	 */
	protected $pagination;

	/**
	 * The model state.
	 *
	 * @var   JObject
	 * @since 1.6
	 */
	protected $state;


	/**
	 * Encrypted file path
	 */
	protected $file;
	/**
	 * List of available overrides
	 */
	protected $overridesList;

	/**
	 * Name of the present file
	 */
	protected $fileName;

	/**
	 * Type of the file - image, source, font
	 */
	protected $type;
	
	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->state         = $this->get('State');
		$this->items         = $this->get('Items');
		$this->pagination    = $this->get('Pagination');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->canDo         = JHelperContent::getActions('com_sim0');
		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		// require helper file
		JLoader::register('JHtmlUsers', JPATH_COMPONENT . '/helpers/html/users.php');
		
		// Include the component HTML helpers.
		JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
		SIM0Helper::addSubmenu('docs');
		
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		
//$this->files = SIM0Helper::getDocCategoriesOptions();
$this->files = array();
$this->files['Um'] = array('Cat1', 'Categoria 1', 'Categoria 2');
$this->files['Cat1'] = array('Cat2', 'Categoria 2');
$this->files['Cat3'] = array('Cat3'=>array(	'li1', 'li1', 'Cat3'=>array('li1', 'lin2')));
	
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
        protected function addToolbar()
	{
		$canDo	= $this->canDo;
		$user 	= JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
		
		//JToolbarHelper::title(JText::_('COM_SIM0_DOCS_TITLE'), 'users user');
		$validated = $this->state->get('filter.validated');
		if(!isset($validated)) 	$title = JText::_('COM_SIM0_MANAGER_DOCS');
		elseif($validated==1)  	$title = JText::_('COM_SIM0_MANAGER_DOCS_ON');
		else 					$title = JText::_('COM_SIM0_MANAGER_DOCS_OFF');
		if ($this->pagination->total){
			$title .= " <span style='font-size: 0.5em; vertical-align: middle;'>(" . $this->pagination->total . ")</span>";
		}
		JToolbarHelper::title($title, 'shield banners-tracks');		
		
		if ($canDo->get('core.create')){
			JToolbarHelper::addNew('doc.add');
		}
		if ($canDo->get('core.edit')){
			JToolbarHelper::editList('doc.edit');
		}
		if ($canDo->get('core.delete')){
			JToolbarHelper::deleteList('', 'docs.delete');
			JToolbarHelper::divider();
		}
		if ($canDo->get('core.admin')){
			JToolbarHelper::preferences('com_sim0');
			JToolbarHelper::divider();
		}			
		
		/*
		if ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::divider();
			JToolbarHelper::publish('users.activate', 'COM_USERS_TOOLBAR_ACTIVATE', true);
			JToolbarHelper::unpublish('users.block', 'COM_USERS_TOOLBAR_BLOCK', true);
			JToolbarHelper::custom('users.unblock', 'unblock.png', 'unblock_f2.png', 'COM_USERS_TOOLBAR_UNBLOCK', true);
			JToolbarHelper::divider();
		}

		if ($canDo->get('core.delete'))
		{
			JToolbarHelper::deleteList('', 'users.delete');
			JToolbarHelper::divider();
		}

		// Add a batch button
		if ($user->authorise('core.create', 'com_users') && $user->authorise('core.edit', 'com_users') && $user->authorise('core.edit.state', 'com_users'))
		{
			JHtml::_('bootstrap.modal', 'collapseModal');
			$title = JText::_('JTOOLBAR_BATCH');

			// Instantiate a new JLayoutFile instance and render the batch button
			$layout = new JLayoutFile('joomla.toolbar.batch');

			$dhtml = $layout->render(array('title' => $title));
			$bar->appendButton('Custom', $dhtml, 'batch');
		}
		*/

		//Mostra modal da div
		//JToolbarHelper::modal('fileModal', 'icon-file', 'COM_SIM0_DOCS_NEWFILE_BUTTON', 500,300);

		/*
		if ($canDo->get('core.admin')){
			JToolbarHelper::preferences('com_users');
			JToolbarHelper::divider();
		}
		JToolbarHelper::help('JHELP_SIM0_DOCS_MANAGER');
		*/
/* TENHO ERROS*/
		// Filtros Laterais
		JHtmlSidebar::setAction('index.php?option=com_sim0&view=docs');
		JHtmlSidebar::addFilter(
			JText::_('COM_SIM0_FILTER_VALIDATED'), 'validated',
				JHtml::_('select.options', SIM0Helper::getValidatedOptions(), 'value', 'text', $this->state->get('filter.validated'), false)
		);	
	
		
/* NOT USED
		JHtmlSidebar::addFilter(
			JText::_('COM_SIM0_SELECT_RANGE'), 'filter_range',
				JHtml::_('select.options', SIM0Helper::getRangeOptions(), 'value', 'text', $this->state->get('filter.range'))
		);

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_published',
			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.published'), true)
		);

		<field
			name="validated"
			type="list"
			class="btn-group btn-group-yesno"
			ddddefault="1"
			label="COM_SIM0_FILTER_VALIDATED_DESC"
			description="COM_SIM0_CONFIG_NOTIFICATION_FORCEFROM_DESC"
			onchange="this.form.submit();"
			>
				<option value="">COM_SIM0_FILTER_VALIDATED</option>
				<option value="1">COM_SIM0_FILTER_VALIDATED_YES</option>
				<option value="0">COM_SIM0_FILTER_VALIDATED_NO</option>
			</field>	
			*/		
		
		
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
				'a.name' => JText::_('COM_USERS_HEADING_NAME'),
				'a.username' => JText::_('JGLOBAL_USERNAME'),
				'a.block' => JText::_('COM_USERS_HEADING_ENABLED'),
				'a.activation' => JText::_('COM_USERS_HEADING_ACTIVATED'),
				'a.email' => JText::_('JGLOBAL_EMAIL'),
				'a.lastvisitDate' => JText::_('COM_USERS_HEADING_LAST_VISIT_DATE'),
				'a.registerDate' => JText::_('COM_USERS_HEADING_REGISTRATION_DATE'),
				'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
	/**
	 * Method for creating the collapsible tree.
	 *
	 * @param   array  $array  The value of the present node for recursion
	 *
	 * @return  string
	 *
	 * @note    Uses recursion
	 * @since   3.2
	 */
	protected function directoryTree($array)
	{
		$temp        = $this->files;
		$this->files = $array;
		$txt         = $this->loadTemplate('tree');
		$this->files = $temp;

		return $txt;
	}

	/**
	 * Method for listing the folder tree in modals.
	 *
	 * @param   array  $array  The value of the present node for recursion
	 *
	 * @return  string
	 *
	 * @note    Uses recursion
	 * @since   3.2
	 */
	protected function folderTree($array)
	{
		$temp        = $this->files;
		$this->files = $array;
		$txt         = $this->loadTemplate('folders');
		$this->files = $temp;

		return $txt;
	}	
}
