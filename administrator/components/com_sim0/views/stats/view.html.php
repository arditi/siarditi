<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * HTML View class for the MCSV Component
 */
class SIM0ViewStats extends JViewLegacy
{

	// Variavel com as op��s de select de status
	protected $statusOptions;
	
	protected $items;
	protected $pagination;
	protected $state;
	
	// Overwriting JView display method
	public function display($tpl = null) 
	{
 		// Get data from the model
		$this->form 			= $this->get('Form');
		$this->state 			= $this->get('State');
		$this->statusOptions 	= $this->get('StatusOptions');
		
		// Carrega os valores selecionados do filtro
		$app = JFactory::getApplication('administrator');
		
		$apps = $app->getUserStateFromRequest('filter.apps', 'filter_apps', 0);
		//$this->setState('filter.apps', $apps);
		$session = JFactory::getSession();
		$session->set('filter.apps', $apps);
		$shops = $app->getUserStateFromRequest('filter.shops', 'filter_shops', 0);
		$session->set('filter.shops', $shops);
		//$this->setState('filter.shops', $shops);
		$machines = $app->getUserStateFromRequest('filter.machines', 'filter_machines', 0);
		$session->set('filter.machines', $shops);
		//$this->setState('filter.machines', $machines);
		$session->set('filter.steps', $app->getUserStateFromRequest('filter.steps', 'filter_steps', 1));
		
		// Carrega os valores possiveis do filtro
		$this->appsOptions 	 	= SIM0Helper::getRangeOptions();//getAppsOptions();
		$this->shopsOptions 	= SIM0Helper::getRangeOptions();//getShopsOptions($apps);// $this->get('ShopsOptions');
		$this->machinesOptions 	= SIM0Helper::getRangeOptions();//getMachinesOptions($apps, $shops);
		$this->stepsOptions 	= SIM0Helper::getRangeOptions();//stepsOptions();

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
 
		// Set the document
		$this->setDocument();		
	}

	/**
	 * Setting the toolbar
	 */
	public function addToolbar()
	{
		JToolBarHelper::title(JText::_('COM_MCSV_STATS_TITLE'), 'stats');

		// Mostra os Eventos
		JToolBarHelper::custom('events.listEvents',    'events.png',    'events.png', JText::_('COM_MCSV_EVENTS'),    false);
		
		// S� se volta ao CPANEL
//		JToolBarHelper::divider();
//		JToolBarHelper::cancel('stats.cancel');
		JToolBarHelper::divider();
		JToolBarHelper::help('components_mcsv_stats', true);
		
		// SubMenu
		$this->loadHelper('html');
		SIM0Helper::addSubmenu('stats');
	}
	
	/**
	 * Method to set up the document properties (html page title)
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_MCSV_STATS_TITLE'));
	
	}

	/**
	 * Prepara dados dos totais
	 *
	 * @return void
	 */
	public function prepareTotals($filter)
	{
		//$db 		= &JFactory::getDBO(); 
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();		
		$session 	= JFactory::getSession();
		$idApp		= $session->get('filter.apps','');
		$idShop		= $session->get('filter.shops','');
		$idMachine	= $session->get('filter.machines','');
		$start  	= 'now()';
		$steps  	= $session->get('filter.steps', 1);
		$sqlCtx		= array();
		// filtra a query pelos formulario
		if($idApp!='')		$sqlCtx[] = 'events.id_app='.$idApp;
		if($idShop!='')		$sqlCtx[] = 'events.id_shop='.$idShop;
		if($idMachine!='')	$sqlCtx[] = 'events.id_machine='.$idMachine;
		//if($start!='')		$sqlCtx[] = 'events.sys_date > DATE_SUB('.$start.', INTERVAL 12 MONTH)';
		if($steps!="")		$sqlCtx[] = 'events.sys_date > DATE_SUB( '.$start.', INTERVAL '.$steps.' MONTH)';
		
		$sqlCtx 	= implode(' AND ', $sqlCtx);
		$sqlCtx 	= (strlen($sqlCtx)>0 ? $sqlCtx.' AND ' : $sqlCtx );
		
		//Y-m-d H:i:s
		$sqls	 = array();
		$sqls[0] = "SELECT sum(id_action) from logactions AS events WHERE ".$sqlCtx." id_action=26 AND sys_date >= DATE_SUB(now(), INTERVAL 365 DAY)";
		$sqls[1] = "SELECT count(*) 	from logactions AS events WHERE ".$sqlCtx." id_action=26 AND sys_date >= DATE_SUB(now(), INTERVAL 365 DAY)";
		$sqls[2] = "SELECT sum(id_action) from logactions AS events WHERE ".$sqlCtx." id_action=24 AND sys_date >= DATE_SUB(now(), INTERVAL 365 DAY)";
		$sqls[3] = "SELECT count(*)     from logactions AS events WHERE ".$sqlCtx." id_action=24 AND sys_date >= DATE_SUB(now(), INTERVAL 365 DAY)";
		$totals = array();
		//for($i=0; $i<count($sqls); $i++){
		foreach($sqls as $i => $item){
			$db->setQuery( $item ); 
			$totals[$i] = $db->loadResult();
			if( empty($totals[$i]) ) $totals[$i]=0;
		}
		//echo("Totals:".$sql[3] ."-".$db->getErrMsg());
		return $totals;
	}
	/**
	 * Prepara dados dos totais
	 *
	 * @return void
	 */
	public function prepareTablesCount($filter)
	{
		$filterDate = "";
		if($filter=="dia"){			$filterDate = ' AND #__charges.sys_date > DATE_SUB(now(), INTERVAL 1 DAY)';
		}elseif($filter=="mes"){	$filterDate = ' AND #__charges.sys_date > DATE_SUB(now(), INTERVAL 32 DAY)';	
		}else{						$filterDate = ' AND #__charges.sys_date > DATE_SUB(now(), INTERVAL 12 MONTH)';
		}

		//$filterDate = '';
		$options= array(); 
		//$sql_0 	= "SELECT card, COUNT( energy ) as en from #__cards, #__charges WHERE #__cards.id_card = #__charges.id_card ".$filterDate." group by #__cards.id_card ORDER BY COUNT(energy) DESC limit 5";
		// Total Products Vistos
		$id_event_type=1;
		//$sql_0 = 'SELECT product, count(*) from #__mcsv_events WHERE id_event_type='.$id_event_type;  //sys_date >= DATE_SUB(now(), INTERVAL 365 DAY)";
		$sql_0 = 'SELECT source as card, count(*)as en from logactions WHERE id_action>0';  //sys_date >= DATE_SUB(now(), INTERVAL 365 DAY)";
		
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		//$db 	= &JFactory::getDBO(); 
		$db->setQuery( $sql_0 ); 
		$messages = $db->loadObjectList();
		if ($messages)
		{
			foreach($messages as $message){
				$options[$message->card] =  $message->en;
			}
		}
		//print_r($options);	die($sql_0);
		return $options;
	}
	

}
?>