<?php
defined( '_JEXEC' ) or die;

//jimport( 'joomla.application.component.view');

class SIM0ViewStats extends JViewLegacy
{
	public function display($tpl = null)
	{
		// Get the data from the form POST
		$data = JRequest::getVar('jform', array(), 'post', 'array');
		if(count($data)==0){// Quando n�o tem o form definido
			// Get all posted data
			$data = JRequest::get('post');
		}
		
		////
		// Calcula os �ltimos meses
		$session 	= JFactory::getSession();
		$steps  	= $session->get('filter.steps', 1);
		//$response = $this->prepareEnergyChart('2013-01-01', '2013-05-11');
		$response 	= &$this->prepareEnergyChart(date("Y-m-d", mktime(0, 0, 0, date("m")-$steps, date("d"), date("Y")) ), date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")+1, date("Y"))));
		//print_r($response);print_r(array_values ($response));die("step 2");
		$response = array_values ($response);
		foreach ($response as &$row) {
			//$row[2] = $row[2] + ((int)$row[1]);
			//print_r($row);
		}
		//print_r($response);die();
	
	
		///////////////////////////
		// OUTPUT
		// 	JSON OR JSONP 
		//	http://getk2.org/blog/786-k2-v257-released-introduces-json-jsonp-content-output
		//	https://code.google.com/p/getk2/source/browse/trunk/components/com_k2/views/item/view.json.php?r=1573
        $json 		= json_encode($response);
	    $callback 	= JRequest::getCmd('callback');
        if ($callback){
            echo $callback.'('.$json.')';
        }else{
            echo $json;
        }
		/* */
	}
	
	/**
	 * Prepara dados dos totais
	 */
	public function &makeRange($startDate, $endDate){
		$dates 	= array();
		//$dias = ConetorViewDashboard::intervaloData($startDate, $endDate);
		$date1 = date_create($startDate);
		$date2 = date_create($endDate);
		$diff  = date_diff($date1, $date2);
	
		//echo $diff->format("%R%a days");
		$dias = $diff->format("%a");
	
		//echo ("dias = ".$dias);
		list($anoInicio, $mesInicio, $diaInicio) = explode('-', $startDate);
	
		for($i=0; $i<$dias; $i++){
			$tm = mktime(0, 0, 0, $mesInicio, ($diaInicio+$i), $anoInicio);
			$dates[date("Y-m-d", $tm)] = array($tm*1000, 0, 0); // data, count,  sum, que fica total total
		}
		//print_r($dates); die("s");
		return $dates;
	}
	
	/**
	 * Prepara dados dos totais
	 */
	public function &prepareEnergyChart($startDate, $endDate){
		error_log("prepareEnergyChart=>".$startDate." - ".$endDate, 0);

		// Create an array with all dates for a given range
		$dates = $this->makeRange($startDate, $endDate);
	
		//////////////////////////////////////////////////
// Para filtrar pelo tipo de utilizador
//$db  = &SysConetor::getDBO();
//$sys = &SysConetor::getInstance();
//if($sys->isOperator)	$sqlUserWhere = ' id_card IN ( SELECT id_card FROM mccn_card WHERE id_operator='.$sys->getOperatorID().') ';
//else					$sqlUserWhere = ' id_card IN ( SELECT id_card FROM mccn_card WHERE id_user='.$sys->getUserID().') ';

//$id_charge_type = JRequest::getInt('id_charge_type', 0);
$sqlChargeTypeA = ' AND id_charging_point	IN ( SELECT id_charging_point FROM mccn_charging_point WHERE id_plug_type!=7)';
$sqlChargeTypeB = ' AND id_charging_point	IN ( SELECT id_charging_point FROM mccn_charging_point WHERE id_plug_type=7)';

		//$db 		= JFactory::getDBO();
		$sys 		= &SysARDITI::getInstance();
		$db 		= &$sys->getDBO();
		$query 		= $db->getQuery(true);
		$idApp  	= 1;
		$idShop  	= 1;
		$idMachine  = 1;
		$start  	= 'now()';
		$stop  		= '';
		
		$session 	= JFactory::getSession();
		$idApp		= $session->get('filter.apps','');
		$idShop		= $session->get('filter.shops','');
		$idMachine	= $session->get('filter.machines','');
		$start  	= 'now()';
		$steps  	= $session->get('filter.steps', 1);

		// Vendas
		$id_event_type = 26;
		//$sql  = 'select count(aux_dec) as A, sum(aux_dec) as B, sum(energy) as C, DATE_FORMAT(end_date,"%Y-%m-%d") as edate FROM #__mcsv_events AS events WHERE '.$sqlUserWhere.' '.$sqlChargeTypeA.' GROUP BY edate';
		$query->select('count(id_action) as A, sum(id_action) as B, sum(id_action) as C, DATE_FORMAT(sys_date,"%Y-%m-%d") as edate ');
		$query->from('logactions AS events');// INNER JOIN #__mcsv_products AS products ON events.id_product=products.id_product');
		//$query->where('events.id_product IS NOT NULL');
		//$query->where('events.id_event_type='.$id_event_type);
		
		// filtra a query pelos formul�rio
		if($idMachine!="")	$query->where('events.id_machine='.$idMachine);
		if($idShop!="")		$query->where('events.id_shop='.$idShop);
		if($idApp!="")		$query->where('events.id_app='.$idApp);
		//if($start!="")		$query->where('events.sys_date > DATE_SUB("'.$start.'", INTERVAL 6 MONTH)');
//		if($endDate!="")		$query->where('events.sys_date > DATE_SUB("'.$endDate.'", INTERVAL 6 MONTH)');
		if($steps!="")		$query->where('events.sys_date > DATE_SUB( '.$start.', INTERVAL '.$steps.' MONTH)');

		$query->group('edate');
		$sql  = (string)$query ;
		//
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		//print_r($rows); die($sql."\n<BR>=>".$db->getErrorMsg());
		if(!empty($rows)){				
			foreach($rows as &$row){
				if(array_key_exists($row->edate, $dates)){
					//$dates[$row->edate] = array($row->A, $row->B);
					$dates[$row->edate][1] = (double)$row->A;
					$dates[$row->edate][2] = (double)$row->B;
				}//else echo "\n<br>".$row->edate." nok";
			}
		}
		/*
		// Carregamento rapido		
		$sql  = 'select count(energy) as A, sum(energy) as B, sum(energy) as C, DATE_FORMAT(end_date,"%Y-%m-%d") as edate FROM mccn_charge WHERE '.$sqlUserWhere.' '.$sqlChargeTypeB.' GROUP BY edate';
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		//print_r($rows); die($sql.'=>'.$db->getErrorMeg());
		if(!empty($rows)){				
			foreach($rows as $row){
				if(array_key_exists($row->edate, $dates)){
					//$dates[$row->edate] = array($row->A, $row->B);
					//$dates[$row->edate][1] = (double)$row->A;
					$dates[$row->edate][2] = (double)$row->C;
				}//else echo "\n<br>".$row->edate." nok";
			}
		}
		*/
		
		//print_r($dates);die("FIM");
		return $dates;
	}
}