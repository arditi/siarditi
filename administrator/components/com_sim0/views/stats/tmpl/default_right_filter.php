<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

	/**
	 * Populate State
	 * @return void
	 * /
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication('administrator');
		$session = &JFactory::getSession();
		
		$apps = $this->getUserStateFromRequest('filter.apps', 'filter_apps');
		$session->set('filter.apps', $apps);
		
		$shops = $this->getUserStateFromRequest('filter.shops', 'filter_shops');
		$session->set('filter.shops', $shops);

		$machines = $this->getUserStateFromRequest('filter.machines', 'filter_machines');
		$session->set('filter.machines', $machines);
*/
?>
	<fieldset classo="adminform" class="fieldsets">
	<legend>Filters</legend>
	
			<table class="adminformlist" cellspacing="0" cellpadding="0" border="0" width="100%">

				<tr><td><label id="id_app-lbl" for="id_app" class="hasTip" title="Name::Application Name">Applications</label><br>
				<select id="filter_apps" name="filter_apps" class="selectbox" size="1" onchange="this.form.submit()">
				<option value=""><?php echo JText::_('COM_MCSV_APPS_OPTION_STATUS');?></option>
				<?php 	
				$session = JFactory::getSession();
				echo JHtml::_('select.options', $this->appsOptions, 'value', 'text', $session->get('filter.apps'), true);?>
				</select>
				</td></tr>
				<tr><td><label id="id_shop-lbl" for="id_shop" class="hasTip" title="Shops::Owner Group Description">Shops</label><br>
				<select name="filter_shops" id="filter_shops" class="selectbox" size="1" onchange="this.form.submit()">
				<option value=""><?php echo JText::_('COM_MCSV_APPS_OPTION_STATUS');?></option>
				<?php
				$session = JFactory::getSession();
				echo JHtml::_('select.options', $this->shopsOptions, 'value', 'text', $session->get('filter.shops'), true);?>
				</select>
				</td></tr>
				<tr><td><label id="id_machine-lbl" for="id_machine" class="hasTip" title="Vending Point::Owner Group Description">Vending Points</label><br>
				<select id="filter_machines" name="filter_machines" class="selectbox" size="1" onchange="this.form.submit()"><?php
				$session = JFactory::getSession();
				echo JHtml::_('select.options', $this->machinesOptions, 'value', 'text', $session->get('filter.machines'), true);?>
				</select>
				</td></tr>			
			
				<tr><td><label id="id_machine-lbl" for="id_machine" class="hasTip" title="Vending Point::Owner Group Description">Analized Period</label><br>
				<select id="filter_steps" name="filter_steps" class="selectbox" size="1" onchange="this.form.submit()"><?php
				$session = JFactory::getSession();
				echo JHtml::_('select.options', $this->stepsOptions, 'value', 'text', $session->get('filter.steps'), true);?>
				</select>
				</td></tr>		
				
				<?php
				// Default Stats Form
				/*
				foreach ($this->form->getFieldset('stats') as $field): ?>
				<tr><td><?php echo $field->label; ?><br>
						<?php echo $field->input; ?>
						</td></tr>
				<?php 
				endforeach */
				?>
<!--				
			</table>		
		</fieldset>		
		<fieldset classo="adminform" class="fieldsets">
			<legend>Grafs</legend>
			<table class="adminformlist" cellspacing="2" cellpadding="2" border="0" width="100%">
-->
<!--
			<tr><td>
				Dates:<br>
				<SELECT name="step" onchange="this.form.submit()" class="selectbox">
					<OPTION value="0">specific
					<OPTION value="1">+1 day
					<OPTION value="2">+1 month
					<OPTION value="3">+6 months
					<OPTION value="4">+1 year
				</SELECT>
			</td></tr>
-->
<!--
			<tr><td>
				<input type="text" id="dateStopInput" value="2012/10/10 2:2:2" style="width:155px"/>
				<img id="dateStopButton" src="calendar.png" alt="[calendar icon]"/>

				<!--<button id="dateStopButton" ><img src="calendar.png" alt="[calendar icon]"/></button>-->
<!--				
				<script type='text/javascript' >
					$('#step').change(
					  function(e) {
						e.preventDefault();
						this.form.submit();
					  } );
					$('#id_app').change(
					  function(e) {
						e.preventDefault();
						this.form.submit();
					  } );
					$('#id_shop').change(
					  function(e) {
						e.preventDefault();
						this.form.submit();
					  } );
					$('#id_machine').change(
					  function(e) {
						e.preventDefault();
						this.form.submit();
					  } );
					  
					$('#ButtonCreationDemoButton').click(
					  function(e) {
						$('#ButtonCreationDemoInput').AnyTime_noPicker().AnyTime_picker().focus();
						e.preventDefault();
					  } );
					  
					$('#dateStopButton').click(
					  function(e) {
						$('#dateStopInput').AnyTime_noPicker().AnyTime_picker().focus();
						e.preventDefault();
					  } );
				</script>
-->
			</td></tr>
			
			</table>
		</fieldset>