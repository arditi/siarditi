﻿<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
 
// load tooltip behavior
JHtml::_('behavior.tooltip');

// Include the component HTML helpers.
//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.multiselect');

$user		= JFactory::getUser();
$userId		= $user->get('id');


$filterBy = "outro";
$prTablesC 	= $this->prepareTablesCount($filterBy);

$document = JFactory::getDocument();
// $document->addScript(JURI::base() . 'media/com_carregadores/js/kiosks_map.js');
// $document->addStyleSheet(JURI::base() . 'media/com_carregadores/css/kiosks_map.css');
$document->addCustomTag("\n
<style type='text/css'>
.element-box { color:red; background-color: #FFFFF;}
.m			 { color:red; background-color: #FFFFF;}
</style>
");
?>


<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
<!--<!--
http://www.ama3.com/anytime/
http://www.dhtmlx.com/docs/products/demoApps/index.shtml
http://www.dhtmlx.com/docs/download.shtml
http://jquery.com/
-->
<!--

<link rel="stylesheet" type="text/css" href="./anytime.css" />
<script src="./jquery-1.8.3.js"></script>
<script src="./anytime.js"></script>

-->

<form action="<?php echo JRoute::_('index.php?option=com_mcsv'); ?>" method="post" name="adminForm">
<fieldset id="filter-bar" style="visibility:hidden;display:none">
		<div class="filter-search fltlft">
			<?php 
			// App filter & search
			//require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'tmpl'.DS.'default_list_filters.php';
			?>
		</div>
		
		<div class="filter-select fltrt">			
			<!--
			<select name="filter_published" class="inputbox" onchange="this.form.submit()">
				<option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED');?></option>
				<?php echo JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.published'), true);?>	
			</select>
			-->

		</div>
	</fieldset>

<TABLE width="99%" cellspacing="0" cellpadding="0" border="12" class="adminlist" style="border-spacing: 0px; border: solid 0px #d5d5d5; background-color:#F3F3FF;">
<COL width="220" />
<COL />
<TR>
<TD valign="top">
		<!-- Filters right-->
		<?php //echo $this->loadTemplate('right_filter');?>
	</TD>
<TD>
	<fieldset classo="adminform" class="fieldsets">
		<legend>Graphs</legend>
			
<!-- TEM QUE SER INCLUIDO ANTES DO AnyTime.css
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
-->

<!-- TIVE QURETIRAR POR CAUSA DOS HIGHTSTOCK-->
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>



<div id="containerw" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script language="javascript">

$(function () {
	alert("xxxxxxxxxx");
    $('#container').highcharts({
        title: {
            text: 'Monthly Average Temperature',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            name: 'New York',
            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
        }, {
            name: 'Berlin',
            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
        }, {
            name: 'London',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }]
    });
});


// Graficos muito sensiveis...
//
$(function() {
	//$.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?
	$.getJSON('./index.php?option=com_sim0&view=stats&filename=range.json&format=json&callback=?', function(data) {
		// Prepara os dados
		var data1 = new Array();
		var data2 = new Array();
		jQuery.each(data, function(i, ponto) {	
			data1.push( [ ponto[0], ponto[1]] );
			data2.push( [ ponto[0], ponto[2]] );
		});
		// coloca o valor 1
		////if(data1[data1.length-1][1]==0)data1[data1.length-1][1] = 0.0;
		////if(data2[data2.length-1][1]==0)data2[data2.length-1][1] = 0.0;

		// Create the chart
		$('#containerchart').highcharts('StockChart', {
		    chart: { type: 'areasplinerange', backgroundColor: '#F3F3FF', zoomType: 'x' },
		    _title: {
		        text: '<?PHP echo JText::_('COM_CONETOR_DASH_GRAPH_CONSUMPTION');?>'
		    },			
		    rangeSelector: {
		    	selected: 0
				// remove range selector: http://jsfiddle.net/Tn8Tw/
				// modifying r s: http://api.highcharts.com/highstock#rangeSelector.buttons
				,
				buttons: [	{ type: 'month', count: 1, text: '1m'}, { type: 'month', count: 3, text: '3m'}, { type: 'all', text: 'All'}]
		    },
	        exporting: {
				enabled: false
			},
			series : [{
					name : 'Sells',
					data : data2,
					type : 'areaspline',
					threshold : -1,
					tooltip : {
						valueSuffix: '€',
						valueDecimals : 1,
						_pointFormat: '{series.name}: <b>{point.y:,.0f} €</b><br/>'
					},
					color: Highcharts.getOptions().colors[0],
					fillColor : {
						linearGradient : { x1: 0, y1: 0, x2: 0,	y2: 1 },
						stops : [[0, Highcharts.getOptions().colors[0]], [1, 'rgba(0,0,0,0)']]
					},
					marker: {
						lineWidth: 3,
						lineColor: Highcharts.getOptions().colors[0],
						fillColor: 'white'
					}
				}
			]
		});
		
		// Create the chart
		$('#containerchartviews').highcharts('StockChart', {
		    chart: {
		        type: 'areasplinerange',
				backgroundColor: '#F3F3FF',
				zoomType: 'x'
		    },
		    _title: {
		        text: '<?PHP echo JText::_('COM_CONETOR_DASH_GRAPH_CONSUMPTION');?>'
		    },			
		    rangeSelector: {
		    	selected: 0,
				buttons: [	{ type: 'month', count: 1, text: '1m'}, { type: 'month', count: 3, text: '3m'}, { type: 'all', text: 'All'}]
		    },
	        exporting: {
				enabled: false
			},
			series : [{
					name : 'Views',
					data : data1,
					type : 'spline',
					threshold : -1,
					tooltip : {
						valueSuffix: ' ',	valueDecimals : 0,	
						_pointFormat: '{series.name}: <b>{point.y:,.0f} Wh</b><br/>'
					},
					color: Highcharts.getOptions().colors[6],
					marker: {
						lineWidth: 2, lineColor: Highcharts.getOptions().colors[6],	fillColor: 'white'
					}
				}
			]
		});
		
	});		

});

$(function () {
    var chart1, chart2, chart3, chart4, chart5;
    $(document).ready(function() {
		///////////////////////////////////////
		/// SPLINE
        chart1 = new Highcharts.Chart({
            chart: {
                renderTo: 'container_graph1',
				backgroundColor: '#F3F3FF',
                type: 'spline'
            },
            title: {text: 'Monthly Average Temperature'},
            xAxis: {categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']},
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                }
            },
			legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            tooltip: {
                //enabled: true,
				crosshairs: true,
                shared: true/*,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C';
                }*/
            },
            plotOptions: {	/*
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }*/
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Tokyo',
                data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: 'London',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        });
		///////////////////////////////////////
		/// SPLINE
        chart2 = new Highcharts.Chart({
            chart: {
                renderTo: 'container_graph2',
				backgroundColor: '#F3F3FF',
                type: 'spline'
            },
            title: {  text: 'Monthly Average Sells (GRAFICO  DE TESTE)' },
            xAxis: {  categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] },
            yAxis: {  title: {  text: 'Amount (€)' } },
            tooltip: {  crosshairs: true, shared: true  },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4, lineColor: '#666666', lineWidth: 1
                    }
                }
            },
			legend: {   enabled: false  },
            series: [{   name: 'VP001',  data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 19.6]
            }, {  name: 'VP002',  data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 16.6, 14.8]
            }, {  name: 'VP003',  data: [5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 3.9, 14.2, 24.8]
            }, {  name: 'VP004',  data: [8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 3.9, 4.2, 15.7, 34.8]
            }, ]
        });
		
		//////////////////////////////////////////////////
		// PIE
        chart3 = new Highcharts.Chart({
            chart: {  renderTo: 'container_graph3', backgroundColor: '#F3F3FF', plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false },
            title: { text: 'Product Market Shares'  },
            tooltip: {  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>', percentageDecimals: 1       },
            plotOptions: {  pie: {  allowPointSelect: true,  cursor: 'pointer' }     },
            _plotOptions: {
                pie: {
                    allowPointSelect: true,  cursor: 'pointer',  dataLabels: {
                        enabled: true, color: '#000000',  connectorColor: '#000000', formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(1) +'%';
                            //return '<b>'+ this.point.name +'</b>: '+ this.percentage +':,.2d %';//:,.0f
							//pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',  name: 'Produdct Share',
				data: [
					<?php
					$data 	= "";
					$items	= SIM0Helper::TopSells(5);
					$n 		= count($items);
					foreach ($items as $i => $item) :
						if($i==1)	$data .= "{name:'".$item->name."', y:".$item->total.", sliced: true, selected: true},\r\n";
						else		$data .= "['".$item->name."',   ".$item->total."],\r\n";	//$data .= "['".$item->name."',   ".number_format($item->total, 2, '.', '')."],\r\n";
					endforeach;
					if($data!="")
						$data = substr($data, 0, -1);// Retira última virgula
					echo $data;
					/*['Product 1',45.0],['Product 2',25.0],{name: 'Product 3', y: 25.0, sliced: true, selected: true}, ['Others',5.0] */
					?>
                ]
            }]
        });
		
		//////////////////////////////////////////////////
		// PIE
        chart4 = new Highcharts.Chart({
            chart: {    renderTo: 'container_graph4',	backgroundColor: '#F3F3FF',	plotBackgroundColor: null,	plotBorderWidth: null,	plotShadow: false      },
            title: {	text: 'Most Viewed'       },
            tooltip: {  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>', percentageDecimals: 1   },
			series: [{
                type: 'pie',
                name: 'Product Views',
                data: [
					<?php
					$data 	= "";
					$items	= SIM0Helper::Tops(24, 5);
					$n 		= count($items);
					foreach ($items as $i => $item) :
						if($i==1)	$data .= "{name:'".$item->name."', y:".$item->total.", sliced: true, selected: true},\r\n";
						else		$data .= "['".$item->name."',   ".$item->total."],\r\n";	//$data .= "['".$item->name."',   ".number_format($item->total, 2, '.', '')."],\r\n";
					endforeach;
					if($data!="")
						$data = substr($data, 0, -1);// Retira última virgula
					echo $data;
					?>
                ]
            }]
        });
		
		//////////////////////////////////////////////////
		// COLUMN
        var chart5 = new Highcharts.Chart({
            chart: { 	renderTo: 'container_graph5',backgroundColor: '#F3F3FF',type: 'column'},
            title: {    text: 'Monthly Average Sells'},
            xAxis: {	categories: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']},
            yAxis: {    min: 0,   title: { text: 'Amount (€)' }   },
            legend: {
                layout: 'vertical',
                backgroundColor: '#CCCCFF',
                align: 'left',
                verticalAlign: 'top',
                x: 75,
                y: 50,
                floating: true,
                shadow: true
            },
            tooltip: {
                formatter: function() {return ''+ this.x +': '+ this.y +' mm'; }
            },
            plotOptions: { column: { pointPadding: 0.2, borderWidth: 0 } },
            series: [{
                name: 'VP001', data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
            }, {
                name: 'VP002', data: [0, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
            }, {
                name: 'VP003', data: [0, 0, 30, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
            }, {
                name: 'VP004', data: [0, 0, 0.0, 0, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
            }]
        });

	
    });
});
</script>

<div >
<style type='text/css'>
#mainNav li.nav a {
  box-sizing:border-box;
  color:#777777;
  display:inline-block;
  font-size:12px;
  font-weight:normal;
  padding:6px 12px 10px 10px;
  text-decoration:initial;
  text-shadow:rgba(0, 0, 0, 0.701961) 1px 1px 2px;
  width:100%;
}
a {
  color:#0066CC;
  text-decoration:initial;
}

#mainNav li.nav {
	list-style-type: none;
}
#mainNav { 
	border-bottom-color: #858585; 
}

#mainNav li.nav {		
	border-top-color: #858585;
	border-bottom-color: #4A4A4A;
}

#mainNav li.nav .subNav {
	background: #555;
	-moz-box-shadow:    inset 0 0 3px #333;
	-webkit-box-shadow: inset 0 0 3px #333;
	box-shadow:         inset 0 0 3px #333;
}

#mainNav li.nav .subNav li a {
	background: url(./images/bullets/bullet_black.png) no-repeat 6px 49% !important;
	filter: none;
}

#mainNav li.nav .subNav li a:hover {
	background-color: #5D5D5D !important;
}

#mainNav .dropdown .dropdownArrow {	
	border-top-color: #525252;
}
.fieldsets {
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
}
.myBox {
	width: 100px;
    height: 40px;
    padding: 2px;
 	border: 3px solid black;
    margin: 2px;
	color: #5D5D5D;
	background-color: #F3F3FF;
	text-align: center;
	vertical-align: middle;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4x;
}
.myBoxTitle {
	float:left;
    position: relative;
	left: 5px;
    color: #5D5D5D;
}
.inputbox2{
	width:100%;
	float:left;
    position: relative;
	left: 5px;
    color: #5D5D5D;
}
.selectbox{
	width:180px;
}


/* =Dashboard
----------------------------------------------- */
h2.dashboard_title      { font-size: 12px; font-weight: 600;  }
h2.dashboard_title span { font-size: 12px; font-weight: normal; font-style: italic; padding-left: 1.5em; }

.dashboard_report {
	width: 98.00%;
	font-size: 13px;
	font-style: italic;
	_color: #777;
	float: left;
	border: 1px solid #C7C7C7;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	text-shadow: 1px 1px 2px rgba(255,255,255,.65);
	margin-top: 0;
	margin-right: 2.2%;
	margin-bottom: 0.5em;
	margin-left: 0;
	background-color: #F3F3FF;
}
.dashboard_report .last { margin-right: auto;}
.dashboard_report .pad { padding: 15px 15px 10px; }
.dashboard_report .value {
	/*valor num*/
	font-size: 18px;
	font-style: normal;
	_color: #444;
	display: block;
	margin-bottom: 0.2em;
	margin-top: 0.1em;
}
.dashboard_report .value3 {
	/*data*/
	font-style: italic;
	_color: #FFF;
	display: block;
	margin-bottom: 0.2em;
	margin-top: 0.3em;
}
.dashboard_report .value4 {
	/*footer comment*/
	font-size: 14px;
	font-style: italic;
	_color: #777;
	display: block;
	margin-bottom: 0.2em;
	margin-top: 0.4em;
}
.dashboard_report .value2 {
	/*kwh*/
	font-size: 12px;
	font-style: normal;
	_color: #fff;
	display: block;
	margin-top: -1em;
	margin-bottom: 0.4em;
	margin-right: -0.8em;
	float:right;
	
	text-shadow: 0px 0px 0px rgba(255,255,255,.65);
}
.dashboard_report .value5 {
	/*kwh*/
	font-size: 12px;
	font-style: normal;
	color: #777;
	display: block;
	margin-top: -1em;
	margin-bottom: 0.4em;
	margin-right:-0.8em;
	float:right;
	text-shadow: 0px 0px 0px rgba(255,255,255,.65);
}
.dashboard_report.activeState { text-shadow: 1px 1px 2px rgba(0,0,0,.5); }
.dashboard_report.activeState .value { color: #999; }

</style>
	<table class="table table-bordered table-striped" width="100%" cellpadding="2" cellspacing="2" border="0">
		<tr>
			<?php
			$totals   	= $this->prepareTotals($filterBy);
			?>
			<td align="left" width="25%">
				<h2 class="dashboard_title">Sold Products<span>Totals</span></h2>				
						<a href="./index.php?option=com_mcsv&view=events&filter.status=26">
						<div class="dashboard_report first activeState">
							<div class="pad">
									<span class="value2">€</span> 
									<span class="value"><?php echo $totals[0] ?></span>                               
									<span class="value3">Money</span> 
							</div>
						</div>
						</A>

				</td>
			<td align="left" width="25%">
				<h2 class="dashboard_title">Sold Products<span>Totals</span>
						</h2>				
						<a href="./index.php?option=com_mcsv&view=events&filter.status=26">
						<div class="dashboard_report defaultState">
							<div class="pad">
								  <span class="value2"></span> 
                                  <span class="value"><?php echo $totals[1] ?></span>                               
                                 <span class="value3">Quantity</span> 
							</div>
						</div>
						</A>
				</td>
			<td align="left" width="25%">
						<h2 class="dashboard_title">Viewed Products<span>Totals</span></h2>				
						<a href="./index.php?option=com_mcsv&view=events&filter.status=24">
						<div class="dashboard_report defaultState">
							<div class="pad">
								  <span class="value2">€</span> 
                                  <span class="value"><?php echo $totals[2] ?></span>                               
                                  <span class="value3">Money</span> 
							</div>
						</div>
						</A>
				</td>
			<td align="left" width="25%">
					<h2 class="dashboard_title">Viewed Products<span>Totals</span>
						</h2>				
						<a href="./index.php?option=com_mcsv&view=events&filter.status=24">
						<div class="dashboard_report defaultState">
							<div class="pad">
								  <span class="value2"> </span> 
                                  <span class="value"><?php echo $totals[3] ?></span>                               
                                  <span class="value3">Quantity</span> 
							</div>
						</div>
						</A>
				</td>

			</tr>
			
		<!--
		<tr><td colspan="4">
			<div id="containerchart" style="min-width: 400px; height: 320px; margin: 0 auto"></div><br></td>
			</tr>
		<!--
		<tr><td colspan="4">
			<div id="container_graph5" style="min-width: 300px; height: 250px; margin: 0 auto"></div><br></td>
			</tr>
			-->
		<tr><td colspan="2"><div id="containerchart"      style="min-width: 300px; height: 270px; margin: 0 auto"></div><br></td>
			<td colspan="2"><div id="containerchartviews" style="min-width: 300px; height: 270px; margin: 0 auto"></div><br></td>
			</tr>

		<!--
		<tr><td colspan="2" valign="top">
				<div id="container_graph3" style="min-width: 250px; height: 220px;"></div><br></td>
			<td colspan="2" valign="top">
				<div id="container_graph4" style="min-width: 250px; height: 220px;"></div><br></td>
			</tr>
	-->			
		<tr><td colspan="2">
				<div id="container_graph2" style="min-width: 300px; height: 220px; margin: 0 auto"></div><br></td>
			<td colspan="2" valign="top">
				<div id="container_graph3" style="float:left; min-width: 150px; width:49%; height: 220px; margin: 0 auto"></div>
				<div id="container_graph4" style="float:right; min-width: 150px; width:49%; height: 220px; margin: 2 auto"></div></td>
			</tr>
	
		<tr style="display:none;"><td colspan="4">
			<div id="container_graph1" style="min-width: 300px; height: 250px; margin: 0 auto"></div><br></td>
			</tr>				
	</table>
</div>



			<!-- 
		</fieldset>
		--> 
		
		
	</TD></TR>
</TABLE>


	
	<input type="hidden" name="view" value="stats" />
	
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHtml::_('form.token'); ?>
	
</form>