<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
?>
<?php //echo JHtml::_('access.usergroups', 'jform[groups]', $this->groups, true); ?>
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('tags'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('tags'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php 	echo $this->form->getLabel('resume'); ?></div>
			<div class="controls"><?php 		echo $this->form->getInput('resume'); ?></div>
		</div>
